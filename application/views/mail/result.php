<div style="background:#f7f7f7;color:#564949;font-family:sans-serif;height:100%;margin:0;padding:20px;text-align:center;">
	<div style="display:inline-block;position:static;text-align:center;">
		<h3 style="margin:0010px;"><?php echo $functions->languageInit('SystemMailResult') ?></h3>
		<p style="margin:10px00;"><?php echo $functions->languageInit('SystemMailResultDesc') ?></p>
		<hr>
		<a href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/orders/view/index/<?php echo $order['order_id'] ?>" style="background:#fff;border-radius:100px;border-width:0;color:#564949;display:inline-block;font-weight:400;margin:5px;padding:12px15px;text-decoration:none;"><?php echo $functions->languageInit('SystemMailResultStatus') ?></a>
		<hr>
		<small><?php echo $functions->config('copyright') ?></small>
	</div>
</div>
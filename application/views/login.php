<!DOCTYPE html>
<html dir="<?php if($functions->isRtl()): ?>rtl<?php else: ?>ltr<?php endif; ?>" lang="<?php echo $functions->languageBy($functions->language()) ?>" prefix="og: https://ogp.me/ns#">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="description" content="<?php echo $functions->languageInit('Main_LoginDesc') ?>">
		<meta name="keywords" content="<?php echo $functions->languageInit('CommonKeywords') ?>">
		<meta name="theme-color" content="<?php echo $functions->config('color') ?>">
		
		<meta property="og:title" content="<?php echo $functions->languageInit('Main_Login') ?>">
		<meta property="og:type" content="website">
		<meta property="og:image" content="<?php echo $functions->config('assets') ?>/png/emblem.png">
		<meta property="og:url" content="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>">
		<meta property="og:description" content="<?php echo $functions->languageInit('Main_LoginDesc') ?>">
		<meta property="og:site_name" content="<?php echo $functions->config('title') ?>">
		
		<title><?php echo $functions->languageInit('Main_Login') ?></title>
		
		<link rel="canonical" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/login?language=<?php echo $functions->languageBy($functions->language()) ?>">
		
		<link rel="alternate" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/login" hreflang="x-default">
		<?php foreach($functions->get('system_languages') as $item): ?>
		<link rel="alternate" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/login?language=<?php echo $item['language_key'] ?>" hreflang="<?php echo $item['language_key'] ?>">
		<?php endforeach; ?>
		
		<link rel="shortcut icon" href="<?php echo $functions->config('assets') ?>/ico/favicon.ico">
		
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/vendor.css">
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/main.css">
		
		<script src="<?php echo $functions->config('assets') ?>/js/vendor.js"></script>
		<script src="<?php echo $functions->config('assets') ?>/js/main.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<section class="app">
				<a href="<?php echo isset($request->get['redirect']) && !is_array($request->get['redirect']) ? htmlspecialchars_decode($request->get['redirect']) : '/' ?>" class="over">
					<div class="text-center">
						<img src="<?php echo $functions->config('assets') ?>/png/logo.png" alt="logo" class="half">
					</div>
				</a>
				<div class="over">
					<div class="control">
						<div class="control-block">
							<div class="name">
								<span><?php echo $functions->languageInit('Main_Login') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Main_LoginDesc') ?></span>
							</div>
						</div>
						<?php if($account->isLogged()): ?>
						<div class="control-addon">
							<button type="button" class="btn second" data-toggle="dialog">
								<i class="zmdi zmdi-more"></i>
							</button>
							<div class="dropdown fade" data-ride="dialog" data-position="true">
								<a href="/orders" class="menu">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-shopping-basket"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonOrders') ?></span>
											</div>
										</div>
									</div>
								</a>
								<a href="/deliveries" class="menu">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-map"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonDeliveries') ?></span>
											</div>
										</div>
									</div>
								</a>
								<a href="/settings" class="menu">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-settings"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonSettings') ?></span>
											</div>
										</div>
									</div>
								</a>
								<i class="divider"></i>
								<div class="navigate" data-navigation="parent">
									<div class="menu hover" data-action="navigation">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $functions->languageInit('CommonLanguage') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<i class="toggle" data-navigation="toggle"></i>
											</div>
										</div>
									</div>
									<div class="navigation" data-navigation="slide">
										<?php foreach($functions->get('system_languages') as $item): ?>
										<a href="/index/language/<?php echo $item['language_id'] ?>?back=/login<?php if(isset($request->get['redirect']) && !is_array($request->get['redirect'])): ?><?php echo urlencode('?redirect=' . urlencode(htmlspecialchars_decode($request->get['redirect']))) ?><?php endif; ?>" class="menu">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<span><?php echo $item['language_value'] ?></span>
													</div>
												</div>
												<?php if($item['language_id'] == $functions->language()): ?>
												<div class="control-addon">
													<div class="btn media small">
														<i class="zmdi zmdi-check"></i>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</a>
										<?php endforeach; ?>
									</div>
								</div>
								<div class="navigate" data-navigation="parent">
									<div class="menu hover" data-action="navigation">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $functions->languageInit('CommonCurrencie') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<i class="toggle" data-navigation="toggle"></i>
											</div>
										</div>
									</div>
									<div class="navigation" data-navigation="slide">
										<?php foreach($functions->get('system_currencies') as $item): ?>
										<a href="/index/currencie/<?php echo $item['currencie_id'] ?>?back=/login<?php if(isset($request->get['redirect']) && !is_array($request->get['redirect'])): ?><?php echo urlencode('?redirect=' . urlencode(htmlspecialchars_decode($request->get['redirect']))) ?><?php endif; ?>" class="menu">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<span><?php echo $item['currencie_key'] ?></span>
													</div>
												</div>
												<?php if($item['currencie_id'] == $functions->currencie()): ?>
												<div class="control-addon">
													<div class="btn media small">
														<i class="zmdi zmdi-check"></i>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</a>
										<?php endforeach; ?>
									</div>
								</div>
								<div class="navigate" data-navigation="parent">
									<div class="menu hover" data-action="navigation">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $functions->languageInit('CommonTimezone') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<i class="toggle" data-navigation="toggle"></i>
											</div>
										</div>
									</div>
									<div class="navigation" data-navigation="slide">
										<?php foreach($functions->get('system_timezones') as $item): ?>
										<a href="/index/timezone/<?php echo $item['timezone_id'] ?>?back=/login<?php if(isset($request->get['redirect']) && !is_array($request->get['redirect'])): ?><?php echo urlencode('?redirect=' . urlencode(htmlspecialchars_decode($request->get['redirect']))) ?><?php endif; ?>" class="menu">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<span><?php echo isset(json_decode($item['timezone_value'], true)[$functions->language()]) ? json_decode($item['timezone_value'], true)[$functions->language()] : json_decode($item['timezone_value'], true)[$functions->config('language')] ?></span>
													</div>
												</div>
												<?php if($item['timezone_id'] == $functions->timezone()): ?>
												<div class="control-addon">
													<div class="btn media small">
														<i class="zmdi zmdi-check"></i>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</a>
										<?php endforeach; ?>
									</div>
								</div>
								<i class="divider"></i>
								<?php foreach($account->accounts() as $key => $value): ?>
								<a href="/index/account/<?php echo $key ?>" class="menu">
									<div class="model">
										<span><?php echo $value['user_nickname'] ?></span>
									</div>
								</a>
								<?php endforeach; ?>
								<a href="/login" class="menu active">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-plus"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonPlus') ?></span>
											</div>
										</div>
									</div>
								</a>
								<i class="divider"></i>
								<a href="/logout" class="menu">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-power"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonLogout') ?></span>
											</div>
										</div>
									</div>
								</a>
							</div>
						</div>
						<?php else: ?>
						<div class="control-addon">
							<button type="button" class="btn second" data-toggle="dialog">
								<i class="zmdi zmdi-more"></i>
							</button>
							<div class="dropdown fade" data-ride="dialog" data-position="true">
								<div class="navigate" data-navigation="parent">
									<div class="menu hover" data-action="navigation">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $functions->languageInit('CommonLanguage') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<i class="toggle" data-navigation="toggle"></i>
											</div>
										</div>
									</div>
									<div class="navigation" data-navigation="slide">
										<?php foreach($functions->get('system_languages') as $item): ?>
										<a href="/index/language/<?php echo $item['language_id'] ?>?back=/login<?php if(isset($request->get['redirect']) && !is_array($request->get['redirect'])): ?><?php echo urlencode('?redirect=' . urlencode(htmlspecialchars_decode($request->get['redirect']))) ?><?php endif; ?>" class="menu">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<span><?php echo $item['language_value'] ?></span>
													</div>
												</div>
												<?php if($item['language_id'] == $functions->language()): ?>
												<div class="control-addon">
													<div class="btn media small">
														<i class="zmdi zmdi-check"></i>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</a>
										<?php endforeach; ?>
									</div>
								</div>
								<div class="navigate" data-navigation="parent">
									<div class="menu hover" data-action="navigation">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $functions->languageInit('CommonCurrencie') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<i class="toggle" data-navigation="toggle"></i>
											</div>
										</div>
									</div>
									<div class="navigation" data-navigation="slide">
										<?php foreach($functions->get('system_currencies') as $item): ?>
										<a href="/index/currencie/<?php echo $item['currencie_id'] ?>?back=/login<?php if(isset($request->get['redirect']) && !is_array($request->get['redirect'])): ?><?php echo urlencode('?redirect=' . urlencode(htmlspecialchars_decode($request->get['redirect']))) ?><?php endif; ?>" class="menu">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<span><?php echo $item['currencie_key'] ?></span>
													</div>
												</div>
												<?php if($item['currencie_id'] == $functions->currencie()): ?>
												<div class="control-addon">
													<div class="btn media small">
														<i class="zmdi zmdi-check"></i>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</a>
										<?php endforeach; ?>
									</div>
								</div>
								<div class="navigate" data-navigation="parent">
									<div class="menu hover" data-action="navigation">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $functions->languageInit('CommonTimezone') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<i class="toggle" data-navigation="toggle"></i>
											</div>
										</div>
									</div>
									<div class="navigation" data-navigation="slide">
										<?php foreach($functions->get('system_timezones') as $item): ?>
										<a href="/index/timezone/<?php echo $item['timezone_id'] ?>?back=/login<?php if(isset($request->get['redirect']) && !is_array($request->get['redirect'])): ?><?php echo urlencode('?redirect=' . urlencode(htmlspecialchars_decode($request->get['redirect']))) ?><?php endif; ?>" class="menu">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<span><?php echo isset(json_decode($item['timezone_value'], true)[$functions->language()]) ? json_decode($item['timezone_value'], true)[$functions->language()] : json_decode($item['timezone_value'], true)[$functions->config('language')] ?></span>
													</div>
												</div>
												<?php if($item['timezone_id'] == $functions->timezone()): ?>
												<div class="control-addon">
													<div class="btn media small">
														<i class="zmdi zmdi-check"></i>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</a>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<form action="/login/ajax/<?php echo $functions->getCsrf() ?>" method="POST" class="widget loginMain">
					<div class="over formNickname">
						<div class="above">
							<label for="nickname" class="name">
								<span class="font-600"><?php echo $functions->languageInit('Main_LoginLabelNickname') ?></span>
							</label>
						</div>
						<div class="above">
							<input type="text" name="nickname" placeholder="<?php echo $functions->languageInit('Main_LoginFormNickname') ?>" class="form block" id="nickname">
						</div>
					</div>
					<div class="over formPassword">
						<div class="above">
							<label for="password" class="name">
								<span class="font-600"><?php echo $functions->languageInit('Main_LoginLabelPassword') ?></span>
							</label>
						</div>
						<div class="above">
							<input type="password" name="password" placeholder="<?php echo $functions->languageInit('Main_LoginFormPassword') ?>" class="form block" id="password">
						</div>
					</div>
					<div class="over">
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Main_LoginSubmit') ?></span>
						</button>
					</div>
					<div class="clearfix">
						<div class="float-left">
							<a href="/restore<?php if(isset($request->get['redirect']) && !is_array($request->get['redirect'])): ?>?redirect=<?php echo urlencode(htmlspecialchars_decode($request->get['redirect'])) ?><?php endif; ?>" class="btn default">
								<span><?php echo $functions->languageInit('CommonRestore') ?></span>
							</a>
						</div>
						<div class="float-right">
							<a href="/register<?php if(isset($request->get['redirect']) && !is_array($request->get['redirect'])): ?>?redirect=<?php echo urlencode(htmlspecialchars_decode($request->get['redirect'])) ?><?php endif; ?>" class="btn default">
								<span><?php echo $functions->languageInit('CommonRegister') ?></span>
							</a>
						</div>
					</div>
				</form>
			</section>
		</div>
		<script>
			$(document).on('submit', '.loginMain', function(event) {
				event.preventDefault();
				
				var form = $(this);
				
				$.ajax({
					contentType: false,
					processData: false,
					type: form.attr('method'),
					url: form.attr('action'),
					data: new FormData(form[0]),
					beforeSend: function(data) {
						form.find('button[type="submit"]').prop('disabled', true);
						
						form.find('.form.error').removeClass('error');
						$('.addonMain').remove();
					},
					success: function(data) {
						data = JSON.parse(data);
						switch(data.status) {
							case 'error':
								if($.isArray(data.error)) {
									$.each(data.error, function() {
										form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
										
										if(this.value) {
											form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonMain">\
												<span class="focus">' + this.value + '</span>\
											</div>');
										}
									});
									
									if($('.tabHome').length || $('.tabEmail').length) {
										if(data.error[0].key == 'nickname') {
											$('.tabHome').tab('show');
										} else if(data.error[0].key == 'password') {
											$('.tabHome').tab('show');
										} else if(data.error[0].key == 'code') {
											$('.tabEmail').tab('show');
										}
									}
								} else {
									$.growl({
										message: data.error,
										type: 'error'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
								break;
							case 'success':
								document.location.href = '<?php echo isset($request->get['redirect']) && !is_array($request->get['redirect']) ? addslashes(htmlspecialchars_decode($request->get['redirect'])) : '/' ?>';
								break;
							case 'email':
								$('.loginMain').html('<div class="over">\
									<div class="justified">\
										<a href="#tabHome" class="btn default block tabHome" data-toggle="tab">\
											<i class="zmdi zmdi-home"></i>\
										</a>\
										<a href="#tabEmail" class="btn default block active tabEmail" data-toggle="tab">\
											<i class="zmdi zmdi-email"></i>\
										</a>\
									</div>\
								</div>\
								<div class="over">\
									<div class="tab fade" id="tabHome">\
										<div class="over formNickname">\
											<div class="above">\
												<label for="nickname" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_LoginLabelNickname')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<input type="text" name="nickname" value="' + $('input[name="nickname"]').val() + '" class="form block" id="nickname" readonly>\
											</div>\
										</div>\
										<div class="over formPassword">\
											<div class="above">\
												<label for="password" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_LoginLabelPassword')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<input type="password" name="password" value="' + $('input[name="password"]').val() + '" class="form block" id="password" readonly>\
											</div>\
										</div>\
									</div>\
									<div class="tab fade open active in" id="tabEmail">\
										<div class="over formCode">\
											<div class="above">\
												<label for="code" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_LoginLabelCode')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<div class="control">\
													<div class="control-addon">\
														<div class="btn media">\
															<span><?php echo addslashes($functions->config('code')) ?></span>\
														</div>\
													</div>\
													<div class="control-block">\
														<input type="text" name="code" placeholder="<?php echo addslashes($functions->languageInit('Main_LoginFormCode')) ?>" class="form block" id="code">\
													</div>\
													<div class="control-addon codeEmail">\
														<button type="button" class="btn info" disabled>\
															<span>' + data.time + '</span>\
														</button>\
													</div>\
												</div>\
											</div>\
										</div>\
										<button type="submit" class="btn">\
											<span><?php echo addslashes($functions->languageInit('Main_LoginSubmit')) ?></span>\
										</button>\
									</div>\
								</div>');
								
								codeEmail(data.time);
								break;
						}
					},
					error: function(data) {
						if(data.statusText != 'abort') {
							$.growl({
								message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
								type: 'warning'
							});
						}
						
						form.find('button[type="submit"]').prop('disabled', false);
					}
				});
			});
			
			$(document).on('click', '.sendEmail', function() {
				var submit = $(this);
				
				var formData = new FormData();
				formData.append('nickname', $('input[name="nickname"]').val());
				formData.append('password', $('input[name="password"]').val());
				
				$.ajax({
					contentType: false,
					processData: false,
					type: 'POST',
					url: '/login/ajax/<?php echo addslashes($functions->getCsrf()) ?>',
					data: formData,
					beforeSend: function(data) {
						submit.prop('disabled', true);
						
						$('.loginMain').find('.form.error').removeClass('error');
						$('.addonMain').remove();
					},
					success: function(data) {
						data = JSON.parse(data);
						switch(data.status) {
							case 'error':
								if($.isArray(data.error)) {
									$.each(data.error, function() {
										$('.loginMain').find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
										
										if(this.value) {
											$('.loginMain').find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonMain">\
												<span class="focus">' + this.value + '</span>\
											</div>');
										}
									});
									
									if($('.tabHome').length || $('.tabEmail').length) {
										if(data.error[0].key == 'nickname') {
											$('.tabHome').tab('show');
										} else if(data.error[0].key == 'password') {
											$('.tabHome').tab('show');
										} else if(data.error[0].key == 'code') {
											$('.tabEmail').tab('show');
										}
									}
								} else {
									$.growl({
										message: data.error,
										type: 'error'
									});
								}
								
								submit.prop('disabled', false);
								break;
							case 'email':
								$('.loginMain').html('<div class="over">\
									<div class="justified">\
										<a href="#tabHome" class="btn default block tabHome" data-toggle="tab">\
											<i class="zmdi zmdi-home"></i>\
										</a>\
										<a href="#tabEmail" class="btn default block active tabEmail" data-toggle="tab">\
											<i class="zmdi zmdi-email"></i>\
										</a>\
									</div>\
								</div>\
								<div class="over">\
									<div class="tab fade" id="tabHome">\
										<div class="over formNickname">\
											<div class="above">\
												<label for="nickname" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_LoginLabelNickname')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<input type="text" name="nickname" value="' + $('input[name="nickname"]').val() + '" class="form block" id="nickname" readonly>\
											</div>\
										</div>\
										<div class="over formPassword">\
											<div class="above">\
												<label for="password" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_LoginLabelPassword')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<input type="password" name="password" value="' + $('input[name="password"]').val() + '" class="form block" id="password" readonly>\
											</div>\
										</div>\
									</div>\
									<div class="tab fade open active in" id="tabEmail">\
										<div class="over formCode">\
											<div class="above">\
												<label for="code" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_LoginLabelCode')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<div class="control">\
													<div class="control-addon">\
														<div class="btn media">\
															<span><?php echo addslashes($functions->config('code')) ?></span>\
														</div>\
													</div>\
													<div class="control-block">\
														<input type="text" name="code" placeholder="<?php echo addslashes($functions->languageInit('Main_LoginFormCode')) ?>" class="form block" id="code">\
													</div>\
													<div class="control-addon codeEmail">\
														<button type="button" class="btn info" disabled>\
															<span>' + data.time + '</span>\
														</button>\
													</div>\
												</div>\
											</div>\
										</div>\
										<button type="submit" class="btn">\
											<span><?php echo addslashes($functions->languageInit('Main_LoginSubmit')) ?></span>\
										</button>\
									</div>\
								</div>');
								
								codeEmail(data.time);
								break;
						}
					},
					error: function(data) {
						if(data.statusText != 'abort') {
							$.growl({
								message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
								type: 'warning'
							});
						}
						
						submit.prop('disabled', false);
					}
				});
			});
			
			var interval_email;
			function codeEmail(time) {
				if(typeof(interval_email) != 'undefined') {
					clearInterval(interval_email);
				}
				
				var date = new Date;
				interval_email = setInterval(function() {
					var interval_time = time - Math.round((new Date - date) / 1000);
					
					if(interval_time > 0) {
						$('.codeEmail').html('<button type="button" class="btn info" disabled>\
							<span>' + interval_time + '</span>\
						</button>');
					} else {
						$('.codeEmail').html('<button type="button" class="btn info sendEmail">\
							<i class="zmdi zmdi-refresh"></i>\
						</button>');
						
						clearInterval(interval_email);
					}
				}, 1000);
			}
		</script>
		<noscript>
			<div style="background-color:#fff;color:#000;position:fixed;width:100%;height:100%;top:0;left:0;z-index:9999;">
				<div style="margin:10%;width:80%;">
					<img src="<?php echo $functions->config('assets') ?>/png/logo.png" alt="logo" style="margin-bottom:20px;max-width:20%;">
					<div style="font-size:18px;word-break:break-word;"><?php echo $functions->languageInit('CommonNoscript') ?></div>
				</div>
			</div>
		</noscript>
	</body>
</html>
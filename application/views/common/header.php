<!DOCTYPE html>
<html dir="<?php if($functions->isRtl()): ?>rtl<?php else: ?>ltr<?php endif; ?>" lang="<?php echo $functions->languageBy($functions->language()) ?>" prefix="og: https://ogp.me/ns#">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="description" content="<?php echo $description ?>">
		<meta name="keywords" content="<?php echo $functions->languageInit('CommonKeywords') ?>">
		<meta name="theme-color" content="<?php echo $functions->config('color') ?>">
		
		<meta property="og:title" content="<?php echo $title ?>">
		<meta property="og:type" content="website">
		<meta property="og:image" content="<?php echo $functions->config('assets') ?>/png/emblem.png">
		<meta property="og:url" content="<?php echo $url ?>">
		<meta property="og:description" content="<?php echo $description ?>">
		<meta property="og:site_name" content="<?php echo $functions->config('title') ?>">
		
		<title><?php echo $title ?></title>
		
		<link rel="canonical" href="<?php echo $canonical ?>">
		
		<link rel="alternate" href="<?php echo $url ?>" hreflang="x-default">
		<?php foreach($alternate as $key => $value): ?>
		<link rel="alternate" href="<?php echo $key ?>" hreflang="<?php echo $value ?>">
		<?php endforeach; ?>
		
		<link rel="shortcut icon" href="<?php echo $functions->config('assets') ?>/ico/favicon.ico">
		
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/vendor.css">
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/main.css">
		
		<script src="<?php echo $functions->config('assets') ?>/js/vendor.js"></script>
		<script src="<?php echo $functions->config('assets') ?>/js/main.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<header class="header">
				<div class="control">
					<div class="control-block">
						<a href="/" class="logo">
							<img src="<?php echo $functions->config('assets') ?>/png/logo.png" alt="logo">
						</a>
					</div>
					<div class="control-addon">
						<a href="/cart" class="btn second<?php if(isset($section['element']) && $section['element'] == 'cart'): ?> active<?php endif; ?>">
							<i class="zmdi zmdi-shopping-cart"></i>
							<span>&nbsp;</span>
							<span class="headerTotal"><?php echo $functions->currencieInit($functions->currencie(), array_sum(array_values($functions->cart(2)))) ?></span>
						</a>
					</div>
					<?php if($account->isLogged()): ?>
					<div class="control-addon">
						<button type="button" class="btn second" data-toggle="dialog">
							<i class="zmdi zmdi-more"></i>
						</button>
						<div class="dropdown fade" data-ride="dialog" data-position="true">
							<a href="/orders" class="menu<?php if(isset($section['element']) && $section['element'] == 'orders'): ?> active<?php endif; ?>">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-shopping-basket"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonOrders') ?></span>
										</div>
									</div>
								</div>
							</a>
							<a href="/deliveries" class="menu<?php if(isset($section['element']) && $section['element'] == 'deliveries'): ?> active<?php endif; ?>">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-map"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonDeliveries') ?></span>
										</div>
									</div>
								</div>
							</a>
							<a href="/settings" class="menu<?php if(isset($section['element']) && $section['element'] == 'settings'): ?> active<?php endif; ?>">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-settings"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonSettings') ?></span>
										</div>
									</div>
								</div>
							</a>
							<i class="divider"></i>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonLanguage') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_languages') as $item): ?>
									<a href="/index/language/<?php echo $item['language_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $item['language_value'] ?></span>
												</div>
											</div>
											<?php if($item['language_id'] == $functions->language()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonCurrencie') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_currencies') as $item): ?>
									<a href="/index/currencie/<?php echo $item['currencie_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $item['currencie_key'] ?></span>
												</div>
											</div>
											<?php if($item['currencie_id'] == $functions->currencie()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonTimezone') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_timezones') as $item): ?>
									<a href="/index/timezone/<?php echo $item['timezone_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo isset(json_decode($item['timezone_value'], true)[$functions->language()]) ? json_decode($item['timezone_value'], true)[$functions->language()] : json_decode($item['timezone_value'], true)[$functions->config('language')] ?></span>
												</div>
											</div>
											<?php if($item['timezone_id'] == $functions->timezone()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<i class="divider"></i>
							<?php foreach($account->accounts() as $key => $value): ?>
							<a href="/index/account/<?php echo $key ?>" class="menu">
								<div class="model">
									<span><?php echo $value['user_nickname'] ?></span>
								</div>
							</a>
							<?php endforeach; ?>
							<a href="/login" class="menu">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-plus"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonPlus') ?></span>
										</div>
									</div>
								</div>
							</a>
							<i class="divider"></i>
							<a href="/logout" class="menu">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-power"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonLogout') ?></span>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<?php else: ?>
					<div class="control-addon">
						<button type="button" class="btn second" data-toggle="dialog">
							<i class="zmdi zmdi-more"></i>
						</button>
						<div class="dropdown fade" data-ride="dialog" data-position="true">
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonLanguage') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_languages') as $item): ?>
									<a href="/index/language/<?php echo $item['language_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $item['language_value'] ?></span>
												</div>
											</div>
											<?php if($item['language_id'] == $functions->language()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonCurrencie') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_currencies') as $item): ?>
									<a href="/index/currencie/<?php echo $item['currencie_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $item['currencie_key'] ?></span>
												</div>
											</div>
											<?php if($item['currencie_id'] == $functions->currencie()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonTimezone') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_timezones') as $item): ?>
									<a href="/index/timezone/<?php echo $item['timezone_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo isset(json_decode($item['timezone_value'], true)[$functions->language()]) ? json_decode($item['timezone_value'], true)[$functions->language()] : json_decode($item['timezone_value'], true)[$functions->config('language')] ?></span>
												</div>
											</div>
											<?php if($item['timezone_id'] == $functions->timezone()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="control-addon">
						<a href="/login" class="btn">
							<span><?php echo $functions->languageInit('CommonLogin') ?></span>
						</a>
					</div>
					<?php endif; ?>
				</div>
			</header>
			<section class="container">
				<?php if(isset($alert)): ?>
				<div class="over">
					<?php if(is_array($alert)): ?>
					<div class="alert <?php echo $alert['key'] ?>">
						<div class="name">
							<span><?php echo $alert['value'] ?></span>
						</div>
					</div>
					<?php else: ?>
					<div class="alert">
						<div class="name">
							<span><?php echo $alert ?></span>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<?php endif; ?>
				<?php if($account->isLogged() && mb_strlen(trim($account->getEmail())) < 1): ?>
				<div class="over">
					<a href="/settings" class="alert">
						<div class="control">
							<div class="control-addon">
								<div class="btn media">
									<i class="zmdi zmdi-email"></i>
								</div>
							</div>
							<div class="control-block">
								<div class="name">
									<span><?php echo $functions->languageInit('CommonEmail') ?></span>
								</div>
							</div>
						</div>
					</a>
				</div>
				<?php endif; ?>
<?php echo $header ?>
				<?php if($functions->getTotal('shop_categories', array('categorie_id' => $product['categorie_id']))): ?>
				<div class="over">
					<div class="name">
						<a href="/categorie/index/<?php echo $product['categorie_id'] ?>" class="link">
							<span><?php echo isset(json_decode($product['categorie_title'], true)[$functions->language()]) ? json_decode($product['categorie_title'], true)[$functions->language()] : json_decode($product['categorie_title'], true)[$functions->config('language')] ?></span>
						</a>
						<span class="focus">&nbsp;/&nbsp;</span>
						<span class="font-600"><?php echo isset(json_decode($product['product_title'], true)[$functions->language()]) ? json_decode($product['product_title'], true)[$functions->language()] : json_decode($product['product_title'], true)[$functions->config('language')] ?></span>
					</div>
				</div>
				<?php endif; ?>
				<?php if(!empty(json_decode($product['product_image'], true))): ?>
				<div class="row">
					<div class="col-6-sm">
						<div class="widget">
							<div class="over">
								<div class="name">
									<span><?php echo isset(json_decode($product['product_title'], true)[$functions->language()]) ? json_decode($product['product_title'], true)[$functions->language()] : json_decode($product['product_title'], true)[$functions->config('language')] ?></span>
								</div>
								<div class="name">
									<?php foreach($functions->string(nl2br(isset(json_decode($product['product_description'], true)[$functions->language()]) ? json_decode($product['product_description'], true)[$functions->language()] : json_decode($product['product_description'], true)[$functions->config('language')]), true) as $item): ?>
									<?php if(isset($item['link'])): ?>
									<a href="<?php echo $item['link'] ?>" class="link">
										<span class="focus"><?php echo $item['link'] ?></span>
									</a>
									<?php else: ?>
									<span class="focus"><?php echo $item['text'] ?></span>
									<?php endif; ?>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="preview" data-gallery="parent">
								<?php foreach(json_decode($product['product_image'], true) as $item): ?>
								<?php if(!empty($functions->image($item))): ?>
								<div class="preview-item hover" data-action="gallery" data-alt="image" data-src="<?php echo $functions->image($item)['original'] ?>">
									<img src="<?php echo $functions->image($item)['crop_m'] ?>" alt="image">
								</div>
								<?php else: ?>
								<div class="preview-item">
									<img src="<?php echo $functions->config('assets') ?>/png/image.png" alt="image">
								</div>
								<?php endif; ?>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
					<div class="col-6-sm">
						<div class="over">
							<div class="name">
								<?php if($product['product_discount']): ?>
								<span class="text-line"><?php echo $functions->currencieInit($functions->currencie(), $functions->convert($product['product_price'], $functions->currencie())) ?></span>
								<span>&nbsp;</span>
								<span class="font-600"><?php echo $functions->currencieInit($functions->currencie(), $functions->convert($product['product_price'] * (100 - $product['product_discount']) / 100, $functions->currencie())) ?></span>
								<?php else: ?>
								<span class="font-600"><?php echo $functions->currencieInit($functions->currencie(), $functions->convert($product['product_price'], $functions->currencie())) ?></span>
								<?php endif; ?>
							</div>
						</div>
						<?php if(!empty(json_decode($product['product_content'], true))): ?>
						<div class="over">
							<div class="above">
								<label for="content" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Main_ProductContent') ?></span>
								</label>
							</div>
							<div class="above">
								<select name="content" class="selectContent" class="form block">
									<?php foreach(json_decode($product['product_content'], true) as $key => $value): ?>
									<option value="<?php echo $key ?>"><?php echo $value ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<?php endif; ?>
						<div class="over">
							<div class="above">
								<label for="count" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Main_ProductCount') ?></span>
								</label>
							</div>
							<div class="above">
								<div class="control">
									<div class="control-block">
										<input type="text" value="1" class="form block inputCount" readonly>
									</div>
									<div class="control-addon">
										<div class="fill">
											<button type="button" class="btn default countMinus">
												<i class="zmdi zmdi-minus"></i>
											</button>
											<button type="button" class="btn default countPlus">
												<i class="zmdi zmdi-plus"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<button type="button" class="btn cartAdd">
							<span><?php echo $functions->languageInit('Main_ProductCart') ?></span>
						</button>
					</div>
				</div>
				<?php else: ?>
				<div class="over">
					<div class="name">
						<span><?php echo isset(json_decode($product['product_title'], true)[$functions->language()]) ? json_decode($product['product_title'], true)[$functions->language()] : json_decode($product['product_title'], true)[$functions->config('language')] ?></span>
					</div>
					<div class="name">
						<?php foreach($functions->string(nl2br(isset(json_decode($product['product_description'], true)[$functions->language()]) ? json_decode($product['product_description'], true)[$functions->language()] : json_decode($product['product_description'], true)[$functions->config('language')]), true) as $item): ?>
						<?php if(isset($item['link'])): ?>
						<a href="<?php echo $item['link'] ?>" class="link">
							<span class="focus"><?php echo $item['link'] ?></span>
						</a>
						<?php else: ?>
						<span class="focus"><?php echo $item['text'] ?></span>
						<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="over">
					<div class="name">
						<?php if($product['product_discount']): ?>
						<span class="text-line"><?php echo $functions->currencieInit($functions->currencie(), $functions->convert($product['product_price'], $functions->currencie())) ?></span>
						<span>&nbsp;</span>
						<span class="font-600"><?php echo $functions->currencieInit($functions->currencie(), $functions->convert($product['product_price'] * (100 - $product['product_discount']) / 100, $functions->currencie())) ?></span>
						<?php else: ?>
						<span class="font-600"><?php echo $functions->currencieInit($functions->currencie(), $functions->convert($product['product_price'], $functions->currencie())) ?></span>
						<?php endif; ?>
					</div>
				</div>
				<?php if(!empty(json_decode($product['product_content'], true))): ?>
				<div class="over">
					<div class="above">
						<label for="content" class="name">
							<span class="font-600"><?php echo $functions->languageInit('Main_ProductContent') ?></span>
						</label>
					</div>
					<div class="above">
						<select name="content" class="selectContent" class="form block">
							<?php foreach(json_decode($product['product_content'], true) as $key => $value): ?>
							<option value="<?php echo $key ?>"><?php echo $value ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<?php endif; ?>
				<div class="over">
					<div class="above">
						<label for="count" class="name">
							<span class="font-600"><?php echo $functions->languageInit('Main_ProductCount') ?></span>
						</label>
					</div>
					<div class="above">
						<div class="control">
							<div class="control-block">
								<input type="text" value="1" class="form block inputCount" readonly>
							</div>
							<div class="control-addon">
								<div class="fill">
									<button type="button" class="btn default countMinus">
										<i class="zmdi zmdi-minus"></i>
									</button>
									<button type="button" class="btn default countPlus">
										<i class="zmdi zmdi-plus"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="over">
					<button type="button" class="btn cartAdd">
						<span><?php echo $functions->languageInit('Main_ProductCart') ?></span>
					</button>
				</div>
				<?php endif; ?>
				<div class="over">
					<div class="widget"><?php echo htmlspecialchars_decode(isset(json_decode($product['product_text'], true)[$functions->language()]) ? json_decode($product['product_text'], true)[$functions->language()] : json_decode($product['product_text'], true)[$functions->config('language')]) ?></div>
				</div>
				<script>
					$(document).on('click', '.cartAdd', function() {
						var submit = $(this);
						
						var formData = new FormData();
						formData.append('product', '<?php echo addslashes($product['product_id']) ?>');
						formData.append('content', $('.selectContent option:selected').val());
						formData.append('count', $('.inputCount').val());
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/cart/ajax/<?php echo addslashes($functions->getCsrf()) ?>',
							data: formData,
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										break;
									case 'success':
										$('.headerTotal').text(data.total);
										break;
								}
								
								submit.prop('disabled', true);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.countMinus', function() {
						if($('.inputCount').val() > 1) {
							$('.inputCount').val(parseInt($('.inputCount').val()) - 1);
						}
					});
					
					$(document).on('click', '.countPlus', function() {
						$('.inputCount').val(parseInt($('.inputCount').val()) + 1);
					});
				</script>
<?php echo $footer ?>
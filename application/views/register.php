<!DOCTYPE html>
<html dir="<?php if($functions->isRtl()): ?>rtl<?php else: ?>ltr<?php endif; ?>" lang="<?php echo $functions->languageBy($functions->language()) ?>" prefix="og: https://ogp.me/ns#">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="description" content="<?php echo $functions->languageInit('Main_RegisterDesc') ?>">
		<meta name="keywords" content="<?php echo $functions->languageInit('CommonKeywords') ?>">
		<meta name="theme-color" content="<?php echo $functions->config('color') ?>">
		
		<meta property="og:title" content="<?php echo $functions->languageInit('Main_Register') ?>">
		<meta property="og:type" content="website">
		<meta property="og:image" content="<?php echo $functions->config('assets') ?>/png/emblem.png">
		<meta property="og:url" content="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>">
		<meta property="og:description" content="<?php echo $functions->languageInit('Main_RegisterDesc') ?>">
		<meta property="og:site_name" content="<?php echo $functions->config('title') ?>">
		
		<title><?php echo $functions->languageInit('Main_Register') ?></title>
		
		<link rel="canonical" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/register?language=<?php echo $functions->languageBy($functions->language()) ?>">
		
		<link rel="alternate" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/register" hreflang="x-default">
		<?php foreach($functions->get('system_languages') as $item): ?>
		<link rel="alternate" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/register?language=<?php echo $item['language_key'] ?>" hreflang="<?php echo $item['language_key'] ?>">
		<?php endforeach; ?>
		
		<link rel="shortcut icon" href="<?php echo $functions->config('assets') ?>/ico/favicon.ico">
		
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/vendor.css">
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/main.css">
		
		<script src="<?php echo $functions->config('assets') ?>/js/vendor.js"></script>
		<script src="<?php echo $functions->config('assets') ?>/js/main.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<section class="app">
				<a href="<?php echo isset($request->get['redirect']) && !is_array($request->get['redirect']) ? htmlspecialchars_decode($request->get['redirect']) : '/' ?>" class="over">
					<div class="text-center">
						<img src="<?php echo $functions->config('assets') ?>/png/logo.png" alt="logo" class="half">
					</div>
				</a>
				<div class="over">
					<div class="name">
						<span><?php echo $functions->languageInit('Main_Register') ?></span>
					</div>
					<div class="name">
						<span class="focus"><?php echo $functions->languageInit('Main_RegisterDesc') ?></span>
					</div>
				</div>
				<form action="/register/ajax/<?php echo $functions->getCsrf() ?>" method="POST" class="widget registerMain">
					<div class="over formNickname">
						<div class="above">
							<label for="nickname" class="name">
								<span class="font-600"><?php echo $functions->languageInit('Main_RegisterLabelNickname') ?></span>
							</label>
						</div>
						<div class="above">
							<input type="text" name="nickname" placeholder="<?php echo $functions->languageInit('Main_RegisterFormNickname') ?>" class="form block" id="nickname">
						</div>
					</div>
					<div class="over formPassword">
						<div class="above">
							<label for="password" class="name">
								<span class="font-600"><?php echo $functions->languageInit('Main_RegisterLabelPassword') ?></span>
							</label>
						</div>
						<div class="above">
							<input type="password" name="password" placeholder="<?php echo $functions->languageInit('Main_RegisterFormPassword') ?>" class="form block" id="password">
						</div>
					</div>
					<div class="over formPassword2">
						<div class="above">
							<label for="password2" class="name">
								<span class="font-600"><?php echo $functions->languageInit('Main_RegisterLabelPassword2') ?></span>
							</label>
						</div>
						<div class="above">
							<input type="password" name="password2" placeholder="<?php echo $functions->languageInit('Main_RegisterFormPassword2') ?>" class="form block" id="password2">
						</div>
					</div>
					<div class="over">
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Main_RegisterSubmit') ?></span>
						</button>
					</div>
					<div class="text-center">
						<a href="/login<?php if(isset($request->get['redirect']) && !is_array($request->get['redirect'])): ?>?redirect=<?php echo urlencode(htmlspecialchars_decode($request->get['redirect'])) ?><?php endif; ?>" class="btn default">
							<span><?php echo $functions->languageInit('CommonLogin') ?></span>
						</a>
					</div>
				</form>
			</section>
		</div>
		<script>
			$(document).on('submit', '.registerMain', function(event) {
				event.preventDefault();
				
				var form = $(this);
				
				$.ajax({
					contentType: false,
					processData: false,
					type: form.attr('method'),
					url: form.attr('action'),
					data: new FormData(form[0]),
					beforeSend: function(data) {
						form.find('button[type="submit"]').prop('disabled', true);
						
						form.find('.form.error').removeClass('error');
						$('.addonMain').remove();
					},
					success: function(data) {
						data = JSON.parse(data);
						switch(data.status) {
							case 'error':
								if($.isArray(data.error)) {
									$.each(data.error, function() {
										form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
										
										if(this.value) {
											form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonMain">\
												<span class="focus">' + this.value + '</span>\
											</div>');
										}
									});
								} else {
									$.growl({
										message: data.error,
										type: 'error'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
								break;
							case 'success':
								document.location.href = '<?php echo isset($request->get['redirect']) && !is_array($request->get['redirect']) ? addslashes(htmlspecialchars_decode($request->get['redirect'])) : '/' ?>';
								break;
						}
					},
					error: function(data) {
						if(data.statusText != 'abort') {
							$.growl({
								message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
								type: 'warning'
							});
						}
						
						form.find('button[type="submit"]').prop('disabled', false);
					}
				});
			});
		</script>
		<noscript>
			<div style="background-color:#fff;color:#000;position:fixed;width:100%;height:100%;top:0;left:0;z-index:9999;">
				<div style="margin:10%;width:80%;">
					<img src="<?php echo $functions->config('assets') ?>/png/logo.png" alt="logo" style="margin-bottom:20px;max-width:20%;">
					<div style="font-size:18px;word-break:break-word;"><?php echo $functions->languageInit('CommonNoscript') ?></div>
				</div>
			</div>
		</noscript>
	</body>
</html>
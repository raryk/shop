<!DOCTYPE html>
<html dir="<?php if($functions->isRtl()): ?>rtl<?php else: ?>ltr<?php endif; ?>" lang="<?php echo $functions->languageBy($functions->language()) ?>" prefix="og: https://ogp.me/ns#">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="description" content="<?php echo $functions->languageInit('Main_IndexDesc') ?>">
		<meta name="keywords" content="<?php echo $functions->languageInit('CommonKeywords') ?>">
		<meta name="theme-color" content="<?php echo $functions->config('color') ?>">
		
		<meta property="og:title" content="<?php echo $functions->languageInit('Main_Index') ?>">
		<meta property="og:type" content="website">
		<meta property="og:image" content="<?php echo $functions->config('assets') ?>/png/emblem.png">
		<meta property="og:url" content="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>">
		<meta property="og:description" content="<?php echo $functions->languageInit('Main_IndexDesc') ?>">
		<meta property="og:site_name" content="<?php echo $functions->config('title') ?>">
		
		<title><?php echo $functions->languageInit('Main_Index') ?></title>
		
		<link rel="canonical" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/?language=<?php echo $functions->languageBy($functions->language()) ?>">
		
		<link rel="alternate" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>" hreflang="x-default">
		<?php foreach($functions->get('system_languages') as $item): ?>
		<link rel="alternate" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/?language=<?php echo $item['language_key'] ?>" hreflang="<?php echo $item['language_key'] ?>">
		<?php endforeach; ?>
		
		<link rel="shortcut icon" href="<?php echo $functions->config('assets') ?>/ico/favicon.ico">
		
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/vendor.css">
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/main.css">
		
		<script src="<?php echo $functions->config('assets') ?>/js/vendor.js"></script>
		<script src="<?php echo $functions->config('assets') ?>/js/main.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<header class="expand">
				<div class="control">
					<div class="control-block">
						<div class="logo">
							<img src="<?php echo $functions->config('assets') ?>/png/emblem.png" alt="emblem">
						</div>
					</div>
					<div class="control-addon">
						<a href="/cart" class="btn second">
							<i class="zmdi zmdi-shopping-cart"></i>
							<span>&nbsp;</span>
							<span class="headerTotal"><?php echo $functions->currencieInit($functions->currencie(), array_sum(array_values($functions->cart(2)))) ?></span>
						</a>
					</div>
					<?php if($account->isLogged()): ?>
					<div class="control-addon">
						<button type="button" class="btn second" data-toggle="dialog">
							<i class="zmdi zmdi-more"></i>
						</button>
						<div class="dropdown fade" data-ride="dialog" data-position="true">
							<a href="/orders" class="menu">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-shopping-basket"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonOrders') ?></span>
										</div>
									</div>
								</div>
							</a>
							<a href="/deliveries" class="menu">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-map"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonDeliveries') ?></span>
										</div>
									</div>
								</div>
							</a>
							<a href="/settings" class="menu">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-settings"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonSettings') ?></span>
										</div>
									</div>
								</div>
							</a>
							<i class="divider"></i>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonLanguage') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_languages') as $item): ?>
									<a href="/index/language/<?php echo $item['language_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $item['language_value'] ?></span>
												</div>
											</div>
											<?php if($item['language_id'] == $functions->language()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonCurrencie') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_currencies') as $item): ?>
									<a href="/index/currencie/<?php echo $item['currencie_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $item['currencie_key'] ?></span>
												</div>
											</div>
											<?php if($item['currencie_id'] == $functions->currencie()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonTimezone') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_timezones') as $item): ?>
									<a href="/index/timezone/<?php echo $item['timezone_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo isset(json_decode($item['timezone_value'], true)[$functions->language()]) ? json_decode($item['timezone_value'], true)[$functions->language()] : json_decode($item['timezone_value'], true)[$functions->config('language')] ?></span>
												</div>
											</div>
											<?php if($item['timezone_id'] == $functions->timezone()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<i class="divider"></i>
							<?php foreach($account->accounts() as $key => $value): ?>
							<a href="/index/account/<?php echo $key ?>" class="menu">
								<div class="model">
									<span><?php echo $value['user_nickname'] ?></span>
								</div>
							</a>
							<?php endforeach; ?>
							<a href="/login" class="menu">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-plus"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonPlus') ?></span>
										</div>
									</div>
								</div>
							</a>
							<i class="divider"></i>
							<a href="/logout" class="menu">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-power"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonLogout') ?></span>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<?php else: ?>
					<div class="control-addon">
						<button type="button" class="btn second" data-toggle="dialog">
							<i class="zmdi zmdi-more"></i>
						</button>
						<div class="dropdown fade" data-ride="dialog" data-position="true">
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonLanguage') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_languages') as $item): ?>
									<a href="/index/language/<?php echo $item['language_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $item['language_value'] ?></span>
												</div>
											</div>
											<?php if($item['language_id'] == $functions->language()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonCurrencie') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_currencies') as $item): ?>
									<a href="/index/currencie/<?php echo $item['currencie_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $item['currencie_key'] ?></span>
												</div>
											</div>
											<?php if($item['currencie_id'] == $functions->currencie()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonTimezone') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_timezones') as $item): ?>
									<a href="/index/timezone/<?php echo $item['timezone_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo isset(json_decode($item['timezone_value'], true)[$functions->language()]) ? json_decode($item['timezone_value'], true)[$functions->language()] : json_decode($item['timezone_value'], true)[$functions->config('language')] ?></span>
												</div>
											</div>
											<?php if($item['timezone_id'] == $functions->timezone()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="control-addon">
						<a href="/login" class="btn">
							<span><?php echo $functions->languageInit('CommonLogin') ?></span>
						</a>
					</div>
					<?php endif; ?>
				</div>
			</header>
			<section class="container">
				<div class="over">
					<div class="text-center">
						<div class="over">
							<img src="<?php echo $functions->config('assets') ?>/png/logo.png" alt="logo" class="half">
						</div>
						<div class="name">
							<span><?php echo $functions->languageInit('Main_Index') ?></span>
						</div>
						<div class="name">
							<span class="focus"><?php echo $functions->languageInit('Main_IndexDesc') ?></span>
						</div>
					</div>
				</div>
				<?php if($functions->getTotal('shop_products', array('product_status' => 1))): ?>
				<div class="over">
					<div class="row">
						<?php foreach($functions->get('shop_products', array('product_status' => 1), array(), array(), array('product_id' => 'DESC'), array('start' => 0, 'limit' => 4)) as $item): ?>
						<div class="col-3-sm col-6-xs">
							<div class="forward">
								<div class="banner">
									<?php if(isset(json_decode($item['product_image'], true)[0]) && !empty($functions->image(json_decode($item['product_image'], true)[0]))): ?>
									<img src="<?php echo $functions->image(json_decode($item['product_image'], true)[0])['p'] ?>" alt="image">
									<?php else: ?>
									<img src="<?php echo $functions->config('assets') ?>/png/image.png" alt="image">
									<?php endif; ?>
								</div>
								<div class="over">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<?php foreach($functions->string(isset(json_decode($item['product_title'], true)[$functions->language()]) ? json_decode($item['product_title'], true)[$functions->language()] : json_decode($item['product_title'], true)[$functions->config('language')]) as $string): ?>
												<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
												<?php endforeach; ?>
											</div>
											<div class="model">
												<?php if($item['product_discount']): ?>
												<span class="text-line"><?php echo $functions->currencieInit($functions->currencie(), $functions->convert($item['product_price'], $functions->currencie())) ?></span>
												<span>&nbsp;</span>
												<span class="font-600"><?php echo $functions->currencieInit($functions->currencie(), $functions->convert($item['product_price'] * (100 - $item['product_discount']) / 100, $functions->currencie())) ?></span>
												<?php else: ?>
												<span class="font-600"><?php echo $functions->currencieInit($functions->currencie(), $functions->convert($item['product_price'], $functions->currencie())) ?></span>
												<?php endif; ?>
											</div>
										</div>
										<div class="control-addon">
											<a href="/product/index/<?php echo $item['product_id'] ?>" class="btn second">
												<i class="zmdi zmdi-laptop"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
				<a href="/products" class="btn default block">
					<span><?php echo $functions->languageInit('CommonMore') ?></span>
				</a>
				<?php else: ?>
				<div class="text-center">
					<div class="above">
						<div class="btn media">
							<i class="zmdi zmdi-alert-triangle"></i>
						</div>
					</div>
					<div class="name">
						<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
					</div>
				</div>
				<?php endif; ?>
			</section>
			<footer class="footer">
				<div class="text-center">
					<div class="name">
						<a href="/about" class="link">
							<span class="font-600"><?php echo $functions->config('copyright') ?></span>
						</a>
					</div>
				</div>
			</footer>
		</div>
		<noscript>
			<div style="background-color:#fff;color:#000;position:fixed;width:100%;height:100%;top:0;left:0;z-index:9999;">
				<div style="margin:10%;width:80%;">
					<img src="<?php echo $functions->config('assets') ?>/png/logo.png" alt="logo" style="margin-bottom:20px;max-width:20%;">
					<div style="font-size:18px;word-break:break-word;"><?php echo $functions->languageInit('CommonNoscript') ?></div>
				</div>
			</div>
		</noscript>
	</body>
</html>
<!DOCTYPE html>
<html dir="<?php if($functions->isRtl()): ?>rtl<?php else: ?>ltr<?php endif; ?>" lang="<?php echo $functions->languageBy($functions->language()) ?>" prefix="og: https://ogp.me/ns#">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="description" content="<?php echo $functions->languageInit('Main_RestoreDesc') ?>">
		<meta name="keywords" content="<?php echo $functions->languageInit('CommonKeywords') ?>">
		<meta name="theme-color" content="<?php echo $functions->config('color') ?>">
		
		<meta property="og:title" content="<?php echo $functions->languageInit('Main_Restore') ?>">
		<meta property="og:type" content="website">
		<meta property="og:image" content="<?php echo $functions->config('assets') ?>/png/emblem.png">
		<meta property="og:url" content="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>">
		<meta property="og:description" content="<?php echo $functions->languageInit('Main_RestoreDesc') ?>">
		<meta property="og:site_name" content="<?php echo $functions->config('title') ?>">
		
		<title><?php echo $functions->languageInit('Main_Restore') ?></title>
		
		<link rel="canonical" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/restore?language=<?php echo $functions->languageBy($functions->language()) ?>">
		
		<link rel="alternate" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/restore" hreflang="x-default">
		<?php foreach($functions->get('system_languages') as $item): ?>
		<link rel="alternate" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/restore?language=<?php echo $item['language_key'] ?>" hreflang="<?php echo $item['language_key'] ?>">
		<?php endforeach; ?>
		
		<link rel="shortcut icon" href="<?php echo $functions->config('assets') ?>/ico/favicon.ico">
		
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/vendor.css">
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/main.css">
		
		<script src="<?php echo $functions->config('assets') ?>/js/vendor.js"></script>
		<script src="<?php echo $functions->config('assets') ?>/js/main.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<section class="app">
				<a href="<?php echo isset($request->get['redirect']) && !is_array($request->get['redirect']) ? htmlspecialchars_decode($request->get['redirect']) : '/' ?>" class="over">
					<div class="text-center">
						<img src="<?php echo $functions->config('assets') ?>/png/logo.png" alt="logo" class="half">
					</div>
				</a>
				<div class="over">
					<div class="name">
						<span><?php echo $functions->languageInit('Main_Restore') ?></span>
					</div>
					<div class="name">
						<span class="focus"><?php echo $functions->languageInit('Main_RestoreDesc') ?></span>
					</div>
				</div>
				<form action="/restore/ajax/<?php echo $functions->getCsrf() ?>" method="POST" class="widget restoreMain">
					<div class="over formEmail">
						<div class="above">
							<label for="email" class="name">
								<span class="font-600"><?php echo $functions->languageInit('Main_RestoreLabelEmail') ?></span>
							</label>
						</div>
						<div class="above">
							<div class="justified">
								<input type="email" name="email" placeholder="<?php echo $functions->languageInit('Main_RestoreFormEmail') ?>" class="form block" id="email">
								<button type="submit" class="btn info">
									<i class="zmdi zmdi-mail-send"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="text-center">
						<a href="/login<?php if(isset($request->get['redirect']) && !is_array($request->get['redirect'])): ?>?redirect=<?php echo urlencode(htmlspecialchars_decode($request->get['redirect'])) ?><?php endif; ?>" class="btn default">
							<span><?php echo $functions->languageInit('CommonLogin') ?></span>
						</a>
					</div>
				</form>
			</section>
		</div>
		<script>
			$(document).on('submit', '.restoreMain', function(event) {
				event.preventDefault();
				
				var form = $(this);
				
				$.ajax({
					contentType: false,
					processData: false,
					type: form.attr('method'),
					url: form.attr('action'),
					data: new FormData(form[0]),
					beforeSend: function(data) {
						form.find('button[type="submit"]').prop('disabled', true);
						
						form.find('.form.error').removeClass('error');
						$('.addonMain').remove();
					},
					success: function(data) {
						data = JSON.parse(data);
						switch(data.status) {
							case 'error':
								if($.isArray(data.error)) {
									$.each(data.error, function() {
										form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
										
										if(this.value) {
											form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonMain">\
												<span class="focus">' + this.value + '</span>\
											</div>');
										}
									});
									
									if($('.tabHome').length || $('.tabPassword').length) {
										if(data.error[0].key == 'email') {
											$('.tabHome').tab('show');
										} else if(data.error[0].key == 'code') {
											$('.tabHome').tab('show');
										} else if(data.error[0].key == 'password') {
											$('.tabPassword').tab('show');
										} else if(data.error[0].key == 'password2') {
											$('.tabPassword').tab('show');
										}
									}
								} else {
									$.growl({
										message: data.error,
										type: 'error'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
								break;
							case 'success':
								document.location.href = '<?php echo isset($request->get['redirect']) && !is_array($request->get['redirect']) ? addslashes(htmlspecialchars_decode($request->get['redirect'])) : '/' ?>';
								break;
							case 'password':
								$('.restoreMain').html('<div class="over">\
									<div class="justified">\
										<a href="#tabHome" class="btn default block tabHome" data-toggle="tab">\
											<i class="zmdi zmdi-home"></i>\
										</a>\
										<a href="#tabPassword" class="btn default block active tabPassword" data-toggle="tab">\
											<i class="zmdi zmdi-lock"></i>\
										</a>\
									</div>\
								</div>\
								<div class="over">\
									<div class="tab fade" id="tabHome">\
										<div class="over formEmail">\
											<div class="above">\
												<label for="email" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_RestoreLabelEmail')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<div class="justified">\
													<input type="email" name="email" value="' + $('input[name="email"]').val() + '" class="form block" id="email" readonly>\
													<button type="button" class="btn default editEmail">\
														<i class="zmdi zmdi-edit"></i>\
													</button>\
												</div>\
											</div>\
										</div>\
										<div class="over formCode">\
											<div class="above">\
												<label for="code" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_RestoreLabelCode')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<div class="control">\
													<div class="control-addon">\
														<div class="btn media">\
															<span><?php echo addslashes($functions->config('code')) ?></span>\
														</div>\
													</div>\
													<div class="control-block">\
														<input type="number" name="code" value="' + $('input[name="code"]').val() + '" class="form block" id="code" readonly>\
													</div>\
												</div>\
											</div>\
										</div>\
									</div>\
									<div class="tab fade open active in" id="tabPassword">\
										<div class="over formPassword">\
											<div class="above">\
												<label for="password" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_RestoreLabelPassword')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<input type="password" name="password" placeholder="<?php echo addslashes($functions->languageInit('Main_RestoreFormPassword')) ?>" class="form block" id="password">\
											</div>\
										</div>\
										<div class="over formPassword2">\
											<div class="above">\
												<label for="password2" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_RestoreLabelPassword2')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<input type="password" name="password2" placeholder="<?php echo addslashes($functions->languageInit('Main_RestoreFormPassword2')) ?>" class="form block" id="password2">\
											</div>\
										</div>\
										<button type="submit" class="btn">\
											<span><?php echo addslashes($functions->languageInit('Main_RestoreSubmit')) ?></span>\
										</button>\
									</div>\
								</div>');
								break;
							case 'restore':
								$('.restoreMain').html('<div class="over formEmail">\
									<div class="above">\
										<label for="email" class="name">\
											<span class="font-600"><?php echo addslashes($functions->languageInit('Main_RestoreLabelEmail')) ?></span>\
										</label>\
									</div>\
									<div class="above">\
										<div class="justified">\
											<input type="email" name="email" value="' + $('input[name="email"]').val() + '" class="form block" id="email" readonly>\
											<button type="button" class="btn default editEmail">\
												<i class="zmdi zmdi-edit"></i>\
											</button>\
										</div>\
									</div>\
								</div>\
								<div class="over formCode">\
									<div class="above">\
										<label for="code" class="name">\
											<span class="font-600"><?php echo addslashes($functions->languageInit('Main_RestoreLabelCode')) ?></span>\
										</label>\
									</div>\
									<div class="above">\
										<div class="control">\
											<div class="control-addon">\
												<div class="btn media">\
													<span><?php echo addslashes($functions->config('code')) ?></span>\
												</div>\
											</div>\
											<div class="control-block">\
												<input type="number" name="code" placeholder="<?php echo addslashes($functions->languageInit('Main_RestoreFormCode')) ?>" class="form block" id="code">\
											</div>\
											<div class="control-addon codeEmail">\
												<button type="button" class="btn info" disabled>\
													<span>' + data.time + '</span>\
												</button>\
											</div>\
										</div>\
									</div>\
								</div>\
								<button type="submit" class="btn">\
									<span><?php echo addslashes($functions->languageInit('Main_RestoreSubmit')) ?></span>\
								</button>');
								
								codeEmail(data.time);
								break;
						}
					},
					error: function(data) {
						if(data.statusText != 'abort') {
							$.growl({
								message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
								type: 'warning'
							});
						}
						
						form.find('button[type="submit"]').prop('disabled', false);
					}
				});
			});
			
			$(document).on('click', '.sendEmail', function() {
				var submit = $(this);
				
				var formData = new FormData();
				formData.append('email', $('input[name="email"]').val());
				
				$.ajax({
					contentType: false,
					processData: false,
					type: 'POST',
					url: '/restore/ajax/<?php echo addslashes($functions->getCsrf()) ?>',
					data: formData,
					beforeSend: function(data) {
						submit.prop('disabled', true);
						
						$('.restoreMain').find('.form.error').removeClass('error');
						$('.addonMain').remove();
					},
					success: function(data) {
						data = JSON.parse(data);
						switch(data.status) {
							case 'error':
								if($.isArray(data.error)) {
									$.each(data.error, function() {
										$('.restoreMain').find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
										
										if(this.value) {
											$('.restoreMain').find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonMain">\
												<span class="focus">' + this.value + '</span>\
											</div>');
										}
									});
									
									if($('.tabHome').length || $('.tabPassword').length) {
										if(data.error[0].key == 'email') {
											$('.tabHome').tab('show');
										} else if(data.error[0].key == 'code') {
											$('.tabHome').tab('show');
										} else if(data.error[0].key == 'password') {
											$('.tabPassword').tab('show');
										} else if(data.error[0].key == 'password2') {
											$('.tabPassword').tab('show');
										}
									}
								} else {
									$.growl({
										message: data.error,
										type: 'error'
									});
								}
								
								submit.prop('disabled', false);
								break;
							case 'restore':
								$('.restoreMain').html('<div class="over formEmail">\
									<div class="above">\
										<label for="email" class="name">\
											<span class="font-600"><?php echo addslashes($functions->languageInit('Main_RestoreLabelEmail')) ?></span>\
										</label>\
									</div>\
									<div class="above">\
										<div class="justified">\
											<input type="email" name="email" value="' + $('input[name="email"]').val() + '" class="form block" id="email" readonly>\
											<button type="button" class="btn default editEmail">\
												<i class="zmdi zmdi-edit"></i>\
											</button>\
										</div>\
									</div>\
								</div>\
								<div class="over formCode">\
									<div class="above">\
										<label for="code" class="name">\
											<span class="font-600"><?php echo addslashes($functions->languageInit('Main_RestoreLabelCode')) ?></span>\
										</label>\
									</div>\
									<div class="control">\
										<div class="control-addon">\
											<div class="btn media">\
												<span><?php echo addslashes($functions->config('code')) ?></span>\
											</div>\
										</div>\
										<div class="control-block">\
											<input type="number" name="code" placeholder="<?php echo addslashes($functions->languageInit('Main_RestoreFormCode')) ?>" class="form block" id="code">\
										</div>\
										<div class="control-addon codeEmail">\
											<button type="button" class="btn info" disabled>\
												<span>' + data.time + '</span>\
											</button>\
										</div>\
									</div>\
								</div>\
								<button type="submit" class="btn">\
									<span><?php echo addslashes($functions->languageInit('Main_RestoreSubmit')) ?></span>\
								</button>');
								
								codeEmail(data.time);
								break;
						}
					},
					error: function(data) {
						if(data.statusText != 'abort') {
							$.growl({
								message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
								type: 'warning'
							});
						}
						
						submit.prop('disabled', false);
					}
				});
			});
			
			$(document).on('click', '.editEmail', function() {
				$('.restoreMain').html('<div class="over formEmail">\
					<div class="above">\
						<label for="email" class="name">\
							<span class="font-600"><?php echo addslashes($functions->languageInit('Main_RestoreLabelEmail')) ?></span>\
						</label>\
					</div>\
					<div class="above">\
						<div class="justified">\
							<input type="email" name="email" placeholder="<?php echo addslashes($functions->languageInit('Main_RestoreFormEmail')) ?>" class="form block" id="email">\
							<button type="submit" class="btn info">\
								<i class="zmdi zmdi-mail-send"></i>\
							</button>\
						</div>\
					</div>\
				</div>\
				<div class="text-center">\
					<a href="/login<?php if(isset($request->get['redirect']) && !is_array($request->get['redirect'])): ?>?redirect=<?php echo addslashes(urlencode(htmlspecialchars_decode($request->get['redirect']))) ?><?php endif; ?>" class="btn default">\
						<span><?php echo addslashes($functions->languageInit('CommonLogin')) ?></span>\
					</a>\
				</div>');
			});
			
			var interval_email;
			function codeEmail(time) {
				if(typeof(interval_email) != 'undefined') {
					clearInterval(interval_email);
				}
				
				var date = new Date;
				interval_email = setInterval(function() {
					var interval_time = time - Math.round((new Date - date) / 1000);
					
					if(interval_time > 0) {
						$('.codeEmail').html('<button type="button" class="btn info" disabled>\
							<span>' + interval_time + '</span>\
						</button>');
					} else {
						$('.codeEmail').html('<button type="button" class="btn info sendEmail">\
							<i class="zmdi zmdi-refresh"></i>\
						</button>');
						
						clearInterval(interval_email);
					}
				}, 1000);
			}
		</script>
		<noscript>
			<div style="background-color:#fff;color:#000;position:fixed;width:100%;height:100%;top:0;left:0;z-index:9999;">
				<div style="margin:10%;width:80%;">
					<img src="<?php echo $functions->config('assets') ?>/png/logo.png" alt="logo" style="margin-bottom:20px;max-width:20%;">
					<div style="font-size:18px;word-break:break-word;"><?php echo $functions->languageInit('CommonNoscript') ?></div>
				</div>
			</div>
		</noscript>
	</body>
</html>
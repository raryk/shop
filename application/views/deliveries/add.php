<?php echo $header ?>
				<div class="over">
					<form action="/deliveries/add/ajax/<?php echo $functions->getCsrf() ?>" method="POST" class="widget addDeliverie">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Main_DeliveriesAdd') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Main_DeliveriesAddDesc') ?></span>
							</div>
						</div>
						<div class="over formName">
							<div class="above">
								<label for="name" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Main_DeliveriesLabelName') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="name" placeholder="<?php echo $functions->languageInit('Main_DeliveriesFormName') ?>" class="form block" id="name">
							</div>
						</div>
						<div class="over formCountry">
							<div class="above">
								<label for="country" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Main_DeliveriesLabelCountry') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="country" placeholder="<?php echo $functions->languageInit('Main_DeliveriesFormCountry') ?>" class="form block" id="country">
							</div>
						</div>
						<div class="over formCity">
							<div class="above">
								<label for="city" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Main_DeliveriesLabelCity') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="city" placeholder="<?php echo $functions->languageInit('Main_DeliveriesFormCity') ?>" class="form block" id="city">
							</div>
						</div>
						<div class="over formAddress">
							<div class="above">
								<label for="address" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Main_DeliveriesLabelAddress') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="address" placeholder="<?php echo $functions->languageInit('Main_DeliveriesFormAddress') ?>" class="form block" id="address">
							</div>
						</div>
						<div class="over formZipcode">
							<div class="above">
								<label for="state" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Main_DeliveriesLabelZipcode') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="zipcode" placeholder="<?php echo $functions->languageInit('Main_DeliveriesFormZipcode') ?>" class="form block" id="zipcode">
							</div>
						</div>
						<div class="over formPhone">
							<div class="above">
								<label for="phone" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Main_DeliveriesLabelPhone') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="phone" placeholder="<?php echo $functions->languageInit('Main_DeliveriesFormPhone') ?>" class="form block" id="phone">
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Main_DeliveriesAddSubmit') ?></span>
						</button>
					</form>
				</div>
				<script>
					$(document).on('submit', '.addDeliverie', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonDeliverie').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonDeliverie">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										
										form.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/deliveries';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
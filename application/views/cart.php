<?php echo $header ?>
				<div class="over" id="cart">
					<?php if(!empty(array_values($functions->cart(1)))): ?>
					<div class="row">
						<div class="col-8-sm">
							<div class="widget">
								<div class="over">
									<div class="name">
										<span><?php echo $functions->languageInit('Main_Cart') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Main_CartDesc') ?></span>
									</div>
								</div>
								<div class="over">
									<div class="responsive">
										<table class="table">
											<thead>
												<tr>
													<th>
														<div class="model">
															<span class="font-600"><?php echo $functions->languageInit('Main_CartTableProduct') ?></span>
														</div>
													</th>
													<th>
														<div class="model">
															<span class="font-600"><?php echo $functions->languageInit('Main_CartTableContent') ?></span>
														</div>
													</th>
													<th>
														<div class="model">
															<span class="font-600"><?php echo $functions->languageInit('Main_CartTableCount') ?></span>
														</div>
													</th>
													<th>
														<div class="model">
															<span class="font-600"><?php echo $functions->languageInit('Main_CartTablePrice') ?></span>
														</div>
													</th>
													<th>
														<div class="model">
															<span class="font-600"><?php echo $functions->languageInit('Main_CartTableAction') ?></span>
														</div>
													</th>
												</tr>
											</thead>
											<tbody class="cartCarts">
												<?php foreach(array_values($functions->cart(1)) as $key => $value): ?>
												<tr>
													<td>
														<div class="model">
															<a href="/product/index/<?php echo $value['id'] ?>" class="link">
																<span><?php echo $value['title'] ?></span>
															</a>
														</div>
													</td>
													<td>
														<div class="model">
															<?php if(isset($value['content'])): ?>
															<span><?php echo $value['content'] ?></span>
															<?php else: ?>
															<span class="font-600">-</span>
															<?php endif; ?>
														</div>
													</td>
													<td>
														<div class="model">
															<span><?php echo $value['count'] ?></span>
														</div>
													</td>
													<td>
														<div class="model">
															<span><?php echo $value['price'] ?></span>
														</div>
													</td>
													<td>
														<button type="button" class="btn error cartDelete" data-key="<?php echo $key ?>">
															<i class="zmdi zmdi-delete"></i>
														</button>
													</td>
												</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
								<button type="button" class="btn default block cartClear">
									<span><?php echo $functions->languageInit('Main_CartClear') ?></span>
								</button>
							</div>
						</div>
						<div class="col-4-sm">
							<form action="/cart/order/<?php echo $functions->getCsrf() ?>" method="POST" class="widget cartMain">
								<div class="over">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-shopping-cart"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="name">
												<span><?php echo $functions->languageInit('Main_CartOrder') ?></span>
											</div>
										</div>
									</div>
								</div>
								<div class="over formEmail">
									<div class="above">
										<label for="email" class="name">
											<span class="font-600"><?php echo $functions->languageInit('Main_CartLabelEmail') ?></span>
										</label>
									</div>
									<div class="above">
										<input type="email" name="email" placeholder="<?php echo $functions->languageInit('Main_CartFormEmail') ?>"<?php if($account->isLogged() && mb_strlen(trim($account->getEmail())) >= 1): ?> value="<?php echo $account->getEmail() ?>"<?php endif; ?> class="form block" id="email">
									</div>
								</div>
								<div class="over formText">
									<div class="above">
										<label for="text" class="name">
											<span class="font-600"><?php echo $functions->languageInit('Main_CartLabelText') ?></span>
										</label>
									</div>
									<div class="above">
										<textarea rows="5" cols="1" name="text" placeholder="<?php echo $functions->languageInit('Main_CartFormText') ?>" class="form block" id="text"></textarea>
									</div>
								</div>
								<div class="over">
									<div class="above">
										<label for="code" class="name">
											<span class="font-600"><?php echo $functions->languageInit('Main_CartLabelCode') ?></span>
										</label>
									</div>
									<div class="above">
										<div class="control">
											<div class="control-block">
												<input type="text" name="code" placeholder="<?php echo $functions->languageInit('Main_CartFormCode') ?>" class="form block" id="code">
											</div>
											<div class="control-addon">
												<button type="button" class="btn default cartCoupon">
													<i class="zmdi zmdi-check"></i>
												</button>
											</div>
										</div>
									</div>
								</div>
								<div class="over">
									<div class="name cartTotal">
										<span class="font-600"><?php echo $functions->currencieInit($functions->currencie(), array_sum(array_values($functions->cart(2)))) ?></span>
									</div>
								</div>
								<button type="submit" class="btn">
									<span><?php echo $functions->languageInit('Main_CartOrderSubmit') ?></span>
								</button>
							</form>
						</div>
					</div>
					<?php else: ?>
					<div class="widget">
						<div class="text-center">
							<div class="above">
								<div class="btn media">
									<i class="zmdi zmdi-alert-triangle"></i>
								</div>
							</div>
							<div class="name">
								<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
							</div>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<script>
					function carts(data) {
						if(!$.isEmptyObject(data.carts)) {
							$('.cartCarts').empty();
							
							$.each(data.carts, function(key) {
								if(typeof(this.content) != 'undefined') {
									var content = '<span>' + this.content + '</span>';
								} else {
									var content = '<span class="font-600">-</span>';
								}
								
								if(typeof(this.oldprice) != 'undefined') {
									var price = '<span class="text-line">' + this.oldprice + '</span>\
									<span>&nbsp;</span>\
									<span class="font-600">' + this.price + '</span>';
								} else {
									var price = '<span class="font-600">' + this.price + '</span>';
								}
								
								$('.cartCarts').append('<tr>\
									<td>\
										<div class="model">\
											<a href="/product/index/' + this.id + '" class="link">\
												<span>' + this.title + '</span>\
											</a>\
										</div>\
									</td>\
									<td>\
										<div class="model">'
											+ content +
										'</div>\
									</td>\
									<td>' + this.count + '</td>\
									<td>\
										<div class="model">'
											+ price +
										'</div>\
									</td>\
									<td>\
										<button type="button" class="btn error cartDelete" data-key="' + key + '">\
											<i class="zmdi zmdi-delete"></i>\
										</button>\
									</td>\
								</td>');
							});
							
							if(typeof(data.oldtotal) != 'undefined') {
								$('.cartTotal').html('<span class="text-line">' + data.oldtotal + '</span>\
								<span>&nbsp;</span>\
								<span class="font-600">' + data.total + '</span>');
							} else {
								$('.cartTotal').html('<span class="font-600">' + data.total + '</span>');
							}
							
							$('.headerTotal').text(data.total);
						} else {
							$('#cart').html('<div class="widget">\
								<div class="text-center">\
									<div class="above">\
										<div class="btn media">\
											<i class="zmdi zmdi-alert-triangle"></i>\
										</div>\
									</div>\
									<div class="name">\
										<span class="font-600"><?php echo addslashes($functions->languageInit('CommonEmpty')) ?></span>\
									</div>\
								</div>\
							</div>');
						}
					}
					
					$(document).on('submit', '.cartMain', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonMain').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonMain">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										
										form.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = data.url;
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.cartDelete', function() {
						var submit = $(this);
						
						var formData = new FormData();
						formData.append('key', submit.data('key'));
						formData.append('code', $('input[name="code"]').val());
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/cart/delete/<?php echo addslashes($functions->getCsrf()) ?>',
							data: formData,
							beforeSend: function(data) {
								submit.prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										break;
									case 'success':
										carts(data);
										break;
								}
								
								submit.prop('disabled', false);
							},
							error: function(data) {
								submit.prop('disabled', false);
								
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
							}
						});
					});
					
					$(document).on('click', '.cartCoupon', function() {
						var submit = $(this);
						
						var formData = new FormData();
						formData.append('code', $('input[name="code"]').val());
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/cart/coupon/<?php echo addslashes($functions->getCsrf()) ?>',
							data: formData,
							beforeSend: function(data) {
								submit.prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										break;
									case 'success':
										carts(data);
										break;
								}
								
								submit.prop('disabled', false);
							},
							error: function(data) {
								submit.prop('disabled', false);
								
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
							}
						});
					});
					
					$(document).on('click', '.cartClear', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/cart/clear/<?php echo addslashes($functions->getCsrf()) ?>',
							beforeSend: function(data) {
								submit.prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										break;
									case 'success':
										carts(data);
										break;
								}
								
								submit.prop('disabled', false);
							},
							error: function(data) {
								submit.prop('disabled', false);
								
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
							}
						});
					});
				</script>
<?php echo $footer ?>
<?php echo $header ?>
				<div class="over">
					<div class="row">
						<div class="col-8-sm">
							<div class="widget">
								<div class="over">
									<div class="name">
										<span><?php echo $functions->languageInit('Main_OrdersView') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Main_OrdersViewDesc') ?></span>
									</div>
								</div>
								<?php if(!empty(json_decode($order['order_content'], true))): ?>
								<div class="responsive">
									<table class="table">
										<thead>
											<tr>
												<th>
													<div class="model">
														<span class="font-600"><?php echo $functions->languageInit('Main_OrdersViewTableProduct') ?></span>
													</div>
												</th>
												<th>
													<div class="model">
														<span class="font-600"><?php echo $functions->languageInit('Main_OrdersViewTableContent') ?></span>
													</div>
												</th>
												<th>
													<div class="model">
														<span class="font-600"><?php echo $functions->languageInit('Main_OrdersViewTableCount') ?></span>
													</div>
												</th>
												<th>
													<div class="model">
														<span class="font-600"><?php echo $functions->languageInit('Main_OrdersViewTablePrice') ?></span>
													</div>
												</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach(json_decode($order['order_content'], true) as $item): ?>
											<?php if($functions->getTotal('shop_products', array('product_id' => $item['id']))): ?>
											<?php $product = $functions->getBy('shop_products', array('product_id' => $item['id'])) ?>
											<tr>
												<td>
													<div class="model">
														<a href="/product/index/<?php echo $product['product_id'] ?>" class="link">
															<span><?php echo isset(json_decode($product['product_title'], true)[$functions->language()]) ? json_decode($product['product_title'], true)[$functions->language()] : json_decode($product['product_title'], true)[$functions->config('language')] ?></span>
														</a>
													</div>
												</td>
												<td>
													<div class="model">
														<?php if(!empty(json_decode($product['product_content'], true)) && isset($item['content']) && !is_array($item['content']) && array_key_exists($item['content'], json_decode($product['product_content'], true))): ?>
														<span><?php echo json_decode($product['product_content'], true)[$item['content']] ?></span>
														<?php else: ?>
														<span class="font-600">-</span>
														<?php endif; ?>
													</div>
												</td>
												<td>
													<div class="model">
														<span><?php echo $item['count'] ?></span>
													</div>
												</td>
												<td>
													<div class="model">
														<span><?php echo $functions->currencieInit($functions->currencie(), $functions->convert(($product['product_price']) * $item['count'], $functions->currencie())) ?></span>
													</div>
												</td>
											</tr>
											<?php endif; ?>
											<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<?php else: ?>
								<div class="text-center">
									<div class="above">
										<div class="btn media">
											<i class="zmdi zmdi-alert-triangle"></i>
										</div>
									</div>
									<div class="name">
										<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
									</div>
								</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-4-sm">
							<div class="over">
								<div class="widget">
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-fire"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<span><?php echo $order['order_id'] ?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-email"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<a href="mailto:<?php echo $order['order_email'] ?>" class="link">
														<span><?php echo $order['order_email'] ?></span>
													</a>
												</div>
											</div>
										</div>
									</div>
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-balance-wallet"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<span><?php echo $functions->currencieInit($order['order_currencie'], $order['order_amount'], true) ?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-label"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<?php if($order['order_status'] == 2): ?>
													<span><?php echo $functions->languageInit('Main_OrdersViewStatus2') ?></span>
													<?php elseif($order['order_status'] == 1): ?>
													<span><?php echo $functions->languageInit('Main_OrdersViewStatus1') ?></span>
													<?php else: ?>
													<span><?php echo $functions->languageInit('Main_OrdersViewStatus0') ?></span>
													<?php endif; ?>
												</div>
											</div>
										</div>
									</div>
									<?php if(mb_strlen(trim($order['order_text'])) >= 1): ?>
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-collection-text"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<?php foreach($functions->string(nl2br($order['order_text']), true) as $item): ?>
													<?php if(isset($item['link'])): ?>
													<a href="<?php echo $item['link'] ?>" class="link">
														<span><?php echo $item['link'] ?></span>
													</a>
													<?php else: ?>
													<span><?php echo $item['text'] ?></span>
													<?php endif; ?>
													<?php endforeach; ?>
												</div>
											</div>
										</div>
									</div>
									<?php endif; ?>
									<?php if(mb_strlen(trim($order['order_track'])) >= 1): ?>
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-shipping"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<span><?php echo $order['order_track'] ?></span>
												</div>
											</div>
										</div>
									</div>
									<?php endif; ?>
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-calendar"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<span><?php echo $functions->datetime(strtotime($order['order_date_add'])) ?></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="over">
								<div class="widget">
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-map"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<span><?php echo $functions->languageInit('Main_OrdersViewDeliverie') ?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Main_OrdersViewDeliverieName') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $order['order_name'] ?></span>
										</div>
									</div>
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Main_OrdersViewDeliverieCountry') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $order['order_country'] ?></span>
										</div>
									</div>
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Main_OrdersViewDeliverieCity') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $order['order_city'] ?></span>
										</div>
									</div>
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Main_OrdersViewDeliverieAddress') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $order['order_address'] ?></span>
										</div>
									</div>
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Main_OrdersViewDeliverieZipcode') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $order['order_zipcode'] ?></span>
										</div>
									</div>
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Main_OrdersViewDeliveriePhone') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $order['order_phone'] ?></span>
										</div>
									</div>
								</div>
							</div>
							<?php if(!$order['order_status']): ?>
							<form action="/orders/view/ajax/<?php echo $functions->getCsrf() ?>/<?php echo addslashes($order['order_id']) ?>" method="POST" class="over viewOrder">
								<div class="name">
									<span class="font-600"><?php echo $functions->currencieInit($order['order_currencie'], $order['order_amount']) ?></span>
								</div>
								<button type="submit" class="btn block">
									<span><?php echo $functions->languageInit('Main_OrdersViewSubmit') ?></span>
								</button>
							</form>
							<?php endif; ?>
							<div class="justified">
								<button type="button" class="btn error block" data-toggle="dialog">
									<span><?php echo $functions->languageInit('Main_OrdersViewDelete') ?></span>
								</button>
								<div class="modal fade" data-ride="dialog">
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Main_OrdersViewDeleteTitle') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $functions->languageInit('Main_OrdersViewDeleteDesc') ?></span>
										</div>
									</div>
									<div class="text-right">
										<div class="fill">
											<button type="button" class="btn second" data-dismiss="dialog">
												<span><?php echo $functions->languageInit('Main_OrdersViewDeleteClose') ?></span>
											</button>
											<button type="button" class="btn error deleteOrder">
												<span><?php echo $functions->languageInit('Main_OrdersViewDeleteSubmit') ?></span>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<script>
					$(document).on('submit', '.viewOrder', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonOrder').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonOrder">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										break;
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.deleteOrder', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/orders/index/delete/<?php echo addslashes($functions->getCsrf()) ?>/<?php echo addslashes($order['order_id']) ?>',
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/orders';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
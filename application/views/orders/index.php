<?php echo $header ?>
				<form action="/orders" class="over">
					<div class="control">
						<div class="control-block">
							<input type="text" name="q" placeholder="<?php echo $functions->languageInit('CommonSearch') ?>"<?php if(isset($request->get['q']) && !is_array($request->get['q'])): ?> value="<?php echo $request->get['q'] ?>"<?php endif; ?> class="form block">
						</div>
						<div class="control-addon">
							<a href="/orders" class="btn second" title="<?php echo $functions->number2format($total) ?>" data-ride="tooltip">
								<span><?php echo $functions->number2string($total) ?></span>
							</a>
						</div>
					</div>
				</form>
				<div class="over">
					<div class="widget">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Main_OrdersIndex') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Main_OrdersIndexDesc') ?></span>
							</div>
						</div>
						<div class="over">
							<?php if(!empty($orders)): ?>
							<div class="responsive">
								<table class="table">
									<thead>
										<tr>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Main_OrdersTableId') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Main_OrdersTableName') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Main_OrdersTableStatus') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Main_OrdersTableAmount') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Main_OrdersTableDate') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Main_OrdersTableAction') ?></span>
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($orders as $item): ?>
										<tr>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['order_id']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['order_name']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<?php if($item['order_status'] == 2): ?>
													<span><?php echo $functions->languageInit('Main_OrdersIndexStatus2') ?></span>
													<?php elseif($item['order_status'] == 1): ?>
													<span><?php echo $functions->languageInit('Main_OrdersIndexStatus1') ?></span>
													<?php else: ?>
													<span><?php echo $functions->languageInit('Main_OrdersIndexStatus0') ?></span>
													<?php endif; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $functions->currencieInit($item['order_currencie'], $item['order_amount'], true) ?></span>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $functions->datetime(strtotime($item['order_date_add'])) ?></span>
												</div>
											</td>
											<td>
												<button type="button" class="btn default" data-toggle="dialog">
													<span><?php echo $functions->languageInit('CommonAction') ?></span>
													<span>&nbsp;</span>
													<i class="zmdi zmdi-caret-down"></i>
												</button>
												<div class="dropdown fade" data-ride="dialog" data-position="true">
													<a href="/orders/view/index/<?php echo $item['order_id'] ?>" class="menu">
														<div class="model">
															<span><?php echo $functions->languageInit('Main_OrdersIndexView') ?></span>
														</div>
													</a>
												</div>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php else: ?>
							<div class="text-center">
								<div class="above">
									<div class="btn media">
										<i class="zmdi zmdi-alert-triangle"></i>
									</div>
								</div>
								<div class="name">
									<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
								</div>
							</div>
							<?php endif; ?>
						</div>
						<?php if(!empty($pagination)): ?>
						<div class="group">
							<div class="wrap">
								<?php foreach($pagination as $item): ?>
								<div class="wrap-item">
									<a href="<?php echo $item['url'] ?>" class="btn background-color-transparent color-inherit<?php if($item['active']): ?> active<?php endif; ?>">
										<span><?php echo $item['text'] ?></span>
									</a>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
<?php echo $footer ?>
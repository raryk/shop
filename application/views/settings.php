<?php echo $header ?>
				<div class="over">
					<div class="row">
						<div class="col-8-sm">
							<div class="over">
								<form action="/settings/ajax/<?php echo $functions->getCsrf() ?>" method="POST" class="widget settingsMain">
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Main_Settings') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $functions->languageInit('Main_SettingsDesc') ?></span>
										</div>
									</div>
									<div class="over formNickname">
										<div class="above">
											<label for="nickname" class="name">
												<span class="font-600"><?php echo $functions->languageInit('Main_SettingsLabelNickname') ?></span>
											</label>
										</div>
										<div class="above">
											<input type="text" name="nickname" placeholder="<?php echo $functions->languageInit('Main_SettingsFormNickname') ?>" value="<?php echo $account->getNickname() ?>" class="form block" id="nickname">
										</div>
									</div>
									<div class="over">
										<div class="above">
											<label for="deliverie" class="name">
												<span class="font-600"><?php echo $functions->languageInit('Main_SettingsLabelDeliverie') ?></span>
											</label>
										</div>
										<select name="deliverie" class="form block" id="deliverie">
											<option value="0"><?php echo $functions->languageInit('CommonSelect') ?></option>
											<?php foreach($functions->get('shop_deliveries', array('user_id' => $account->getId())) as $item): ?>
											<option value="<?php echo $item['deliverie_id'] ?>"<?php if($account->getDeliverie() == $item['deliverie_id']): ?> selected="selected"<?php endif; ?>><?php echo $item['deliverie_name'] ?></option>
											<?php endforeach; ?>
										</select>
									</div>
									<div class="over formLanguage">
										<div class="above">
											<label for="language" class="name">
												<span class="font-600"><?php echo $functions->languageInit('Main_SettingsLabelLanguage') ?></span>
											</label>
										</div>
										<div class="above">
											<select name="language" class="form block" id="language">
												<?php foreach($functions->get('system_languages') as $item): ?>
												<option value="<?php echo $item['language_id'] ?>"<?php if($functions->language() == $item['language_id']): ?> selected="selected"<?php endif; ?>><?php echo $item['language_value'] ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<div class="over formTimezone">
										<div class="above">
											<label for="timezone" class="name">
												<span class="font-600"><?php echo $functions->languageInit('Main_SettingsLabelTimezone') ?></span>
											</label>
										</div>
										<div class="above">
											<select name="timezone" class="form block" id="timezone">
												<?php foreach($functions->get('system_timezones') as $item): ?>
												<option value="<?php echo $item['timezone_id'] ?>"<?php if($functions->timezone() == $item['timezone_id']): ?> selected="selected"<?php endif; ?>><?php echo isset(json_decode($item['timezone_value'], true)[$functions->language()]) ? json_decode($item['timezone_value'], true)[$functions->language()] : json_decode($item['timezone_value'], true)[$functions->config('language')] ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<div class="over formCurrencie">
										<div class="above">
											<label for="currencie" class="name">
												<span class="font-600"><?php echo $functions->languageInit('Main_SettingsLabelCurrencie') ?></span>
											</label>
										</div>
										<div class="above">
											<select name="currencie" class="form block" id="currencie">
												<?php foreach($functions->get('system_currencies') as $item): ?>
												<option value="<?php echo $item['currencie_id'] ?>"<?php if($functions->currencie() == $item['currencie_id']): ?> selected="selected"<?php endif; ?>><?php echo $item['currencie_key'] ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<div class="over">
										<label class="group hover">
											<div class="control">
												<div class="control-block">
													<div class="name">
														<span class="font-600"><?php echo $functions->languageInit('Main_SettingsLabelTwa') ?></span>
													</div>
												</div>
												<div class="control-addon">
													<div class="check">
														<input type="checkbox" name="twa" class="custom"<?php if($account->getTwa()): ?> checked<?php endif; ?>>
														<i class="check-switch"></i>
													</div>
												</div>
											</div>
										</label>
									</div>
									<button type="submit" class="btn">
										<span><?php echo $functions->languageInit('Main_SettingsSubmit') ?></span>
									</button>
								</form>
							</div>
							<div class="widget">
								<div class="over">
									<div class="name">
										<span><?php echo $functions->languageInit('Main_SettingsTokens') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Main_SettingsTokensDesc') ?></span>
									</div>
								</div>
								<?php if($functions->getTotal('users_tokens', array('user_id' => $account->getId()))): ?>
								<div class="over">
									<div class="responsive">
										<table class="table">
											<thead>
												<tr>
													<th>
														<div class="model">
															<span class="font-600"><?php echo $functions->languageInit('Main_TokensTableId') ?></span>
														</div>
													</th>
													<th>
														<div class="model">
															<span class="font-600"><?php echo $functions->languageInit('Main_TokensTableIp') ?></span>
														</div>
													</th>
													<th>
														<div class="model">
															<span class="font-600"><?php echo $functions->languageInit('Main_TokensTableAgent') ?></span>
														</div>
													</th>
													<th>
														<div class="model">
															<span class="font-600"><?php echo $functions->languageInit('Main_TokensTableDate') ?></span>
														</div>
													</th>
													<th>
														<div class="model">
															<span class="font-600"><?php echo $functions->languageInit('Main_TokensTableAction') ?></span>
														</div>
													</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach($functions->get('users_tokens', array('user_id' => $account->getId()), array(), array(), array('token_id' => 'DESC'), array('start' => 0, 'limit' => 5)) as $item): ?>
												<tr>
													<td>
														<div class="model">
															<span><?php echo $item['token_id'] ?></span>
														</div>
													</td>
													<td>
														<div class="model">
															<span><?php echo $item['token_ip'] ?></span>
														</div>
													</td>
													<td>
														<div class="model">
															<span><?php echo $item['token_agent'] ?></span>
														</div>
													</td>
													<td>
														<div class="model">
															<span><?php echo $functions->datetime(strtotime($item['token_date_add'])) ?></span>
														</div>
													</td>
													<td>
														<div class="fill">
															<button type="button" class="btn default addon" data-toggle="dialog">
																<span><?php echo $functions->languageInit('CommonAction') ?></span>
																<span>&nbsp;</span>
																<i class="zmdi zmdi-caret-down"></i>
															</button>
															<div class="dropdown fade" data-ride="dialog" data-position="true">
																<div class="menu hover" data-target="#users_token<?php echo $item['token_id'] ?>_delete" data-toggle="dialog">
																	<div class="model">
																		<span><?php echo $functions->languageInit('Main_TokensDelete') ?></span>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal fade" data-ride="dialog" id="users_token<?php echo $item['token_id'] ?>_delete">
															<div class="over">
																<div class="name">
																	<span><?php echo $functions->languageInit('Main_TokensDeleteTitle') ?></span>
																</div>
																<div class="name">
																	<span class="focus"><?php echo $functions->languageInit('Main_TokensDeleteDesc') ?></span>
																</div>
															</div>
															<div class="text-right">
																<div class="fill">
																	<button type="button" class="btn second" data-dismiss="dialog">
																		<span><?php echo $functions->languageInit('Main_TokensDeleteClose') ?></span>
																	</button>
																	<button type="button" class="btn error deleteToken" data-id="<?php echo $item['token_id'] ?>">
																		<span><?php echo $functions->languageInit('Main_TokensDeleteSubmit') ?></span>
																	</button>
																</div>
															</div>
														</div>
													</td>
												</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
								<a href="/tokens" class="btn default block">
									<span><?php echo $functions->languageInit('CommonMore') ?></span>
								</a>
								<?php else: ?>
								<div class="text-center">
									<div class="above">
										<div class="btn media">
											<i class="zmdi zmdi-alert-triangle"></i>
										</div>
									</div>
									<div class="name">
										<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
									</div>
								</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-4-sm">
							<div class="over">
								<form action="/settings/password/<?php echo $functions->getCsrf() ?>" method="POST" class="widget settingsMain">
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-lock"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<span><?php echo $functions->languageInit('Main_SettingsPassword') ?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="over formOldpassword">
										<div class="above">
											<label for="oldpassword" class="name">
												<span class="font-600"><?php echo $functions->languageInit('Main_SettingsLabelOldpassword') ?></span>
											</label>
										</div>
										<div class="above">
											<input type="password" name="oldpassword" placeholder="<?php echo $functions->languageInit('Main_SettingsFormOldpassword') ?>" class="form block" id="oldpassword">
										</div>
									</div>
									<div class="over formPassword">
										<div class="above">
											<label for="password" class="name">
												<span class="font-600"><?php echo $functions->languageInit('Main_SettingsLabelPassword') ?></span>
											</label>
										</div>
										<div class="above">
											<input type="password" name="password" placeholder="<?php echo $functions->languageInit('Main_SettingsFormPassword') ?>" class="form block" id="password">
										</div>
									</div>
									<div class="over formPassword2">
										<div class="above">
											<label for="password2" class="name">
												<span class="font-600"><?php echo $functions->languageInit('Main_SettingsLabelPassword2') ?></span>
											</label>
										</div>
										<div class="above">
											<input type="password" name="password2" placeholder="<?php echo $functions->languageInit('Main_SettingsFormPassword2') ?>" class="form block" id="password2">
										</div>
									</div>
									<div class="over">
										<label class="group hover">
											<div class="control">
												<div class="control-block">
													<div class="name">
														<span class="font-600"><?php echo $functions->languageInit('Main_SettingsLabelTokens') ?></span>
													</div>
												</div>
												<div class="control-addon">
													<div class="check">
														<input type="checkbox" name="tokens" class="custom">
														<i class="check-checkbox"></i>
													</div>
												</div>
											</div>
										</label>
									</div>
									<button type="submit" class="btn">
										<span><?php echo $functions->languageInit('Main_SettingsPasswordSubmit') ?></span>
									</button>
								</form>
							</div>
							<?php if(mb_strlen(trim($account->getEmail())) >= 1): ?>
							<form action="/settings/email/<?php echo $functions->getCsrf() ?>" method="POST" class="widget settingsMainEmail">
								<div class="over">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-email"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="name">
												<span><?php echo $functions->languageInit('Main_SettingsEmailUnbind') ?></span>
											</div>
										</div>
									</div>
								</div>
								<div class="over formEmail">
									<div class="above">
										<label for="email" class="name">
											<span class="font-600"><?php echo $functions->languageInit('Main_SettingsLabelEmail') ?></span>
										</label>
									</div>
									<div class="above">
										<div class="justified">
											<input type="email" name="email" value="<?php echo $account->getEmail() ?>" class="form block" id="email" readonly>
											<button type="submit" class="btn info">
												<i class="zmdi zmdi-mail-send"></i>
											</button>
										</div>
									</div>
								</div>
							</form>
							<?php else: ?>
							<form action="/settings/email/<?php echo $functions->getCsrf() ?>" method="POST" class="widget settingsMainEmail">
								<div class="over">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-email"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="name">
												<span><?php echo $functions->languageInit('Main_SettingsEmailBind') ?></span>
											</div>
										</div>
									</div>
								</div>
								<div class="over formEmail">
									<div class="above">
										<label for="email" class="name">
											<span class="font-600"><?php echo $functions->languageInit('Main_SettingsLabelEmail') ?></span>
										</label>
									</div>
									<div class="above">
										<div class="justified">
											<input type="email" name="email" placeholder="<?php echo $functions->languageInit('Main_SettingsFormEmail') ?>" class="form block" id="email">
											<button type="submit" class="btn info">
												<i class="zmdi zmdi-mail-send"></i>
											</button>
										</div>
									</div>
								</div>
							</form>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<script>
					$(document).on('submit', '.settingsMain', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonMain').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonMain">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										break;
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('submit', '.settingsMainEmail', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonMainEmail').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonMainEmail">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										
										form.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.reload();
										break;
									case 'bind':
										$('.settingsMainEmail').html('<div class="over">\
											<div class="control">\
												<div class="control-addon">\
													<div class="btn media">\
														<i class="zmdi zmdi-email"></i>\
													</div>\
												</div>\
												<div class="control-block">\
													<div class="name">\
														<span><?php echo addslashes($functions->languageInit('Main_SettingsEmailBind')) ?></span>\
													</div>\
												</div>\
											</div>\
										</div>\
										<div class="over formEmail">\
											<div class="above">\
												<label for="email" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_SettingsLabelEmail')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<div class="justified">\
													<input type="email" name="email" value="' + $('input[name="email"]').val() + '" class="form block" id="email" readonly>\
													<button type="button" class="btn default editEmail">\
														<i class="zmdi zmdi-edit"></i>\
													</button>\
												</div>\
											</div>\
										</div>\
										<div class="over formCode">\
											<div class="above">\
												<label for="code" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_SettingsLabelCode')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<div class="control">\
													<div class="control-addon">\
														<div class="btn media">\
															<span><?php echo addslashes($functions->config('code')) ?></span>\
														</div>\
													</div>\
													<div class="control-block">\
														<input type="number" name="code" placeholder="<?php echo addslashes($functions->languageInit('Main_SettingsFormCode')) ?>" class="form block" id="code">\
													</div>\
													<div class="control-addon codeEmail">\
														<button type="button" class="btn info" disabled>\
															<span>' + data.time + '</span>\
														</button>\
													</div>\
												</div>\
											</div>\
										</div>\
										<button type="submit" class="btn">\
											<span><?php echo addslashes($functions->languageInit('Main_SettingsEmailBindSubmit')) ?></span>\
										</button>');
										
										codeEmail(data.time);
										break;
									case 'unbind':
										$('.settingsMainEmail').html('<div class="over">\
											<div class="control">\
												<div class="control-addon">\
													<div class="btn media">\
														<i class="zmdi zmdi-email"></i>\
													</div>\
												</div>\
												<div class="control-block">\
													<div class="name">\
														<span><?php echo addslashes($functions->languageInit('Main_SettingsEmailUnbind')) ?></span>\
													</div>\
												</div>\
											</div>\
										</div>\
										<div class="over formCode">\
											<div class="above">\
												<label for="code" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_SettingsLabelCode')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<div class="control">\
													<div class="control-addon">\
														<div class="btn media">\
															<span><?php echo addslashes($functions->config('code')) ?></span>\
														</div>\
													</div>\
													<div class="control-block">\
														<input type="number" name="code" placeholder="<?php echo addslashes($functions->languageInit('Main_SettingsFormCode')) ?>" class="form block" id="code">\
													</div>\
													<div class="control-addon codeEmail">\
														<button type="button" class="btn info" disabled>\
															<span>' + data.time + '</span>\
														</button>\
													</div>\
												</div>\
											</div>\
										</div>\
										<button type="submit" class="btn">\
											<span><?php echo addslashes($functions->languageInit('Main_SettingsEmailUnbindSubmit')) ?></span>\
										</button>');
										
										codeEmail(data.time);
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.sendEmail', function() {
						var submit = $(this);
						
						var formData = new FormData();
						formData.append('email', $('input[name="email"]').val());
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/settings/email/<?php echo addslashes($functions->getCsrf()) ?>',
							data: formData,
							beforeSend: function(data) {
								submit.prop('disabled', true);
								
								$('.settingsMainEmail').find('.form.error').removeClass('error');
								$('.addonMainEmail').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												$('.settingsMainEmail').find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													$('.settingsMainEmail').find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonMainEmail">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										
										submit.prop('disabled', false);
										break;
									case 'bind':
										$('.settingsMainEmail').html('<div class="over">\
											<div class="control">\
												<div class="control-addon">\
													<div class="btn media">\
														<i class="zmdi zmdi-email"></i>\
													</div>\
												</div>\
												<div class="control-block">\
													<div class="name">\
														<span><?php echo addslashes($functions->languageInit('Main_SettingsEmailBind')) ?></span>\
													</div>\
												</div>\
											</div>\
										</div>\
										<div class="over formEmail">\
											<div class="above">\
												<label for="email" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_SettingsLabelEmail')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<div class="justified">\
													<input type="email" name="email" value="' + $('input[name="email"]').val() + '" class="form block" id="email" readonly>\
													<button type="button" class="btn default editEmail">\
														<i class="zmdi zmdi-edit"></i>\
													</button>\
												</div>\
											</div>\
										</div>\
										<div class="over formCode">\
											<div class="above">\
												<label for="code" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_SettingsLabelCode')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<div class="control">\
													<div class="control-addon">\
														<div class="btn media">\
															<span><?php echo addslashes($functions->config('code')) ?></span>\
														</div>\
													</div>\
													<div class="control-block">\
														<input type="number" name="code" placeholder="<?php echo addslashes($functions->languageInit('Main_SettingsFormCode')) ?>" class="form block" id="code">\
													</div>\
													<div class="control-addon codeEmail">\
														<button type="button" class="btn info" disabled>\
															<span>' + data.time + '</span>\
														</button>\
													</div>\
												</div>\
											</div>\
										</div>\
										<button type="submit" class="btn">\
											<span><?php echo addslashes($functions->languageInit('Main_SettingsEmailBindSubmit')) ?></span>\
										</button>');
										
										codeEmail(data.time);
										break;
									case 'unbind':
										$('.settingsMainEmail').html('<div class="over">\
											<div class="control">\
												<div class="control-addon">\
													<div class="btn media">\
														<i class="zmdi zmdi-email"></i>\
													</div>\
												</div>\
												<div class="control-block">\
													<div class="name">\
														<span><?php echo addslashes($functions->languageInit('Main_SettingsEmailUnbind')) ?></span>\
													</div>\
												</div>\
											</div>\
										</div>\
										<div class="over formCode">\
											<div class="above">\
												<label for="code" class="name">\
													<span class="font-600"><?php echo addslashes($functions->languageInit('Main_SettingsLabelCode')) ?></span>\
												</label>\
											</div>\
											<div class="above">\
												<div class="control">\
													<div class="control-addon">\
														<div class="btn media">\
															<span><?php echo addslashes($functions->config('code')) ?></span>\
														</div>\
													</div>\
													<div class="control-block">\
														<input type="number" name="code" placeholder="<?php echo addslashes($functions->languageInit('Main_SettingsFormCode')) ?>" class="form block" id="code">\
													</div>\
													<div class="control-addon codeEmail">\
														<button type="button" class="btn info" disabled>\
															<span>' + data.time + '</span>\
														</button>\
													</div>\
												</div>\
											</div>\
										</div>\
										<button type="submit" class="btn">\
											<span><?php echo addslashes($functions->languageInit('Main_SettingsEmailUnbindSubmit')) ?></span>\
										</button>');
										
										codeEmail(data.time);
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.editEmail', function() {
						$('.settingsMainEmail').html('<div class="over">\
							<div class="control">\
								<div class="control-addon">\
									<div class="btn media">\
										<i class="zmdi zmdi-email"></i>\
									</div>\
								</div>\
								<div class="control-block">\
									<div class="name">\
										<span><?php echo addslashes($functions->languageInit('Main_SettingsEmailBind')) ?></span>\
									</div>\
								</div>\
							</div>\
						</div>\
						<div class="over formEmail">\
							<div class="above">\
								<label for="email" class="name">\
									<span class="font-600"><?php echo addslashes($functions->languageInit('Main_SettingsLabelEmail')) ?></span>\
								</label>\
							</div>\
							<div class="above">\
								<div class="justified">\
									<input type="email" name="email" placeholder="<?php echo addslashes($functions->languageInit('Main_SettingsFormEmail')) ?>" class="form block" id="email">\
									<button type="submit" class="btn info">\
										<i class="zmdi zmdi-mail-send"></i>\
									</button>\
								</div>\
							</div>\
						</div>');
					});
					
					var interval_email;
					function codeEmail(time) {
						if(typeof(interval_email) != 'undefined') {
							clearInterval(interval_email);
						}
						
						var date = new Date;
						interval_email = setInterval(function() {
							var interval_time = time - Math.round((new Date - date) / 1000);
							
							if(interval_time > 0) {
								$('.codeEmail').html('<button type="button" class="btn info" disabled>\
									<span>' + interval_time + '</span>\
								</button>');
							} else {
								$('.codeEmail').html('<button type="button" class="btn info sendEmail">\
									<i class="zmdi zmdi-refresh"></i>\
								</button>');
								
								clearInterval(interval_email);
							}
						}, 1000);
					}
					
					$(document).on('click', '.deleteToken', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/tokens/delete/<?php echo addslashes($functions->getCsrf()) ?>/' + submit.data('id'),
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.reload();
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
<?php echo $header ?>
				<div class="over">
					<div class="widget">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_Index') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_IndexDesc') ?></span>
							</div>
						</div>
						<?php if($functions->getTotal('shop_orders', array('order_status' => 1), array(), array('users' => 'shop_orders.user_id=users.user_id'))): ?>
						<div class="over">
							<div class="responsive">
								<table class="table">
									<thead>
										<tr>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersTableId') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersTableUser') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersTableName') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersTableAmount') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersTableDate') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersTableAction') ?></span>
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($functions->get('shop_orders', array('order_status' => 1), array(), array('users' => 'shop_orders.user_id=users.user_id'), array('order_id' => 'DESC'), array('start' => 0, 'limit' => 5)) as $item): ?>
										<tr>
											<td>
												<div class="model">
													<span><?php echo $item['order_id'] ?></span>
												</div>
											</td>
											<td>
												<div class="model">
													<a href="/admin/users/edit/index/<?php echo $item['user_id'] ?>" class="link">
														<span><?php echo $item['user_nickname'] ?></span>
													</a>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $item['order_name'] ?></span>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $functions->currencieInit($item['order_currencie'], $item['order_amount'], true) ?></span>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $functions->datetime(strtotime($item['order_date_add'])) ?></span>
												</div>
											</td>
											<td>
												<button type="button" class="btn default" data-toggle="dialog">
													<span><?php echo $functions->languageInit('CommonAction') ?></span>
													<span>&nbsp;</span>
													<i class="zmdi zmdi-caret-down"></i>
												</button>
												<div class="dropdown fade" data-ride="dialog" data-position="true">
													<a href="/admin/shop/orders/view/index/<?php echo $item['order_id'] ?>" class="menu">
														<div class="model">
															<span><?php echo $functions->languageInit('Admin_ShopOrdersIndexView') ?></span>
														</div>
													</a>
												</div>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
						<a href="/admin/shop/orders" class="btn default block">
							<span><?php echo $functions->languageInit('CommonMore') ?></span>
						</a>
						<?php else: ?>
						<div class="text-center">
							<div class="above">
								<div class="btn media">
									<i class="zmdi zmdi-alert-triangle"></i>
								</div>
							</div>
							<div class="name">
								<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="over">
					<div class="widget">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_IndexUsers') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_IndexUsersDesc') ?></span>
							</div>
						</div>
						<?php if($functions->getTotal('users')): ?>
						<div class="over">
							<div class="responsive">
								<table class="table">
									<thead>
										<tr>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTableId') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTableNickname') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTableBanned') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTableDate') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTableAction') ?></span>
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($functions->get('users', array(), array(), array(), array('user_id' => 'DESC'), array('start' => 0, 'limit' => 5)) as $item): ?>
										<tr>
											<td>
												<div class="model">
													<span><?php echo $item['user_id'] ?></span>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $item['user_nickname'] ?></span>
												</div>
											</td>
											<td>
												<div class="model">
													<?php if($account->isBanned($item['user_id'])): ?>
													<span><?php echo $functions->languageInit('Admin_UsersIndexBanned1') ?></span>
													<?php else: ?>
													<span><?php echo $functions->languageInit('Admin_UsersIndexBanned0') ?></span>
													<?php endif; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $functions->datetime(strtotime($item['user_date_reg'])) ?></span>
												</div>
											</td>
											<td>
												<div class="fill">
													<button type="button" class="btn default addon" data-toggle="dialog">
														<span><?php echo $functions->languageInit('CommonAction') ?></span>
														<span>&nbsp;</span>
														<i class="zmdi zmdi-caret-down"></i>
													</button>
													<div class="dropdown fade" data-ride="dialog" data-position="true">
														<a href="/admin/users/edit/index/<?php echo $item['user_id'] ?>" class="menu">
															<div class="model">
																<span><?php echo $functions->languageInit('Admin_UsersIndexEdit') ?></span>
															</div>
														</a>
														<div class="menu hover" data-target="#user<?php echo $item['user_id'] ?>_login" data-toggle="dialog">
															<div class="model">
																<span><?php echo $functions->languageInit('Admin_UsersIndexLogin') ?></span>
															</div>
														</div>
													</div>
												</div>
												<div class="modal fade" data-ride="dialog" id="user<?php echo $item['user_id'] ?>_login">
													<div class="over">
														<div class="name">
															<span><?php echo $functions->languageInit('Admin_UsersIndexLoginTitle') ?></span>
														</div>
														<div class="name">
															<span class="focus"><?php echo $functions->languageInit('Admin_UsersIndexLoginDesc') ?></span>
														</div>
													</div>
													<div class="text-right">
														<div class="fill">
															<button type="button" class="btn second" data-dismiss="dialog">
																<span><?php echo $functions->languageInit('Admin_UsersIndexLoginClose') ?></span>
															</button>
															<button type="button" class="btn error loginUser" data-id="<?php echo $item['user_id'] ?>">
																<span><?php echo $functions->languageInit('Admin_UsersIndexLoginSubmit') ?></span>
															</button>
														</div>
													</div>
												</div>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
						<a href="/admin/users" class="btn default block">
							<span><?php echo $functions->languageInit('CommonMore') ?></span>
						</a>
						<?php else: ?>
						<div class="text-center">
							<div class="above">
								<div class="btn media">
									<i class="zmdi zmdi-alert-triangle"></i>
								</div>
							</div>
							<div class="name">
								<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<script>
					$(document).on('click', '.loginUser', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/users/index/login/<?php echo addslashes($functions->getCsrf()) ?>/' + submit.data('id'),
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
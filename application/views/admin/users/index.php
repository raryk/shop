<?php echo $header ?>
				<form action="/admin/users" class="over">
					<div class="control">
						<div class="control-block">
							<input type="text" name="q" placeholder="<?php echo $functions->languageInit('CommonSearch') ?>"<?php if(isset($request->get['q']) && !is_array($request->get['q'])): ?> value="<?php echo $request->get['q'] ?>"<?php endif; ?> class="form block">
						</div>
						<div class="control-addon">
							<a href="/admin/users" class="btn second" title="<?php echo $functions->number2format($total) ?>" data-ride="tooltip">
								<span><?php echo $functions->number2string($total) ?></span>
							</a>
						</div>
					</div>
				</form>
				<div class="over">
					<div class="widget">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_UsersIndex') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_UsersIndexDesc') ?></span>
							</div>
						</div>
						<div class="over">
							<?php if(!empty($users)): ?>
							<div class="responsive">
								<table class="table">
									<thead>
										<tr>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTableId') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTableNickname') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTableBanned') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTableDate') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTableAction') ?></span>
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($users as $item): ?>
										<tr>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['user_id']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['user_nickname']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<?php if($account->isBanned($item['user_id'])): ?>
													<span><?php echo $functions->languageInit('Admin_UsersIndexBanned1') ?></span>
													<?php else: ?>
													<span><?php echo $functions->languageInit('Admin_UsersIndexBanned0') ?></span>
													<?php endif; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $functions->datetime(strtotime($item['user_date_reg'])) ?></span>
												</div>
											</td>
											<td>
												<div class="fill">
													<button type="button" class="btn default addon" data-toggle="dialog">
														<span><?php echo $functions->languageInit('CommonAction') ?></span>
														<span>&nbsp;</span>
														<i class="zmdi zmdi-caret-down"></i>
													</button>
													<div class="dropdown fade" data-ride="dialog" data-position="true">
														<a href="/admin/users/edit/index/<?php echo $item['user_id'] ?>" class="menu">
															<div class="model">
																<span><?php echo $functions->languageInit('Admin_UsersIndexEdit') ?></span>
															</div>
														</a>
														<div class="menu hover" data-target="#user<?php echo $item['user_id'] ?>_login" data-toggle="dialog">
															<div class="model">
																<span><?php echo $functions->languageInit('Admin_UsersIndexLogin') ?></span>
															</div>
														</div>
													</div>
												</div>
												<div class="modal fade" data-ride="dialog" id="user<?php echo $item['user_id'] ?>_login">
													<div class="over">
														<div class="name">
															<span><?php echo $functions->languageInit('Admin_UsersIndexLoginTitle') ?></span>
														</div>
														<div class="name">
															<span class="focus"><?php echo $functions->languageInit('Admin_UsersIndexLoginDesc') ?></span>
														</div>
													</div>
													<div class="text-right">
														<div class="fill">
															<button type="button" class="btn second" data-dismiss="dialog">
																<span><?php echo $functions->languageInit('Admin_UsersIndexLoginClose') ?></span>
															</button>
															<button type="button" class="btn error loginUser" data-id="<?php echo $item['user_id'] ?>">
																<span><?php echo $functions->languageInit('Admin_UsersIndexLoginSubmit') ?></span>
															</button>
														</div>
													</div>
												</div>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php else: ?>
							<div class="text-center">
								<div class="above">
									<div class="btn media">
										<i class="zmdi zmdi-alert-triangle"></i>
									</div>
								</div>
								<div class="name">
									<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
								</div>
							</div>
							<?php endif; ?>
						</div>
						<?php if(!empty($pagination)): ?>
						<div class="group">
							<div class="wrap">
								<?php foreach($pagination as $item): ?>
								<div class="wrap-item">
									<a href="<?php echo $item['url'] ?>" class="btn background-color-transparent color-inherit<?php if($item['active']): ?> active<?php endif; ?>">
										<span><?php echo $item['text'] ?></span>
									</a>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<script>
					$(document).on('click', '.loginUser', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/users/index/login/<?php echo addslashes($functions->getCsrf()) ?>/' + submit.data('id'),
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
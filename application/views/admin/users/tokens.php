<?php echo $header ?>
				<form action="/admin/users/tokens" class="over">
					<?php if(isset($request->get['userid']) && !is_array($request->get['userid'])): ?>
					<input type="hidden" name="userid" value="<?php echo $request->get['userid'] ?>">
					<?php endif; ?>
					<div class="control">
						<div class="control-block">
							<input type="text" name="q" placeholder="<?php echo $functions->languageInit('CommonSearch') ?>"<?php if(isset($request->get['q']) && !is_array($request->get['q'])): ?> value="<?php echo $request->get['q'] ?>"<?php endif; ?> class="form block">
						</div>
						<div class="control-addon">
							<a href="/admin/users/tokens" class="btn second" title="<?php echo $functions->number2format($total) ?>" data-ride="tooltip">
								<span><?php echo $functions->number2string($total) ?></span>
							</a>
						</div>
					</div>
				</form>
				<div class="over">
					<div class="widget">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_UsersTokens') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_UsersTokensDesc') ?></span>
							</div>
						</div>
						<div class="over">
							<?php if(!empty($tokens)): ?>
							<div class="responsive">
								<table class="table">
									<thead>
										<tr>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTokensTableId') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTokensTableUser') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTokensTableIp') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTokensTableCode') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTokensTableAgent') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTokensTableDate') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_UsersTokensTableAction') ?></span>
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($tokens as $item): ?>
										<tr>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['token_id']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<a href="/admin/users/edit/index/<?php echo $item['user_id'] ?>" class="link">
														<span><?php echo $item['user_nickname'] ?></span>
													</a>
												</div>
											</td>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['token_ip']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['token_code']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['token_agent']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $functions->datetime(strtotime($item['token_date_add'])) ?></span>
												</div>
											</td>
											<td>
												<div class="fill">
													<button type="button" class="btn default addon" data-toggle="dialog">
														<span><?php echo $functions->languageInit('CommonAction') ?></span>
														<span>&nbsp;</span>
														<i class="zmdi zmdi-caret-down"></i>
													</button>
													<div class="dropdown fade" data-ride="dialog" data-position="true">
														<div class="menu hover" data-target="#users_token<?php echo $item['token_id'] ?>_delete" data-toggle="dialog">
															<div class="model">
																<span><?php echo $functions->languageInit('Admin_UsersTokensDelete') ?></span>
															</div>
														</div>
													</div>
												</div>
												<div class="modal fade" data-ride="dialog" id="users_token<?php echo $item['token_id'] ?>_delete">
													<div class="over">
														<div class="name">
															<span><?php echo $functions->languageInit('Admin_UsersTokensDeleteTitle') ?></span>
														</div>
														<div class="name">
															<span class="focus"><?php echo $functions->languageInit('Admin_UsersTokensDeleteDesc') ?></span>
														</div>
													</div>
													<div class="text-right">
														<div class="fill">
															<button type="button" class="btn second" data-dismiss="dialog">
																<span><?php echo $functions->languageInit('Admin_UsersTokensDeleteClose') ?></span>
															</button>
															<button type="button" class="btn error deleteToken" data-id="<?php echo $item['token_id'] ?>">
																<span><?php echo $functions->languageInit('Admin_UsersTokensDeleteSubmit') ?></span>
															</button>
														</div>
													</div>
												</div>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php else: ?>
							<div class="text-center">
								<div class="above">
									<div class="btn media">
										<i class="zmdi zmdi-alert-triangle"></i>
									</div>
								</div>
								<div class="name">
									<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
								</div>
							</div>
							<?php endif; ?>
						</div>
						<?php if(!empty($pagination)): ?>
						<div class="group">
							<div class="wrap">
								<?php foreach($pagination as $item): ?>
								<div class="wrap-item">
									<a href="<?php echo $item['url'] ?>" class="btn background-color-transparent color-inherit<?php if($item['active']): ?> active<?php endif; ?>">
										<span><?php echo $item['text'] ?></span>
									</a>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<script>
					$(document).on('click', '.deleteToken', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/users/tokens/delete/<?php echo addslashes($functions->getCsrf()) ?>/' + submit.data('id'),
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/users/tokens';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
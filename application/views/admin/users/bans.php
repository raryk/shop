<?php echo $header ?>
				<form action="/admin/users/bans" class="over">
					<?php if(isset($request->get['userid']) && !is_array($request->get['userid'])): ?>
					<input type="hidden" name="userid" value="<?php echo $request->get['userid'] ?>">
					<?php endif; ?>
					<div class="control">
						<div class="control-block">
							<input type="text" name="q" placeholder="<?php echo $functions->languageInit('CommonSearch') ?>"<?php if(isset($request->get['q']) && !is_array($request->get['q'])): ?> value="<?php echo $request->get['q'] ?>"<?php endif; ?> class="form block">
						</div>
						<div class="control-addon">
							<a href="/admin/users/bans" class="btn second" title="<?php echo $functions->number2format($total) ?>" data-ride="tooltip">
								<span><?php echo $functions->number2string($total) ?></span>
							</a>
						</div>
					</div>
				</form>
				<div class="over">
					<div class="widget">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_UsersBans') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_UsersBansDesc') ?></span>
							</div>
						</div>
						<?php if(!empty($bans)): ?>
						<?php foreach($bans as $item): ?>
						<div class="over">
							<div class="control">
								<div class="control-block">
									<div class="over">
										<div class="name">
											<a href="/admin/users/edit/index/<?php echo $item['user_id'] ?>" class="link">
												<span><?php echo $item['user_nickname'] ?></span>
											</a>
										</div>
										<div class="name">
											<?php foreach($functions->string(nl2br($item['ban_text']), true) as $string): ?>
											<?php if(isset($string['link'])): ?>
											<a href="<?php echo $string['link'] ?>" class="link">
												<?php foreach($string['string'] as $string): ?>
												<span class="focus<?php if($string['type'] == 'search'): ?> background-color-yellow color-black<?php endif; ?>"><?php echo $string['text'] ?></span>
												<?php endforeach; ?>
											</a>
											<?php else: ?>
											<span class="focus<?php if($string['type'] == 'search'): ?> background-color-yellow color-black<?php endif; ?>"><?php echo $string['text'] ?></span>
											<?php endif; ?>
											<?php endforeach; ?>
										</div>
									</div>
									<div class="wrap">
										<div class="wrap-item">
											<div class="btn small background-color-transparent color-inherit">
												<?php foreach($functions->string($item['ban_id']) as $string): ?>
												<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
												<?php endforeach; ?>
											</div>
										</div>
										<div class="wrap-item">
											<div class="btn info small background-color-transparent color-inherit">
												<?php if($item['ban_time']): ?>
												<span><?php echo $functions->datetime($item['ban_time']) ?></span>
												<?php else: ?>
												<span><?php echo $functions->languageInit('Admin_UsersBansTime') ?></span>
												<?php endif; ?>
											</div>
										</div>
										<?php if(!$item['ban_time'] || $item['ban_time'] > $functions->time()): ?>
										<div class="wrap-item">
											<div class="btn info small background-color-transparent color-inherit">
												<?php if($item['ban_status']): ?>
												<span><?php echo $functions->languageInit('Admin_UsersBansStatus1') ?></span>
												<?php else: ?>
												<span><?php echo $functions->languageInit('Admin_UsersBansStatus0') ?></span>
												<?php endif; ?>
											</div>
										</div>
										<?php endif; ?>
										<div class="wrap-item">
											<div class="btn small background-color-transparent color-inherit">
												<span><?php echo $functions->datetime(strtotime($item['ban_date_add'])) ?></span>
											</div>
										</div>
									</div>
								</div>
								<div class="control-addon">
									<div class="fill">
										<button type="button" class="btn second addon" data-toggle="dialog">
											<i class="zmdi zmdi-more"></i>
										</button>
										<div class="dropdown fade" data-ride="dialog" data-position="true">
											<div class="menu hover" data-target="#users_ban<?php echo $item['ban_id'] ?>_delete" data-toggle="dialog">
												<div class="model">
													<span><?php echo $functions->languageInit('Admin_UsersBansDelete') ?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="modal fade" data-ride="dialog" id="users_ban<?php echo $item['ban_id'] ?>_delete">
										<div class="over">
											<div class="name">
												<span><?php echo $functions->languageInit('Admin_UsersBansDeleteTitle') ?></span>
											</div>
											<div class="name">
												<span class="focus"><?php echo $functions->languageInit('Admin_UsersBansDeleteDesc') ?></span>
											</div>
										</div>
										<div class="text-right">
											<div class="fill">
												<button type="button" class="btn second" data-dismiss="dialog">
													<span><?php echo $functions->languageInit('Admin_UsersBansDeleteClose') ?></span>
												</button>
												<button type="button" class="btn error deleteBan" data-id="<?php echo $item['ban_id'] ?>">
													<span><?php echo $functions->languageInit('Admin_UsersBansDeleteSubmit') ?></span>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php endforeach; ?>
						<?php else: ?>
						<div class="over">
							<div class="text-center">
								<div class="above">
									<div class="btn media">
										<i class="zmdi zmdi-alert-triangle"></i>
									</div>
								</div>
								<div class="name">
									<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
								</div>
							</div>
						</div>
						<?php endif; ?>
						<?php if(!empty($pagination)): ?>
						<div class="group">
							<div class="wrap">
								<?php foreach($pagination as $item): ?>
								<div class="wrap-item">
									<a href="<?php echo $item['url'] ?>" class="btn background-color-transparent color-inherit<?php if($item['active']): ?> active<?php endif; ?>">
										<span><?php echo $item['text'] ?></span>
									</a>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="modal fade" data-ride="dialog" data-click="false" id="modal_loader">
					<div class="text-center">
						<i class="preloader"></i>
					</div>
				</div>
				<script>
					$(document).on('click', '.deleteBan', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/users/bans/delete/<?php echo addslashes($functions->getCsrf()) ?>/' + submit.data('id'),
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/users/bans';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
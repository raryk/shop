<?php echo $header ?>
				<div class="over">
					<form action="/admin/users/edit/ajax/<?php echo $functions->getCsrf() ?>/<?php echo $user['user_id'] ?>" method="POST" class="widget editUser">
						<div class="over">
							<div class="control">
								<div class="control-block">
									<div class="name">
										<span><?php echo $functions->languageInit('Admin_UsersEdit') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Admin_UsersEditDesc') ?></span>
									</div>
								</div>
								<div class="control-addon">
									<button type="button" class="btn second" data-toggle="dialog">
										<i class="zmdi zmdi-more"></i>
									</button>
									<div class="dropdown fade" data-ride="dialog" data-position="true">
										<div class="menu hover" data-target="#user_login" data-toggle="dialog">
											<div class="model">
												<span><?php echo $functions->languageInit('Admin_UsersIndexLogin') ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over formNickname">
							<div class="above">
								<label for="nickname" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelNickname') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="nickname" placeholder="<?php echo $functions->languageInit('Admin_UsersFormNickname') ?>" value="<?php echo $user['user_nickname'] ?>" class="form block" id="nickname">
							</div>
						</div>
						<div class="over formIp">
							<div class="above">
								<label for="ip" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelIp') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="ip" placeholder="<?php echo $functions->languageInit('Admin_UsersFormIp') ?>" value="<?php echo $user['user_ip'] ?>" class="form block" id="ip">
							</div>
						</div>
						<div class="over">
							<div class="above">
								<label for="deliverie" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelDeliverie') ?></span>
								</label>
							</div>
							<select name="deliverie" class="form block" id="deliverie">
								<option value="0"><?php echo $functions->languageInit('CommonSelect') ?></option>
								<?php foreach($functions->get('shop_deliveries', array('user_id' => $user['user_id'])) as $item): ?>
								<option value="<?php echo $item['deliverie_id'] ?>"<?php if($user['user_deliverie'] == $item['deliverie_id']): ?> selected="selected"<?php endif; ?>><?php echo $item['deliverie_name'] ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="over formLanguage">
							<div class="above">
								<label for="language" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelLanguage') ?></span>
								</label>
							</div>
							<div class="above">
								<select name="language" class="form block" id="language">
									<?php foreach($functions->get('system_languages') as $item): ?>
									<option value="<?php echo $item['language_id'] ?>"<?php if($user['user_language'] == $item['language_id']): ?> selected="selected"<?php endif; ?>><?php echo $item['language_value'] ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="over formTimezone">
							<div class="above">
								<label for="timezone" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelTimezone') ?></span>
								</label>
							</div>
							<div class="above">
								<select name="timezone" class="form block" id="timezone">
									<?php foreach($functions->get('system_timezones') as $item): ?>
									<option value="<?php echo $item['timezone_id'] ?>"<?php if($user['user_timezone'] == $item['timezone_id']): ?> selected="selected"<?php endif; ?>><?php echo isset(json_decode($item['timezone_value'], true)[$functions->language()]) ? json_decode($item['timezone_value'], true)[$functions->language()] : json_decode($item['timezone_value'], true)[$functions->config('language')] ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="over formCurrencie">
							<div class="above">
								<label for="currencie" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelCurrencie') ?></span>
								</label>
							</div>
							<div class="above">
								<select name="currencie" class="form block" id="currencie">
									<?php foreach($functions->get('system_currencies') as $item): ?>
									<option value="<?php echo $item['currencie_id'] ?>"<?php if($user['user_currencie'] == $item['currencie_id']): ?> selected="selected"<?php endif; ?>><?php echo $item['currencie_key'] ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="over">
							<label class="group hover">
								<div class="control">
									<div class="control-block">
										<div class="name">
											<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelAdmin') ?></span>
										</div>
									</div>
									<div class="control-addon">
										<div class="check">
											<input type="checkbox" name="admin" class="custom"<?php if($user['user_admin']): ?> checked<?php endif; ?>>
											<i class="check-switch"></i>
										</div>
									</div>
								</div>
							</label>
						</div>
						<div class="over">
							<div class="above">
								<label for="level" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelLevel') ?></span>
								</label>
							</div>
							<select name="level" class="form block" id="level">
								<option value="0"><?php echo $functions->languageInit('CommonSelect') ?></option>
								<?php foreach($functions->get('system_levels') as $item): ?>
								<option value="<?php echo $item['level_id'] ?>"<?php if($user['user_level'] == $item['level_id']): ?> selected="selected"<?php endif; ?>><?php echo $item['level_name'] ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="over formEmail">
							<div class="above">
								<label for="email" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelEmail') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="email" name="email" placeholder="<?php echo $functions->languageInit('Admin_UsersFormEmail') ?>" value="<?php echo $user['user_email'] ?>" class="form block" id="email">
							</div>
						</div>
						<div class="over">
							<label class="group hover">
								<div class="control">
									<div class="control-block">
										<div class="name">
											<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelTwa') ?></span>
										</div>
									</div>
									<div class="control-addon">
										<div class="check">
											<input type="checkbox" name="twa" class="custom"<?php if($user['user_twa']): ?> checked<?php endif; ?>>
											<i class="check-switch"></i>
										</div>
									</div>
								</div>
							</label>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_UsersEditSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="over">
					<form action="/admin/users/edit/password/<?php echo $functions->getCsrf() ?>/<?php echo $user['user_id'] ?>" method="POST" class="widget editUser">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_UsersEditPassword') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_UsersEditPasswordDesc') ?></span>
							</div>
						</div>
						<div class="over formPassword">
							<div class="above">
								<label for="password" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelPassword') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="password" name="password" placeholder="<?php echo $functions->languageInit('Admin_UsersFormPassword') ?>" class="form block" id="password">
							</div>
						</div>
						<div class="over formPassword2">
							<div class="above">
								<label for="password2" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelPassword2') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="password" name="password2" placeholder="<?php echo $functions->languageInit('Admin_UsersFormPassword2') ?>" class="form block" id="password2">
							</div>
						</div>
						<div class="over">
							<label class="group hover">
								<div class="control">
									<div class="control-block">
										<div class="name">
											<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelTokens') ?></span>
										</div>
									</div>
									<div class="control-addon">
										<div class="check">
											<input type="checkbox" name="tokens" class="custom">
											<i class="check-checkbox"></i>
										</div>
									</div>
								</div>
							</label>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_UsersEditPasswordSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="over">
					<form action="/admin/users/edit/ban/<?php echo $functions->getCsrf() ?>/<?php echo $user['user_id'] ?>" method="POST" class="widget editUser">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_UsersEditBan') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_UsersEditBanDesc') ?></span>
							</div>
						</div>
						<div class="over formTime">
							<div class="above">
								<label for="time" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelTime') ?></span>
								</label>
							</div>
							<div class="above">
								<div class="control">
									<div class="control-block">
										<select name="time[day]" class="form block" id="time">
											<option value="0"><?php echo $functions->languageInit('SystemDateDay') ?></option>
											<option value="01">1</option>
											<option value="02">2</option>
											<option value="03">3</option>
											<option value="04">4</option>
											<option value="05">5</option>
											<option value="06">6</option>
											<option value="07">7</option>
											<option value="08">8</option>
											<option value="09">9</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
											<option value="24">24</option>
											<option value="25">25</option>
											<option value="26">26</option>
											<option value="27">27</option>
											<option value="28">28</option>
											<option value="29">29</option>
											<option value="30">30</option>
											<option value="31">31</option>
										</select>
									</div>
									<div class="control-block">
										<select name="time[month]" class="form block" id="time">
											<option value="0"><?php echo $functions->languageInit('SystemDateMonth') ?></option>
											<option value="01"><?php echo $functions->languageInit('SystemDateMonth1') ?></option>
											<option value="02"><?php echo $functions->languageInit('SystemDateMonth2') ?></option>
											<option value="03"><?php echo $functions->languageInit('SystemDateMonth3') ?></option>
											<option value="04"><?php echo $functions->languageInit('SystemDateMonth4') ?></option>
											<option value="05"><?php echo $functions->languageInit('SystemDateMonth5') ?></option>
											<option value="06"><?php echo $functions->languageInit('SystemDateMonth6') ?></option>
											<option value="07"><?php echo $functions->languageInit('SystemDateMonth7') ?></option>
											<option value="08"><?php echo $functions->languageInit('SystemDateMonth8') ?></option>
											<option value="09"><?php echo $functions->languageInit('SystemDateMonth9') ?></option>
											<option value="10"><?php echo $functions->languageInit('SystemDateMonth10') ?></option>
											<option value="11"><?php echo $functions->languageInit('SystemDateMonth11') ?></option>
											<option value="12"><?php echo $functions->languageInit('SystemDateMonth12') ?></option>
										</select>
									</div>
									<div class="control-block">
										<select name="time[year]" class="form block" id="time">
											<option value="0"><?php echo $functions->languageInit('SystemDateYear') ?></option>
											<?php for($i = $this->registry->functions->config('date_min'); $i <= $this->registry->functions->config('date_max'); $i++): ?>
											<option value="<?php echo $i ?>"><?php echo $i ?></option>
											<?php endfor; ?>
										</select>
									</div>
								</div>
							</div>
							<?php if($functions->isHour12()): ?>
							<div class="above">
								<div class="control">
									<div class="control-block">
										<select name="time[hour]" class="form block" id="time">
											<option value="0"><?php echo $functions->languageInit('SystemDateHour') ?></option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8</option>
											<option value="9">9</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
										</select>
									</div>
									<div class="control-block">
										<select name="time[minute]" class="form block" id="time">
											<option value="0"><?php echo $functions->languageInit('SystemDateMinute') ?></option>
											<option value="00">00</option>
											<option value="01">01</option>
											<option value="02">02</option>
											<option value="03">03</option>
											<option value="04">04</option>
											<option value="05">05</option>
											<option value="06">06</option>
											<option value="07">07</option>
											<option value="08">08</option>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
											<option value="24">24</option>
											<option value="25">25</option>
											<option value="26">26</option>
											<option value="27">27</option>
											<option value="28">28</option>
											<option value="29">29</option>
											<option value="30">30</option>
											<option value="31">31</option>
											<option value="32">32</option>
											<option value="33">33</option>
											<option value="34">34</option>
											<option value="35">35</option>
											<option value="36">36</option>
											<option value="37">37</option>
											<option value="38">38</option>
											<option value="39">39</option>
											<option value="40">40</option>
											<option value="41">41</option>
											<option value="42">42</option>
											<option value="43">43</option>
											<option value="44">44</option>
											<option value="45">45</option>
											<option value="46">46</option>
											<option value="47">47</option>
											<option value="48">48</option>
											<option value="49">49</option>
											<option value="50">50</option>
											<option value="51">51</option>
											<option value="52">52</option>
											<option value="53">53</option>
											<option value="54">54</option>
											<option value="55">55</option>
											<option value="56">56</option>
											<option value="57">57</option>
											<option value="58">58</option>
											<option value="59">59</option>
										</select>
									</div>
									<div class="control-block">
										<select name="time[second]" class="form block" id="time">
											<option value="0"><?php echo $functions->languageInit('SystemDateSecond') ?></option>
											<option value="00">00</option>
											<option value="01">01</option>
											<option value="02">02</option>
											<option value="03">03</option>
											<option value="04">04</option>
											<option value="05">05</option>
											<option value="06">06</option>
											<option value="07">07</option>
											<option value="08">08</option>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
											<option value="24">24</option>
											<option value="25">25</option>
											<option value="26">26</option>
											<option value="27">27</option>
											<option value="28">28</option>
											<option value="29">29</option>
											<option value="30">30</option>
											<option value="31">31</option>
											<option value="32">32</option>
											<option value="33">33</option>
											<option value="34">34</option>
											<option value="35">35</option>
											<option value="36">36</option>
											<option value="37">37</option>
											<option value="38">38</option>
											<option value="39">39</option>
											<option value="40">40</option>
											<option value="41">41</option>
											<option value="42">42</option>
											<option value="43">43</option>
											<option value="44">44</option>
											<option value="45">45</option>
											<option value="46">46</option>
											<option value="47">47</option>
											<option value="48">48</option>
											<option value="49">49</option>
											<option value="50">50</option>
											<option value="51">51</option>
											<option value="52">52</option>
											<option value="53">53</option>
											<option value="54">54</option>
											<option value="55">55</option>
											<option value="56">56</option>
											<option value="57">57</option>
											<option value="58">58</option>
											<option value="59">59</option>
										</select>
									</div>
									<div class="control-block">
										<select name="time[meridiem]" class="form block" id="time">
											<option value="0">AM</option>
											<option value="1">PM</option>
										</select>
									</div>
								</div>
							</div>
							<?php else: ?>
							<div class="above">
								<div class="control">
									<div class="control-block">
										<select name="time[hour]" class="form block" id="time">
											<option value="0"><?php echo $functions->languageInit('SystemDateHour') ?></option>
											<option value="00">00</option>
											<option value="01">01</option>
											<option value="02">02</option>
											<option value="03">03</option>
											<option value="04">04</option>
											<option value="05">05</option>
											<option value="06">06</option>
											<option value="07">07</option>
											<option value="08">08</option>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
											<option value="24">24</option>
											<option value="25">25</option>
											<option value="26">26</option>
											<option value="27">27</option>
											<option value="28">28</option>
											<option value="29">29</option>
											<option value="30">30</option>
											<option value="31">31</option>
											<option value="32">32</option>
											<option value="33">33</option>
											<option value="34">34</option>
											<option value="35">35</option>
											<option value="36">36</option>
											<option value="37">37</option>
											<option value="38">38</option>
											<option value="39">39</option>
											<option value="40">40</option>
											<option value="41">41</option>
											<option value="42">42</option>
											<option value="43">43</option>
											<option value="44">44</option>
											<option value="45">45</option>
											<option value="46">46</option>
											<option value="47">47</option>
											<option value="48">48</option>
											<option value="49">49</option>
											<option value="50">50</option>
											<option value="51">51</option>
											<option value="52">52</option>
											<option value="53">53</option>
											<option value="54">54</option>
											<option value="55">55</option>
											<option value="56">56</option>
											<option value="57">57</option>
											<option value="58">58</option>
											<option value="59">59</option>
										</select>
									</div>
									<div class="control-block">
										<select name="time[minute]" class="form block" id="time">
											<option value="0"><?php echo $functions->languageInit('SystemDateMinute') ?></option>
											<option value="00">00</option>
											<option value="01">01</option>
											<option value="02">02</option>
											<option value="03">03</option>
											<option value="04">04</option>
											<option value="05">05</option>
											<option value="06">06</option>
											<option value="07">07</option>
											<option value="08">08</option>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
											<option value="24">24</option>
											<option value="25">25</option>
											<option value="26">26</option>
											<option value="27">27</option>
											<option value="28">28</option>
											<option value="29">29</option>
											<option value="30">30</option>
											<option value="31">31</option>
											<option value="32">32</option>
											<option value="33">33</option>
											<option value="34">34</option>
											<option value="35">35</option>
											<option value="36">36</option>
											<option value="37">37</option>
											<option value="38">38</option>
											<option value="39">39</option>
											<option value="40">40</option>
											<option value="41">41</option>
											<option value="42">42</option>
											<option value="43">43</option>
											<option value="44">44</option>
											<option value="45">45</option>
											<option value="46">46</option>
											<option value="47">47</option>
											<option value="48">48</option>
											<option value="49">49</option>
											<option value="50">50</option>
											<option value="51">51</option>
											<option value="52">52</option>
											<option value="53">53</option>
											<option value="54">54</option>
											<option value="55">55</option>
											<option value="56">56</option>
											<option value="57">57</option>
											<option value="58">58</option>
											<option value="59">59</option>
										</select>
									</div>
									<div class="control-block">
										<select name="time[second]" class="form block" id="time">
											<option value="0"><?php echo $functions->languageInit('SystemDateSecond') ?></option>
											<option value="00">00</option>
											<option value="01">01</option>
											<option value="02">02</option>
											<option value="03">03</option>
											<option value="04">04</option>
											<option value="05">05</option>
											<option value="06">06</option>
											<option value="07">07</option>
											<option value="08">08</option>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
											<option value="24">24</option>
											<option value="25">25</option>
											<option value="26">26</option>
											<option value="27">27</option>
											<option value="28">28</option>
											<option value="29">29</option>
											<option value="30">30</option>
											<option value="31">31</option>
											<option value="32">32</option>
											<option value="33">33</option>
											<option value="34">34</option>
											<option value="35">35</option>
											<option value="36">36</option>
											<option value="37">37</option>
											<option value="38">38</option>
											<option value="39">39</option>
											<option value="40">40</option>
											<option value="41">41</option>
											<option value="42">42</option>
											<option value="43">43</option>
											<option value="44">44</option>
											<option value="45">45</option>
											<option value="46">46</option>
											<option value="47">47</option>
											<option value="48">48</option>
											<option value="49">49</option>
											<option value="50">50</option>
											<option value="51">51</option>
											<option value="52">52</option>
											<option value="53">53</option>
											<option value="54">54</option>
											<option value="55">55</option>
											<option value="56">56</option>
											<option value="57">57</option>
											<option value="58">58</option>
											<option value="59">59</option>
										</select>
									</div>
								</div>
							</div>
							<?php endif; ?>
						</div>
						<div class="over formText">
							<div class="above">
								<label for="text" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UsersLabelText') ?></span>
								</label>
							</div>
							<div class="above">
								<textarea rows="5" cols="1" name="text" placeholder="<?php echo $functions->languageInit('Admin_UsersFormText') ?>" class="form block" id="text"></textarea>
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_UsersEditBanSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="over">
					<button type="button" class="btn warning block" data-toggle="dialog">
						<span><?php echo $functions->languageInit('Admin_UsersEditUnban') ?></span>
					</button>
					<div class="modal fade" data-ride="dialog">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_UsersEditUnbanTitle') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_UsersEditUnbanDesc') ?></span>
							</div>
						</div>
						<div class="text-right">
							<div class="fill">
								<button type="button" class="btn second" data-dismiss="dialog">
									<span><?php echo $functions->languageInit('Admin_UsersEditUnbanClose') ?></span>
								</button>
								<button type="button" class="btn warning unbanUser">
									<span><?php echo $functions->languageInit('Admin_UsersEditUnbanSubmit') ?></span>
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" data-ride="dialog" id="user_login">
					<div class="over">
						<div class="name">
							<span><?php echo $functions->languageInit('Admin_UsersIndexLoginTitle') ?></span>
						</div>
						<div class="name">
							<span class="focus"><?php echo $functions->languageInit('Admin_UsersIndexLoginDesc') ?></span>
						</div>
					</div>
					<div class="text-right">
						<div class="fill">
							<button type="button" class="btn second" data-dismiss="dialog">
								<span><?php echo $functions->languageInit('Admin_UsersIndexLoginClose') ?></span>
							</button>
							<button type="button" class="btn error loginUser">
								<span><?php echo $functions->languageInit('Admin_UsersIndexLoginSubmit') ?></span>
							</button>
						</div>
					</div>
				</div>
				<script>
					$(document).on('submit', '.editUser', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonUser').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonUser">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										break;
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.unbanUser', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/users/edit/unban/<?php echo addslashes($functions->getCsrf()) ?>/<?php echo addslashes($user['user_id']) ?>',
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										
										submit.closest('[data-ride="dialog"]').dialog('hide');
										break;
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.loginUser', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/users/index/login/<?php echo addslashes($functions->getCsrf()) ?>/<?php echo addslashes($user['user_id']) ?>',
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
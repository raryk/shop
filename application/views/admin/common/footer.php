			</section>
			<footer class="footer">
				<div class="text-center">
					<div class="name">
						<a href="/about" class="link">
							<span class="font-600"><?php echo $functions->config('copyright') ?></span>
						</a>
					</div>
				</div>
			</footer>
		</div>
		<noscript>
			<div style="background-color:#fff;color:#000;position:fixed;width:100%;height:100%;top:0;left:0;z-index:9999;">
				<div style="margin:10%;width:80%;">
					<img src="<?php echo $functions->config('assets') ?>/png/logo.png" alt="logo" style="margin-bottom:20px;max-width:20%;">
					<div style="font-size:18px;word-break:break-word;"><?php echo $functions->languageInit('CommonNoscript') ?></div>
				</div>
			</div>
		</noscript>
	</body>
</html>
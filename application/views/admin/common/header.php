<!DOCTYPE html>
<html dir="<?php if($functions->isRtl()): ?>rtl<?php else: ?>ltr<?php endif; ?>" lang="<?php echo $functions->languageBy($functions->language()) ?>" prefix="og: https://ogp.me/ns#">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="description" content="<?php echo $description ?>">
		<meta name="keywords" content="<?php echo $functions->languageInit('CommonKeywords') ?>">
		<meta name="theme-color" content="<?php echo $functions->config('color') ?>">
		
		<meta property="og:title" content="<?php echo $title ?>">
		<meta property="og:type" content="website">
		<meta property="og:image" content="<?php echo $functions->config('assets') ?>/png/emblem.png">
		<meta property="og:url" content="<?php echo $url ?>">
		<meta property="og:description" content="<?php echo $description ?>">
		<meta property="og:site_name" content="<?php echo $functions->config('title') ?>">
		
		<title><?php echo $title ?></title>
		
		<link rel="canonical" href="<?php echo $canonical ?>">
		
		<link rel="alternate" href="<?php echo $url ?>" hreflang="x-default">
		<?php foreach($alternate as $key => $value): ?>
		<link rel="alternate" href="<?php echo $key ?>" hreflang="<?php echo $value ?>">
		<?php endforeach; ?>
		
		<link rel="shortcut icon" href="<?php echo $functions->config('assets') ?>/ico/favicon.ico">
		
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/vendor.css">
		<link rel="stylesheet" href="<?php echo $functions->config('assets') ?>/css/main.css">
		
		<script src="<?php echo $functions->config('assets') ?>/js/vendor.js"></script>
		<script src="<?php echo $functions->config('assets') ?>/js/main.js"></script>
	</head>
	<body>
		<div class="wrapper">
			<header class="header">
				<div class="control">
					<div class="control-addon">
						<button type="button" class="btn second" data-target="#sidebar" data-toggle="dialog">
							<i class="zmdi zmdi-menu"></i>
						</button>
					</div>
					<div class="control-block">
						<a href="/" class="logo">
							<img src="<?php echo $functions->config('assets') ?>/png/logo.png" alt="logo">
						</a>
					</div>
					<div class="control-addon">
						<a href="/cart" class="btn second">
							<i class="zmdi zmdi-shopping-cart"></i>
							<span>&nbsp;</span>
							<span class="headerTotal"><?php echo $functions->currencieInit($functions->currencie(), array_sum(array_values($functions->cart(2)))) ?></span>
						</a>
					</div>
					<?php if($account->isLogged()): ?>
					<div class="control-addon">
						<button type="button" class="btn second" data-toggle="dialog">
							<i class="zmdi zmdi-more"></i>
						</button>
						<div class="dropdown fade" data-ride="dialog" data-position="true">
							<a href="/orders" class="menu">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-shopping-basket"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonOrders') ?></span>
										</div>
									</div>
								</div>
							</a>
							<a href="/deliveries" class="menu">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-map"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonDeliveries') ?></span>
										</div>
									</div>
								</div>
							</a>
							<a href="/settings" class="menu">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-settings"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonSettings') ?></span>
										</div>
									</div>
								</div>
							</a>
							<i class="divider"></i>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonLanguage') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_languages') as $item): ?>
									<a href="/index/language/<?php echo $item['language_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $item['language_value'] ?></span>
												</div>
											</div>
											<?php if($item['language_id'] == $functions->language()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonCurrencie') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_currencies') as $item): ?>
									<a href="/index/currencie/<?php echo $item['currencie_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $item['currencie_key'] ?></span>
												</div>
											</div>
											<?php if($item['currencie_id'] == $functions->currencie()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonTimezone') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_timezones') as $item): ?>
									<a href="/index/timezone/<?php echo $item['timezone_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo isset(json_decode($item['timezone_value'], true)[$functions->language()]) ? json_decode($item['timezone_value'], true)[$functions->language()] : json_decode($item['timezone_value'], true)[$functions->config('language')] ?></span>
												</div>
											</div>
											<?php if($item['timezone_id'] == $functions->timezone()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<i class="divider"></i>
							<?php foreach($account->accounts() as $key => $value): ?>
							<a href="/index/account/<?php echo $key ?>" class="menu">
								<div class="model">
									<span><?php echo $value['user_nickname'] ?></span>
								</div>
							</a>
							<?php endforeach; ?>
							<a href="/login" class="menu">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-plus"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonPlus') ?></span>
										</div>
									</div>
								</div>
							</a>
							<i class="divider"></i>
							<a href="/logout" class="menu">
								<div class="control">
									<div class="control-addon">
										<div class="btn media">
											<i class="zmdi zmdi-power"></i>
										</div>
									</div>
									<div class="control-block">
										<div class="model">
											<span><?php echo $functions->languageInit('CommonLogout') ?></span>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<?php else: ?>
					<div class="control-addon">
						<button type="button" class="btn second" data-toggle="dialog">
							<i class="zmdi zmdi-more"></i>
						</button>
						<div class="dropdown fade" data-ride="dialog" data-position="true">
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonLanguage') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_languages') as $item): ?>
									<a href="/index/language/<?php echo $item['language_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $item['language_value'] ?></span>
												</div>
											</div>
											<?php if($item['language_id'] == $functions->language()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonCurrencie') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_currencies') as $item): ?>
									<a href="/index/currencie/<?php echo $item['currencie_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $item['currencie_key'] ?></span>
												</div>
											</div>
											<?php if($item['currencie_id'] == $functions->currencie()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="navigate" data-navigation="parent">
								<div class="menu hover" data-action="navigation">
									<div class="control">
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonTimezone') ?></span>
											</div>
										</div>
										<div class="control-addon">
											<i class="toggle" data-navigation="toggle"></i>
										</div>
									</div>
								</div>
								<div class="navigation" data-navigation="slide">
									<?php foreach($functions->get('system_timezones') as $item): ?>
									<a href="/index/timezone/<?php echo $item['timezone_id'] ?>" class="menu">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo isset(json_decode($item['timezone_value'], true)[$functions->language()]) ? json_decode($item['timezone_value'], true)[$functions->language()] : json_decode($item['timezone_value'], true)[$functions->config('language')] ?></span>
												</div>
											</div>
											<?php if($item['timezone_id'] == $functions->timezone()): ?>
											<div class="control-addon">
												<div class="btn media small">
													<i class="zmdi zmdi-check"></i>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</a>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="control-addon">
						<a href="/login" class="btn">
							<span><?php echo $functions->languageInit('CommonLogin') ?></span>
						</a>
					</div>
					<?php endif; ?>
				</div>
			</header>
			<aside class="sidebar fade" id="sidebar" data-ride="dialog">
				<a href="/admin" class="menu<?php if(isset($section['element']) && $section['element'] == 'index'): ?> active<?php endif; ?>">
					<div class="control">
						<div class="control-addon">
							<div class="btn media">
								<i class="zmdi zmdi-input-antenna"></i>
							</div>
						</div>
						<div class="control-block">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonIndex') ?></span>
							</div>
						</div>
					</div>
				</a>
				<?php if($account->isLogged() && $account->isLevel('shop')): ?>
				<div class="navigate" data-navigation="parent">
					<div class="menu hover<?php if(isset($section['element']) && $section['element'] == 'shop'): ?> active<?php endif; ?>" data-action="navigation">
						<div class="control">
							<div class="control-addon">
								<div class="btn media">
									<i class="zmdi zmdi-store"></i>
								</div>
							</div>
							<div class="control-block">
								<div class="model">
									<span><?php echo $functions->languageInit('Admin_CommonShop') ?></span>
								</div>
							</div>
							<div class="control-addon">
								<div class="btn media">
									<i class="toggle<?php if(isset($section['element']) && $section['element'] == 'shop'): ?> active<?php endif; ?>" data-navigation="toggle"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="navigation<?php if(isset($section['element']) && $section['element'] == 'shop'): ?> active<?php endif; ?>" data-navigation="slide">
						<a href="/admin/shop/categories" class="menu<?php if(isset($section['element_shop']) && $section['element_shop'] == 'categories'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonShopCategories') ?></span>
							</div>
						</a>
						<a href="/admin/shop/products" class="menu<?php if(isset($section['element_shop']) && $section['element_shop'] == 'products'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonShopProducts') ?></span>
							</div>
						</a>
						<a href="/admin/shop/orders" class="menu<?php if(isset($section['element_shop']) && $section['element_shop'] == 'orders'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonShopOrders') ?></span>
							</div>
						</a>
						<a href="/admin/shop/deliveries" class="menu<?php if(isset($section['element_shop']) && $section['element_shop'] == 'deliveries'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonShopDeliveries') ?></span>
							</div>
						</a>
						<a href="/admin/shop/coupons" class="menu<?php if(isset($section['element_shop']) && $section['element_shop'] == 'coupons'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonShopCoupons') ?></span>
							</div>
						</a>
					</div>
				</div>
				<?php endif; ?>
				<?php if($account->isLogged() && $account->isLevel('guard')): ?>
				<div class="navigate" data-navigation="parent">
					<div class="menu hover<?php if(isset($section['element']) && $section['element'] == 'guard'): ?> active<?php endif; ?>" data-action="navigation">
						<div class="control">
							<div class="control-addon">
								<div class="btn media">
									<i class="zmdi zmdi-file-text"></i>
								</div>
							</div>
							<div class="control-block">
								<div class="model">
									<span><?php echo $functions->languageInit('Admin_CommonGuard') ?></span>
								</div>
							</div>
							<div class="control-addon">
								<div class="btn media">
									<i class="toggle<?php if(isset($section['element']) && $section['element'] == 'guard'): ?> active<?php endif; ?>" data-navigation="toggle"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="navigation<?php if(isset($section['element']) && $section['element'] == 'guard'): ?> active<?php endif; ?>" data-navigation="slide">
						<a href="/admin/guard/events" class="menu<?php if(isset($section['element_guard']) && $section['element_guard'] == 'events'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonGuardEvents') ?></span>
							</div>
						</a>
						<a href="/admin/guard/logs" class="menu<?php if(isset($section['element_guard']) && $section['element_guard'] == 'logs'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonGuardLogs') ?></span>
							</div>
						</a>
					</div>
				</div>
				<?php endif; ?>
				<?php if($account->isLogged() && $account->isLevel('upload')): ?>
				<div class="navigate" data-navigation="parent">
					<div class="menu hover<?php if(isset($section['element']) && $section['element'] == 'upload'): ?> active<?php endif; ?>" data-action="navigation">
						<div class="control">
							<div class="control-addon">
								<div class="btn media">
									<i class="zmdi zmdi-upload"></i>
								</div>
							</div>
							<div class="control-block">
								<div class="model">
									<span><?php echo $functions->languageInit('Admin_CommonUpload') ?></span>
								</div>
							</div>
							<div class="control-addon">
								<div class="btn media">
									<i class="toggle<?php if(isset($section['element']) && $section['element'] == 'upload'): ?> active<?php endif; ?>" data-navigation="toggle"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="navigation<?php if(isset($section['element']) && $section['element'] == 'upload'): ?> active<?php endif; ?>" data-navigation="slide">
						<a href="/admin/upload/images" class="menu<?php if(isset($section['element_upload']) && $section['element_upload'] == 'images'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonUploadImages') ?></span>
							</div>
						</a>
						<a href="/admin/upload/servers" class="menu<?php if(isset($section['element_upload']) && $section['element_upload'] == 'servers'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonUploadServers') ?></span>
							</div>
						</a>
					</div>
				</div>
				<?php endif; ?>
				<?php if($account->isLogged() && $account->isLevel('users')): ?>
				<div class="navigate" data-navigation="parent">
					<div class="menu hover<?php if(isset($section['element']) && $section['element'] == 'users'): ?> active<?php endif; ?>" data-action="navigation">
						<div class="control">
							<div class="control-addon">
								<div class="btn media">
									<i class="zmdi zmdi-accounts-list"></i>
								</div>
							</div>
							<div class="control-block">
								<div class="model">
									<span><?php echo $functions->languageInit('Admin_CommonUsers') ?></span>
								</div>
							</div>
							<div class="control-addon">
								<div class="btn media">
									<i class="toggle<?php if(isset($section['element']) && $section['element'] == 'users'): ?> active<?php endif; ?>" data-navigation="toggle"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="navigation<?php if(isset($section['element']) && $section['element'] == 'users'): ?> active<?php endif; ?>" data-navigation="slide">
						<a href="/admin/users" class="menu<?php if(isset($section['element_users']) && $section['element_users'] == 'index'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonUsersIndex') ?></span>
							</div>
						</a>
						<a href="/admin/users/tokens" class="menu<?php if(isset($section['element_users']) && $section['element_users'] == 'tokens'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonUsersTokens') ?></span>
							</div>
						</a>
						<a href="/admin/users/bans" class="menu<?php if(isset($section['element_users']) && $section['element_users'] == 'bans'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonUsersBans') ?></span>
							</div>
						</a>
					</div>
				</div>
				<?php endif; ?>
				<?php if($account->isLogged() && $account->isLevel('code')): ?>
				<div class="navigate" data-navigation="parent">
					<div class="menu hover<?php if(isset($section['element']) && $section['element'] == 'code'): ?> active<?php endif; ?>" data-action="navigation">
						<div class="control">
							<div class="control-addon">
								<div class="btn media">
									<i class="zmdi zmdi-key"></i>
								</div>
							</div>
							<div class="control-block">
								<div class="model">
									<span><?php echo $functions->languageInit('Admin_CommonCode') ?></span>
								</div>
							</div>
							<div class="control-addon">
								<div class="btn media">
									<i class="toggle<?php if(isset($section['element']) && $section['element'] == 'code'): ?> active<?php endif; ?>" data-navigation="toggle"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="navigation<?php if(isset($section['element']) && $section['element'] == 'code'): ?> active<?php endif; ?>" data-navigation="slide">
						<a href="/admin/code/confirms" class="menu<?php if(isset($section['element_code']) && $section['element_code'] == 'confirms'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonCodeConfirms') ?></span>
							</div>
						</a>
						<a href="/admin/code/limits" class="menu<?php if(isset($section['element_code']) && $section['element_code'] == 'limits'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonCodeLimits') ?></span>
							</div>
						</a>
					</div>
				</div>
				<?php endif; ?>
				<?php if($account->isLogged() && $account->isLevel('system')): ?>
				<div class="navigate" data-navigation="parent">
					<div class="menu hover<?php if(isset($section['element']) && $section['element'] == 'system'): ?> active<?php endif; ?>" data-action="navigation">
						<div class="control">
							<div class="control-addon">
								<div class="btn media">
									<i class="zmdi zmdi-code-setting"></i>
								</div>
							</div>
							<div class="control-block">
								<div class="model">
									<span><?php echo $functions->languageInit('Admin_CommonSystem') ?></span>
								</div>
							</div>
							<div class="control-addon">
								<div class="btn media">
									<i class="toggle<?php if(isset($section['element']) && $section['element'] == 'system'): ?> active<?php endif; ?>" data-navigation="toggle"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="navigation<?php if(isset($section['element']) && $section['element'] == 'system'): ?> active<?php endif; ?>" data-navigation="slide">
						<a href="/admin/system/pages" class="menu<?php if(isset($section['element_system']) && $section['element_system'] == 'pages'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonSystemPages') ?></span>
							</div>
						</a>
						<a href="/admin/system/configs" class="menu<?php if(isset($section['element_system']) && $section['element_system'] == 'configs'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonSystemConfigs') ?></span>
							</div>
						</a>
						<a href="/admin/system/levels" class="menu<?php if(isset($section['element_system']) && $section['element_system'] == 'levels'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonSystemLevels') ?></span>
							</div>
						</a>
						<a href="/admin/system/languages" class="menu<?php if(isset($section['element_system']) && $section['element_system'] == 'languages'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonSystemLanguages') ?></span>
							</div>
						</a>
						<a href="/admin/system/translates" class="menu<?php if(isset($section['element_system']) && $section['element_system'] == 'translates'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonSystemTranslates') ?></span>
							</div>
						</a>
						<a href="/admin/system/timezones" class="menu<?php if(isset($section['element_system']) && $section['element_system'] == 'timezones'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonSystemTimezones') ?></span>
							</div>
						</a>
						<a href="/admin/system/currencies" class="menu<?php if(isset($section['element_system']) && $section['element_system'] == 'currencies'): ?> active<?php endif; ?>">
							<div class="model">
								<span><?php echo $functions->languageInit('Admin_CommonSystemCurrencies') ?></span>
							</div>
						</a>
					</div>
				</div>
				<?php endif; ?>
			</aside>
			<section class="content">
				<?php if(isset($alert)): ?>
				<div class="over">
					<?php if(is_array($alert)): ?>
					<div class="alert <?php echo $alert['key'] ?>">
						<div class="name">
							<span><?php echo $alert['value'] ?></span>
						</div>
					</div>
					<?php else: ?>
					<div class="alert">
						<div class="name">
							<span><?php echo $alert ?></span>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<?php endif; ?>
				<?php if($account->isLogged() && mb_strlen(trim($account->getEmail())) < 1): ?>
				<div class="over">
					<a href="/settings" class="alert">
						<div class="control">
							<div class="control-addon">
								<div class="btn media">
									<i class="zmdi zmdi-email"></i>
								</div>
							</div>
							<div class="control-block">
								<div class="name">
									<span><?php echo $functions->languageInit('CommonEmail') ?></span>
								</div>
							</div>
						</div>
					</a>
				</div>
				<?php endif; ?>
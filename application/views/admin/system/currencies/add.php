<?php echo $header ?>
				<div class="over">
					<form action="/admin/system/currencies/add/ajax/<?php echo $functions->getCsrf() ?>" method="POST" class="widget addCurrencie">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_SystemCurrenciesAdd') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_SystemCurrenciesAddDesc') ?></span>
							</div>
						</div>
						<div class="over formName">
							<div class="above">
								<label for="name" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemCurrenciesLabelName') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="name" placeholder="<?php echo $functions->languageInit('Admin_SystemCurrenciesFormName') ?>" class="form block" id="name">
							</div>
						</div>
						<div class="over formKey">
							<div class="above">
								<label for="key" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemCurrenciesLabelKey') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="key" placeholder="<?php echo $functions->languageInit('Admin_SystemCurrenciesFormKey') ?>" class="form block" id="key">
							</div>
						</div>
						<div class="over formValue">
							<div class="above">
								<label for="value" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemCurrenciesLabelValue') ?></span>
								</label>
							</div>
							<div class="above">
								<div class="control">
									<div class="control-block">
										<input type="text" name="value" placeholder="<?php echo $functions->languageInit('Admin_SystemCurrenciesFormValue') ?>" class="form block" id="value">
									</div>
									<div class="control-addon">
										<div class="btn media">
											<span><?php echo $functions->currencieBy() ?></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_SystemCurrenciesAddSubmit') ?></span>
						</button>
					</form>
				</div>
				<script>
					$(document).on('submit', '.addCurrencie', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonCurrencie').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonCurrencie">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										
										form.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/system/currencies';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
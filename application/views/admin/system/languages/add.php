<?php echo $header ?>
				<div class="over">
					<form action="/admin/system/languages/add/ajax/<?php echo $functions->getCsrf() ?>" method="POST" class="widget addLanguage">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_SystemLanguagesAdd') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_SystemLanguagesAddDesc') ?></span>
							</div>
						</div>
						<div class="over">
							<label class="group hover">
								<div class="control">
									<div class="control-block">
										<div class="name">
											<span class="font-600"><?php echo $functions->languageInit('Admin_SystemLanguagesLabelHour12') ?></span>
										</div>
									</div>
									<div class="control-addon">
										<div class="check">
											<input type="checkbox" name="hour12" class="custom">
											<i class="check-switch"></i>
										</div>
									</div>
								</div>
							</label>
						</div>
						<div class="over">
							<label class="group hover">
								<div class="control">
									<div class="control-block">
										<div class="name">
											<span class="font-600"><?php echo $functions->languageInit('Admin_SystemLanguagesLabelRtl') ?></span>
										</div>
									</div>
									<div class="control-addon">
										<div class="check">
											<input type="checkbox" name="rtl" class="custom">
											<i class="check-switch"></i>
										</div>
									</div>
								</div>
							</label>
						</div>
						<div class="over formKey">
							<div class="above">
								<label for="key" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemLanguagesLabelKey') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="key" placeholder="<?php echo $functions->languageInit('Admin_SystemLanguagesFormKey') ?>" class="form block" id="key">
							</div>
						</div>
						<div class="over formValue">
							<div class="above">
								<label for="value" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemLanguagesLabelValue') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="value" placeholder="<?php echo $functions->languageInit('Admin_SystemLanguagesFormValue') ?>" class="form block" id="value">
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_SystemLanguagesAddSubmit') ?></span>
						</button>
					</form>
				</div>
				<script>
					$(document).on('submit', '.addLanguage', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonLanguage').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonLanguage">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										
										form.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/system/languages';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
<?php echo $header ?>
				<div class="over">
					<form action="/admin/system/languages/edit/ajax/<?php echo $functions->getCsrf() ?>/<?php echo $language['language_id'] ?>" method="POST" class="widget editLanguage">
						<div class="over">
							<div class="control">
								<div class="control-block">
									<div class="name">
										<span><?php echo $functions->languageInit('Admin_SystemLanguagesEdit') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Admin_SystemLanguagesEditDesc') ?></span>
									</div>
								</div>
								<div class="control-addon">
									<button type="button" class="btn second" data-toggle="dialog">
										<i class="zmdi zmdi-more"></i>
									</button>
									<div class="dropdown fade" data-ride="dialog" data-position="true">
										<div class="menu hover" data-target="#system_language_delete" data-toggle="dialog">
											<div class="model">
												<span><?php echo $functions->languageInit('Admin_SystemLanguagesIndexDelete') ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over">
							<label class="group hover">
								<div class="control">
									<div class="control-block">
										<div class="name">
											<span class="font-600"><?php echo $functions->languageInit('Admin_SystemLanguagesLabelHour12') ?></span>
										</div>
									</div>
									<div class="control-addon">
										<div class="check">
											<input type="checkbox" name="hour12" class="custom"<?php if($language['language_hour12']): ?> checked<?php endif; ?>>
											<i class="check-switch"></i>
										</div>
									</div>
								</div>
							</label>
						</div>
						<div class="over">
							<label class="group hover">
								<div class="control">
									<div class="control-block">
										<div class="name">
											<span class="font-600"><?php echo $functions->languageInit('Admin_SystemLanguagesLabelRtl') ?></span>
										</div>
									</div>
									<div class="control-addon">
										<div class="check">
											<input type="checkbox" name="rtl" class="custom"<?php if($language['language_rtl']): ?> checked<?php endif; ?>>
											<i class="check-switch"></i>
										</div>
									</div>
								</div>
							</label>
						</div>
						<div class="over formKey">
							<div class="above">
								<label for="key" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemLanguagesLabelKey') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="key" placeholder="<?php echo $functions->languageInit('Admin_SystemLanguagesFormKey') ?>" value="<?php echo $language['language_key'] ?>" class="form block" id="key">
							</div>
						</div>
						<div class="over formValue">
							<div class="above">
								<label for="value" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemLanguagesLabelValue') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="value" placeholder="<?php echo $functions->languageInit('Admin_SystemLanguagesFormValue') ?>" value="<?php echo $language['language_value'] ?>" class="form block" id="value">
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_SystemLanguagesEditSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="modal fade" data-ride="dialog" id="system_language_delete">
					<div class="over">
						<div class="name">
							<span><?php echo $functions->languageInit('Admin_SystemLanguagesIndexDeleteTitle') ?></span>
						</div>
						<div class="name">
							<span class="focus"><?php echo $functions->languageInit('Admin_SystemLanguagesIndexDeleteDesc') ?></span>
						</div>
					</div>
					<div class="text-right">
						<div class="fill">
							<button type="button" class="btn second" data-dismiss="dialog">
								<span><?php echo $functions->languageInit('Admin_SystemLanguagesIndexDeleteClose') ?></span>
							</button>
							<button type="button" class="btn error deleteLanguage">
								<span><?php echo $functions->languageInit('Admin_SystemLanguagesIndexDeleteSubmit') ?></span>
							</button>
						</div>
					</div>
				</div>
				<script>
					$(document).on('submit', '.editLanguage', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonLanguage').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonLanguage">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										break;
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.deleteLanguage', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/system/languages/index/delete/<?php echo addslashes($functions->getCsrf()) ?>/<?php echo addslashes($language['language_id']) ?>',
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/system/languages';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
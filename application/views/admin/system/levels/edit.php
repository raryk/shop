<?php echo $header ?>
				<div class="over">
					<form action="/admin/system/levels/edit/ajax/<?php echo $functions->getCsrf() ?>/<?php echo $level['level_id'] ?>" method="POST" class="widget editLevel">
						<div class="over">
							<div class="control">
								<div class="control-block">
									<div class="name">
										<span><?php echo $functions->languageInit('Admin_SystemLevelsEdit') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Admin_SystemLevelsEditDesc') ?></span>
									</div>
								</div>
								<div class="control-addon">
									<button type="button" class="btn second" data-toggle="dialog">
										<i class="zmdi zmdi-more"></i>
									</button>
									<div class="dropdown fade" data-ride="dialog" data-position="true">
										<div class="menu hover" data-target="#system_level_delete" data-toggle="dialog">
											<div class="model">
												<span><?php echo $functions->languageInit('Admin_SystemLevelsIndexDelete') ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over formName">
							<div class="above">
								<label for="name" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_SystemLevelsLabelName') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="name" placeholder="<?php echo $functions->languageInit('Admin_SystemLevelsFormName') ?>" value="<?php echo $level['level_name'] ?>" class="form block" id="name">
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_SystemLevelsEditSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="over">
					<form action="/admin/system/levels/edit/content/<?php echo $functions->getCsrf() ?>/<?php echo $level['level_id'] ?>" method="POST" class="widget editLevel">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_SystemLevelsEditContent') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_SystemLevelsEditContentDesc') ?></span>
							</div>
						</div>
						<div class="over mainSortable">
							<?php foreach(json_decode($level['level_content'], true) as $item): ?>
							<div class="above levelContent">
								<div class="control">
									<div class="control-addon">
										<div class="btn media mainSortableHandle">
											<i class="zmdi zmdi-arrows"></i>
										</div>
									</div>
									<div class="control-block">
										<input type="text" name="content[]" placeholder="<?php echo $functions->languageInit('Admin_SystemLevelsFormContent') ?>" value="<?php echo $item ?>" class="form block">
									</div>
									<div class="control-addon">
										<button type="button" class="btn error removeContent">
											<i class="zmdi zmdi-close"></i>
										</button>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
							<div class="above formContent">
								<div class="control">
									<div class="control-addon">
										<div class="btn media mainSortableHandle">
											<i class="zmdi zmdi-arrows"></i>
										</div>
									</div>
									<div class="control-block">
										<input type="text" name="content[]" placeholder="<?php echo $functions->languageInit('Admin_SystemLevelsFormContent') ?>" class="form block inputContent">
									</div>
									<div class="control-addon">
										<button type="button" class="btn info addContent">
											<i class="zmdi zmdi-plus"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_SystemLevelsEditContentSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="modal fade" data-ride="dialog" id="system_level_delete">
					<div class="over">
						<div class="name">
							<span><?php echo $functions->languageInit('Admin_SystemLevelsIndexDeleteTitle') ?></span>
						</div>
						<div class="name">
							<span class="focus"><?php echo $functions->languageInit('Admin_SystemLevelsIndexDeleteDesc') ?></span>
						</div>
					</div>
					<div class="text-right">
						<div class="fill">
							<button type="button" class="btn second" data-dismiss="dialog">
								<span><?php echo $functions->languageInit('Admin_SystemLevelsIndexDeleteClose') ?></span>
							</button>
							<button type="button" class="btn error deleteLevel">
								<span><?php echo $functions->languageInit('Admin_SystemLevelsIndexDeleteSubmit') ?></span>
							</button>
						</div>
					</div>
				</div>
				<script>
					$(document).on('submit', '.editLevel', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonLevel').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonLevel">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										break;
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.addContent', function() {
						$('.formContent').before('<div class="above levelContent">\
							<div class="control">\
								<div class="control-addon">\
									<div class="btn media mainSortableHandle">\
										<i class="zmdi zmdi-arrows"></i>\
									</div>\
								</div>\
								<div class="control-block">\
									<input type="text" name="content[]" placeholder="<?php echo addslashes($functions->languageInit('Admin_SystemLevelsFormContent')) ?>" value="' + $('.inputContent').val() + '" class="form block">\
								</div>\
								<div class="control-addon">\
									<button type="button" class="btn error removeContent">\
										<i class="zmdi zmdi-close"></i>\
									</button>\
								</div>\
							</div>\
						</div>');
						
						$('.formContent').html('<div class="control">\
							<div class="control-addon">\
								<div class="btn media mainSortableHandle">\
									<i class="zmdi zmdi-arrows"></i>\
								</div>\
							</div>\
							<div class="control-block">\
								<input type="text" name="content[]" placeholder="<?php echo addslashes($functions->languageInit('Admin_SystemLevelsFormContent')) ?>" class="form block inputContent">\
							</div>\
							<div class="control-addon">\
								<button type="button" class="btn info addContent">\
									<i class="zmdi zmdi-plus"></i>\
								</button>\
							</div>\
						</div>');
					});
					$(document).on('click', '.removeContent', function() {
						$(this).closest('.levelContent').remove();
					});
					
					$(document).on('click', '.deleteLevel', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/system/levels/index/delete/<?php echo addslashes($functions->getCsrf()) ?>/<?php echo addslashes($level['level_id']) ?>',
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/system/levels';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
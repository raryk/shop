<?php echo $header ?>
				<div class="over">
					<div class="row">
						<div class="col-8-sm">
							<div class="over">
								<form action="/admin/shop/orders/view/ajax/<?php echo $functions->getCsrf() ?>/<?php echo $order['order_id'] ?>" method="POST" class="widget viewOrder">
									<div class="over">
										<div class="control">
											<div class="control-block">
												<div class="name">
													<span><?php echo $functions->languageInit('Admin_ShopOrdersView') ?></span>
												</div>
												<div class="name">
													<span class="focus"><?php echo $functions->languageInit('Admin_ShopOrdersViewDesc') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<button type="button" class="btn second" data-toggle="dialog">
													<i class="zmdi zmdi-more"></i>
												</button>
												<div class="dropdown fade" data-ride="dialog" data-position="true">
													<a href="/admin/users/edit/index/<?php echo $order['user_id'] ?>" class="menu">
														<div class="model">
															<span><?php echo $order['user_nickname'] ?></span>
														</div>
													</a>
												</div>
											</div>
										</div>
									</div>
									<div class="over formStatus">
										<div class="above">
											<label for="access" class="name">
												<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersLabelStatus') ?></span>
											</label>
										</div>
										<div class="above">
											<select name="access" class="form block" id="status">
												<option value="0"<?php if($order['order_status'] == 0): ?> selected="selected"<?php endif; ?>><?php echo $functions->languageInit('Admin_ShopOrdersFormStatus0') ?></option>
												<option value="1"<?php if($order['order_status'] == 1): ?> selected="selected"<?php endif; ?>><?php echo $functions->languageInit('Admin_ShopOrdersFormStatus1') ?></option>
												<option value="2"<?php if($order['order_status'] == 2): ?> selected="selected"<?php endif; ?>><?php echo $functions->languageInit('Admin_ShopOrdersFormStatus2') ?></option>
											</select>
										</div>
									</div>
									<div class="over formEmail">
										<div class="above">
											<label for="email" class="name">
												<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersLabelEmail') ?></span>
											</label>
										</div>
										<div class="above">
											<input type="text" name="email" placeholder="<?php echo $functions->languageInit('Admin_ShopOrdersFormEmail') ?>" value="<?php echo $order['order_email'] ?>" class="form block" id="email">
										</div>
									</div>
									<div class="over formText">
										<div class="above">
											<label for="text" class="name">
												<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersLabelText') ?></span>
											</label>
										</div>
										<div class="above">
											<textarea rows="5" cols="1" name="text" placeholder="<?php echo $functions->languageInit('Admin_ShopOrdersFormText') ?>" class="form block" id="text"><?php echo $order['order_text'] ?></textarea>
										</div>
									</div>
									<div class="over formTrack">
										<div class="above">
											<label for="track" class="name">
												<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersLabelTrack') ?></span>
											</label>
										</div>
										<div class="above">
											<input type="text" name="track" placeholder="<?php echo $functions->languageInit('Admin_ShopOrdersFormTrack') ?>" value="<?php echo $order['order_track'] ?>" class="form block" id="track">
										</div>
									</div>
									<div class="over">
										<label class="group hover">
											<div class="control">
												<div class="control-block">
													<div class="name">
														<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersLabelMail') ?></span>
													</div>
												</div>
												<div class="control-addon">
													<div class="check">
														<input type="checkbox" name="mail" class="custom">
														<i class="check-checkbox"></i>
													</div>
												</div>
											</div>
										</label>
									</div>
									<button type="submit" class="btn">
										<span><?php echo $functions->languageInit('Admin_ShopOrdersViewSubmit') ?></span>
									</button>
								</form>
							</div>
							<div class="widget">
								<div class="over">
									<div class="name">
										<span><?php echo $functions->languageInit('Admin_ShopOrdersViewContent') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Admin_ShopOrdersViewContentDesc') ?></span>
									</div>
								</div>
								<?php if(!empty(json_decode($order['order_content'], true))): ?>
								<div class="responsive">
									<table class="table">
										<thead>
											<tr>
												<th>
													<div class="model">
														<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersViewTableProduct') ?></span>
													</div>
												</th>
												<th>
													<div class="model">
														<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersViewTableContent') ?></span>
													</div>
												</th>
												<th>
													<div class="model">
														<span class="font-600"><?php echo $functions->languageInit('Admin_ShopOrdersViewTableCount') ?></span>
													</div>
												</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach(json_decode($order['order_content'], true) as $item): ?>
											<?php if($functions->getTotal('shop_products', array('product_id' => $item['id']))): ?>
											<?php $product = $functions->getBy('shop_products', array('product_id' => $item['id'])) ?>
											<tr>
												<td>
													<div class="model">
														<a href="/product/index/<?php echo $product['product_id'] ?>" class="link">
															<span><?php echo isset(json_decode($product['product_title'], true)[$functions->language()]) ? json_decode($product['product_title'], true)[$functions->language()] : json_decode($product['product_title'], true)[$functions->config('language')] ?></span>
														</a>
													</div>
												</td>
												<td>
													<div class="model">
														<?php if(!empty(json_decode($product['product_content'], true)) && isset($item['content']) && !is_array($item['content']) && array_key_exists($item['content'], json_decode($product['product_content'], true))): ?>
														<span><?php echo json_decode($product['product_content'], true)[$item['content']] ?></span>
														<?php else: ?>
														<span class="font-600">-</span>
														<?php endif; ?>
													</div>
												</td>
												<td>
													<div class="model">
														<span><?php echo $item['count'] ?></span>
													</div>
												</td>
											</tr>
											<?php endif; ?>
											<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<?php else: ?>
								<div class="text-center">
									<div class="above">
										<div class="btn media">
											<i class="zmdi zmdi-alert-triangle"></i>
										</div>
									</div>
									<div class="name">
										<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
									</div>
								</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-4-sm">
							<div class="over">
								<div class="widget">
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-fire"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<span><?php echo $order['order_id'] ?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-balance-wallet"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<span><?php echo $functions->currencieInit($order['order_currencie'], $order['order_amount'], true) ?></span>
												</div>
											</div>
										</div>
									</div>
									<?php if($functions->getTotal('shop_coupons', array('coupon_id' => $order['order_coupon']))): ?>
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-key"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<a href="/admin/shop/coupons/edit/index/<?php echo $order['coupon_id'] ?>" class="link">
														<span><?php echo $order['coupon_key'] ?></span>
													</a>
												</div>
											</div>
										</div>
									</div>
									<?php endif; ?>
								</div>
							</div>
							<div class="over">
								<div class="widget">
									<div class="over">
										<div class="control">
											<div class="control-addon">
												<div class="btn media">
													<i class="zmdi zmdi-map"></i>
												</div>
											</div>
											<div class="control-block">
												<div class="name">
													<span><?php echo $functions->languageInit('Admin_ShopOrdersViewDeliverie') ?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Admin_ShopOrdersViewDeliverieName') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $order['order_name'] ?></span>
										</div>
									</div>
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Admin_ShopOrdersViewDeliverieCountry') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $order['order_country'] ?></span>
										</div>
									</div>
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Admin_ShopOrdersViewDeliverieCity') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $order['order_city'] ?></span>
										</div>
									</div>
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Admin_ShopOrdersViewDeliverieAddress') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $order['order_address'] ?></span>
										</div>
									</div>
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Admin_ShopOrdersViewDeliverieZipcode') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $order['order_zipcode'] ?></span>
										</div>
									</div>
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Admin_ShopOrdersViewDeliveriePhone') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $order['order_phone'] ?></span>
										</div>
									</div>
								</div>
							</div>
							<div class="justified">
								<button type="button" class="btn error block" data-toggle="dialog">
									<span><?php echo $functions->languageInit('Admin_ShopOrdersViewDelete') ?></span>
								</button>
								<div class="modal fade" data-ride="dialog">
									<div class="over">
										<div class="name">
											<span><?php echo $functions->languageInit('Admin_ShopOrdersViewDeleteTitle') ?></span>
										</div>
										<div class="name">
											<span class="focus"><?php echo $functions->languageInit('Admin_ShopOrdersViewDeleteDesc') ?></span>
										</div>
									</div>
									<div class="text-right">
										<div class="fill">
											<button type="button" class="btn second" data-dismiss="dialog">
												<span><?php echo $functions->languageInit('Admin_ShopOrdersViewDeleteClose') ?></span>
											</button>
											<button type="button" class="btn error deleteOrder">
												<span><?php echo $functions->languageInit('Admin_ShopOrdersViewDeleteSubmit') ?></span>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<script>
					$(document).on('submit', '.viewOrder', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonOrder').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonOrder">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										break;
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.deleteOrder', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/shop/orders/view/delete/<?php echo addslashes($functions->getCsrf()) ?>/<?php echo addslashes($order['order_id']) ?>',
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/shop/orders';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
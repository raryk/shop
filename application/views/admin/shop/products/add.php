<?php echo $header ?>
				<div class="over">
					<form action="/admin/shop/products/add/ajax/<?php echo $functions->getCsrf() ?>" method="POST" class="widget addProduct">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_ShopProductsAdd') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_ShopProductsAddDesc') ?></span>
							</div>
						</div>
						<div class="over formTitle">
							<div class="above">
								<label for="title" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelTitle') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<input type="text" name="title[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_ShopProductsFormTitle') ?>" class="form block" id="title">
							</div>
							<?php endforeach; ?>
						</div>
						<div class="over formDescription">
							<div class="above">
								<label for="description" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelDescription') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<textarea rows="5" cols="1" name="description[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_ShopProductsFormDescription') ?>" class="form block" id="description"></textarea>
							</div>
							<?php endforeach; ?>
						</div>
						<div class="over formText">
							<div class="above">
								<label for="text" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelText') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<textarea rows="5" cols="1" name="text[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_ShopProductsFormText') ?>" class="form block mainSummernote" class="form block" id="text"></textarea>
							</div>
							<?php endforeach; ?>
						</div>
						<div class="over formPrice">
							<div class="above">
								<label for="price" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelPrice') ?></span>
								</label>
							</div>
							<div class="above">
								<div class="control">
									<div class="control-block">
										<input type="text" name="price" placeholder="<?php echo $functions->languageInit('Admin_ShopProductsFormPrice') ?>" class="form block" id="price">
									</div>
									<div class="control-addon">
										<div class="btn media">
											<span><?php echo $functions->currencieBy() ?></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over">
							<div class="above">
								<label for="categorie" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelCategorie') ?></span>
								</label>
							</div>
							<select name="categorie" class="form block" id="categorie">
								<option value="0"><?php echo $functions->languageInit('CommonSelect') ?></option>
								<?php foreach($functions->get('shop_categories') as $item): ?>
								<option value="<?php echo $item['categorie_id'] ?>"><?php echo isset(json_decode($item['categorie_title'], true)[$functions->language()]) ? json_decode($item['categorie_title'], true)[$functions->language()] : json_decode($item['categorie_title'], true)[$functions->config('language')] ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="over formDiscount">
							<div class="above">
								<label for="discount" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelDiscount') ?></span>
								</label>
							</div>
							<div class="above">
								<div class="control">
									<div class="control-block">
										<input type="text" name="discount" placeholder="<?php echo $functions->languageInit('Admin_ShopProductsFormDiscount') ?>" class="form block" id="discount">
									</div>
									<div class="control-addon">
										<div class="btn media">
											<span>%</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over">
							<label class="group hover">
								<div class="control">
									<div class="control-block">
										<div class="name">
											<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelStatus') ?></span>
										</div>
									</div>
									<div class="control-addon">
										<div class="check">
											<input type="checkbox" name="status" class="custom">
											<i class="check-switch"></i>
										</div>
									</div>
								</div>
							</label>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_ShopProductsAddSubmit') ?></span>
						</button>
					</form>
				</div>
				<script>
					$(document).on('submit', '.addProduct', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonProduct').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonProduct">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										
										form.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/shop/products';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
<?php echo $header ?>
				<div class="over">
					<form action="/admin/shop/products/edit/ajax/<?php echo $functions->getCsrf() ?>/<?php echo $product['product_id'] ?>" method="POST" class="widget editProduct">
						<div class="over">
							<div class="control">
								<div class="control-block">
									<div class="name">
										<span><?php echo $functions->languageInit('Admin_ShopProductsEdit') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Admin_ShopProductsEditDesc') ?></span>
									</div>
								</div>
								<div class="control-addon">
									<button type="button" class="btn second" data-toggle="dialog">
										<i class="zmdi zmdi-more"></i>
									</button>
									<div class="dropdown fade" data-ride="dialog" data-position="true">
										<a href="/product/index/<?php echo $product['product_id'] ?>" class="menu">
											<div class="model">
												<span><?php echo $functions->languageInit('Admin_ShopProductsIndexPage') ?></span>
											</div>
										</a>
										<div class="menu hover" data-target="#shop_product_delete" data-toggle="dialog">
											<div class="model">
												<span><?php echo $functions->languageInit('Admin_ShopProductsIndexDelete') ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over formTitle">
							<div class="above">
								<label for="title" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelTitle') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<input type="text" name="title[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_ShopProductsFormTitle') ?>" value="<?php echo isset(json_decode($product['product_title'], true)[$item['language_id']]) ? json_decode($product['product_title'], true)[$item['language_id']] : null ?>" class="form block" id="title">
							</div>
							<?php endforeach; ?>
						</div>
						<div class="over formDescription">
							<div class="above">
								<label for="description" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelDescription') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<textarea rows="5" cols="1" name="description[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_ShopProductsFormDescription') ?>" class="form block" id="description"><?php echo isset(json_decode($product['product_description'], true)[$item['language_id']]) ? json_decode($product['product_description'], true)[$item['language_id']] : null ?></textarea>
							</div>
							<?php endforeach; ?>
						</div>
						<div class="over formText">
							<div class="above">
								<label for="text" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelText') ?></span>
								</label>
							</div>
							<?php foreach($functions->get('system_languages') as $item): ?>
							<div class="above">
								<div class="name">
									<span><?php echo $item['language_value'] ?></span>
								</div>
								<textarea rows="5" cols="1" name="text[<?php echo $item['language_id'] ?>]" placeholder="<?php echo $functions->languageInit('Admin_ShopProductsFormText') ?>" class="form block mainSummernote" class="form block" id="text"><?php echo isset(json_decode($product['product_text'], true)[$item['language_id']]) ? json_decode($product['product_text'], true)[$item['language_id']] : null ?></textarea>
							</div>
							<?php endforeach; ?>
						</div>
						<div class="over formPrice">
							<div class="above">
								<label for="price" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelPrice') ?></span>
								</label>
							</div>
							<div class="above">
								<div class="control">
									<div class="control-block">
										<input type="text" name="price" placeholder="<?php echo $functions->languageInit('Admin_ShopProductsFormPrice') ?>" value="<?php echo $product['product_price'] ?>" class="form block" id="price">
									</div>
									<div class="control-addon">
										<div class="btn media">
											<span><?php echo $functions->currencieBy() ?></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over">
							<div class="above">
								<label for="categorie" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelCategorie') ?></span>
								</label>
							</div>
							<select name="categorie" class="form block" id="categorie">
								<option value="0"><?php echo $functions->languageInit('CommonSelect') ?></option>
								<?php foreach($functions->get('shop_categories') as $item): ?>
								<option value="<?php echo $item['categorie_id'] ?>"<?php if($product['product_categorie'] == $item['categorie_id']): ?> selected="selected"<?php endif; ?>><?php echo isset(json_decode($item['categorie_title'], true)[$functions->language()]) ? json_decode($item['categorie_title'], true)[$functions->language()] : json_decode($item['categorie_title'], true)[$functions->config('language')] ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="over formDiscount">
							<div class="above">
								<label for="discount" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelDiscount') ?></span>
								</label>
							</div>
							<div class="above">
								<div class="control">
									<div class="control-block">
										<input type="text" name="discount" placeholder="<?php echo $functions->languageInit('Admin_ShopProductsFormDiscount') ?>" value="<?php echo $product['product_discount'] ?>" class="form block" id="discount">
									</div>
									<div class="control-addon">
										<div class="btn media">
											<span>%</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over">
							<label class="group hover">
								<div class="control">
									<div class="control-block">
										<div class="name">
											<span class="font-600"><?php echo $functions->languageInit('Admin_ShopProductsLabelStatus') ?></span>
										</div>
									</div>
									<div class="control-addon">
										<div class="check">
											<input type="checkbox" name="status" class="custom"<?php if($product['product_status']): ?> checked<?php endif; ?>>
											<i class="check-switch"></i>
										</div>
									</div>
								</div>
							</label>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_ShopProductsEditSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="over">
					<form action="/admin/shop/products/edit/content/<?php echo $functions->getCsrf() ?>/<?php echo $product['product_id'] ?>" method="POST" class="widget editProduct">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_ShopProductsEditContent') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_ShopProductsEditContentDesc') ?></span>
							</div>
						</div>
						<div class="over mainSortable">
							<?php foreach(json_decode($product['product_content'], true) as $item): ?>
							<div class="above productContent">
								<div class="control">
									<div class="control-addon">
										<div class="btn media mainSortableHandle">
											<i class="zmdi zmdi-arrows"></i>
										</div>
									</div>
									<div class="control-block">
										<input type="text" name="content[]" placeholder="<?php echo $functions->languageInit('Admin_ShopProductsFormContent') ?>" value="<?php echo $item ?>" class="form block">
									</div>
									<div class="control-addon">
										<button type="button" class="btn error removeContent">
											<i class="zmdi zmdi-close"></i>
										</button>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
							<div class="above formContent">
								<div class="control">
									<div class="control-addon">
										<div class="btn media mainSortableHandle">
											<i class="zmdi zmdi-arrows"></i>
										</div>
									</div>
									<div class="control-block">
										<input type="text" name="content[]" placeholder="<?php echo $functions->languageInit('Admin_ShopProductsFormContent') ?>" class="form block inputContent">
									</div>
									<div class="control-addon">
										<button type="button" class="btn info addContent">
											<i class="zmdi zmdi-plus"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_ShopProductsEditContentSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="over">
					<form action="/admin/shop/products/edit/image/<?php echo $functions->getCsrf() ?>/<?php echo $product['product_id'] ?>" method="POST" class="widget editProduct">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_ShopProductsEditImage') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_ShopProductsEditImageDesc') ?></span>
							</div>
						</div>
						<div class="over">
							<div class="row mainSortable">
								<?php foreach(json_decode($product['product_image'], true) as $item): ?>
								<div class="col-2-sm col-6-xs productImage">
									<input type="hidden" name="image[]" value="<?php echo $item ?>">
									<div class="miniature">
										<div class="forward">
											<div class="banner mainSortableHandle">
												<?php if(!empty($functions->image($item))): ?>
												<img src="<?php echo $functions->image($item)['p'] ?>" alt="image">
												<?php else: ?>
												<img src="<?php echo $functions->config('assets') ?>/png/image.png" alt="image">
												<?php endif; ?>
											</div>
											<div class="over">
												<button type="button" class="btn error block removeImage">
													<i class="zmdi zmdi-close"></i>
												</button>
											</div>
										</div>
									</div>
								</div>
								<?php endforeach; ?>
								<div class="col-2-sm col-6-xs formImage">
									<input type="file" class="custom inputImage" id="image" multiple>
									<div class="miniature">
										<div class="forward">
											<div class="banner mainSortableHandle">
												<img src="<?php echo $functions->config('assets') ?>/png/image.png" alt="image">
											</div>
											<div class="over">
												<label for="image" class="btn info block hover">
													<i class="zmdi zmdi-plus"></i>
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_ShopProductsEditImageSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="modal fade" data-ride="dialog" id="shop_product_delete">
					<div class="over">
						<div class="name">
							<span><?php echo $functions->languageInit('Admin_ShopProductsIndexDeleteTitle') ?></span>
						</div>
						<div class="name">
							<span class="focus"><?php echo $functions->languageInit('Admin_ShopProductsIndexDeleteDesc') ?></span>
						</div>
					</div>
					<div class="text-right">
						<div class="fill">
							<button type="button" class="btn second" data-dismiss="dialog">
								<span><?php echo $functions->languageInit('Admin_ShopProductsIndexDeleteClose') ?></span>
							</button>
							<button type="button" class="btn error deleteProduct">
								<span><?php echo $functions->languageInit('Admin_ShopProductsIndexDeleteSubmit') ?></span>
							</button>
						</div>
					</div>
				</div>
				<script>
					$(document).on('submit', '.editProduct', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonProduct').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonProduct">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										break;
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.addContent', function() {
						$('.formContent').before('<div class="above productContent">\
							<div class="control">\
								<div class="control-addon">\
									<div class="btn media mainSortableHandle">\
										<i class="zmdi zmdi-arrows"></i>\
									</div>\
								</div>\
								<div class="control-block">\
									<input type="text" name="content[]" placeholder="<?php echo addslashes($functions->languageInit('Admin_ShopProductsFormContent')) ?>" value="' + $('.inputContent').val() + '" class="form block">\
								</div>\
								<div class="control-addon">\
									<button type="button" class="btn error removeContent">\
										<i class="zmdi zmdi-close"></i>\
									</button>\
								</div>\
							</div>\
						</div>');
						
						$('.formContent').html('<div class="control">\
							<div class="control-addon">\
								<div class="btn media mainSortableHandle">\
									<i class="zmdi zmdi-arrows"></i>\
								</div>\
							</div>\
							<div class="control-block">\
								<input type="text" name="content[]" placeholder="<?php echo addslashes($functions->languageInit('Admin_ShopProductsFormContent')) ?>" class="form block inputContent">\
							</div>\
							<div class="control-addon">\
								<button type="button" class="btn info addContent">\
									<i class="zmdi zmdi-plus"></i>\
								</button>\
							</div>\
						</div>');
					});
					$(document).on('click', '.removeContent', function() {
						$(this).closest('.productContent').remove();
					});
					
					$(document).on('change', '.inputImage', function(event) {
						var files = typeof(event.target.files) == 'undefined' ? (event.target && event.target.value ? [{name: event.target.value.replace(/^.+\\/, '')}] : []) : event.target.files;
						
						$('.formImage').html('<input type="file" class="custom inputImage" id="image" multiple>\
						<div class="miniature">\
							<div class="forward">\
								<div class="banner mainSortableHandle">\
									<img src="<?php echo $functions->config('assets') ?>/png/image.png" alt="image">\
								</div>\
								<div class="over">\
									<label for="image" class="btn info block hover">\
										<i class="zmdi zmdi-plus"></i>\
									</label>\
								</div>\
							</div>\
						</div>');
						
						for(var i = 0; i < files.length; i++) {
							if(files[i].type.match('image.*') && !$('.loadImage_' + files[i].size).length) {
								(function(file) {
									var formData = new FormData();
									formData.append('type', 'image');
									formData.append('file', file);
									
									var xhr_image = $.ajax({
										contentType: false,
										processData: false,
										type: 'POST',
										url: '<?php echo addslashes($functions->config('upload')) ?>/upload?language=<?php echo addslashes($functions->languageBy($functions->language())) ?>',
										data: formData,
										beforeSend: function(data) {
											$('.formImage').before('<div class="col-2-sm col-6-xs loadImage_' + file.size + '">\
												<div class="miniature">\
													<div class="forward">\
														<div class="banner mainSortableHandle">\
															<img src="<?php echo $functions->config('assets') ?>/png/image.png" alt="image">\
														</div>\
														<div class="over">\
															<div class="control">\
																<div class="control-block">\
																	<div class="progress">\
																		<i class="progress-bar progressImage_' + file.size + '""></i>\
																	</div>\
																</div>\
																<div class="control-addon">\
																	<button type="button" class="btn second cancelImage_' + file.size + '">\
																		<i class="zmdi zmdi-block"></i>\
																	</button>\
																</div>\
															</div>\
														</div>\
													</div>\
												</div>\
											</div>');
										},
										xhr: function() {
											var xhr = new window.XMLHttpRequest();
											
											xhr.upload.addEventListener('progress', function(event) {
												if(event.lengthComputable) {
													$('.progressImage_' + file.size).width(event.loaded / event.total * 100 + '%');
													
												}
											}, false);
											
											return xhr;
										},
										success: function(data) {
											data = JSON.parse(data);
											switch(data.status) {
												case 'error':
													$.growl({
														message: data.error,
														type: 'error'
													});
													
													$(document).off('click', '.cancelImage_' + file.size);
													
													$('.loadImage_' + file.size).remove();
													break;
												case 'success':
													$(document).off('click', '.cancelImage_' + file.size);
													
													if(!$.isEmptyObject(data.images)) {
														$('.loadImage_' + file.size).removeClass('loadImage_' + file.size).addClass('productImage').html('<input type="hidden" name="image[]" value="' + data.image + '">\
														<div class="miniature">\
															<div class="forward">\
																<div class="banner mainSortableHandle">\
																	<img src="' + data.images['p'] + '" alt="image">\
																</div>\
																<div class="over">\
																	<button type="button" class="btn error block removeImage">\
																		<i class="zmdi zmdi-close"></i>\
																	</button>\
																</div>\
															</div>\
														</div>');
													}
													break;
											}
										},
										error: function(data) {
											$(document).off('click', '.cancelImage_' + file.size);
											
											$('.loadImage_' + file.size).remove();
											
											if(data.statusText != 'abort') {
												$.growl({
													message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
													type: 'warning'
												});
											}
										}
									});
									
									$(document).on('click', '.cancelImage_' + file.size, function() {
										xhr_image.abort();
									});
								})(files[i]);
							}
						}
					});
					$(document).on('click', '.removeImage', function() {
						$(this).closest('.productImage').remove();
					});
					
					$(document).on('click', '.deleteProduct', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/shop/products/index/delete/<?php echo addslashes($functions->getCsrf()) ?>/<?php echo addslashes($product['product_id']) ?>',
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/shop/products';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
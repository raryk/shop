<?php echo $header ?>
				<div class="over">
					<form action="/admin/shop/deliveries/edit/ajax/<?php echo $functions->getCsrf() ?>/<?php echo $deliverie['deliverie_id'] ?>" method="POST" class="widget editDeliverie">
						<div class="over">
							<div class="control">
								<div class="control-block">
									<div class="name">
										<span><?php echo $functions->languageInit('Admin_ShopDeliveriesEdit') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Admin_ShopDeliveriesEditDesc') ?></span>
									</div>
								</div>
								<div class="control-addon">
									<button type="button" class="btn second" data-toggle="dialog">
										<i class="zmdi zmdi-more"></i>
									</button>
									<div class="menu hover" data-target="#shop_deliverie_delete" data-toggle="dialog">
										<div class="model">
											<span><?php echo $functions->languageInit('Admin_ShopDeliveriesIndexDelete') ?></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over formName">
							<div class="above">
								<label for="name" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopDeliveriesLabelName') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="name" placeholder="<?php echo $functions->languageInit('Admin_ShopDeliveriesFormName') ?>" value="<?php echo $deliverie['deliverie_name'] ?>" class="form block" id="name">
							</div>
						</div>
						<div class="over formCountry">
							<div class="above">
								<label for="country" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopDeliveriesLabelCountry') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="country" placeholder="<?php echo $functions->languageInit('Admin_ShopDeliveriesFormCountry') ?>" value="<?php echo $deliverie['deliverie_country'] ?>" class="form block" id="country">
							</div>
						</div>
						<div class="over formCity">
							<div class="above">
								<label for="city" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopDeliveriesLabelCity') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="city" placeholder="<?php echo $functions->languageInit('Admin_ShopDeliveriesFormCity') ?>" value="<?php echo $deliverie['deliverie_city'] ?>" class="form block" id="city">
							</div>
						</div>
						<div class="over formAddress">
							<div class="above">
								<label for="address" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopDeliveriesLabelAddress') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="address" placeholder="<?php echo $functions->languageInit('Admin_ShopDeliveriesFormAddress') ?>" value="<?php echo $deliverie['deliverie_address'] ?>" class="form block" id="address">
							</div>
						</div>
						<div class="over formZipcode">
							<div class="above">
								<label for="zipcode" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopDeliveriesLabelZipcode') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="zipcode" placeholder="<?php echo $functions->languageInit('Admin_ShopDeliveriesFormZipcode') ?>" value="<?php echo $deliverie['deliverie_zipcode'] ?>" class="form block" id="zipcode">
							</div>
						</div>
						<div class="over formPhone">
							<div class="above">
								<label for="phone" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_ShopDeliveriesLabelPhone') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="phone" placeholder="<?php echo $functions->languageInit('Admin_ShopDeliveriesFormPhone') ?>" value="<?php echo $deliverie['deliverie_phone'] ?>" class="form block" id="phone">
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_ShopDeliveriesEditSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="modal fade" data-ride="dialog" id="shop_deliverie_delete">
					<div class="over">
						<div class="name">
							<span><?php echo $functions->languageInit('Admin_ShopDeliveriesIndexDeleteTitle') ?></span>
						</div>
						<div class="name">
							<span class="focus"><?php echo $functions->languageInit('Admin_ShopDeliveriesIndexDeleteDesc') ?></span>
						</div>
					</div>
					<div class="text-right">
						<div class="fill">
							<button type="button" class="btn second" data-dismiss="dialog">
								<span><?php echo $functions->languageInit('Admin_ShopDeliveriesIndexDeleteClose') ?></span>
							</button>
							<button type="button" class="btn error deleteDeliverie">
								<span><?php echo $functions->languageInit('Admin_ShopDeliveriesIndexDeleteSubmit') ?></span>
							</button>
						</div>
					</div>
				</div>
				<script>
					$(document).on('submit', '.editDeliverie', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonDeliverie').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonDeliverie">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										break;
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.deleteDeliverie', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/shop/deliveries/index/delete/<?php echo addslashes($functions->getCsrf()) ?>/<?php echo addslashes($deliverie['deliverie_id']) ?>',
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/shop/deliveries';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
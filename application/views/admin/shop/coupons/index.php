<?php echo $header ?>
				<form action="/admin/shop/coupons" class="over">
					<div class="control">
						<div class="control-block">
							<input type="text" name="q" placeholder="<?php echo $functions->languageInit('CommonSearch') ?>"<?php if(isset($request->get['q']) && !is_array($request->get['q'])): ?> value="<?php echo $request->get['q'] ?>"<?php endif; ?> class="form block">
						</div>
						<div class="control-addon">
							<a href="/admin/shop/coupons" class="btn second" title="<?php echo $functions->number2format($total) ?>" data-ride="tooltip">
								<span><?php echo $functions->number2string($total) ?></span>
							</a>
						</div>
					</div>
				</form>
				<div class="over">
					<div class="widget">
						<div class="over">
							<div class="control">
								<div class="control-block">
									<div class="name">
										<span><?php echo $functions->languageInit('Admin_ShopCouponsIndex') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Admin_ShopCouponsIndexDesc') ?></span>
									</div>
								</div>
								<div class="control-addon">
									<button type="button" class="btn second" data-toggle="dialog">
										<i class="zmdi zmdi-more"></i>
									</button>
									<div class="dropdown fade" data-ride="dialog" data-position="true">
										<a href="/admin/shop/coupons/add" class="menu">
											<div class="model">
												<span><?php echo $functions->languageInit('Admin_ShopCouponsIndexAdd') ?></span>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="over">
							<?php if(!empty($coupons)): ?>
							<div class="responsive">
								<table class="table">
									<thead>
										<tr>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopCouponsTableId') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopCouponsTableKey') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopCouponsTableValue') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopCouponsTableDate') ?></span>
												</div>
											</th>
											<th>
												<div class="model">
													<span class="font-600"><?php echo $functions->languageInit('Admin_ShopCouponsTableAction') ?></span>
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($coupons as $item): ?>
										<tr>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['coupon_id']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['coupon_key']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
												</div>
											</td>
											<td>
												<div class="model">
													<?php foreach($functions->string($item['coupon_value']) as $string): ?>
													<span<?php if($string['type'] == 'search'): ?> class="background-color-yellow color-black"<?php endif; ?>><?php echo $string['text'] ?></span>
													<?php endforeach; ?>
													<span>%</span>
												</div>
											</td>
											<td>
												<div class="model">
													<span><?php echo $functions->datetime(strtotime($item['coupon_date_add'])) ?></span>
												</div>
											</td>
											<td>
												<div class="fill">
													<button type="button" class="btn default addon" data-toggle="dialog">
														<span><?php echo $functions->languageInit('CommonAction') ?></span>
														<span>&nbsp;</span>
														<i class="zmdi zmdi-caret-down"></i>
													</button>
													<div class="dropdown fade" data-ride="dialog" data-position="true">
														<a href="/admin/shop/coupons/edit/index/<?php echo $item['coupon_id'] ?>" class="menu">
															<div class="model">
																<span><?php echo $functions->languageInit('Admin_ShopCouponsIndexEdit') ?></span>
															</div>
														</a>
														<div class="menu hover" data-target="#shop_coupon<?php echo $item['coupon_id'] ?>_delete" data-toggle="dialog">
															<div class="model">
																<span><?php echo $functions->languageInit('Admin_ShopCouponsIndexDelete') ?></span>
															</div>
														</div>
													</div>
												</div>
												<div class="modal fade" data-ride="dialog" id="shop_coupon<?php echo $item['coupon_id'] ?>_delete">
													<div class="over">
														<div class="name">
															<span><?php echo $functions->languageInit('Admin_ShopCouponsIndexDeleteTitle') ?></span>
														</div>
														<div class="name">
															<span class="focus"><?php echo $functions->languageInit('Admin_ShopCouponsIndexDeleteDesc') ?></span>
														</div>
													</div>
													<div class="text-right">
														<div class="fill">
															<button type="button" class="btn second" data-dismiss="dialog">
																<span><?php echo $functions->languageInit('Admin_ShopCouponsIndexDeleteClose') ?></span>
															</button>
															<button type="button" class="btn error deleteCoupon" data-id="<?php echo $item['coupon_id'] ?>">
																<span><?php echo $functions->languageInit('Admin_ShopCouponsIndexDeleteSubmit') ?></span>
															</button>
														</div>
													</div>
												</div>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<?php else: ?>
							<div class="text-center">
								<div class="above">
									<div class="btn media">
										<i class="zmdi zmdi-alert-triangle"></i>
									</div>
								</div>
								<div class="name">
									<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
								</div>
							</div>
							<?php endif; ?>
						</div>
						<?php if(!empty($pagination)): ?>
						<div class="group">
							<div class="wrap">
								<?php foreach($pagination as $item): ?>
								<div class="wrap-item">
									<a href="<?php echo $item['url'] ?>" class="btn background-color-transparent color-inherit<?php if($item['active']): ?> active<?php endif; ?>">
										<span><?php echo $item['text'] ?></span>
									</a>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<script>
					$(document).on('click', '.deleteCoupon', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/shop/coupons/index/delete/<?php echo addslashes($functions->getCsrf()) ?>/' + submit.data('id'),
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/shop/coupons';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
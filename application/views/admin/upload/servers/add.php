<?php echo $header ?>
				<div class="over">
					<form action="/admin/upload/servers/add/ajax/<?php echo $functions->getCsrf() ?>" method="POST" class="widget addServer">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_UploadServersAdd') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_UploadServersAddDesc') ?></span>
							</div>
						</div>
						<div class="over formUrl">
							<div class="above">
								<label for="url" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UploadServersLabelUrl') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="url" placeholder="<?php echo $functions->languageInit('Admin_UploadServersFormUrl') ?>" class="form block" id="url">
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_UploadServersAddSubmit') ?></span>
						</button>
					</form>
				</div>
				<script>
					$(document).on('submit', '.addServer', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonServer').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonServer">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										
										form.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/upload/servers';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
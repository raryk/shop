<?php echo $header ?>
				<div class="over">
					<form action="/admin/upload/servers/edit/ajax/<?php echo $functions->getCsrf() ?>/<?php echo $server['server_id'] ?>" method="POST" class="widget editServer">
						<div class="over">
							<div class="control">
								<div class="control-block">
									<div class="name">
										<span><?php echo $functions->languageInit('Admin_UploadServersEdit') ?></span>
									</div>
									<div class="name">
										<span class="focus"><?php echo $functions->languageInit('Admin_UploadServersEditDesc') ?></span>
									</div>
								</div>
								<div class="control-addon">
									<button type="button" class="btn second" data-toggle="dialog">
										<i class="zmdi zmdi-more"></i>
									</button>
									<div class="dropdown fade" data-ride="dialog" data-position="true">
										<div class="menu hover" data-target="#upload_server_delete" data-toggle="dialog">
											<div class="model">
												<span><?php echo $functions->languageInit('Admin_UploadServersIndexDelete') ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="over formUrl">
							<div class="above">
								<label for="url" class="name">
									<span class="font-600"><?php echo $functions->languageInit('Admin_UploadServersLabelUrl') ?></span>
								</label>
							</div>
							<div class="above">
								<input type="text" name="url" placeholder="<?php echo $functions->languageInit('Admin_UploadServersFormUrl') ?>" value="<?php echo $server['server_url'] ?>" class="form block" id="url">
							</div>
						</div>
						<button type="submit" class="btn">
							<span><?php echo $functions->languageInit('Admin_UploadServersEditSubmit') ?></span>
						</button>
					</form>
				</div>
				<div class="modal fade" data-ride="dialog" id="upload_server_delete">
					<div class="over">
						<div class="name">
							<span><?php echo $functions->languageInit('Admin_UploadServersIndexDeleteTitle') ?></span>
						</div>
						<div class="name">
							<span class="focus"><?php echo $functions->languageInit('Admin_UploadServersIndexDeleteDesc') ?></span>
						</div>
					</div>
					<div class="text-right">
						<div class="fill">
							<button type="button" class="btn second" data-dismiss="dialog">
								<span><?php echo $functions->languageInit('Admin_UploadServersIndexDeleteClose') ?></span>
							</button>
							<button type="button" class="btn error deleteServer">
								<span><?php echo $functions->languageInit('Admin_UploadServersIndexDeleteSubmit') ?></span>
							</button>
						</div>
					</div>
				</div>
				<script>
					$(document).on('submit', '.editServer', function(event) {
						event.preventDefault();
						
						var form = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: form.attr('method'),
							url: form.attr('action'),
							data: new FormData(form[0]),
							beforeSend: function(data) {
								form.find('button[type="submit"]').prop('disabled', true);
								
								form.find('.form.error').removeClass('error');
								$('.addonServer').remove();
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										if($.isArray(data.error)) {
											$.each(data.error, function() {
												form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).find('.form').addClass('error');
												
												if(this.value) {
													form.find('.form' + this.key[0].toUpperCase() + this.key.slice(1)).append('<div class="name addonServer">\
														<span class="focus">' + this.value + '</span>\
													</div>');
												}
											});
										} else {
											$.growl({
												message: data.error,
												type: 'error'
											});
										}
										break;
									case 'success':
										$.growl({
											message: data.success,
											type: 'success'
										});
										break;
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								form.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
					
					$(document).on('click', '.deleteServer', function() {
						var submit = $(this);
						
						$.ajax({
							contentType: false,
							processData: false,
							type: 'POST',
							url: '/admin/upload/servers/index/delete/<?php echo addslashes($functions->getCsrf()) ?>/<?php echo addslashes($server['server_id']) ?>',
							beforeSend: function(data) {
								submit.find('button[type="submit"]').prop('disabled', true);
							},
							success: function(data) {
								data = JSON.parse(data);
								switch(data.status) {
									case 'error':
										$.growl({
											message: data.error,
											type: 'error'
										});
										
										submit.find('button[type="submit"]').prop('disabled', false);
										break;
									case 'success':
										document.location.href = '/admin/upload/servers';
										break;
								}
							},
							error: function(data) {
								if(data.statusText != 'abort') {
									$.growl({
										message: '<?php echo addslashes($functions->languageInit('CommonNetwork')) ?>',
										type: 'warning'
									});
								}
								
								submit.find('button[type="submit"]').prop('disabled', false);
							}
						});
					});
				</script>
<?php echo $footer ?>
<?php echo $header ?>
				<form action="/admin/upload/images" class="over">
					<div class="control">
						<div class="control-block">
							<input type="text" name="q" placeholder="<?php echo $functions->languageInit('CommonSearch') ?>"<?php if(isset($request->get['q']) && !is_array($request->get['q'])): ?> value="<?php echo $request->get['q'] ?>"<?php endif; ?> class="form block">
						</div>
						<div class="control-addon">
							<a href="/admin/upload/images" class="btn second" title="<?php echo $functions->number2format($total) ?>" data-ride="tooltip">
								<span><?php echo $functions->number2string($total) ?></span>
							</a>
						</div>
					</div>
				</form>
				<div class="over">
					<div class="widget">
						<div class="over">
							<div class="name">
								<span><?php echo $functions->languageInit('Admin_UploadImages') ?></span>
							</div>
							<div class="name">
								<span class="focus"><?php echo $functions->languageInit('Admin_UploadImagesDesc') ?></span>
							</div>
						</div>
						<div class="over">
							<?php if(!empty($images)): ?>
							<div class="preview" data-gallery="parent">
								<?php foreach($images as $item): ?>
								<?php if(!empty($functions->image($item['image_secret']))): ?>
								<div class="preview-item hover" data-action="gallery" data-alt="image" data-src="<?php echo $functions->image($item['image_secret'])['original'] ?>">
									<img src="<?php echo $functions->image($item['image_secret'])['crop_m'] ?>" alt="image">
									<span><?php echo $functions->datetime(strtotime($item['image_date_add'])) ?></span>
								</div>
								<?php else: ?>
								<div class="preview-item">
									<img src="<?php echo $functions->config('assets') ?>/png/image.png" alt="image">
									<span><?php echo $functions->datetime(strtotime($item['image_date_add'])) ?></span>
								</div>
								<?php endif; ?>
								<?php endforeach; ?>
							</div>
							<?php else: ?>
							<div class="text-center">
								<div class="above">
									<div class="btn media">
										<i class="zmdi zmdi-alert-triangle"></i>
									</div>
								</div>
								<div class="name">
									<span class="font-600"><?php echo $functions->languageInit('CommonEmpty') ?></span>
								</div>
							</div>
							<?php endif; ?>
						</div>
						<?php if(!empty($pagination)): ?>
						<div class="group">
							<div class="wrap">
								<?php foreach($pagination as $item): ?>
								<div class="wrap-item">
									<a href="<?php echo $item['url'] ?>" class="btn background-color-transparent color-inherit<?php if($item['active']): ?> active<?php endif; ?>">
										<span><?php echo $item['text'] ?></span>
									</a>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
<?php echo $footer ?>
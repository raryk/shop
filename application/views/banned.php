<!DOCTYPE html>
<html dir="<?php if($functions->isRtl()): ?>rtl<?php else: ?>ltr<?php endif; ?>" lang="<?php echo $functions->languageBy($functions->language()) ?>" prefix="og: https://ogp.me/ns#">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="description" content="<?php echo $ban['ban_text'] ?>">
		<meta name="keywords" content="<?php echo $functions->languageInit('CommonKeywords') ?>">
		<meta name="theme-color" content="<?php echo $functions->config('color') ?>">
		
		<?php if($ban['ban_time']): ?>
		<meta property="og:title" content="<?php echo $functions->datetime($ban['ban_time']) ?>">
		<?php else: ?>
		<meta property="og:title" content="<?php echo $functions->languageInit('Main_Banned') ?>">
		<?php endif; ?>
		<meta property="og:type" content="website">
		<meta property="og:image" content="<?php echo $functions->config('assets') ?>/png/emblem.png">
		<meta property="og:url" content="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/banned">
		<meta property="og:description" content="<?php echo $ban['ban_text'] ?>">
		<meta property="og:site_name" content="<?php echo $functions->config('title') ?>">
		
		<?php if($ban['ban_time']): ?>
		<title><?php echo $functions->datetime($ban['ban_time']) ?></title>
		<?php else: ?>
		<title><?php echo $functions->languageInit('Main_Banned') ?></title>
		<?php endif; ?>
		
		<link rel="canonical" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/banned?language=<?php echo $functions->languageBy($functions->language()) ?>">
		
		<link rel="alternate" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/banned" hreflang="x-default">
		<?php foreach($functions->get('system_languages') as $item): ?>
		<link rel="alternate" href="<?php echo $functions->config('connect') ?><?php echo $functions->config('domain') ?>/banned?language=<?php echo $item['language_key'] ?>" hreflang="<?php echo $item['language_key'] ?>">
		<?php endforeach; ?>
		
		<link rel="shortcut icon" href="<?php echo $functions->config('assets') ?>/ico/favicon.ico">
	</head>
	<body>
		<div class="wrapper">
			<section class="app">
				<div class="widget">
					<div class="control">
						<div class="control-block">
							<div class="name">
								<?php if($ban['ban_time']): ?>
								<span><?php echo $functions->datetime($ban['ban_time']) ?></span>
								<?php else: ?>
								<span><?php echo $functions->languageInit('Main_Banned') ?></span>
								<?php endif; ?>
							</div>
							<div class="name">
								<?php foreach($functions->string(nl2br($ban['ban_text']), true) as $item): ?>
								<?php if(isset($item['link'])): ?>
								<a href="<?php echo $item['link'] ?>" class="link">
									<span class="focus"><?php echo $item['link'] ?></span>
								</a>
								<?php else: ?>
								<span class="focus"><?php echo $item['text'] ?></span>
								<?php endif; ?>
								<?php endforeach; ?>
							</div>
						</div>
						<?php if($account->isLogged()): ?>
						<div class="control-addon">
							<button type="button" class="btn second" data-toggle="dialog">
								<i class="zmdi zmdi-more"></i>
							</button>
							<div class="dropdown fade" data-ride="dialog" data-position="true">
								<a href="/orders" class="menu">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-shopping-basket"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonOrders') ?></span>
											</div>
										</div>
									</div>
								</a>
								<a href="/deliveries" class="menu">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-map"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonDeliveries') ?></span>
											</div>
										</div>
									</div>
								</a>
								<a href="/settings" class="menu">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-settings"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonSettings') ?></span>
											</div>
										</div>
									</div>
								</a>
								<i class="divider"></i>
								<div class="navigate" data-navigation="parent">
									<div class="menu hover" data-action="navigation">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $functions->languageInit('CommonLanguage') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<i class="toggle" data-navigation="toggle"></i>
											</div>
										</div>
									</div>
									<div class="navigation" data-navigation="slide">
										<?php foreach($functions->get('system_languages') as $item): ?>
										<a href="/index/language/<?php echo $item['language_id'] ?>" class="menu">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<span><?php echo $item['language_value'] ?></span>
													</div>
												</div>
												<?php if($item['language_id'] == $functions->language()): ?>
												<div class="control-addon">
													<div class="btn media small">
														<i class="zmdi zmdi-check"></i>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</a>
										<?php endforeach; ?>
									</div>
								</div>
								<div class="navigate" data-navigation="parent">
									<div class="menu hover" data-action="navigation">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $functions->languageInit('CommonCurrencie') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<i class="toggle" data-navigation="toggle"></i>
											</div>
										</div>
									</div>
									<div class="navigation" data-navigation="slide">
										<?php foreach($functions->get('system_currencies') as $item): ?>
										<a href="/index/currencie/<?php echo $item['currencie_id'] ?>" class="menu">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<span><?php echo $item['currencie_key'] ?></span>
													</div>
												</div>
												<?php if($item['currencie_id'] == $functions->currencie()): ?>
												<div class="control-addon">
													<div class="btn media small">
														<i class="zmdi zmdi-check"></i>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</a>
										<?php endforeach; ?>
									</div>
								</div>
								<div class="navigate" data-navigation="parent">
									<div class="menu hover" data-action="navigation">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $functions->languageInit('CommonTimezone') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<i class="toggle" data-navigation="toggle"></i>
											</div>
										</div>
									</div>
									<div class="navigation" data-navigation="slide">
										<?php foreach($functions->get('system_timezones') as $item): ?>
										<a href="/index/timezone/<?php echo $item['timezone_id'] ?>" class="menu">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<span><?php echo isset(json_decode($item['timezone_value'], true)[$functions->language()]) ? json_decode($item['timezone_value'], true)[$functions->language()] : json_decode($item['timezone_value'], true)[$functions->config('language')] ?></span>
													</div>
												</div>
												<?php if($item['timezone_id'] == $functions->timezone()): ?>
												<div class="control-addon">
													<div class="btn media small">
														<i class="zmdi zmdi-check"></i>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</a>
										<?php endforeach; ?>
									</div>
								</div>
								<i class="divider"></i>
								<?php foreach($account->accounts() as $key => $value): ?>
								<a href="/index/account/<?php echo $key ?>" class="menu">
									<div class="model">
										<span><?php echo $value['user_nickname'] ?></span>
									</div>
								</a>
								<?php endforeach; ?>
								<a href="/login" class="menu active">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-plus"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonPlus') ?></span>
											</div>
										</div>
									</div>
								</a>
								<i class="divider"></i>
								<a href="/logout" class="menu">
									<div class="control">
										<div class="control-addon">
											<div class="btn media">
												<i class="zmdi zmdi-power"></i>
											</div>
										</div>
										<div class="control-block">
											<div class="model">
												<span><?php echo $functions->languageInit('CommonLogout') ?></span>
											</div>
										</div>
									</div>
								</a>
							</div>
						</div>
						<?php else: ?>
						<div class="control-addon">
							<button type="button" class="btn second" data-toggle="dialog">
								<i class="zmdi zmdi-more"></i>
							</button>
							<div class="dropdown fade" data-ride="dialog" data-position="true">
								<div class="navigate" data-navigation="parent">
									<div class="menu hover" data-action="navigation">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $functions->languageInit('CommonLanguage') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<i class="toggle" data-navigation="toggle"></i>
											</div>
										</div>
									</div>
									<div class="navigation" data-navigation="slide">
										<?php foreach($functions->get('system_languages') as $item): ?>
										<a href="/index/language/<?php echo $item['language_id'] ?>" class="menu">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<span><?php echo $item['language_value'] ?></span>
													</div>
												</div>
												<?php if($item['language_id'] == $functions->language()): ?>
												<div class="control-addon">
													<div class="btn media small">
														<i class="zmdi zmdi-check"></i>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</a>
										<?php endforeach; ?>
									</div>
								</div>
								<div class="navigate" data-navigation="parent">
									<div class="menu hover" data-action="navigation">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $functions->languageInit('CommonCurrencie') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<i class="toggle" data-navigation="toggle"></i>
											</div>
										</div>
									</div>
									<div class="navigation" data-navigation="slide">
										<?php foreach($functions->get('system_currencies') as $item): ?>
										<a href="/index/currencie/<?php echo $item['currencie_id'] ?>" class="menu">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<span><?php echo $item['currencie_key'] ?></span>
													</div>
												</div>
												<?php if($item['currencie_id'] == $functions->currencie()): ?>
												<div class="control-addon">
													<div class="btn media small">
														<i class="zmdi zmdi-check"></i>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</a>
										<?php endforeach; ?>
									</div>
								</div>
								<div class="navigate" data-navigation="parent">
									<div class="menu hover" data-action="navigation">
										<div class="control">
											<div class="control-block">
												<div class="model">
													<span><?php echo $functions->languageInit('CommonTimezone') ?></span>
												</div>
											</div>
											<div class="control-addon">
												<i class="toggle" data-navigation="toggle"></i>
											</div>
										</div>
									</div>
									<div class="navigation" data-navigation="slide">
										<?php foreach($functions->get('system_timezones') as $item): ?>
										<a href="/index/timezone/<?php echo $item['timezone_id'] ?>" class="menu">
											<div class="control">
												<div class="control-block">
													<div class="model">
														<span><?php echo isset(json_decode($item['timezone_value'], true)[$functions->language()]) ? json_decode($item['timezone_value'], true)[$functions->language()] : json_decode($item['timezone_value'], true)[$functions->config('language')] ?></span>
													</div>
												</div>
												<?php if($item['timezone_id'] == $functions->timezone()): ?>
												<div class="control-addon">
													<div class="btn media small">
														<i class="zmdi zmdi-check"></i>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</a>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<a href="/" class="btn block">
					<span><?php echo $functions->languageInit('CommonIndex') ?></span>
				</a>
			</div>
		</div>
	</body>
</html>
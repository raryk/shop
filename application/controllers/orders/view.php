<?php
class viewController extends Controller {
	public function index($orderid = null) {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		
		if(!$this->functions->getTotal('shop_orders', array('order_id' => $orderid, 'user_id' => $this->account->getId()))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Main_ErrorOrdersTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/orders');
			exit;
		}
		
		$order = $this->functions->getBy('shop_orders', array('order_id' => $orderid));
		$this->data['order'] = $order;
		
		$section = array(
			'element' => 'orders'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/orders/view/index/' . $order['order_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Main_TitleOrdersView'));
		$this->document->setDescription($this->functions->languageInit('Main_DescriptionOrdersView'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/orders/view/index/' . $order['order_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/orders/view/index/' . $order['order_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('common/header');
		$this->data['footer'] = $this->action->child('common/footer');
		
		return $this->load->view('orders/view', $this->data);
	}
	
	public function ajax($csrf = null, $orderid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_orders', array('order_id' => $orderid, 'user_id' => $this->account->getId()))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Main_ErrorOrdersTotal');
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$order = $this->functions->getBy('shop_orders', array('order_id' => $orderid));
		
		if(!$order['order_status']) {
			$data = array(
				'id' => 7,
				'amount' => $this->functions->convert($order['order_amount'], 2, $order['order_currencie']),
				'currencie' => 'RUB',
				'email' => $order['order_email'],
				'key' => $order['order_id'],
				'description' => 'Order payment #' . $order['order_id']
			);
			ksort($data, SORT_STRING);
			$data['secret'] = 'B6A75631682599B5';
			$signature = base64_encode(md5(implode(':', $data), true));
			unset($data['secret']);
			$data['signature'] = $signature;
			
			$this->data['status'] = 'success';
			$this->data['url'] = 'https://flyxen.com/pay?' . http_build_query($data);
			return json_encode($this->data);
		} else {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Main_ErrorOrdersView');
			return json_encode($this->data);
		}
	}
	
	public function delete($csrf = null, $orderid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_orders', array('order_id' => $orderid, 'user_id' => $this->account->getId()))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Main_ErrorOrdersTotal');
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$this->functions->delete('shop_orders', array('order_secret' => $ordersecret));
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
}
?>
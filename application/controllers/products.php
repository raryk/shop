<?php
class productsController extends Controller {
	public function index() {
		$this->load->library('pagination');
		
		$page = isset($this->request->get['page']) && !is_array($this->request->get['page']) ? intval($this->request->get['page']) : 0;
		$limit = $this->functions->config('limit');
		$query = array();
		
		$getData = array(
			'product_status' => 1
		);
		
		$getSearch = array();
		
		$getJoins = array();
		
		$getSort = array(
			'product_id' => 'DESC'
		);
		
		$getOptions = array(
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);
		
		if(isset($this->request->get['q']) && !is_array($this->request->get['q'])) {
			$getSearch['product_id'] = $this->request->get['q'];
			$getSearch['product_title'] = $this->request->get['q'];
			$getSearch['product_description'] = $this->request->get['q'];
			$getSearch['product_text'] = $this->request->get['q'];
			$getSearch['product_price'] = $this->request->get['q'];
			$getSearch['product_content'] = $this->request->get['q'];
			
			$query['q'] = htmlspecialchars_decode($this->request->get['q']);
		}
		
		$total = $this->functions->getTotal('shop_products', $getData, $getSearch, $getJoins);
		$products = $this->functions->get('shop_products', $getData, $getSearch, $getJoins, $getSort, $getOptions);
		
		$paginationLib = new paginationLibrary();
		$paginationLib->url = '/products';
		$paginationLib->query = $query;
		$paginationLib->total = $total;
		$paginationLib->page = $page;
		$paginationLib->limit = $limit;
		
		$this->data['total'] = $total;
		$this->data['products'] = $products;
		$this->data['pagination'] = $paginationLib->render();
		
		$section = array(
			'element' => 'products'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/products?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Main_TitleProducts'));
		$this->document->setDescription($this->functions->languageInit('Main_DescriptionProducts'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain'));
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/products?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('common/header');
		$this->data['footer'] = $this->action->child('common/footer');
		
		return $this->load->view('index', $this->data);
	}
}
?>
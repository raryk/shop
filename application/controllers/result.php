<?php
class resultController extends Controller {
	public function index() {
		if(!isset($this->request->post['key']) || is_array($this->request->post['key'])) {
			return 'ERROR: key';
		}
		if(!isset($this->request->post['signature']) || is_array($this->request->post['signature'])) {
			return 'ERROR: signature';
		}
		if(!isset($this->request->post['status']) || is_array($this->request->post['status'])) {
			return 'ERROR: status';
		}
		
		foreach($this->request->post as $key => $value) {
			if($key != 'signature') {
				$data[$key] = htmlspecialchars_decode($value);
			}
		}
		
		ksort($data, SORT_STRING);
		array_push($data, 'jC0vAJp43vbbRwEY');
		
		if($this->request->post['signature'] != base64_encode(md5(implode(':', $data), true))) {
			return 'ERROR: Not signature';
		}
		
		if($this->request->post['status'] == 'success') {
			if(!$this->functions->getTotal('shop_orders', array('order_id' => $this->request->post['key'], 'order_status' => 0))) {
				return 'ERROR: Not found order';
			}
			
			$this->load->library('mail');
			
			$mailLib = new mailLibrary();
			
			$this->functions->update('shop_orders', array('order_status' => 1), array('order_id' => $this->request->post['key']));
			
			$order = $this->functions->getBy('shop_orders', array('order_id' => $this->request->post['key']));
			$mailData['order'] = $order;
			
			$mailData['account'] = $this->account;
			$mailData['request'] = $this->request;
			$mailData['functions'] = $this->functions;
			
			$mailLib->addTo($order['order_email'], $order['order_name']);
			$mailLib->setFrom($this->functions->config('mail_from'), $this->functions->config('mail_sender'));
			$mailLib->setSubject($this->functions->languageInit('SystemMailSubjectResult'));
			$mailLib->setText($this->load->view('mail/result', $mailData));
			
			if($this->functions->config('smtp')) {
				$mailLib->send(true, $this->functions->config('smtp_server'), $this->functions->config('smtp_username'), $this->functions->config('smtp_password'));
			} else {
				$mailLib->send();
			}
			
			return 'OK';
		} else {
			return 'ERROR: Not status';
		}
	}
}
?>
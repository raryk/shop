<?php
class productController extends Controller {
	public function index($productid = null) {
		if(!$this->functions->getTotal('shop_products', array('product_id' => $productid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Main_ErrorProductTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		
		$product = $this->functions->getBy('shop_products', array('product_id' => $productid), array(), array('shop_categories' => 'shop_products.product_categorie=shop_categories.categorie_id'));
		$this->data['product'] = $product;
		
		$section = array(
			'element' => 'product',
			'element_product' => $product['product_id']
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/product/index/' . $product['product_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle(isset(json_decode($product['product_title'], true)[$this->functions->language()]) ? json_decode($product['product_title'], true)[$this->functions->language()] : json_decode($product['product_title'], true)[$this->functions->config('language')]);
		$this->document->setDescription(isset(json_decode($product['product_description'], true)[$this->functions->language()]) ? json_decode($product['product_description'], true)[$this->functions->language()] : json_decode($product['product_description'], true)[$this->functions->config('language')]);
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/product/index/' . $product['product_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/product/index/' . $product['product_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('common/header');
		$this->data['footer'] = $this->action->child('common/footer');
		
		return $this->load->view('product', $this->data);
	}
}
?>
<?php
class editController extends Controller {
	public function index($userid = null) {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('users')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		if(!$this->functions->getTotal('users', array('user_id' => $userid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Admin_ErrorUsersTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin/users');
			exit;
		}
		
		$user = $this->functions->getBy('users', array('user_id' => $userid));
		$this->data['user'] = $user;
		
		$section = array(
			'element' => 'users',
			'element_users' => 'index'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/users/edit/index/' . $user['user_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleUsersEdit'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionUsersEdit'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/users/edit/index/' . $user['user_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/users/edit/index/' . $user['user_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/users/edit', $this->data);
	}
	
	public function ajax($csrf = null, $userid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('users')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('users', array('user_id' => $userid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorUsersTotal');
			return json_encode($this->data);
		}
		
		$validate = $this->validate($userid);
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$userData = array(
			'user_nickname' => $this->request->post['nickname'],
			'user_deliverie' => isset($this->request->post['deliverie']) && !is_array($this->request->post['deliverie']) && $this->functions->getTotal('shop_deliveries', array('deliverie_id' => $this->request->post['deliverie'], 'user_id' => $userid)) ? $this->request->post['deliverie'] : 0,
			'user_language' => $this->request->post['language'],
			'user_timezone' => $this->request->post['timezone'],
			'user_currencie' => $this->request->post['currencie'],
			'user_admin' => isset($this->request->post['admin']) && !is_array($this->request->post['admin']) && $this->request->post['admin'] ? 1 : 0,
			'user_level' => isset($this->request->post['level']) && !is_array($this->request->post['level']) && $this->functions->getTotal('system_levels', array('level_id' => $this->request->post['level'])) ? $this->request->post['level'] : 0,
			'user_email' => isset($this->request->post['email']) && !is_array($this->request->post['email']) && mb_strlen(trim($this->request->post['email'])) >= 1 ? $this->request->post['email'] : null,
			'user_twa' => isset($this->request->post['twa']) && !is_array($this->request->post['twa']) && $this->request->post['twa'] ? 1 : 0,
			'user_ip' => $this->request->post['ip']
		);
		$this->functions->update('users', $userData, array('user_id' => $userid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessUsersEdit');
		return json_encode($this->data);
	}
	
	public function ban($csrf = null, $userid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('users')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('users', array('user_id' => $userid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorUsersTotal');
			return json_encode($this->data);
		}
		
		$validateBan = $this->validateBan();
		if(!is_null($validateBan)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validateBan;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$this->functions->update('users_bans', array('ban_status' => 0), array('user_id' => $userid));
		
		if(isset($this->request->post['time']) && is_array($this->request->post['time']) && isset($this->request->post['time']['year']) && !is_array($this->request->post['time']['year']) && $this->request->post['time']['year'] && isset($this->request->post['time']['month']) && !is_array($this->request->post['time']['month']) && $this->request->post['time']['month'] && isset($this->request->post['time']['day']) && !is_array($this->request->post['time']['day']) && $this->request->post['time']['day'] && isset($this->request->post['time']['hour']) && !is_array($this->request->post['time']['hour']) && $this->request->post['time']['hour'] && isset($this->request->post['time']['minute']) && !is_array($this->request->post['time']['minute']) && $this->request->post['time']['minute'] && isset($this->request->post['time']['second']) && !is_array($this->request->post['time']['second']) && $this->request->post['time']['second']) {
			$timeFormat = $this->functions->isHour12() ? (new DateTime())->createFromFormat('Y-m-d g:i:s A', $this->request->post['time']['year'] . '-' . $this->request->post['time']['month'] . '-' . $this->request->post['time']['day'] . ' ' . $this->request->post['time']['hour'] . ':' . $this->request->post['time']['minute'] . ':' . $this->request->post['time']['second'] . ($this->request->post['time']['meridiem'] ? ' PM' : ' AM')) : (new DateTime())->createFromFormat('Y-m-d H:i:s', $this->request->post['time']['year'] . '-' . $this->request->post['time']['month'] . '-' . $this->request->post['time']['day'] . ' ' . $this->request->post['time']['hour'] . ':' . $this->request->post['time']['minute'] . ':' . $this->request->post['time']['second']);
			
			$time = $this->functions->time($timeFormat->format($this->functions->isHour12() ? 'Y-m-d g:i:s A' : 'Y-m-d H:i:s'), true);
		} else {
			$time = 0;
		}
		
		$banData = array(
			'user_id' => $userid,
			'ban_status' => 1,
			'ban_time' => $time,
			'ban_text' => $this->request->post['text']
		);
		$this->functions->create('users_bans', $banData, 'ban_date_add');
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessUsersEditBan');
		return json_encode($this->data);
	}
	
	public function password($csrf = null, $userid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('users')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('users', array('user_id' => $userid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorUsersTotal');
			return json_encode($this->data);
		}
		
		$validatePassword = $this->validatePassword();
		if(!is_null($validatePassword)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validatePassword;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$userData = array(
			'user_password' => md5($this->request->post['password'])
		);
		$this->functions->update('users', $userData, array('user_id' => $userid));
		
		if(isset($this->request->post['tokens']) && !is_array($this->request->post['tokens']) && $this->request->post['tokens']) {
			$this->functions->delete('users_tokens', array('user_id' => $userid));
		}
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessUsersEditPassword');
		return json_encode($this->data);
	}
	
	public function unban($csrf = null, $userid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('users')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('users', array('user_id' => $userid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorUsersTotal');
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$this->functions->update('users_bans', array('ban_status' => 0), array('user_id' => $userid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessUsersEditUnban');
		return json_encode($this->data);
	}
	
	private function validate($userid) {
		$result = null;
		
		$user = $this->functions->getBy('users', array('user_id' => $userid));
		
		if(!isset($this->request->post['nickname']) || is_array($this->request->post['nickname']) || !preg_match('/^[a-zA-Z0-9_\.-]{3,32}$/', $this->request->post['nickname'])) {
			$result[] = array('key' => 'nickname', 'value' => $this->functions->languageInit('Admin_ErrorUsersNickname'));
		} elseif(mb_strtolower($user['user_nickname']) != mb_strtolower($this->request->post['nickname']) && $this->functions->getTotal('users', array('user_nickname' => $this->request->post['nickname']))) {
			$result[] = array('key' => 'nickname', 'value' => $this->functions->languageInit('Admin_ErrorUsersNicknameTotal'));
		}
		if(!isset($this->request->post['language']) || is_array($this->request->post['language']) || !$this->functions->getTotal('system_languages', array('language_id' => $this->request->post['language']))) {
			$result[] = array('key' => 'language', 'value' => $this->functions->languageInit('Admin_ErrorUsersLanguage'));
		}
		if(!isset($this->request->post['timezone']) || is_array($this->request->post['timezone']) || !$this->functions->getTotal('system_timezones', array('timezone_id' => $this->request->post['timezone']))) {
			$result[] = array('key' => 'timezone', 'value' => $this->functions->languageInit('Admin_ErrorUsersTimezone'));
		}
		if(!isset($this->request->post['currencie']) || is_array($this->request->post['currencie']) || !$this->functions->getTotal('system_currencies', array('currencie_id' => $this->request->post['currencie']))) {
			$result[] = array('key' => 'currencie', 'value' => $this->functions->languageInit('Admin_ErrorUsersCurrencie'));
		}
		if(isset($this->request->post['email']) && !is_array($this->request->post['email']) && mb_strlen(trim($this->request->post['email'])) >= 1) {
			if(!preg_match('/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{1,8})$/i', $this->request->post['email'])) {
				$result[] = array('key' => 'email', 'value' => $this->functions->languageInit('Admin_ErrorUsersEmail'));
			} elseif(mb_strtolower($user['user_email']) != mb_strtolower($this->request->post['email']) && $this->functions->getTotal('users', array('user_email' => $this->request->post['email']))) {
				$result[] = array('key' => 'email', 'value' => $this->functions->languageInit('Admin_ErrorUsersEmailTotal'));
			}
		}
		if(!isset($this->request->post['ip']) || is_array($this->request->post['ip']) || !preg_match('/^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$/', $this->request->post['ip'])) {
			$result[] = array('key' => 'ip', 'value' => $this->functions->languageInit('Admin_ErrorUsersIp'));
		}
		
		return $result;
	}
	
	private function validateBan() {
		$result = null;
		
		if(isset($this->request->post['time']) && is_array($this->request->post['time']) && isset($this->request->post['time']['year']) && !is_array($this->request->post['time']['year']) && $this->request->post['time']['year'] && isset($this->request->post['time']['month']) && !is_array($this->request->post['time']['month']) && $this->request->post['time']['month'] && isset($this->request->post['time']['day']) && !is_array($this->request->post['time']['day']) && $this->request->post['time']['day'] && isset($this->request->post['time']['hour']) && !is_array($this->request->post['time']['hour']) && $this->request->post['time']['hour'] && isset($this->request->post['time']['minute']) && !is_array($this->request->post['time']['minute']) && $this->request->post['time']['minute'] && isset($this->request->post['time']['second']) && !is_array($this->request->post['time']['second']) && $this->request->post['time']['second']) {
			$timeFormat = $this->functions->isHour12() ? (new DateTime())->createFromFormat('Y-m-d g:i:s A', $this->request->post['time']['year'] . '-' . $this->request->post['time']['month'] . '-' . $this->request->post['time']['day'] . ' ' . $this->request->post['time']['hour'] . ':' . $this->request->post['time']['minute'] . ':' . $this->request->post['time']['second'] . ($this->request->post['time']['meridiem'] ? ' PM' : ' AM')) : (new DateTime())->createFromFormat('Y-m-d H:i:s', $this->request->post['time']['year'] . '-' . $this->request->post['time']['month'] . '-' . $this->request->post['time']['day'] . ' ' . $this->request->post['time']['hour'] . ':' . $this->request->post['time']['minute'] . ':' . $this->request->post['time']['second']);
			
			if(!$timeFormat || $this->functions->isHour12() ? $timeFormat->format('Y-m-d g:i:s A') != $this->request->post['time']['year'] . '-' . $this->request->post['time']['month'] . '-' . $this->request->post['time']['day'] . ' ' . $this->request->post['time']['hour'] . ':' . $this->request->post['time']['minute'] . ':' . $this->request->post['time']['second'] . ($this->request->post['time']['meridiem'] ? ' PM' : ' AM') || $this->functions->time() >= $this->functions->time($timeFormat->format('Y-m-d g:i:s A'), true) : $timeFormat->format('Y-m-d H:i:s') != $this->request->post['time']['year'] . '-' . $this->request->post['time']['month'] . '-' . $this->request->post['time']['day'] . ' ' . $this->request->post['time']['hour'] . ':' . $this->request->post['time']['minute'] . ':' . $this->request->post['time']['second'] || $this->request->post['time']['year'] < $this->functions->config('date_min') || $this->request->post['time']['year'] > $this->functions->config('date_max') || $this->functions->time() >= $this->functions->time($timeFormat->format('Y-m-d H:i:s'), true)) {
				$result[] = array('key' => 'time', 'value' => $this->functions->languageInit('Admin_ErrorUsersTime'));
			}
		}
		if(!isset($this->request->post['text']) || is_array($this->request->post['text']) || mb_strlen(trim($this->request->post['text'])) < 1 || mb_strlen(trim($this->request->post['text'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'text', 'value' => $this->functions->languageInit('Admin_ErrorUsersText'));
		}
		
		return $result;
	}
	
	private function validatePassword() {
		$result = null;
		
		if(!isset($this->request->post['password']) || is_array($this->request->post['password']) || mb_strlen(trim($this->request->post['password'])) < 6 || mb_strlen(trim($this->request->post['password'])) > 64) {
			$result[] = array('key' => 'password', 'value' => $this->functions->languageInit('Admin_ErrorUsersPassword'));
		} elseif(!isset($this->request->post['password2']) || is_array($this->request->post['password2']) || $this->request->post['password'] != $this->request->post['password2']) {
			$result[] = array('key' => 'password2', 'value' => $this->functions->languageInit('Admin_ErrorUsersPassword2'));
		}
		
		return $result;
	}
}
?>
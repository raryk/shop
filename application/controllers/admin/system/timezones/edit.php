<?php
class editController extends Controller {
	public function index($timezoneid = null) {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('system')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		if(!$this->functions->getTotal('system_timezones', array('timezone_id' => $timezoneid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Admin_ErrorSystemTimezonesTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/timezones');
			exit;
		}
		
		$timezone = $this->functions->getBy('system_timezones', array('timezone_id' => $timezoneid));
		$this->data['timezone'] = $timezone;
		
		$section = array(
			'element' => 'system',
			'element_system' => 'timezones'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/timezones/edit/index/' . $timezone['timezone_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleSystemTimezonesEdit'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionSystemTimezonesEdit'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/timezones/edit/index/' . $timezone['timezone_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/timezones/edit/index/' . $timezone['timezone_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/system/timezones/edit', $this->data);
	}
	
	public function ajax($csrf = null, $timezoneid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('system')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('system_timezones', array('timezone_id' => $timezoneid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorSystemTimezonesTotal');
			return json_encode($this->data);
		}
		
		$validate = $this->validate($timezoneid);
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$value = array();
		if(isset($this->request->post['value']) && is_array($this->request->post['value'])) {
			foreach($this->request->post['value'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$value[$k] = $v;
				}
			}
		}
		
		$timezoneData = array(
			'timezone_key' => $this->request->post['key'],
			'timezone_value' => json_encode($value)
		);
		$this->functions->update('system_timezones', $timezoneData, array('timezone_id' => $timezoneid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessSystemTimezonesEdit');
		return json_encode($this->data);
	}
	
	private function validate($timezoneid) {
		$result = null;
		
		$timezone = $this->functions->getBy('system_timezones', array('timezone_id' => $timezoneid));
		
		$value = array();
		if(isset($this->request->post['value']) && is_array($this->request->post['value'])) {
			foreach($this->request->post['value'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$value[$k] = $v;
				}
			}
		}
		
		if(!isset($this->request->post['key']) || is_array($this->request->post['key']) || mb_strlen(trim($this->request->post['key'])) < 1 || mb_strlen(trim($this->request->post['key'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'key', 'value' => $this->functions->languageInit('Admin_ErrorSystemTimezonesKey'));
		} elseif(mb_strtolower($timezone['timezone_key']) != mb_strtolower($this->request->post['key']) && $this->functions->getTotal('system_timezones', array('timezone_key' => $this->request->post['key']))) {
			$result[] = array('key' => 'key', 'value' => $this->functions->languageInit('Admin_ErrorSystemTimezonesKeyTotal'));
		}
		if(empty($value) || mb_strlen(trim(json_encode($value))) > $this->functions->config('count')) {
			$result[] = array('key' => 'value', 'value' => $this->functions->languageInit('Admin_ErrorSystemTimezonesValue'));
		}
		
		return $result;
	}
}
?>
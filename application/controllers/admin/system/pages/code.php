<?php
class codeController extends Controller {
	public function index($languageid = null, $pageid = null) {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('system')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		if(!$this->functions->getTotal('system_languages', array('language_id' => $languageid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Admin_ErrorSystemLanguagesTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/languages');
			exit;
		}
		if(!$this->functions->getTotal('system_pages', array('page_id' => $pageid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Admin_ErrorSystemPagesTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/pages');
			exit;
		}
		
		$language = $this->functions->getBy('system_languages', array('language_id' => $languageid));
		$page = $this->functions->getBy('system_pages', array('page_id' => $pageid));
		$this->data['language'] = $language;
		$this->data['page'] = $page;
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		return $this->load->view('admin/system/pages/code', $this->data);
	}
	
	public function ajax($csrf = null, $languageid = null, $pageid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('system')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('system_languages', array('language_id' => $languageid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorSystemLanguagesTotal');
			return json_encode($this->data);
		}
		if(!$this->functions->getTotal('system_pages', array('page_id' => $pageid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorSystemPagesTotal');
			return json_encode($this->data);
		}
		
		$validate = $this->validate($languageid, $pageid);
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$page = $this->functions->getBy('system_pages', array('page_id' => $pageid));
		
		$code = json_decode($page['page_code'], true);
		$code[$languageid] = $this->request->post['code'];
		
		$this->functions->update('system_pages', array('page_code' => json_encode($code)), array('page_id' => $page['page_id']));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessSystemPagesCode');
		return json_encode($this->data);
	}
	
	private function validate($languageid, $pageid) {
		$result = null;
		
		$page = $this->functions->getBy('system_pages', array('page_id' => $pageid));
		
		$code = json_decode($page['page_code'], true);
		$code[$languageid] = $this->request->post['code'];
		
		if(mb_strlen(trim(json_encode($code))) > $this->functions->config('count')) {
			$result = $this->functions->languageInit('Admin_ErrorSystemPagesCode');
		}
		
		return $result;
	}
}
?>
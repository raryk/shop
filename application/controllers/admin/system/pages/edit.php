<?php
class editController extends Controller {
	public function index($pageid = null) {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('system')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		if(!$this->functions->getTotal('system_pages', array('page_id' => $pageid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Admin_ErrorSystemPagesTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/pages');
			exit;
		}
		
		$page = $this->functions->getBy('system_pages', array('page_id' => $pageid));
		$this->data['page'] = $page;
		
		$section = array(
			'element' => 'system',
			'element_system' => 'pages'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/pages/edit/index/' . $page['page_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleSystemPagesEdit'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionSystemPagesEdit'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/pages/edit/index/' . $page['page_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/pages/edit/index/' . $page['page_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/system/pages/edit', $this->data);
	}
	
	public function ajax($csrf = null, $pageid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('system')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('system_pages', array('page_id' => $pageid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorSystemPagesTotal');
			return json_encode($this->data);
		}
		
		$validate = $this->validate($pageid);
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$title = array();
		if(isset($this->request->post['title']) && is_array($this->request->post['title'])) {
			foreach($this->request->post['title'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$title[$k] = $v;
				}
			}
		}
		$description = array();
		if(isset($this->request->post['description']) && is_array($this->request->post['description'])) {
			foreach($this->request->post['description'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$description[$k] = $v;
				}
			}
		}
		
		$pageData = array(
			'page_address' => $this->request->post['address'],
			'page_title' => json_encode($title),
			'page_description' => json_encode($description)
		);
		$this->functions->update('system_pages', $pageData, array('page_id' => $pageid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessSystemPagesEdit');
		return json_encode($this->data);
	}
	
	public function section($csrf = null, $pageid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('system')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('system_pages', array('page_id' => $pageid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorSystemPagesTotal');
			return json_encode($this->data);
		}
		
		$validateSection = $this->validateSection();
		if(!is_null($validateSection)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validateSection;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$section = array();
		if(isset($this->request->post['section']) && is_array($this->request->post['section']) && isset($this->request->post['section']['key']) && is_array($this->request->post['section']['key']) && isset($this->request->post['section']['value']) && is_array($this->request->post['section']['value'])) {
			for($i = 0; $i < max(count($this->request->post['section']['key']), count($this->request->post['section']['value'])); $i++) {
				if(isset($this->request->post['section']['key'][$i]) && !is_array($this->request->post['section']['key'][$i]) && mb_strlen(trim($this->request->post['section']['key'][$i])) >= 1 && isset($this->request->post['section']['value'][$i]) && !is_array($this->request->post['section']['value'][$i]) && mb_strlen(trim($this->request->post['section']['value'][$i])) >= 1) {
					$section[] = array('key' => $this->request->post['section']['key'][$i], 'value' => $this->request->post['section']['value'][$i]);
				}
			}
		}
		
		$this->functions->update('system_pages', array('page_section' => json_encode($section)), array('page_id' => $pageid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessSystemPagesEditSection');
		return json_encode($this->data);
	}
	
	public function common($csrf = null, $pageid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('system')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('system_pages', array('page_id' => $pageid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorSystemPagesTotal');
			return json_encode($this->data);
		}
		
		$validateCommon = $this->validateCommon();
		if(!is_null($validateCommon)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validateCommon;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$common = array();
		if(isset($this->request->post['common']) && is_array($this->request->post['common']) && isset($this->request->post['common']['key']) && is_array($this->request->post['common']['key']) && isset($this->request->post['common']['value']) && is_array($this->request->post['common']['value'])) {
			for($i = 0; $i < max(count($this->request->post['common']['key']), count($this->request->post['common']['value'])); $i++) {
				if(isset($this->request->post['common']['key'][$i]) && !is_array($this->request->post['common']['key'][$i]) && mb_strlen(trim($this->request->post['common']['key'][$i])) >= 1 && isset($this->request->post['common']['value'][$i]) && !is_array($this->request->post['common']['value'][$i]) && mb_strlen(trim($this->request->post['common']['value'][$i])) >= 1) {
					$common[] = array('key' => $this->request->post['common']['key'][$i], 'value' => $this->request->post['common']['value'][$i]);
				}
			}
		}
		
		$this->functions->update('system_pages', array('page_common' => json_encode($common)), array('page_id' => $pageid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessSystemPagesEditCommon');
		return json_encode($this->data);
	}
	
	private function validate($pageid) {
		$result = null;
		
		$page = $this->functions->getBy('system_pages', array('page_id' => $pageid));
		
		$title = array();
		if(isset($this->request->post['title']) && is_array($this->request->post['title'])) {
			foreach($this->request->post['title'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$title[$k] = $v;
				}
			}
		}
		$description = array();
		if(isset($this->request->post['description']) && is_array($this->request->post['description'])) {
			foreach($this->request->post['description'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$description[$k] = $v;
				}
			}
		}
		
		if(!isset($this->request->post['address']) || is_array($this->request->post['address']) || !preg_match('/^[a-zA-Zа-яА-Я0-9]{1,64}$/', $this->request->post['address'])) {
			$result[] = array('key' => 'address', 'value' => $this->functions->languageInit('Admin_ErrorSystemPagesAddress'));
		} elseif(mb_strtolower($page['page_address']) != mb_strtolower($this->request->post['address']) && $this->functions->getTotal('system_pages', array('page_address' => $this->request->post['address']))) {
			$result[] = array('key' => 'address', 'value' => $this->functions->languageInit('Admin_ErrorSystemPagesAddressTotal'));
		}
		if(empty($title) || mb_strlen(trim(json_encode($title))) > $this->functions->config('count')) {
			$result[] = array('key' => 'title', 'value' => $this->functions->languageInit('Admin_ErrorSystemPagesTitle'));
		}
		if(empty($description) || mb_strlen(trim(json_encode($description))) > $this->functions->config('count')) {
			$result[] = array('key' => 'description', 'value' => $this->functions->languageInit('Admin_ErrorSystemPagesDescription'));
		}
		
		return $result;
	}
	
	private function validateSection() {
		$result = null;
		
		$section = array();
		if(isset($this->request->post['section']) && is_array($this->request->post['section']) && isset($this->request->post['section']['key']) && is_array($this->request->post['section']['key']) && isset($this->request->post['section']['value']) && is_array($this->request->post['section']['value'])) {
			for($i = 0; $i < max(count($this->request->post['section']['key']), count($this->request->post['section']['value'])); $i++) {
				if(isset($this->request->post['section']['key'][$i]) && !is_array($this->request->post['section']['key'][$i]) && mb_strlen(trim($this->request->post['section']['key'][$i])) >= 1 && isset($this->request->post['section']['value'][$i]) && !is_array($this->request->post['section']['value'][$i]) && mb_strlen(trim($this->request->post['section']['value'][$i])) >= 1) {
					$section[] = array('key' => $this->request->post['section']['key'][$i], 'value' => $this->request->post['section']['value'][$i]);
				}
			}
		}
		
		if(mb_strlen(trim(json_encode($section))) > $this->functions->config('count')) {
			$result = $this->functions->languageInit('Admin_ErrorSystemPagesSection');
		}
		
		return $result;
	}
	
	private function validateCommon() {
		$result = null;
		
		$common = array();
		if(isset($this->request->post['common']) && is_array($this->request->post['common']) && isset($this->request->post['common']['key']) && is_array($this->request->post['common']['key']) && isset($this->request->post['common']['value']) && is_array($this->request->post['common']['value'])) {
			for($i = 0; $i < max(count($this->request->post['common']['key']), count($this->request->post['common']['value'])); $i++) {
				if(isset($this->request->post['common']['key'][$i]) && !is_array($this->request->post['common']['key'][$i]) && mb_strlen(trim($this->request->post['common']['key'][$i])) >= 1 && isset($this->request->post['common']['value'][$i]) && !is_array($this->request->post['common']['value'][$i]) && mb_strlen(trim($this->request->post['common']['value'][$i])) >= 1) {
					$common[] = array('key' => $this->request->post['common']['key'][$i], 'value' => $this->request->post['common']['value'][$i]);
				}
			}
		}
		
		if(mb_strlen(trim(json_encode($common))) > $this->functions->config('count')) {
			$result = $this->functions->languageInit('Admin_ErrorSystemPagesCommon');
		}
		
		return $result;
	}
}
?>
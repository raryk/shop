<?php
class addController extends Controller {
	public function index() {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('system')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		$section = array(
			'element' => 'system',
			'element_system' => 'pages'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/pages/add?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleSystemPagesAdd'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionSystemPagesAdd'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/pages/add');
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/pages/add?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/system/pages/add', $this->data);
	}
	
	public function ajax($csrf = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('system')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		$validate = $this->validate();
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$title = array();
		if(isset($this->request->post['title']) && is_array($this->request->post['title'])) {
			foreach($this->request->post['title'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$title[$k] = $v;
				}
			}
		}
		$description = array();
		if(isset($this->request->post['description']) && is_array($this->request->post['description'])) {
			foreach($this->request->post['description'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$description[$k] = $v;
				}
			}
		}
		
		$pageData = array(
			'page_address' => $this->request->post['address'],
			'page_title' => json_encode($title),
			'page_description' => json_encode($description),
			'page_section' => json_encode(array()),
			'page_common' => json_encode(array()),
			'page_code' => json_encode(array())
		);
		$this->functions->create('system_pages', $pageData, 'page_date_add');
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
	
	private function validate() {
		$result = null;
		
		$title = array();
		if(isset($this->request->post['title']) && is_array($this->request->post['title'])) {
			foreach($this->request->post['title'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$title[$k] = $v;
				}
			}
		}
		$description = array();
		if(isset($this->request->post['description']) && is_array($this->request->post['description'])) {
			foreach($this->request->post['description'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$description[$k] = $v;
				}
			}
		}
		
		if(!isset($this->request->post['address']) || is_array($this->request->post['address']) || !preg_match('/^[a-zA-Zа-яА-Я0-9]{1,64}$/', $this->request->post['address'])) {
			$result[] = array('key' => 'address', 'value' => $this->functions->languageInit('Admin_ErrorSystemPagesAddress'));
		} elseif($this->functions->getTotal('system_pages', array('page_address' => $this->request->post['address']))) {
			$result[] = array('key' => 'address', 'value' => $this->functions->languageInit('Admin_ErrorSystemPagesAddressTotal'));
		}
		if(empty($title) || mb_strlen(trim(json_encode($title))) > $this->functions->config('count')) {
			$result[] = array('key' => 'title', 'value' => $this->functions->languageInit('Admin_ErrorSystemPagesTitle'));
		}
		if(empty($description) || mb_strlen(trim(json_encode($description))) > $this->functions->config('count')) {
			$result[] = array('key' => 'description', 'value' => $this->functions->languageInit('Admin_ErrorSystemPagesDescription'));
		}
		
		return $result;
	}
}
?>
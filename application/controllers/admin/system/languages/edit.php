<?php
class editController extends Controller {
	public function index($languageid = null) {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('system')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		if(!$this->functions->getTotal('system_languages', array('language_id' => $languageid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Admin_ErrorSystemLanguagesTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/languages');
			exit;
		}
		
		$language = $this->functions->getBy('system_languages', array('language_id' => $languageid));
		$this->data['language'] = $language;
		
		$section = array(
			'element' => 'system',
			'element_system' => 'languages'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/languages/edit/index/' . $language['language_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleSystemLanguagesEdit'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionSystemLanguagesEdit'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/languages/edit/index/' . $language['language_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/languages/edit/index/' . $language['language_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/system/languages/edit', $this->data);
	}
	
	public function ajax($csrf = null, $languageid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('system')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('system_languages', array('language_id' => $languageid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorSystemLanguagesTotal');
			return json_encode($this->data);
		}
		
		$validate = $this->validate($languageid);
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$languageData = array(
			'language_hour12' => isset($this->request->post['hour12']) && !is_array($this->request->post['hour12']) && $this->request->post['hour12'] ? 1 : 0,
			'language_rtl' => isset($this->request->post['rtl']) && !is_array($this->request->post['rtl']) && $this->request->post['rtl'] ? 1 : 0,
			'language_key' => $this->request->post['key'],
			'language_value' => $this->request->post['value']
		);
		$this->functions->update('system_languages', $languageData, array('language_id' => $languageid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessSystemLanguagesEdit');
		return json_encode($this->data);
	}
	
	private function validate($languageid) {
		$result = null;
		
		$language = $this->functions->getBy('system_languages', array('language_id' => $languageid));
		
		if(!isset($this->request->post['key']) || is_array($this->request->post['key']) || mb_strlen(trim($this->request->post['key'])) < 1 || mb_strlen(trim($this->request->post['key'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'key', 'value' => $this->functions->languageInit('Admin_ErrorSystemLanguagesKey'));
		} elseif(mb_strtolower($language['language_key']) != mb_strtolower($this->request->post['key']) && $this->functions->getTotal('system_languages', array('language_key' => $this->request->post['key']))) {
			$result[] = array('key' => 'key', 'value' => $this->functions->languageInit('Admin_ErrorSystemLanguagesKeyTotal'));
		}
		if(!isset($this->request->post['value']) || is_array($this->request->post['value']) || mb_strlen(trim($this->request->post['value'])) < 1 || mb_strlen(trim($this->request->post['value'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'value', 'value' => $this->functions->languageInit('Admin_ErrorSystemLanguagesValue'));
		}
		
		return $result;
	}
}
?>
<?php
class addController extends Controller {
	public function index() {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('system')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		$section = array(
			'element' => 'system',
			'element_system' => 'currencies'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/currencies/add?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleSystemCurrenciesAdd'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionSystemCurrenciesAdd'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/currencies/add');
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/system/currencies/add?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/system/currencies/add', $this->data);
	}
	
	public function ajax($csrf = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('system')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		$validate = $this->validate();
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$currencieData = array(
			'currencie_name' => $this->request->post['name'],
			'currencie_key' => $this->request->post['key'],
			'currencie_value' => $this->request->post['value']
		);
		$this->functions->create('system_currencies', $currencieData, 'currencie_date_add');
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
	
	private function validate() {
		$result = null;
		
		if(!isset($this->request->post['name']) || is_array($this->request->post['name']) || mb_strlen(trim($this->request->post['name'])) < 1 || mb_strlen(trim($this->request->post['name'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'name', 'value' => $this->functions->languageInit('Admin_ErrorSystemCurrenciesName'));
		}
		if(!isset($this->request->post['key']) || is_array($this->request->post['key']) || mb_strlen(trim($this->request->post['key'])) < 1 || mb_strlen(trim($this->request->post['key'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'key', 'value' => $this->functions->languageInit('Admin_ErrorSystemCurrenciesKey'));
		} elseif($this->functions->getTotal('system_currencies', array('currencie_key' => $this->request->post['key']))) {
			$result[] = array('key' => 'key', 'value' => $this->functions->languageInit('Admin_ErrorSystemCurrenciesKeyTotal'));
		}
		if(!isset($this->request->post['value']) || is_array($this->request->post['value']) || !preg_match('/^([0-9]{1,10})(\.[0-9]{2})?$/', $this->request->post['value'])) {
			$result[] = array('key' => 'value', 'value' => $this->functions->languageInit('Admin_ErrorSystemCurrenciesValue'));
		}
		
		return $result;
	}
}
?>
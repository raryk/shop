<?php
class footerController extends Controller {
	public function index() {
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		return $this->load->view('admin/common/footer', $this->data);
	}
}
?>
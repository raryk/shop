<?php
class headerController extends Controller {
	public function index() {
		$this->data['title'] = $this->document->getTitle();
		$this->data['description'] = $this->document->getDescription();
		$this->data['section'] = $this->document->getSection();
		$this->data['url'] = $this->document->getUrl();
		$this->data['canonical'] = $this->document->getCanonical();
		$this->data['alternate'] = $this->document->getAlternate();
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		if(isset($this->session->data['alert'])) {
			$this->data['alert'] = $this->session->data['alert'];
			unset($this->session->data['alert']);
		}
		
		return $this->load->view('admin/common/header', $this->data);
	}
}
?>
<?php
class editController extends Controller {
	public function index($serverid = null) {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('servers')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		if(!$this->functions->getTotal('upload_servers', array('server_id' => $serverid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Admin_ErrorUploadServersTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin/upload/servers');
			exit;
		}
		
		$server = $this->functions->getBy('upload_servers', array('server_id' => $serverid));
		$this->data['server'] = $server;
		
		$section = array(
			'element' => 'upload',
			'element_upload' => 'servers'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/upload/servers/edit/index/' . $server['server_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleUploadServersEdit'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionUploadServersEdit'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/upload/servers/edit/index/' . $server['server_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/upload/servers/edit/index/' . $server['server_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/upload/servers/edit', $this->data);
	}
	
	public function ajax($csrf = null, $serverid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('servers')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('upload_servers', array('server_id' => $serverid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorUploadServersTotal');
			return json_encode($this->data);
		}
		
		$validate = $this->validate();
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$serverData = array(
			'server_url' => $this->request->post['url']
		);
		$this->functions->update('upload_servers', $serverData, array('server_id' => $serverid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessUploadServersEdit');
		return json_encode($this->data);
	}
	
	private function validate() {
		$result = null;
		
		if(!isset($this->request->post['url']) || is_array($this->request->post['url']) || !preg_match('/^https?:\/\/[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]$/i', $this->request->post['url'])) {
			$result[] = array('key' => 'url', 'value' => $this->functions->languageInit('Admin_ErrorUploadServersUrl'));
		}
		
		return $result;
	}
}
?>
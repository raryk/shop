<?php
class viewController extends Controller {
	public function index($orderid = null) {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('shop')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		if(!$this->functions->getTotal('shop_orders', array('order_id' => $orderid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Admin_ErrorShopOrdersTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/orders');
			exit;
		}
		
		$order = $this->functions->getBy('shop_orders', array('order_id' => $orderid), array(), array('shop_coupons' => 'shop_orders.order_coupon=shop_coupons.coupon_id', 'users' => 'shop_orders.user_id=users.user_id'));
		$this->data['order'] = $order;
		
		$section = array(
			'element' => 'shop',
			'element_shop' => 'orders'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/orders/edit/index/' . $order['order_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleShopOrdersView'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionShopOrdersView'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/orders/edit/index/' . $order['order_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/orders/edit/index/' . $order['order_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/shop/orders/view', $this->data);
	}
	
	public function ajax($csrf = null, $orderid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('shop')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_orders', array('order_id' => $orderid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorShopOrdersTotal');
			return json_encode($this->data);
		}
		
		$validate = $this->validate();
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$orderData = array(
			'order_status' => $this->request->post['status'],
			'order_email' => $this->request->post['email'],
			'order_text' => isset($this->request->post['text']) && !is_array($this->request->post['text']) && mb_strlen(trim($this->request->post['text'])) >= 1 ? $this->request->post['text'] : null,
			'order_track' => isset($this->request->post['track']) && !is_array($this->request->post['track']) && mb_strlen(trim($this->request->post['track'])) >= 1 ? $this->request->post['track'] : null
		);
		$this->functions->update('shop_orders', $orderData, array('order_id' => $orderid));
		
		if(isset($this->request->post['mail']) && !is_array($this->request->post['mail']) && $this->request->post['mail']) {
			$this->load->library('mail');
			
			$mailLib = new mailLibrary();
			
			$order = $this->functions->getBy('shop_orders', array('order_id' => $orderid));
			$mailData['order'] = $order;
			
			$mailData['account'] = $this->account;
			$mailData['request'] = $this->request;
			$mailData['functions'] = $this->functions;
			
			$mailLib->addTo($order['order_email'], $order['order_name']);
			$mailLib->setFrom($this->functions->config('mail_from'), $this->functions->config('mail_sender'));
			$mailLib->setSubject($this->functions->languageInit('SystemMailSubjectView'));
			$mailLib->setText($this->load->view('mail/view', $mailData));
			
			if($this->functions->config('smtp')) {
				$mailLib->send(true, $this->functions->config('smtp_server'), $this->functions->config('smtp_username'), $this->functions->config('smtp_password'));
			} else {
				$mailLib->send();
			}
		}
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessShopOrdersView');
		return json_encode($this->data);
	}
	
	public function delete($csrf = null, $orderid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('shop')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_orders', array('order_id' => $orderid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorShopOrdersTotal');
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$this->functions->delete('shop_orders', array('order_id' => $orderid));
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
	
	private function validate() {
		$result = null;
		
		if(!isset($this->request->post['status']) || is_array($this->request->post['status']) || $this->request->post['status'] < 0 || $this->request->post['status'] > 2) {
			$result[] = array('key' => 'status', 'value' => $this->functions->languageInit('Admin_ErrorShopOrdersStatus'));
		}
		if(!isset($this->request->post['email']) || is_array($this->request->post['email']) || !preg_match('/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{1,8})$/i', $this->request->post['email'])) {
			$result[] = array('key' => 'email', 'value' => $this->functions->languageInit('Admin_ErrorShopOrdersEmail'));
		}
		if(isset($this->request->post['text']) && !is_array($this->request->post['text']) && mb_strlen(trim($this->request->post['text'])) >= 1) {
			if(mb_strlen(trim($this->request->post['text'])) > $this->functions->config('count')) {
				$result[] = array('key' => 'text', 'value' => $this->functions->languageInit('Admin_ErrorShopOrdersText'));
			}
		}
		if(isset($this->request->post['track']) && !is_array($this->request->post['track']) && mb_strlen(trim($this->request->post['track'])) >= 1) {
			if(mb_strlen(trim($this->request->post['track'])) > $this->functions->config('count')) {
				$result[] = array('key' => 'track', 'value' => $this->functions->languageInit('Admin_ErrorShopOrdersTrack'));
			}
		}
		
		return $result;
	}
}
?>
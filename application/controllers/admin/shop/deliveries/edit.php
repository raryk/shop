<?php
class editController extends Controller {
	public function index($deliverieid = null) {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('shop')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		if(!$this->functions->getTotal('shop_deliveries', array('deliverie_id' => $deliverieid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Admin_ErrorShopDeliveriesTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/deliveries');
			exit;
		}
		
		$deliverie = $this->functions->getBy('shop_deliveries', array('deliverie_id' => $deliverieid), array(), array('users' => 'shop_deliveries.user_id=users.user_id'));
		$this->data['deliverie'] = $deliverie;
		
		$section = array(
			'element' => 'shop',
			'element_shop' => 'deliveries'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/deliveries/edit/index/' . $deliverie['deliverie_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleShopDeliveriesEdit'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionShopDeliveriesEdit'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/deliveries/edit/index/' . $deliverie['deliverie_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/deliveries/edit/index/' . $deliverie['deliverie_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/shop/deliveries/edit', $this->data);
	}
	
	public function ajax($csrf = null, $deliverieid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('shop')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_deliveries', array('deliverie_id' => $deliverieid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorShopDeliveriesTotal');
			return json_encode($this->data);
		}
		
		$validate = $this->validate();
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$deliverieData = array(
			'deliverie_name' => $this->request->post['name'],
			'deliverie_country' => $this->request->post['country'],
			'deliverie_city' => $this->request->post['city'],
			'deliverie_address' => $this->request->post['address'],
			'deliverie_zipcode' => $this->request->post['zipcode'],
			'deliverie_phone' => $this->request->post['phone']
		);
		$this->functions->update('shop_deliveries', $deliverieData, array('deliverie_id' => $deliverieid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessShopDeliveriesEdit');
		return json_encode($this->data);
	}
	
	private function validate() {
		$result = null;
		
		if(!isset($this->request->post['name']) || is_array($this->request->post['name']) || mb_strlen(trim($this->request->post['name'])) < 1 || mb_strlen(trim($this->request->post['name'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'name', 'value' => $this->functions->languageInit('Admin_ErrorShopDeliveriesName'));
		}
		if(!isset($this->request->post['country']) || is_array($this->request->post['country']) || mb_strlen(trim($this->request->post['country'])) < 1 || mb_strlen(trim($this->request->post['country'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'country', 'value' => $this->functions->languageInit('Admin_ErrorShopDeliveriesCountry'));
		}
		if(!isset($this->request->post['city']) || is_array($this->request->post['city']) || mb_strlen(trim($this->request->post['city'])) < 1 || mb_strlen(trim($this->request->post['city'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'city', 'value' => $this->functions->languageInit('Admin_ErrorShopDeliveriesCity'));
		}
		if(!isset($this->request->post['address']) || is_array($this->request->post['address']) || mb_strlen(trim($this->request->post['address'])) < 1 || mb_strlen(trim($this->request->post['address'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'address', 'value' => $this->functions->languageInit('Admin_ErrorShopDeliveriesAddress'));
		}
		if(!isset($this->request->post['zipcode']) || is_array($this->request->post['zipcode']) || mb_strlen(trim($this->request->post['zipcode'])) < 1 || mb_strlen(trim($this->request->post['zipcode'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'zipcode', 'value' => $this->functions->languageInit('Admin_ErrorShopDeliveriesZipcode'));
		}
		if(!isset($this->request->post['phone']) || is_array($this->request->post['phone']) || mb_strlen(trim($this->request->post['phone'])) < 1 || mb_strlen(trim($this->request->post['phone'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'phone', 'value' => $this->functions->languageInit('Admin_ErrorShopDeliveriesPhone'));
		}
		
		return $result;
	}
}
?>
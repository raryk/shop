<?php
class indexController extends Controller {
	public function index() {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('shop')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		$this->load->library('pagination');
		
		$page = isset($this->request->get['page']) && !is_array($this->request->get['page']) ? intval($this->request->get['page']) : 0;
		$limit = $this->functions->config('limit');
		$query = array();
		
		$getData = array();
		
		$getSearch = array();
		
		$getJoins = array(
			'users' => 'shop_deliveries.user_id=users.user_id'
		);
		
		$getSort = array(
			'deliverie_id' => 'DESC'
		);
		
		$getOptions = array(
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);
		
		if(isset($this->request->get['userid']) && !is_array($this->request->get['userid'])) {
			$getData['shop_deliveries.user_id'] = $this->request->get['userid'];
			
			$query['userid'] = htmlspecialchars_decode($this->request->get['userid']);
		}
		
		if(isset($this->request->get['q']) && !is_array($this->request->get['q'])) {
			$getSearch['deliverie_id'] = $this->request->get['q'];
			$getSearch['deliverie_name'] = $this->request->get['q'];
			$getSearch['deliverie_country'] = $this->request->get['q'];
			$getSearch['deliverie_city'] = $this->request->get['q'];
			$getSearch['deliverie_address'] = $this->request->get['q'];
			$getSearch['deliverie_zipcode'] = $this->request->get['q'];
			$getSearch['deliverie_phone'] = $this->request->get['q'];
			
			$query['q'] = htmlspecialchars_decode($this->request->get['q']);
		}
		
		$total = $this->functions->getTotal('shop_deliveries', $getData, $getSearch, $getJoins);
		$deliveries = $this->functions->get('shop_deliveries', $getData, $getSearch, $getJoins, $getSort, $getOptions);
		
		$paginationLib = new paginationLibrary();
		$paginationLib->url = '/admin/shop/deliveries';
		$paginationLib->query = $query;
		$paginationLib->total = $total;
		$paginationLib->page = $page;
		$paginationLib->limit = $limit;
		
		$this->data['total'] = $total;
		$this->data['deliveries'] = $deliveries;
		$this->data['pagination'] = $paginationLib->render();
		
		$section = array(
			'element' => 'shop',
			'element_shop' => 'deliveries'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/deliveries?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleShopDeliveries'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionShopDeliveries'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/deliveries');
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/deliveries?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/shop/deliveries/index', $this->data);
	}
	
	public function delete($csrf = null, $deliverieid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('shop')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_deliveries', array('deliverie_id' => $deliverieid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorShopDeliveriesTotal');
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$this->functions->delete('shop_deliveries', array('deliverie_id' => $deliverieid));
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
}
?>
<?php
class editController extends Controller {
	public function index($couponid = null) {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('shop')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		if(!$this->functions->getTotal('shop_coupons', array('coupon_id' => $couponid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Admin_ErrorShopCouponsTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/coupons');
			exit;
		}
		
		$coupon = $this->functions->getBy('shop_coupons', array('coupon_id' => $couponid));
		$this->data['coupon'] = $coupon;
		
		$section = array(
			'element' => 'shop',
			'element_shop' => 'coupons'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/coupons/edit/index/' . $coupon['coupon_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleShopCouponsEdit'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionShopCouponsEdit'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/coupons/edit/index/' . $coupon['coupon_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/coupons/edit/index/' . $coupon['coupon_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/shop/coupons/edit', $this->data);
	}
	
	public function ajax($csrf = null, $couponid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('shop')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_coupons', array('coupon_id' => $couponid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorShopCouponsTotal');
			return json_encode($this->data);
		}
		
		$validate = $this->validate($couponid);
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$couponData = array(
			'coupon_key' => $this->request->post['key'],
			'coupon_value' => $this->request->post['value']
		);
		$this->functions->update('shop_coupons', $couponData, array('coupon_id' => $couponid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessShopCouponsEdit');
		return json_encode($this->data);
	}
	
	private function validate($couponid) {
		$result = null;
		
		$coupon = $this->functions->getBy('shop_coupons', array('coupon_id' => $couponid));
		
		if(!isset($this->request->post['key']) || is_array($this->request->post['key']) || mb_strlen(trim($this->request->post['key'])) < 1 || mb_strlen(trim($this->request->post['key'])) > $this->functions->config('count')) {
			$result[] = array('key' => 'key', 'value' => $this->functions->languageInit('Admin_ErrorShopCouponsKey'));
		} elseif(mb_strtolower($coupon['coupon_key']) != mb_strtolower($this->request->post['key']) && $this->functions->getTotal('shop_coupons', array('coupon_key' => $this->request->post['key']))) {
			$result[] = array('key' => 'key', 'value' => $this->functions->languageInit('Admin_ErrorShopCouponsKeyTotal'));
		}
		if(!isset($this->request->post['value']) || is_array($this->request->post['value']) || $value < 1 || $value > 100) {
			$result[] = array('key' => 'value', 'value' => $this->functions->languageInit('Admin_ErrorShopCouponsValue'));
		}
		
		return $result;
	}
}
?>
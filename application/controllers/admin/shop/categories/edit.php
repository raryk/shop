<?php
class editController extends Controller {
	public function index($categorieid = null) {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('shop')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		if(!$this->functions->getTotal('shop_categories', array('categorie_id' => $categorieid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Admin_ErrorShopCategoriesTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/categories');
			exit;
		}
		
		$categorie = $this->functions->getBy('shop_categories', array('categorie_id' => $categorieid));
		$this->data['categorie'] = $categorie;
		
		$section = array(
			'element' => 'shop',
			'element_shop' => 'categories'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/categories/edit/index/' . $categorie['categorie_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleShopCategoriesEdit'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionShopCategoriesEdit'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/categories/edit/index/' . $categorie['categorie_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/categories/edit/index/' . $categorie['categorie_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/shop/categories/edit', $this->data);
	}
	
	public function ajax($csrf = null, $categorieid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('shop')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_categories', array('categorie_id' => $categorieid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorShopCategoriesTotal');
			return json_encode($this->data);
		}
		
		$validate = $this->validate();
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$title = array();
		if(isset($this->request->post['title']) && is_array($this->request->post['title'])) {
			foreach($this->request->post['title'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$title[$k] = $v;
				}
			}
		}
		$description = array();
		if(isset($this->request->post['description']) && is_array($this->request->post['description'])) {
			foreach($this->request->post['description'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$description[$k] = $v;
				}
			}
		}
		
		$categorieData = array(
			'categorie_title' => json_encode($title),
			'categorie_description' => json_encode($description),
			'categorie_status' => isset($this->request->post['status']) && !is_array($this->request->post['status']) && $this->request->post['status'] ? 1 : 0
		);
		$this->functions->update('shop_categories', $categorieData, array('categorie_id' => $categorieid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessShopCategoriesEdit');
		return json_encode($this->data);
	}
	
	private function validate() {
		$result = null;
		
		$title = array();
		if(isset($this->request->post['title']) && is_array($this->request->post['title'])) {
			foreach($this->request->post['title'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$title[$k] = $v;
				}
			}
		}
		$description = array();
		if(isset($this->request->post['description']) && is_array($this->request->post['description'])) {
			foreach($this->request->post['description'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$description[$k] = $v;
				}
			}
		}
		
		if(empty($title) || mb_strlen(trim(json_encode($title))) > $this->functions->config('count')) {
			$result[] = array('key' => 'title', 'value' => $this->functions->languageInit('Admin_ErrorShopCategoriesTitle'));
		}
		if(empty($description) || mb_strlen(trim(json_encode($description))) > $this->functions->config('count')) {
			$result[] = array('key' => 'description', 'value' => $this->functions->languageInit('Admin_ErrorShopCategoriesDescription'));
		}
		
		return $result;
	}
}
?>
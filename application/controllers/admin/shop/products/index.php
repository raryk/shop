<?php
class indexController extends Controller {
	public function index() {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('shop')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		$this->load->library('pagination');
		
		$page = isset($this->request->get['page']) && !is_array($this->request->get['page']) ? intval($this->request->get['page']) : 0;
		$limit = $this->functions->config('limit');
		$query = array();
		
		$getData = array();
		
		$getSearch = array();
		
		$getJoins = array();
		
		$getSort = array(
			'product_id' => 'DESC'
		);
		
		$getOptions = array(
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);
		
		if(isset($this->request->get['q']) && !is_array($this->request->get['q'])) {
			$getSearch['product_id'] = $this->request->get['q'];
			$getSearch['product_title'] = $this->request->get['q'];
			$getSearch['product_description'] = $this->request->get['q'];
			$getSearch['product_text'] = $this->request->get['q'];
			$getSearch['product_price'] = $this->request->get['q'];
			$getSearch['product_content'] = $this->request->get['q'];
			
			$query['q'] = htmlspecialchars_decode($this->request->get['q']);
		}
		
		$total = $this->functions->getTotal('shop_products', $getData, $getSearch, $getJoins);
		$products = $this->functions->get('shop_products', $getData, $getSearch, $getJoins, $getSort, $getOptions);
		
		$paginationLib = new paginationLibrary();
		$paginationLib->url = '/admin/shop/products';
		$paginationLib->query = $query;
		$paginationLib->total = $total;
		$paginationLib->page = $page;
		$paginationLib->limit = $limit;
		
		$this->data['total'] = $total;
		$this->data['products'] = $products;
		$this->data['pagination'] = $paginationLib->render();
		
		$section = array(
			'element' => 'shop',
			'element_shop' => 'products'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/products?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleShopProducts'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionShopProducts'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/products');
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/products?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/shop/products/index', $this->data);
	}
	
	public function delete($csrf = null, $productid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('shop')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_products', array('product_id' => $productid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorShopProductsTotal');
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$this->functions->delete('shop_products', array('product_id' => $productid));
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
}
?>
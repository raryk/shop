<?php
class editController extends Controller {
	public function index($productid = null) {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		if(!$this->account->isLevel('shop')) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin');
			exit;
		}
		
		if(!$this->functions->getTotal('shop_products', array('product_id' => $productid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Admin_ErrorShopProductsTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/products');
			exit;
		}
		
		$product = $this->functions->getBy('shop_products', array('product_id' => $productid));
		$this->data['product'] = $product;
		
		$section = array(
			'element' => 'shop',
			'element_shop' => 'products'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/products/edit/index/' . $product['product_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleShopProductsEdit'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionShopProductsEdit'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/products/edit/index/' . $product['product_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin/shop/products/edit/index/' . $product['product_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/shop/products/edit', $this->data);
	}
	
	public function ajax($csrf = null, $productid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('shop')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_products', array('product_id' => $productid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorShopProductsTotal');
			return json_encode($this->data);
		}
		
		$validate = $this->validate();
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$title = array();
		if(isset($this->request->post['title']) && is_array($this->request->post['title'])) {
			foreach($this->request->post['title'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$title[$k] = $v;
				}
			}
		}
		$description = array();
		if(isset($this->request->post['description']) && is_array($this->request->post['description'])) {
			foreach($this->request->post['description'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$description[$k] = $v;
				}
			}
		}
		$text = array();
		if(isset($this->request->post['text']) && is_array($this->request->post['text'])) {
			foreach($this->request->post['text'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$text[$k] = $v;
				}
			}
		}
		
		$productData = array(
			'product_title' => json_encode($title),
			'product_description' => json_encode($description),
			'product_text' => json_encode($text),
			'product_price' => $this->request->post['price'],
			'product_categorie' => isset($this->request->post['categorie']) && !is_array($this->request->post['categorie']) && $this->functions->getTotal('shop_categories', array('categorie_id' => $this->request->post['categorie'])) ? $this->request->post['categorie'] : 0,
			'product_discount' => $this->request->post['discount'],
			'product_status' => isset($this->request->post['status']) && !is_array($this->request->post['status']) && $this->request->post['status'] ? 1 : 0
		);
		$this->functions->update('shop_products', $productData, array('product_id' => $productid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessShopProductsEdit');
		return json_encode($this->data);
	}
	
	public function image($csrf = null, $productid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('shop')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_products', array('product_id' => $productid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorShopProductsTotal');
			return json_encode($this->data);
		}
		
		$validateImage = $this->validateImage();
		if(!is_null($validateImage)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validateImage;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$image = array();
		if(isset($this->request->post['image']) && is_array($this->request->post['image'])) {
			for($i = 0; $i < count($this->request->post['image']); $i++) {
				if(isset($this->request->post['image'][$i]) && !is_array($this->request->post['image'][$i]) && !empty($this->functions->image($this->request->post['image'][$i]))) {
					$image[] = $this->request->post['image'][$i];
				}
			}
		}
		
		$this->functions->update('shop_products', array('product_image' => json_encode($image)), array('product_id' => $productid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessShopProductsEditImage');
		return json_encode($this->data);
	}
	
	public function content($csrf = null, $productid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		if(!$this->account->getAdmin()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorAdmin');
			return json_encode($this->data);
		}
		if(!$this->account->isLevel('shop')) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLevel');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_products', array('product_id' => $productid))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Admin_ErrorShopProductsTotal');
			return json_encode($this->data);
		}
		
		$validateContent = $this->validateContent();
		if(!is_null($validateContent)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validateContent;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$content = array();
		if(isset($this->request->post['content']) && is_array($this->request->post['content'])) {
			for($i = 0; $i < count($this->request->post['content']); $i++) {
				if(isset($this->request->post['content'][$i]) && !is_array($this->request->post['content'][$i]) && mb_strlen(trim($this->request->post['content'][$i])) >= 1) {
					$content[] = $this->request->post['content'][$i];
				}
			}
		}
		
		$this->functions->update('shop_products', array('product_content' => json_encode($content)), array('product_id' => $productid));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Admin_SuccessShopProductsEditContent');
		return json_encode($this->data);
	}
	
	private function validate() {
		$result = null;
		
		$title = array();
		if(isset($this->request->post['title']) && is_array($this->request->post['title'])) {
			foreach($this->request->post['title'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$title[$k] = $v;
				}
			}
		}
		$description = array();
		if(isset($this->request->post['description']) && is_array($this->request->post['description'])) {
			foreach($this->request->post['description'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$description[$k] = $v;
				}
			}
		}
		$text = array();
		if(isset($this->request->post['text']) && is_array($this->request->post['text'])) {
			foreach($this->request->post['text'] as $k => $v) {
				if(!is_array($k) && $this->functions->getTotal('system_languages', array('language_id' => $k)) && !is_array($v) && mb_strlen(trim($v)) >= 1) {
					$text[$k] = $v;
				}
			}
		}
		
		if(empty($title) || mb_strlen(trim(json_encode($title))) > $this->functions->config('count')) {
			$result[] = array('key' => 'title', 'value' => $this->functions->languageInit('Admin_ErrorShopProductsTitle'));
		}
		if(empty($description) || mb_strlen(trim(json_encode($description))) > $this->functions->config('count')) {
			$result[] = array('key' => 'description', 'value' => $this->functions->languageInit('Admin_ErrorShopProductsDescription'));
		}
		if(empty($text) || mb_strlen(trim(json_encode($text))) > $this->functions->config('count')) {
			$result[] = array('key' => 'text', 'value' => $this->functions->languageInit('Admin_ErrorShopProductsText'));
		}
		if(!isset($this->request->post['price']) || is_array($this->request->post['price']) || !preg_match('/^([0-9]{1,10})(\.[0-9]{2})?$/', $this->request->post['price'])) {
			$result[] = array('key' => 'price', 'value' => $this->functions->languageInit('Admin_ErrorShopProductsPrice'));
		}
		if(!isset($this->request->post['discount']) || is_array($this->request->post['discount']) || $this->request->post['discount'] > 100) {
			$result[] = array('key' => 'discount', 'value' => $this->functions->languageInit('Admin_ErrorShopProductsDiscount'));
		}
		
		return $result;
	}
	
	private function validateImage() {
		$result = null;
		
		$image = array();
		if(isset($this->request->post['image']) && is_array($this->request->post['image'])) {
			for($i = 0; $i < count($this->request->post['image']); $i++) {
				if(isset($this->request->post['image'][$i]) && !is_array($this->request->post['image'][$i]) && !empty($this->functions->image($this->request->post['image'][$i]))) {
					$image[] = $this->request->post['image'][$i];
				}
			}
		}
		
		if(mb_strlen(trim(json_encode($image))) > $this->functions->config('count')) {
			$result = $this->functions->languageInit('Admin_ErrorShopProductsImage');
		}
		
		return $result;
	}
	
	private function validateContent() {
		$result = null;
		
		$content = array();
		if(isset($this->request->post['content']) && is_array($this->request->post['content'])) {
			for($i = 0; $i < count($this->request->post['content']); $i++) {
				if(isset($this->request->post['content'][$i]) && !is_array($this->request->post['content'][$i]) && mb_strlen(trim($this->request->post['content'][$i])) >= 1) {
					$content[] = $this->request->post['content'][$i];
				}
			}
		}
		
		if(mb_strlen(trim(json_encode($content))) > $this->functions->config('count')) {
			$result = $this->functions->languageInit('Admin_ErrorShopProductsContent');
		}
		
		return $result;
	}
}
?>
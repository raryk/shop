<?php
class indexController extends Controller {
	public function index() {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if(!$this->account->getAdmin()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		
		$section = array(
			'element' => 'index'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/admin?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Admin_TitleIndex'));
		$this->document->setDescription($this->functions->languageInit('Admin_DescriptionIndex'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/admin');
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/admin?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('admin/common/header');
		$this->data['footer'] = $this->action->child('admin/common/footer');
		
		return $this->load->view('admin/index', $this->data);
	}
}
?>
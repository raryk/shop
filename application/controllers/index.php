<?php
class indexController extends Controller {
	public function index() {
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		return $this->load->view('index', $this->data);
	}
	
	public function account($account = 0) {
		$accounts = $this->account->accounts();
		if(!empty($accounts)) {
			if(isset($accounts[$account])) {
				$userid = $accounts[$account]['user_id'];
			} else {
				$userid = $accounts[0]['user_id'];
			}
			
			$tokens = $this->functions->get('users_tokens', array('token_code' => $this->request->cookie['token']), array(), array(), array('token_id' => 'ASC'));
			setcookie('account', array_search($userid, array_column($tokens, 'user_id')), time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
		}
		
		if(isset($this->request->get['back']) && !is_array($this->request->get['back'])) {
			header('Location: ' . htmlspecialchars_decode($this->request->get['back']));
			exit;
		} else {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
	}
	
	public function language($languageid = null) {
		if($this->functions->getTotal('system_languages', array('language_id' => $languageid))) {
			$language = $this->functions->getBy('system_languages', array('language_id' => $languageid));
			
			if($this->account->isLogged()) {
				$this->functions->update('users', array('user_language' => $language['language_id']), array('user_id' => $this->account->getId()));
			} else {
				setcookie('language', $language['language_id'], time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
			}
		} else {
			if($this->account->isLogged()) {
				$this->functions->update('users', array('user_language' => 1), array('user_id' => $this->account->getId()));
			} else {
				setcookie('language', 1, time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
			}
		}
		
		if(isset($this->request->get['back']) && !is_array($this->request->get['back'])) {
			header('Location: ' . htmlspecialchars_decode($this->request->get['back']));
			exit;
		} else {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
	}
	
	public function timezone($timezoneid = null) {
		if($this->functions->getTotal('system_timezones', array('timezone_id' => $timezoneid))) {
			$timezone = $this->functions->getBy('system_timezones', array('timezone_id' => $timezoneid));
			
			if($this->account->isLogged()) {
				$this->functions->update('users', array('user_timezone' => $timezone['timezone_id']), array('user_id' => $this->account->getId()));
			} else {
				setcookie('timezone', $timezone['timezone_id'], time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
			}
		} else {
			if($this->account->isLogged()) {
				$this->functions->update('users', array('user_timezone' => 1), array('user_id' => $this->account->getId()));
			} else {
				setcookie('timezone', 1, time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
			}
		}
		
		if(isset($this->request->get['back']) && !is_array($this->request->get['back'])) {
			header('Location: ' . htmlspecialchars_decode($this->request->get['back']));
			exit;
		} else {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
	}
	
	public function currencie($currencieid = null) {
		if($this->functions->getTotal('system_currencies', array('currencie_id' => $currencieid))) {
			$currencie = $this->functions->getBy('system_currencies', array('currencie_id' => $currencieid));
			
			if($this->account->isLogged()) {
				$this->functions->update('users', array('user_currencie' => $currencie['currencie_id']), array('user_id' => $this->account->getId()));
			} else {
				setcookie('currencie', $currencie['currencie_id'], time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
			}
		} else {
			if($this->account->isLogged()) {
				$this->functions->update('users', array('user_currencie' => 1), array('user_id' => $this->account->getId()));
			} else {
				setcookie('currencie', 1, time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
			}
		}
		
		if(isset($this->request->get['back']) && !is_array($this->request->get['back'])) {
			header('Location: ' . htmlspecialchars_decode($this->request->get['back']));
			exit;
		} else {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
	}
}
?>
<?php
class bannedController extends Controller {
	public function index() {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		
		if($this->functions->getTotal('users_bans', array('user_id' => $this->account->getId(), 'ban_status' => 1, 'ban_time' => 0))) {
			$ban = $this->functions->getBy('users_bans', array('user_id' => $this->account->getId(), 'ban_status' => 1, 'ban_time' => 0), array(), array(), array('ban_id' => 'DESC'));
			$this->data['ban'] = $ban;
			
			$this->data['account'] = $this->account;
			$this->data['request'] = $this->request;
			$this->data['functions'] = $this->functions;
			
			return $this->load->view('banned', $this->data);
		} else {
			$query = $this->db->query("SELECT * FROM users_bans WHERE user_id = '" . $this->db->escape($this->account->getId()) . "' AND ban_status = '1' AND ban_time > '" . $this->db->escape($this->functions->time()) . "' ORDER BY ban_time DESC");
			if($query->num_rows) {
				$this->data['ban'] = $query->row;
				
				$this->data['account'] = $this->account;
				$this->data['request'] = $this->request;
				$this->data['functions'] = $this->functions;
				
				return $this->load->view('banned', $this->data);
			} else {
				if(isset($this->request->get['back']) && !is_array($this->request->get['back'])) {
					header('Location: ' . htmlspecialchars_decode($this->request->get['back']));
					exit;
				} else {
					header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
					exit;
				}
			}
		}
	}
}
?>
<?php
class logoutController extends Controller {
	public function index() {
		if($this->account->isLogged()) {
			$this->account->logout();
		}
		
		header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
		exit;
	}
}
?>
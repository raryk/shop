<?php
class cartController extends Controller {
	public function index() {
		$section = array(
			'element' => 'cart'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/cart?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Main_TitleCart'));
		$this->document->setDescription($this->functions->languageInit('Main_DescriptionCart'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/cart');
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/cart?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('common/header');
		$this->data['footer'] = $this->action->child('common/footer');
		
		return $this->load->view('cart', $this->data);
	}
	
	public function ajax($csrf = null) {
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		$validate = $this->validate();
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$cookie = $this->functions->cart();
		$carts = $this->functions->cart(1, isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) ? $this->request->post['coupon'] : null);
		$total = $this->functions->cart(2, isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) ? $this->request->post['coupon'] : null);
		$oldtotal = $this->functions->cart(3, isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) ? $this->request->post['coupon'] : null);
		
		$product = $this->functions->getBy('shop_products', array('product_id' => $this->request->post['product']));
		
		if(!empty(json_decode($product['product_content'], true)) && array_key_exists($this->request->post['content'], json_decode($product['product_content'], true))) {
			$c = array_key_exists($product['product_id'] . '+' . $this->request->post['content'], $carts) ? $carts[$product['product_id'] . '+' . $this->request->post['content']]['count'] + intval($this->request->post['count']) : intval($this->request->post['count']);
			
			if($product['product_discount']) {
				$price = $this->functions->convert($product['product_price'] * (100 - $product['product_discount']) / 100 * $c, $this->functions->currencie());
			} else {
				$price = $this->functions->convert($product['product_price'] * $c, $this->functions->currencie());
			}
			
			if(isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) && $this->functions->getTotal('shop_coupons', array('coupon_key' => $this->request->post['coupon']))) {
				$coupon = $this->functions->getBy('shop_coupons', array('coupon_key' => $this->request->post['coupon']));
				
				$amount = $price * (100 - $coupon['coupon_value']) / 100;
			} else {
				$amount = $price;
			}
			
			$cookie[$product['product_id'] . '+' . $this->request->post['content']] = array(
				'id' => $product['product_id'],
				'content' => $this->request->post['content'],
				'count' => $c
			);
			$carts[$product['product_id'] . '+' . $this->request->post['content']] = array(
				'id' => $product['product_id'],
				'title' => isset(json_decode($product['product_title'], true)[$this->functions->language()]) ? json_decode($product['product_title'], true)[$this->functions->language()] : json_decode($product['product_title'], true)[$this->functions->config('language')],
				'description' => $this->functions->string(nl2br(isset(json_decode($product['product_description'], true)[$this->functions->language()]) ? json_decode($product['product_description'], true)[$this->functions->language()] : json_decode($product['product_description'], true)[$this->functions->config('language')]), true),
				'text' => htmlspecialchars_decode(isset(json_decode($product['product_text'], true)[$this->functions->language()]) ? json_decode($product['product_text'], true)[$this->functions->language()] : json_decode($product['product_text'], true)[$this->functions->config('language')]),
				'image' => $this->functions->image(json_decode($product['product_image'], true)[0]),
				'content' => json_decode($product['product_content'], true)[$this->request->post['content']],
				'discount' => $product['product_discount'],
				'price' => $this->functions->currencieInit($this->functions->currencie(), $amount),
				'count' => $c
			);
			$total[$product['product_id'] . '+' . $this->request->post['content']] = $amount;
		} else {
			$c = array_key_exists($product['product_id'], $carts) ? $carts[$product['product_id']]['count'] + intval($this->request->post['count']) : intval($this->request->post['count']);
			
			if($product['product_discount']) {
				$price = $this->functions->convert($product['product_price'] * (100 - $product['product_discount']) / 100 * $c, $this->functions->currencie());
			} else {
				$price = $this->functions->convert($product['product_price'] * $c, $this->functions->currencie());
			}
			
			if(isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) && $this->functions->getTotal('shop_coupons', array('coupon_key' => $this->request->post['coupon']))) {
				$coupon = $this->functions->getBy('shop_coupons', array('coupon_key' => $this->request->post['coupon']));
				
				$amount = $price * (100 - $coupon['coupon_value']) / 100;
			} else {
				$amount = $price;
			}
			
			$cookie[$product['product_id']] = array(
				'id' => $product['product_id'],
				'count' => $c
			);
			$carts[$product['product_id']] = array(
				'id' => $product['product_id'],
				'title' => isset(json_decode($product['product_title'], true)[$this->functions->language()]) ? json_decode($product['product_title'], true)[$this->functions->language()] : json_decode($product['product_title'], true)[$this->functions->config('language')],
				'description' => $this->functions->string(nl2br(isset(json_decode($product['product_description'], true)[$this->functions->language()]) ? json_decode($product['product_description'], true)[$this->functions->language()] : json_decode($product['product_description'], true)[$this->functions->config('language')]), true),
				'text' => htmlspecialchars_decode(isset(json_decode($product['product_text'], true)[$this->functions->language()]) ? json_decode($product['product_text'], true)[$this->functions->language()] : json_decode($product['product_text'], true)[$this->functions->config('language')]),
				'image' => $this->functions->image(json_decode($product['product_image'], true)[0]),
				'discount' => $product['product_discount'],
				'price' => $this->functions->currencieInit($this->functions->currencie(), $amount),
				'count' => $c
			);
			$total[$product['product_id']] = $amount;
		}
		
		if(isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) && $this->functions->getTotal('shop_coupons', array('coupon_key' => $this->request->post['coupon']))) {
			$carts[$product['product_id']]['oldprice'] = $this->functions->currencieInit($this->functions->currencie(), $price);
			$oldtotal[$product['product_id']] = $price;
		}
		
		setcookie('cart', json_encode(array_values($cookie)), time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
		
		$this->data['carts'] = array_values($carts);
		$this->data['total'] = $this->functions->currencieInit($this->functions->currencie(), array_sum(array_values($total)), true);
		
		if(isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) && $this->functions->getTotal('shop_coupons', array('coupon_key' => $this->request->post['coupon']))) {
			$this->data['oldtotal'] = $this->functions->currencieInit($this->functions->currencie(), array_sum(array_values($oldtotal)), true);
		}
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
	
	public function clear($csrf = null) {
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$cookie = array();
		$carts = array();
		$total = array();
		$oldtotal = array();
		
		setcookie('cart', json_encode($cookie), time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
		
		$this->data['carts'] = array_values($carts);
		$this->data['total'] = $this->functions->currencieInit($this->functions->currencie(), array_sum(array_values($total)), true);
		
		if(isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) && $this->functions->getTotal('shop_coupons', array('coupon_key' => $this->request->post['coupon']))) {
			$this->data['oldtotal'] = $this->functions->currencieInit($this->functions->currencie(), array_sum(array_values($oldtotal)), true);
		}
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
	
	public function delete($csrf = null) {
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		$validateDelete = $this->validateDelete();
		if(!is_null($validateDelete)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validateDelete;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$cookie = $this->functions->cart();
		$carts = $this->functions->cart(1, isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) ? $this->request->post['coupon'] : null);
		$total = $this->functions->cart(2, isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) ? $this->request->post['coupon'] : null);
		$oldtotal = $this->functions->cart(3, isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) ? $this->request->post['coupon'] : null);
		
		array_splice($cookie, $this->request->post['key'], 1);
		array_splice($carts, $this->request->post['key'], 1);
		array_splice($total, $this->request->post['key'], 1);
		array_splice($oldtotal, $this->request->post['key'], 1);
		
		setcookie('cart', json_encode(array_values($cookie)), time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
		
		$this->data['carts'] = array_values($carts);
		$this->data['total'] = $this->functions->currencieInit($this->functions->currencie(), array_sum(array_values($total)), true);
		
		if(isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) && $this->functions->getTotal('shop_coupons', array('coupon_key' => $this->request->post['coupon']))) {
			$this->data['oldtotal'] = $this->functions->currencieInit($this->functions->currencie(), array_sum(array_values($oldtotal)), true);
		}
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
	
	public function coupon($csrf = null) {
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		$validateCoupon = $this->validateCoupon();
		if(!is_null($validateCoupon)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validateCoupon;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$cookie = $this->functions->cart();
		$carts = $this->functions->cart(1, isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) ? $this->request->post['coupon'] : null);
		$total = $this->functions->cart(2, isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) ? $this->request->post['coupon'] : null);
		$oldtotal = $this->functions->cart(3, isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) ? $this->request->post['coupon'] : null);
		
		$this->data['carts'] = array_values($carts);
		$this->data['total'] = $this->functions->currencieInit($this->functions->currencie(), array_sum(array_values($total)), true);
		
		if(isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) && $this->functions->getTotal('shop_coupons', array('coupon_key' => $this->request->post['coupon']))) {
			$this->data['oldtotal'] = $this->functions->currencieInit($this->functions->currencie(), array_sum(array_values($oldtotal)), true);
		}
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
	
	public function order($csrf = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_deliveries', array('deliverie_id' => $this->account->getDeliverie()))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Main_ErrorCartDeliverie');
			return json_encode($this->data);
		}
		
		$validateOrder = $this->validateOrder();
		if(!is_null($validateOrder)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validateOrder;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$cookie = $this->functions->cart();
		$carts = $this->functions->cart(1, isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) ? $this->request->post['coupon'] : null);
		$total = $this->functions->cart(2, isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) ? $this->request->post['coupon'] : null);
		$oldtotal = $this->functions->cart(3, isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) ? $this->request->post['coupon'] : null);
		
		$this->load->library('mail');
		
		$mailLib = new mailLibrary();
		
		setcookie('cart', json_encode(array()), time() + $this->functions->config('cookie'), '/', '.' . $this->functions->config('domain'), ($this->functions->config('connect') == 'https://' ? true : false), true);
		
		$deliverie = $this->functions->getBy('shop_deliveries', array('deliverie_id' => $this->account->getDeliverie()));
		
		$orderData = array(
			'user_id' => $this->account->getId(),
			'order_content' => json_encode(array_values($cookie)),
			'order_name' => $deliverie['deliverie_name'],
			'order_country' => $deliverie['deliverie_country'],
			'order_city' => $deliverie['deliverie_city'],
			'order_address' => $deliverie['deliverie_address'],
			'order_zipcode' => $deliverie['deliverie_zipcode'],
			'order_phone' => $deliverie['deliverie_phone'],
			'order_email' => $this->request->post['email'],
			'order_text' => isset($this->request->post['text']) && !is_array($this->request->post['text']) && mb_strlen(trim($this->request->post['text'])) >= 1 ? $this->request->post['text'] : null,
			'order_currencie' => $this->functions->currencie(),
			'order_amount' => array_sum(array_values($total)),
			'order_status' => 0
		);
		
		if(isset($this->request->post['coupon']) && !is_array($this->request->post['coupon']) && $this->functions->getTotal('shop_coupons', array('coupon_key' => $this->request->post['coupon']))) {
			$coupon = $this->functions->getBy('shop_coupons', array('coupon_key' => $this->request->post['coupon']));
			
			$orderData['order_coupon'] = $coupon['coupon_id'];
		}
		
		$orderid = $this->functions->create('shop_orders', $orderData, 'order_date_add');
		
		$order = $this->functions->getBy('shop_orders', array('order_id' => $orderid));
		$mailData['order'] = $order;
		
		$mailData['account'] = $this->account;
		$mailData['request'] = $this->request;
		$mailData['functions'] = $this->functions;
		
		$mailLib->addTo($order['order_email'], $order['order_name']);
		$mailLib->setFrom($this->functions->config('mail_from'), $this->functions->config('mail_sender'));
		$mailLib->setSubject($this->functions->languageInit('SystemMailSubjectCart'));
		$mailLib->setText($this->load->view('mail/cart', $mailData));
		
		if($this->functions->config('smtp')) {
			$mailLib->send(true, $this->functions->config('smtp_server'), $this->functions->config('smtp_username'), $this->functions->config('smtp_password'));
		} else {
			$mailLib->send();
		}
		
		$data = array(
			'id' => 7,
			'amount' => $this->functions->convert($order['order_amount'], 2, $order['order_currencie']),
			'currencie' => 'RUB',
			'email' => $order['order_email'],
			'key' => $order['order_id'],
			'value' => 'Order payment #' . $order['order_id']
		);
		ksort($data, SORT_STRING);
		$data['secret'] = 'B6A75631682599B5';
		$signature = base64_encode(md5(implode(':', $data), true));
		unset($data['secret']);
		$data['signature'] = $signature;
		
		$this->data['status'] = 'success';
		$this->data['url'] = 'https://flyxen.com/pay?' . http_build_query($data);
		return json_encode($this->data);
	}
	
	private function validate() {
		$result = null;
		
		if(!isset($this->request->post['product']) || is_array($this->request->post['product']) || $this->functions->getTotal('shop_products', array('product_id' => $this->request->post['product']))) {
			$product = $this->functions->getBy('shop_products', array('product_id' => $this->request->post['product']));
			
			if(!empty(json_decode($product['product_content'], true)) && (!isset($this->request->post['content']) || is_array($this->request->post['content']) || !array_key_exists($this->request->post['content'], json_decode($product['product_content'], true)))) {
				$result = $this->functions->languageInit('Main_ErrorCartContent');
			}
		} else {
			$result = $this->functions->languageInit('Main_ErrorCartProduct');
		}
		if(!isset($this->request->post['count']) || is_array($this->request->post['count']) || $this->request->post['count'] <= 0) {
			$result = $this->functions->languageInit('Main_ErrorCartCount');
		}
		
		return $result;
	}
	
	private function validateDelete() {
		$result = null;
		
		if(!isset($this->request->post['key']) || is_array($this->request->post['key']) || !array_key_exists($this->request->post['key'], array_values($this->functions->cart()))) {
			$result = $this->functions->languageInit('Main_ErrorCartDelete');
		}
		
		return $result;
	}
	
	private function validateCoupon() {
		$result = null;
		
		if(!isset($this->request->post['coupon']) || is_array($this->request->post['coupon']) || !$this->functions->getTotal('shop_coupons', array('coupon_key' => $this->request->post['coupon']))) {
			$result = $this->functions->languageInit('Main_ErrorCartCoupon');
		}
		
		return $result;
	}
	
	private function validateOrder() {
		$result = null;
		
		if(!empty(array_values($this->functions->cart())) && mb_strlen(trim(json_encode(array_values($this->functions->cart())))) <= $this->functions->config('count')) {
			if(!isset($this->request->post['email']) || is_array($this->request->post['email']) || !preg_match('/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{1,8})$/i', $this->request->post['email'])) {
				$result[] = array('key' => 'email', 'value' => $this->functions->languageInit('Main_ErrorCartEmail'));
			}
			if(isset($this->request->post['text']) && !is_array($this->request->post['text']) && mb_strlen(trim($this->request->post['text'])) >= 1) {
				if(mb_strlen(trim($this->request->post['text'])) > $this->functions->config('count')) {
					$result[] = array('key' => 'text', 'value' => $this->functions->languageInit('Main_ErrorCartText'));
				}
			}
		} else {
			$result = $this->functions->languageInit('Main_ErrorCartOrder');
		}
		
		return $result;
	}
}
?>
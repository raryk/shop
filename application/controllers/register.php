<?php
class registerController extends Controller {
	public function index() {
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		return $this->load->view('register', $this->data);
	}
	
	public function ajax($csrf = null) {
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		$validate = $this->validate();
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$userData = array(
			'user_nickname' => $this->request->post['nickname'],
			'user_password' => md5($this->request->post['password']),
			'user_ip' => $this->request->server['REMOTE_ADDR'],
			'user_language' => $this->functions->language(),
			'user_timezone' => $this->functions->timezone(),
			'user_currencie' => $this->functions->currencie(),
			'user_deliverie' => 0,
			'user_admin' => 0,
			'user_level' => 0,
			'user_twa' => 0
		);
		$userid = $this->functions->create('users', $userData, 'user_date_reg');
		
		$this->account->login($userid);
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
	
	private function validate() {
		$result = null;
		
		if(!isset($this->request->post['nickname']) || is_array($this->request->post['nickname']) || !preg_match('/^[a-zA-Z0-9_\.-]{3,32}$/', $this->request->post['nickname'])) {
			$result[] = array('key' => 'nickname', 'value' => $this->functions->languageInit('Main_ErrorRegisterNickname'));
		} elseif($this->functions->getTotal('users', array('user_nickname' => $this->request->post['nickname']))) {
			$result[] = array('key' => 'nickname', 'value' => $this->functions->languageInit('Main_ErrorRegisterNicknameTotal'));
		}
		if(!isset($this->request->post['password']) || is_array($this->request->post['password']) || mb_strlen(trim($this->request->post['password'])) < 6 || mb_strlen(trim($this->request->post['password'])) > 64) {
			$result[] = array('key' => 'password', 'value' => $this->functions->languageInit('Main_ErrorRegisterPassword'));
		} elseif(!isset($this->request->post['password2']) || is_array($this->request->post['password2']) || $this->request->post['password'] != $this->request->post['password2']) {
			$result[] = array('key' => 'password2', 'value' => $this->functions->languageInit('Main_ErrorRegisterPassword2'));
		}
		
		return $result;
	}
}
?>
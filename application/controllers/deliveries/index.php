<?php
class indexController extends Controller {
	public function index() {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		
		$this->load->library('pagination');
		
		$page = isset($this->request->get['page']) && !is_array($this->request->get['page']) ? intval($this->request->get['page']) : 0;
		$limit = $this->functions->config('limit');
		$query = array();
		
		$getData = array(
			'user_id' => $this->account->getId()
		);
		
		$getSearch = array();
		
		$getJoins = array();
		
		$getSort = array(
			'deliverie_id' => 'DESC'
		);
		
		$getOptions = array(
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);
		
		if(isset($this->request->get['q']) && !is_array($this->request->get['q'])) {
			$getSearch['deliverie_id'] = $this->request->get['q'];
			$getSearch['deliverie_name'] = $this->request->get['q'];
			$getSearch['deliverie_country'] = $this->request->get['q'];
			$getSearch['deliverie_city'] = $this->request->get['q'];
			$getSearch['deliverie_address'] = $this->request->get['q'];
			$getSearch['deliverie_zipcode'] = $this->request->get['q'];
			$getSearch['deliverie_phone'] = $this->request->get['q'];
			
			$query['q'] = htmlspecialchars_decode($this->request->get['q']);
		}
		
		$total = $this->functions->getTotal('shop_deliveries', $getData, $getSearch, $getJoins);
		$deliveries = $this->functions->get('shop_deliveries', $getData, $getSearch, $getJoins, $getSort, $getOptions);
		
		$paginationLib = new paginationLibrary();
		$paginationLib->url = '/deliveries';
		$paginationLib->query = $query;
		$paginationLib->total = $total;
		$paginationLib->page = $page;
		$paginationLib->limit = $limit;
		
		$this->data['total'] = $total;
		$this->data['deliveries'] = $deliveries;
		$this->data['pagination'] = $paginationLib->render();
		
		$section = array(
			'element' => 'deliveries'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/deliveries?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Main_TitleDeliveries'));
		$this->document->setDescription($this->functions->languageInit('Main_DescriptionDeliveries'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/deliveries');
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/deliveries?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('common/header');
		$this->data['footer'] = $this->action->child('common/footer');
		
		return $this->load->view('deliveries/index', $this->data);
	}
	
	public function delete($csrf = null, $deliverieid = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		if(!$this->functions->getTotal('shop_deliveries', array('deliverie_id' => $deliverieid, 'user_id' => $this->account->getId()))) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('Main_ErrorDeliveriesTotal');
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$this->functions->delete('shop_deliveries', array('deliverie_id' => $deliverieid));
		
		$this->data['status'] = 'success';
		return json_encode($this->data);
	}
}
?>
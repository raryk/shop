<?php
class loginController extends Controller {
	public function index() {
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		return $this->load->view('login', $this->data);
	}
	
	public function ajax($csrf = null) {
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		$validate = $this->validate();
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$user = $this->functions->getBy('users', array('user_nickname' => $this->request->post['nickname'], 'user_password' => md5($this->request->post['password'])));
		
		if(!$this->account->isLogged($user['user_id']) && $user['user_twa'] && mb_strlen(trim($user['user_email'])) >= 1) {
			if(isset($this->request->post['code']) && !is_array($this->request->post['code'])) {
				$this->functions->delete('code_confirms', array('confirm_key' => 'login', 'confirm_value' => $user['user_email']));
				$this->functions->delete('code_limits', array('limit_key' => 'login', 'limit_value' => $user['user_email']));
				
				$this->account->login($user['user_id']);
				
				$this->data['status'] = 'success';
				return json_encode($this->data);
			} else {
				$query = $this->db->query("SELECT * FROM code_confirms WHERE confirm_key = 'login' AND confirm_value = '" . $this->db->escape($user['user_email']) . "' AND confirm_date_add > NOW() - INTERVAL '" . $this->db->escape($this->functions->config('time')) . "' SECOND");
				if($query->num_rows) {
					$this->data['status'] = 'email';
					$this->data['time'] = strtotime($query->row['confirm_date_add']) - ($this->functions->time() - $this->functions->config('time'));
					return json_encode($this->data);
				} else {
					$this->load->library('mail');
					
					$mailLib = new mailLibrary();
					
					$this->functions->delete('code_confirms', array('confirm_key' => 'login', 'confirm_value' => $user['user_email']));
					$this->functions->delete('code_limits', array('limit_key' => 'login', 'limit_value' => $user['user_email']));
					
					$confirmData = array(
						'confirm_code' => sprintf('%06d', mt_rand(1, 999999)),
						'confirm_key' => 'login',
						'confirm_value' => $user['user_email']
					);
					$confirmid = $this->functions->create('code_confirms', $confirmData, 'confirm_date_add');
					
					$confirm = $this->functions->getBy('code_confirms', array('confirm_id' => $confirmid));
					$mailData['confirm'] = $confirm;
					
					$mailData['account'] = $this->account;
					$mailData['request'] = $this->request;
					$mailData['functions'] = $this->functions;
					
					$mailLib->addTo($confirm['confirm_value'], '@' . $user['user_nickname'] . ' (' . $user['user_name'] . ')');
					$mailLib->setFrom($this->functions->config('mail_from'), $this->functions->config('mail_sender'));
					$mailLib->setSubject($this->functions->languageInit('SystemMailSubjectLogin'));
					$mailLib->setText($this->load->view('mail/login', $mailData));
					
					if($this->functions->config('smtp')) {
						$mailLib->send(true, $this->functions->config('smtp_server'), $this->functions->config('smtp_username'), $this->functions->config('smtp_password'));
					} else {
						$mailLib->send();
					}
					
					$this->data['status'] = 'email';
					$this->data['time'] = strtotime($confirm['confirm_date_add']) - ($this->functions->time() - $this->functions->config('time'));
					return json_encode($this->data);
				}
			}
		} else {
			$this->account->login($user['user_id']);
			
			$this->data['status'] = 'success';
			return json_encode($this->data);
		}
	}
	
	private function validate() {
		$result = null;
		
		if($this->functions->getTotal('users', array('user_nickname' => $this->request->post['nickname'], 'user_password' => md5($this->request->post['password'])))) {
			$user = $this->functions->getBy('users', array('user_nickname' => $this->request->post['nickname'], 'user_password' => md5($this->request->post['password'])));
			
			if(!$this->account->isLogged($user['user_id']) && $user['user_twa'] && mb_strlen(trim($user['user_email'])) >= 1) {
				if(isset($this->request->post['code']) && !is_array($this->request->post['code'])) {
					if(!$this->functions->getTotal('code_confirms', array('confirm_code' => $this->request->post['code'], 'confirm_key' => 'login', 'confirm_value' => $user['user_email']))) {
						if($this->functions->getTotal('code_limits', array('limit_key' => 'login', 'limit_value' => $user['user_email'])) >= $this->functions->config('limit')) {
							$this->functions->delete('code_confirms', array('confirm_key' => 'login', 'confirm_value' => $user['user_email']));
							
							$result[] = array('key' => 'code', 'value' => $this->functions->languageInit('Main_ErrorLoginLimit'));
						} else {
							$this->functions->create('code_limits', array('limit_code' => $this->request->post['code'], 'limit_key' => 'login', 'limit_value' => $user['user_email']), 'limit_date_add');
							
							$result[] = array('key' => 'code', 'value' => $this->functions->languageInit('Main_ErrorLoginConfirm'));
						}
					}
				}
			}
		} else {
			$result = $this->functions->languageInit('Main_ErrorLoginTotal');
		}
		
		return $result;
	}
}
?>
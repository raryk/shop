<?php
class categorieController extends Controller {
	public function index($categorieid = null) {
		if(!$this->functions->getTotal('shop_categories', array('categorie_id' => $categorieid))) {
			$this->session->data['alert'] = array('key' => 'error', 'value' => $this->functions->languageInit('Main_ErrorCategorieTotal'));
			
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain'));
			exit;
		}
		
		$categorie = $this->functions->getBy('shop_categories', array('categorie_id' => $categorieid));
		$this->data['categorie'] = $categorie;
		
		$this->load->library('pagination');
		
		$page = isset($this->request->get['page']) && !is_array($this->request->get['page']) ? intval($this->request->get['page']) : 0;
		$limit = $this->functions->config('limit');
		$query = array();
		
		$getData = array(
			'product_categorie' => $categorie['categorie_id'],
			'product_status' => 1
		);
		
		$getSearch = array();
		
		$getJoins = array();
		
		$getSort = array(
			'product_id' => 'DESC'
		);
		
		$getOptions = array(
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);
		
		if(isset($this->request->get['q']) && !is_array($this->request->get['q'])) {
			$getSearch['product_id'] = $this->request->get['q'];
			$getSearch['product_title'] = $this->request->get['q'];
			$getSearch['product_description'] = $this->request->get['q'];
			$getSearch['product_text'] = $this->request->get['q'];
			$getSearch['product_price'] = $this->request->get['q'];
			$getSearch['product_content'] = $this->request->get['q'];
			
			$query['q'] = htmlspecialchars_decode($this->request->get['q']);
		}
		
		$total = $this->functions->getTotal('shop_products', $getData, $getSearch, $getJoins);
		$products = $this->functions->get('shop_products', $getData, $getSearch, $getJoins, $getSort, $getOptions);
		
		$paginationLib = new paginationLibrary();
		$paginationLib->url = '/categorie/index/' . $categorie['categorie_id'];
		$paginationLib->query = $query;
		$paginationLib->total = $total;
		$paginationLib->page = $page;
		$paginationLib->limit = $limit;
		
		$this->data['total'] = $total;
		$this->data['products'] = $products;
		$this->data['pagination'] = $paginationLib->render();
		
		$section = array(
			'element' => 'categorie',
			'element_categorie' => $categorie['categorie_id']
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/categorie/index/' . $categorie['categorie_id'] . '?language=' . $item['language_key'];
		}
		
		$this->document->setTitle(isset(json_decode($categorie['categorie_title'], true)[$this->functions->language()]) ? json_decode($categorie['categorie_title'], true)[$this->functions->language()] : json_decode($categorie['categorie_title'], true)[$this->functions->config('language')]);
		$this->document->setDescription(isset(json_decode($categorie['categorie_description'], true)[$this->functions->language()]) ? json_decode($categorie['categorie_description'], true)[$this->functions->language()] : json_decode($categorie['categorie_description'], true)[$this->functions->config('language')]);
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/categorie/index/' . $categorie['categorie_id']);
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/categorie/index/' . $categorie['categorie_id'] . '?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('common/header');
		$this->data['footer'] = $this->action->child('common/footer');
		
		return $this->load->view('categorie', $this->data);
	}
}
?>
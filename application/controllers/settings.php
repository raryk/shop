<?php
class settingsController extends Controller {
	public function index() {
		if(!$this->account->isLogged()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/login?redirect=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		if($this->account->isBanned()) {
			header('Location: ' . $this->functions->config('connect') . $this->functions->config('domain') . '/banned?back=' . urlencode($this->functions->config('connect') . htmlspecialchars_decode($this->request->server['SERVER_NAME']) . htmlspecialchars_decode($this->request->server['REQUEST_URI'])));
			exit;
		}
		
		$section = array(
			'element' => 'settings'
		);
		
		$alternate = array();
		foreach($this->functions->get('system_languages') as $item) {
			$alternate[$item['language_key']] = $this->functions->config('connect') . $this->functions->config('domain') . '/settings?language=' . $item['language_key'];
		}
		
		$this->document->setTitle($this->functions->languageInit('Main_TitleSettings'));
		$this->document->setDescription($this->functions->languageInit('Main_DescriptionSettings'));
		$this->document->setSection($section);
		$this->document->setUrl($this->functions->config('connect') . $this->functions->config('domain') . '/settings');
		$this->document->setCanonical($this->functions->config('connect') . $this->functions->config('domain') . '/settings?language=' . $this->functions->languageBy($this->functions->language()));
		$this->document->setAlternate($alternate);
		
		$this->data['account'] = $this->account;
		$this->data['request'] = $this->request;
		$this->data['functions'] = $this->functions;
		
		$this->data['header'] = $this->action->child('common/header');
		$this->data['footer'] = $this->action->child('common/footer');
		
		return $this->load->view('settings', $this->data);
	}
	
	public function ajax($csrf = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		$validate = $this->validate();
		if(!is_null($validate)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validate;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$userData = array(
			'user_nickname' => $this->request->post['nickname'],
			'user_deliverie' => isset($this->request->post['deliverie']) && !is_array($this->request->post['deliverie']) && $this->functions->getTotal('shop_deliveries', array('deliverie_id' => $this->request->post['deliverie'], 'user_id' => $this->account->getId())) ? $this->request->post['deliverie'] : 0,
			'user_language' => $this->request->post['language'],
			'user_timezone' => $this->request->post['timezone'],
			'user_currencie' => $this->request->post['currencie'],
			'user_twa' => isset($this->request->post['twa']) && !is_array($this->request->post['twa']) && $this->request->post['twa'] ? 1 : 0
		);
		$this->functions->update('users', $userData, array('user_id' => $this->account->getId()));
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Main_SuccessSettings');
		return json_encode($this->data);
	}
	
	public function email($csrf = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		$validateEmail = $this->validateEmail();
		if(!is_null($validateEmail)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validateEmail;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		if(mb_strlen(trim($this->account->getEmail())) >= 1) {
			if(isset($this->request->post['code']) && !is_array($this->request->post['code'])) {
				$this->functions->delete('code_confirms', array('confirm_key' => 'unbind', 'confirm_value' => $this->account->getEmail()));
				$this->functions->delete('code_limits', array('limit_key' => 'unbind', 'limit_value' => $this->account->getEmail()));
				
				$this->functions->update('users', array('user_email' => null), array('user_id' => $this->account->getId()));
				
				$this->data['status'] = 'success';
				return json_encode($this->data);
			} else {
				$query = $this->db->query("SELECT * FROM code_confirms WHERE confirm_key = 'unbind' AND confirm_value = '" . $this->db->escape($this->account->getEmail()) . "' AND confirm_date_add > NOW() - INTERVAL '" . $this->db->escape($this->functions->config('time')) . "' SECOND");
				if($query->num_rows) {
					$this->data['status'] = 'unbind';
					$this->data['time'] = strtotime($query->row['confirm_date_add']) - ($this->functions->time() - $this->functions->config('time'));
					return json_encode($this->data);
				} else {
					$this->load->library('mail');
					
					$mailLib = new mailLibrary();
					
					$this->functions->delete('code_confirms', array('confirm_key' => 'unbind', 'confirm_value' => $this->account->getEmail()));
					$this->functions->delete('code_limits', array('limit_key' => 'unbind', 'limit_value' => $this->account->getEmail()));
					
					$confirmData = array(
						'confirm_code' => sprintf('%06d', mt_rand(1, 999999)),
						'confirm_key' => 'unbind',
						'confirm_value' => $this->account->getEmail()
					);
					$confirmid = $this->functions->create('code_confirms', $confirmData, 'confirm_date_add');
					
					$confirm = $this->functions->getBy('code_confirms', array('confirm_id' => $confirmid));
					$mailData['confirm'] = $confirm;
					
					$mailData['account'] = $this->account;
					$mailData['request'] = $this->request;
					$mailData['functions'] = $this->functions;
					
					$mailLib->addTo($confirm['confirm_value'], $this->account->getNickname());
					$mailLib->setFrom($this->functions->config('mail_from'), $this->functions->config('mail_sender'));
					$mailLib->setSubject($this->functions->languageInit('SystemMailSubjectUnbind'));
					$mailLib->setText($this->load->view('mail/unbind', $mailData));
					
					if($this->functions->config('smtp')) {
						$mailLib->send(true, $this->functions->config('smtp_server'), $this->functions->config('smtp_username'), $this->functions->config('smtp_password'));
					} else {
						$mailLib->send();
					}
					
					$this->data['status'] = 'unbind';
					$this->data['time'] = strtotime($confirm['confirm_date_add']) - ($this->functions->time() - $this->functions->config('time'));
					return json_encode($this->data);
				}
			}
		} else {
			if(isset($this->request->post['code']) && !is_array($this->request->post['code'])) {
				$this->functions->delete('code_confirms', array('confirm_key' => 'bind', 'confirm_value' => $this->request->post['email']));
				$this->functions->delete('code_limits', array('limit_key' => 'bind', 'limit_value' => $this->request->post['email']));
				
				$this->functions->update('users', array('user_email' => $this->request->post['email']), array('user_id' => $this->account->getId()));
				
				$this->data['status'] = 'success';
				return json_encode($this->data);
			} else {
				$query = $this->db->query("SELECT * FROM code_confirms WHERE confirm_key = 'bind' AND confirm_value = '" . $this->db->escape($this->request->post['email']) . "' AND confirm_date_add > NOW() - INTERVAL '" . $this->db->escape($this->functions->config('time')) . "' SECOND");
				if($query->num_rows) {
					$this->data['status'] = 'bind';
					$this->data['time'] = strtotime($query->row['confirm_date_add']) - ($this->functions->time() - $this->functions->config('time'));
					return json_encode($this->data);
				} else {
					$this->load->library('mail');
					
					$mailLib = new mailLibrary();
					
					$this->functions->delete('code_confirms', array('confirm_key' => 'bind', 'confirm_value' => $this->request->post['email']));
					$this->functions->delete('code_limits', array('limit_key' => 'bind', 'limit_value' => $this->request->post['email']));
					
					$confirmData = array(
						'confirm_code' => sprintf('%06d', mt_rand(1, 999999)),
						'confirm_key' => 'bind',
						'confirm_value' => $this->request->post['email']
					);
					$confirmid = $this->functions->create('code_confirms', $confirmData, 'confirm_date_add');
					
					$confirm = $this->functions->getBy('code_confirms', array('confirm_id' => $confirmid));
					$mailData['confirm'] = $confirm;
					
					$mailData['account'] = $this->account;
					$mailData['request'] = $this->request;
					$mailData['functions'] = $this->functions;
					
					$mailLib->addTo($confirm['confirm_value'], $this->account->getNickname());
					$mailLib->setFrom($this->functions->config('mail_from'), $this->functions->config('mail_sender'));
					$mailLib->setSubject($this->functions->languageInit('SystemMailSubjectBind'));
					$mailLib->setText($this->load->view('mail/bind', $mailData));
					
					if($this->functions->config('smtp')) {
						$mailLib->send(true, $this->functions->config('smtp_server'), $this->functions->config('smtp_username'), $this->functions->config('smtp_password'));
					} else {
						$mailLib->send();
					}
					
					$this->data['status'] = 'bind';
					$this->data['time'] = strtotime($confirm['confirm_date_add']) - ($this->functions->time() - $this->functions->config('time'));
					return json_encode($this->data);
				}
			}
		}
	}
	
	public function password($csrf = null) {
		if(!$this->account->isLogged()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorLogged');
			return json_encode($this->data);
		}
		if($this->account->isBanned()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorBanned');
			return json_encode($this->data);
		}
		
		if(mb_strtolower($csrf) != mb_strtolower($this->functions->getCsrf())) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorCsrf');
			return json_encode($this->data);
		}
		
		$validatePassword = $this->validatePassword();
		if(!is_null($validatePassword)) {
			$this->data['status'] = 'error';
			$this->data['error'] = $validatePassword;
			return json_encode($this->data);
		}
		
		if(!$this->functions->guard()) {
			$this->data['status'] = 'error';
			$this->data['error'] = $this->functions->languageInit('ErrorGuard');
			return json_encode($this->data);
		}
		
		$this->functions->update('users', array('user_password' => md5($this->request->post['password'])), array('user_id' => $this->account->getId()));
		
		if(isset($this->request->post['tokens']) && !is_array($this->request->post['tokens']) && $this->request->post['tokens']) {
			$this->account->deleteTokens();
		}
		
		$this->data['status'] = 'success';
		$this->data['success'] = $this->functions->languageInit('Main_SuccessSettingsPassword');
		return json_encode($this->data);
	}
	
	private function validate() {
		$result = null;
		
		$user = $this->functions->getBy('users', array('user_id' => $this->account->getId()));
		
		if(!isset($this->request->post['nickname']) || is_array($this->request->post['nickname']) || !preg_match('/^[a-zA-Z0-9_\.-]{3,32}$/', $this->request->post['nickname'])) {
			$result[] = array('key' => 'nickname', 'value' => $this->functions->languageInit('Main_ErrorSettingsNickname'));
		} elseif(mb_strtolower($user['user_nickname']) != mb_strtolower($this->request->post['nickname']) && $this->functions->getTotal('users', array('user_nickname' => $this->request->post['nickname']))) {
			$result[] = array('key' => 'nickname', 'value' => $this->functions->languageInit('Main_ErrorSettingsNicknameTotal'));
		}
		if(!isset($this->request->post['language']) || is_array($this->request->post['language']) || !$this->functions->getTotal('system_languages', array('language_id' => $this->request->post['language']))) {
			$result[] = array('key' => 'language', 'value' => $this->functions->languageInit('Main_ErrorSettingsLanguage'));
		}
		if(!isset($this->request->post['timezone']) || is_array($this->request->post['timezone']) || !$this->functions->getTotal('system_timezones', array('timezone_id' => $this->request->post['timezone']))) {
			$result[] = array('key' => 'timezone', 'value' => $this->functions->languageInit('Main_ErrorSettingsTimezone'));
		}
		if(!isset($this->request->post['currencie']) || is_array($this->request->post['currencie']) || !$this->functions->getTotal('system_currencies', array('currencie_id' => $this->request->post['currencie']))) {
			$result[] = array('key' => 'currencie', 'value' => $this->functions->languageInit('Main_ErrorSettingsCurrencie'));
		}
		
		return $result;
	}
	
	private function validateEmail() {
		$result = null;
		
		$user = $this->functions->getBy('users', array('user_id' => $this->account->getId()));
		
		if(mb_strlen(trim($user['user_email'])) >= 1) {
			if(isset($this->request->post['code']) && !is_array($this->request->post['code'])) {
				if(!$this->functions->getTotal('code_confirms', array('confirm_code' => $this->request->post['code'], 'confirm_key' => 'unbind', 'confirm_value' => $user['user_email']))) {
					if($this->functions->getTotal('code_limits', array('limit_key' => 'unbind', 'limit_value' => $user['user_email'])) >= $this->functions->config('limit')) {
						$this->functions->delete('code_confirms', array('confirm_key' => 'unbind', 'confirm_value' => $user['user_email']));
						
						$result[] = array('key' => 'code', 'value' => $this->functions->languageInit('Main_ErrorSettingsLimit'));
					} else {
						$this->functions->create('code_limits', array('limit_code' => $this->request->post['code'], 'limit_key' => 'unbind', 'limit_value' => $user['user_email']), 'limit_date_add');
						
						$result[] = array('key' => 'code', 'value' => $this->functions->languageInit('Main_ErrorSettingsConfirm'));
					}
				}
			}
		} else {
			if(!isset($this->request->post['email']) || is_array($this->request->post['email']) || !preg_match('/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{1,8})$/i', $this->request->post['email'])) {
				$result[] = array('key' => 'email', 'value' => $this->functions->languageInit('Main_ErrorSettingsEmail'));
			} elseif($this->functions->getTotal('users', array('user_email' => $this->request->post['email']))) {
				$result[] = array('key' => 'email', 'value' => $this->functions->languageInit('Main_ErrorSettingsEmailTotal'));
			} elseif(isset($this->request->post['code']) && !is_array($this->request->post['code'])) {
				if(!$this->functions->getTotal('code_confirms', array('confirm_code' => $this->request->post['code'], 'confirm_key' => 'bind', 'confirm_value' => $this->request->post['email']))) {
					if($this->functions->getTotal('code_limits', array('limit_key' => 'bind', 'limit_value' => $this->request->post['email'])) >= $this->functions->config('limit')) {
						$this->functions->delete('code_confirms', array('confirm_key' => 'bind', 'confirm_value' => $this->request->post['email']));
						
						$result[] = array('key' => 'code', 'value' => $this->functions->languageInit('Main_ErrorSettingsLimit'));
					} else {
						$this->functions->create('code_limits', array('limit_code' => $this->request->post['code'], 'limit_key' => 'bind', 'limit_value' => $this->request->post['email']), 'limit_date_add');
						
						$result[] = array('key' => 'code', 'value' => $this->functions->languageInit('Main_ErrorSettingsConfirm'));
					}
				}
			}
		}
		
		return $result;
	}
	
	private function validatePassword() {
		$result = null;
		
		$user = $this->functions->getBy('users', array('user_id' => $this->account->getId()));
		
		if(!isset($this->request->post['oldpassword']) || is_array($this->request->post['oldpassword']) || $user['user_password'] != md5($this->request->post['oldpassword'])) {
			$result[] = array('key' => 'oldpassword', 'value' => $this->functions->languageInit('Main_ErrorSettingsOldpassword'));
		}
		if(!isset($this->request->post['password']) || is_array($this->request->post['password']) || mb_strlen(trim($this->request->post['password'])) < 6 || mb_strlen(trim($this->request->post['password'])) > 64) {
			$result[] = array('key' => 'password', 'value' => $this->functions->languageInit('Main_ErrorSettingsPassword'));
		} elseif(!isset($this->request->post['password2']) || is_array($this->request->post['password2']) || $this->request->post['password'] != $this->request->post['password2']) {
			$result[] = array('key' => 'password2', 'value' => $this->functions->languageInit('Main_ErrorSettingsPassword2'));
		}
		
		return $result;
	}
}
?>
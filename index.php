<?php
mb_internal_encoding('UTF-8');

define('ENGINE_DIR', dirname(__FILE__) . '/engine/');
define('APPLICATION_DIR', dirname(__FILE__) . '/application/');

require_once(ENGINE_DIR . 'main/controller.php');
require_once(ENGINE_DIR . 'main/model.php');
require_once(ENGINE_DIR . 'main/registry.php');
require_once(ENGINE_DIR . 'main/db.php');
require_once(ENGINE_DIR . 'main/request.php');
require_once(ENGINE_DIR . 'main/session.php');
require_once(ENGINE_DIR . 'main/document.php');
require_once(ENGINE_DIR . 'main/functions.php');
require_once(ENGINE_DIR . 'main/account.php');
require_once(ENGINE_DIR . 'main/load.php');
require_once(ENGINE_DIR . 'main/action.php');

$registry = new Registry();

$db = new DB('hostname', 'username', 'password', 'database');
$registry->db = $db;

$request = new Request();
$registry->request = $request;

$session = new Session();
$registry->session = $session;

$document = new Document();
$registry->document = $document;

$functions = new Functions($registry);
$registry->functions = $functions;

$account = new Account($registry);
$registry->account = $account;

$load = new Load($registry);
$registry->load = $load;

$action = new Action($registry);
$registry->action = $action;

if($functions->config('connect') != $request->server['REQUEST_SCHEME'] . '://') {
	header('Location: ' . $functions->config('connect') . htmlspecialchars_decode($request->server['SERVER_NAME']) . htmlspecialchars_decode($request->server['REQUEST_URI']));
}

if(isset($request->get['action']) && !is_array($request->get['action'])) {
	$action->make($request->get['action']);
} else {
	$action->make('index');
}

echo $action->go();
?>
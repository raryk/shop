<?php
class paginationLibrary {
	public $total = 0;
	public $page = 1;
	public $num = 1;
	public $limit = 20;
	public $links = 10;
	public $url = '';
	public $request = 'page';
	public $query = array();
	
	public function render() {
		$total = $this->total;
		
		if($this->page < 1) {
			$page = 1;
		} else {
			$page = $this->page;
		}
		
		$limit = $this->limit;
		
		$links = $this->links;
		$pages = ceil($total / $limit);
		
		$output = array();
		
		if($pages > 1) {
			if($pages <= $links) {
				$start = 1;
				$end = $pages;
			} else {
				$start = $page - floor($links / 2);
				$end = $page + floor($links / 2);
				
				if($start < 1) {
					$end += abs($start) + 1;
					$start = 1;
				}
				
				if($end > $pages) {
					$start -= ($end - $pages);
					$end = $pages;
				}
			}
			
			if($start > 1) {
				if($this->num == 1) {
					$output[] = array(
						'url' => $this->url . (!empty($this->query) ? '?' . http_build_query($this->query) : null),
						'text' => 1,
						'active' => false
					);
				} else {
					$output[] = array(
						'url' => $this->url . (!empty($this->query) ? '?' . http_build_query($this->query) . '&' . $this->request . '=1' : '?' . $this->request . '=1'),
						'text' => 1,
						'active' => false
					);
				}
				
				if($this->num == $start - 1) {
					$output[] = array(
						'url' => $this->url . (!empty($this->query) ? '?' . http_build_query($this->query) : null),
						'text' => '...',
						'active' => false
					);
				} else {
					$output[] = array(
						'url' => $this->url . (!empty($this->query) ? '?' . http_build_query($this->query) . '&' . $this->request . '=' . ($start - 1) : '?' . $this->request . '=' . ($start - 1)),
						'text' => '...',
						'active' => false
					);
				}
			}
			
			for($i = $start; $i <= $end; $i++) {
				if($this->num == $i) {
					$output[] = array(
						'url' => $this->url . (!empty($this->query) ? '?' . http_build_query($this->query) : null),
						'text' => $i,
						'active' => $page == $i ? true : false
					);
				} else {
					$output[] = array(
						'url' => $this->url . (!empty($this->query) ? '?' . http_build_query($this->query) . '&' . $this->request . '=' . $i : '?' . $this->request . '=' . $i),
						'text' => $i,
						'active' => $page == $i ? true : false
					);
				}
			}
			
			if($end < $pages) {
				if($this->num == $end + 1) {
					$output[] = array(
						'url' => $this->url . (!empty($this->query) ? '?' . http_build_query($this->query) : null),
						'text' => '...',
						'active' => false
					);
				} else {
					$output[] = array(
						'url' => $this->url . (!empty($this->query) ? '?' . http_build_query($this->query) . '&' . $this->request . '=' . ($end + 1) : '?' . $this->request . '=' . ($end + 1)),
						'text' => '...',
						'active' => false
					);
				}
				
				if($this->num == $pages) {
					$output[] = array(
						'url' => $this->url . (!empty($this->query) ? '?' . http_build_query($this->query) : null),
						'text' => $pages,
						'active' => false
					);
				} else {
					$output[] = array(
						'url' => $this->url . (!empty($this->query) ? '?' . http_build_query($this->query) . '&' . $this->request . '=' . $pages : '?' . $this->request . '=' . $pages),
						'text' => $pages,
						'active' => false
					);
				}
			}
		}
		
		return $output;
	}
}
?>
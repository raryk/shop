<?php
class mailLibrary {
	protected $to = array();
	protected $from = array();
	protected $subject;
	protected $text;
	protected $socket;
	
	public function addTo($address, $name = null) {
		$this->to[] = array($address, $name);
	}
	
	public function setFrom($address, $name = null) {
		$this->from = array($address, $name);
	}
	
	public function setSubject($subject) {
		$this->subject = $subject;
	}
	
	public function setText($text) {
		$this->text = $text;
	}
	
	public function send($smtp = false, $server = null, $username = null, $password = null) {
		if(!$this->to) {
			exit("Error: E-Mail to required!");
		}
		
		if(!$this->from) {
			exit("Error: E-Mail from required!");
		}
		
		if(!$this->subject) {
			exit("Error: E-Mail subject required!");
		}
		
		if(!$this->text) {
			exit("Error: E-Mail message required!");
		}
		
		$to = array();
		foreach($this->to as $item) {
			$to[] = empty($item[1]) ? $item[0] : '"' . $item[1] . '" <' . $item[0] . '>';
		}
		
		$from = empty($this->from[1]) ? $this->from[0] : '"' . $this->from[1] . '" <' . $this->from[0] . '>';
		
		$headers = "";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "X-Mailer: PHP/" . phpversion() . "\n";
		$headers .= "Date: " . date('r') . "\n";
		$headers .= "From: " . $from . "\n";
		$headers .= "Return-Path: " . $from . "\n";
		$headers .= "Content-Type: text/html; charset=\"utf-8\"\n";
		
		if($smtp) {
			preg_match('/^((ssl|tls):\/\/)*([a-zA-Z0-9\.-]*|\[[a-fA-F0-9:]+\]):?([0-9]*)$/', $server, $hostinfo);
			
			$this->socket = @fsockopen(
				$hostinfo[2] ? $hostinfo[2] . '://' . $hostinfo[3] : $hostinfo[3],
				$hostinfo[4],
				$errorNumber,
				$errorMessage,
				30
			);
			
			if(empty($this->socket)) {
				return false;
			}
			
			$data['CONNECTION'] = $this->getResponse();
			$data['HELLO'][1] = $this->sendCommand('EHLO ' . gethostname());
			
			if($hostinfo[2] == 'tls') {
				$data['STARTTLS'] = $this->sendCommand('STARTTLS');
				@stream_socket_enable_crypto($this->socket, true, STREAM_CRYPTO_METHOD_TLS_CLIENT);
				$data['HELLO'][2] = $this->sendCommand('EHLO ' . gethostname());
			}
			
			$data['AUTH'] = $this->sendCommand('AUTH LOGIN');
			$data['USERNAME'] = $this->sendCommand(base64_encode($username));
			$data['PASSWORD'] = $this->sendCommand(base64_encode($password));
			$data['MAIL_FROM'] = $this->sendCommand('MAIL FROM: <' . $this->from[0] . '>');
			
			foreach($this->to as $address) {
				$data['RECIPIENTS'][] = $this->sendCommand('RCPT TO: <' . $address[0] . '>');
			}
			
			$headers .= "Subject: " . $this->subject . "\n";
			$headers .= "To: " . implode(', ', $to) . "\n";
			
			$data['DATA'][1] = $this->sendCommand('DATA');
			$data['DATA'][2] = $this->sendCommand($headers . "\n" . $this->text . "\n" . '.');
			$data['QUIT'] = $this->sendCommand('QUIT');
			
			fclose($this->socket);
			
			return $data;
		} else {
			return mail(implode(', ', $to), $this->subject, $this->text, $headers);
		}
	}
	
	protected function getResponse() {
		$response = '';
		stream_set_timeout($this->socket, 8);
		while(($line = @fgets($this->socket, 515)) !== false) {
			$response .= trim($line) . "\n";
			if(mb_substr($line, 3, 1) == ' ') {
				break;
			}
		}
		
		return trim($response);
	}
	
	protected function sendCommand($command) {
		fwrite($this->socket, $command . "\n");
		
		return $this->getResponse();
	}
}
?>
<?php
class DB {
	private $link;
	
	public function __construct($hostname, $username, $password, $database) {
		$this->link = mysqli_connect($hostname, $username, $password);
		
		if(!$this->link) {
			exit('Error: Failed to connect to the database server!');
		}
		
		if(!mysqli_select_db($this->link, $database)) {
			exit('Error: Failed to connect to ' . $database . ' database!');
		}
		
		mysqli_query($this->link, "SET NAMES 'utf8'");
		mysqli_query($this->link, "SET CHARACTER SET utf8");
		mysqli_query($this->link, "SET CHARACTER_SET_CONNECTION=utf8");
		mysqli_query($this->link, "SET SQL_MODE = ''");
	}
	
	public function query($sql) {
		$result = mysqli_query($this->link, $sql);
		
		if($result) {
			if($result instanceof mysqli_result) {
				$i = 0;
				$data = array();
				
				while($row = mysqli_fetch_assoc($result)) {
					$data[$i] = $row;
					$i++;
				}
				
				mysqli_free_result($result);
				
				$query = new stdClass();
				$query->row = isset($data[0]) ? $data[0] : array();
				$query->rows = $data;
				$query->num_rows = $i;
				
				unset($data);
				return $query;
			} else {
				return true;
			}
		} else {
			exit('Error: ' . mysqli_error($this->link) . '<br>Error number: ' . mysqli_errno($this->link) . '<br>' . $sql);
		}
	}
	
	public function escape($string) {
		return mysqli_real_escape_string($this->link, $string);
	}
	
	public function insert() {
		return mysqli_insert_id($this->link);
	}
	
	public function __destruct() {
		mysqli_close($this->link);
	}
}
?>
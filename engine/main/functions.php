<?php
class Functions {
	private $registry;
	
	public function __construct($registry) {
		$this->registry = $registry;
	}
	
	public function config($key = null) {
		$query = $this->registry->db->query("SELECT * FROM system_configs WHERE config_key = '" . $this->registry->db->escape($key) . "'");
		if($query->num_rows) {
			return htmlspecialchars_decode($query->row['config_value']);
		} else {
			return $key;
		}
	}
	
	public function datetime($time = 0, $status = 0, $type = true) {
		if($type) {
			try {
				$date = date_create(date('Y-m-d H:i:s', $time), new DateTimeZone($this->timezoneInit()));
				$date->setTimezone(new DateTimeZone($this->timezoneInit($this->timezone())));
			} catch(Exception $e) {
				$date = date_create(date('Y-m-d H:i:s', $time));
			}
		} else {
			$date = date_create(date('Y-m-d H:i:s', $time));
		}
		
		if($status == 2) {
			return $this->isHour12() ? $date->format('g:i A') : $date->format('H:i');
		} else {
			if($date->format('n') == 1) {
				$content = $status == 1 ? $this->languageInit('SystemDatetime1Month1') : $this->languageInit('SystemDatetime0Month1');
			} elseif($date->format('n') == 2) {
				$content = $status == 1 ? $this->languageInit('SystemDatetime1Month2') : $this->languageInit('SystemDatetime0Month2');
			} elseif($date->format('n') == 3) {
				$content = $status == 1 ? $this->languageInit('SystemDatetime1Month3') : $this->languageInit('SystemDatetime0Month3');
			} elseif($date->format('n') == 4) {
				$content = $status == 1 ? $this->languageInit('SystemDatetime1Month4') : $this->languageInit('SystemDatetime0Month4');
			} elseif($date->format('n') == 5) {
				$content = $status == 1 ? $this->languageInit('SystemDatetime1Month5') : $this->languageInit('SystemDatetime0Month5');
			} elseif($date->format('n') == 6) {
				$content = $status == 1 ? $this->languageInit('SystemDatetime1Month6') : $this->languageInit('SystemDatetime0Month6');
			} elseif($date->format('n') == 7) {
				$content = $status == 1 ? $this->languageInit('SystemDatetime1Month7') : $this->languageInit('SystemDatetime0Month7');
			} elseif($date->format('n') == 8) {
				$content = $status == 1 ? $this->languageInit('SystemDatetime1Month8') : $this->languageInit('SystemDatetime0Month8');
			} elseif($date->format('n') == 9) {
				$content = $status == 1 ? $this->languageInit('SystemDatetime1Month9') : $this->languageInit('SystemDatetime0Month9');
			} elseif($date->format('n') == 10) {
				$content = $status == 1 ? $this->languageInit('SystemDatetime1Month10') : $this->languageInit('SystemDatetime0Month10');
			} elseif($date->format('n') == 11) {
				$content = $status == 1 ? $this->languageInit('SystemDatetime1Month11') : $this->languageInit('SystemDatetime0Month11');
			} elseif($date->format('n') == 12) {
				$content = $status == 1 ? $this->languageInit('SystemDatetime1Month12') : $this->languageInit('SystemDatetime0Month12');
			}
			
			return str_replace(array('{day}', '{year}', '{time}'), array($date->format('j'), $date->format('Y'), $this->isHour12() ? $date->format('g:i A') : $date->format('H:i')), $content);
		}
	}
	
	public function time($time = 'now', $type = false) {
		if($type) {
			try {
				$date = (new DateTime($time, new DateTimeZone($this->timezoneInit($this->timezone()))));
				$date->setTimezone(new DateTimeZone($this->timezoneInit()));
				
				return strtotime($date->format('Y-m-d H:i:s'));
			} catch(Exception $e) {
				try {
					return strtotime((new DateTime($time, new DateTimeZone($this->timezoneInit())))->format('Y-m-d H:i:s'));
				} catch(Exception $e) {
					return strtotime((new DateTime($time))->format('Y-m-d H:i:s'));
				}
			}
		} else {
			try {
				return strtotime((new DateTime($time, new DateTimeZone($this->timezoneInit())))->format('Y-m-d H:i:s'));
			} catch(Exception $e) {
				return strtotime((new DateTime($time))->format('Y-m-d H:i:s'));
			}
		}
	}
	
	public function convert($amount = 0, $to = null, $from = null) {
		$query = $this->registry->db->query("SELECT * FROM system_currencies WHERE currencie_id = '" . $this->registry->db->escape($from) . "'");
		if($query->num_rows) {
			$from = floatval($query->row['currencie_value']);
			
			$query = $this->registry->db->query("SELECT * FROM system_currencies WHERE currencie_id = '" . $this->registry->db->escape($to) . "'");
			if($query->num_rows) {
				$to = floatval($query->row['currencie_value']);
				
				if($to) {
					return floor($amount / $from * $to * 100) / 100;
				} else {
					return floor($amount * $to * 100) / 100;
				}
			} else {
				if($from) {
					return floor($amount / $from * 100) / 100;
				} else {
					return floor($amount * 100) / 100;
				}
			}
		} else {
			$query = $this->registry->db->query("SELECT * FROM system_currencies WHERE currencie_id = '" . $this->registry->db->escape($to) . "'");
			if($query->num_rows) {
				$to = floatval($query->row['currencie_value']);
				
				return floor($amount * $to * 100) / 100;
			} else {
				return floor($amount * 100) / 100;
			}
		}
	}
	
	public function image($imagesecret = null) {
		$image = array();
		
		$query = $this->registry->db->query("SELECT * FROM upload_images LEFT JOIN upload_servers ON upload_images.image_server=upload_servers.server_id WHERE image_secret = '" . $this->registry->db->escape($imagesecret) . "'");
		if($query->num_rows) {
			foreach(json_decode($query->row['image_content'], true) as $key => $value) {
				$image[$key] = $query->row['server_url'] . '/' . $value;
			}
		}
		
		return $image;
	}
	
	public function isHour12() {
		$query = $this->registry->db->query("SELECT * FROM system_languages WHERE language_id = '" . $this->registry->db->escape($this->language()) . "'");
		if($query->num_rows) {
			return $query->row['language_hour12'] ? true : false;
		} else {
			return false;
		}
	}
	
	public function isRtl() {
		$query = $this->registry->db->query("SELECT * FROM system_languages WHERE language_id = '" . $this->registry->db->escape($this->language()) . "'");
		if($query->num_rows) {
			return $query->row['language_rtl'] ? true : false;
		} else {
			return false;
		}
	}
	
	public function languageInit($key = null) {
		$query = $this->registry->db->query("SELECT * FROM system_translates WHERE translate_key = '" . $this->registry->db->escape($key) . "'");
		if($query->num_rows) {
			if(isset(json_decode($query->row['translate_value'], true)[$this->language()])) {
				return json_decode($query->row['translate_value'], true)[$this->language()];
			} else {
				return json_decode($query->row['translate_value'], true)[$this->config('language')];
			}
		} else {
			return $key;
		}
	}
	
	public function languageBy($language = null, $type = true) {
		$query = $this->registry->db->query("SELECT * FROM system_languages WHERE language_id = '" . $this->registry->db->escape($language) . "' OR language_key = '" . $this->registry->db->escape($language) . "'");
		if($query->num_rows) {
			return $type ? $query->row['language_key'] : $query->row['language_id'];
		} else {
			$query = $this->registry->db->query("SELECT * FROM system_languages WHERE language_id = '" . $this->registry->db->escape($this->config('language')) . "'");
			if($query->num_rows) {
				return $type ? $query->row['language_key'] : $query->row['language_id'];
			} else {
				return $language;
			}
		}
	}
	
	public function timezoneInit($timezone = null) {
		$query = $this->registry->db->query("SELECT * FROM system_timezones WHERE timezone_id = '" . $this->registry->db->escape($timezone) . "'");
		if($query->num_rows) {
			return $query->row['timezone_key'];
		} else {
			$query = $this->registry->db->query("SELECT * FROM system_timezones WHERE timezone_id = '" . $this->registry->db->escape($this->config('timezone')) . "'");
			if($query->num_rows) {
				return $query->row['timezone_key'];
			} else {
				return $timezone;
			}
		}
	}
	
	public function currencieInit($currencie = null, $amount = 0, $type = false) {
		$query = $this->registry->db->query("SELECT * FROM system_currencies WHERE currencie_id = '" . $this->registry->db->escape($currencie) . "'");
		if($query->num_rows) {
			if($type) {
				return number_format($amount, 2, ',', ' ') . ' ' . $query->row['currencie_key'];
			} else {
				return str_replace('{amount}', number_format($amount, 2, ',', ' '), $query->row['currencie_name']);
			}
		} else {
			$query = $this->registry->db->query("SELECT * FROM system_currencies WHERE currencie_id = '" . $this->registry->db->escape($this->config('currencie')) . "'");
			if($query->num_rows) {
				if($type) {
					return number_format($amount, 2, ',', ' ') . ' ' . $query->row['currencie_key'];
				} else {
					return str_replace('{amount}', number_format($amount, 2, ',', ' '), $query->row['currencie_name']);
				}
			} else {
				return number_format($amount, 2, ',', ' ');
			}
		}
	}
	
	public function currencieBy($currencie = null, $type = true) {
		$query = $this->registry->db->query("SELECT * FROM system_currencies WHERE currencie_id = '" . $this->registry->db->escape($currencie) . "' OR currencie_key = '" . $this->registry->db->escape($currencie) . "'");
		if($query->num_rows) {
			return $type ? $query->row['currencie_key'] : $query->row['currencie_id'];
		} else {
			$query = $this->registry->db->query("SELECT * FROM system_currencies WHERE currencie_id = '" . $this->registry->db->escape($this->config('currencie')) . "'");
			if($query->num_rows) {
				return $type ? $query->row['currencie_key'] : $query->row['currencie_id'];
			} else {
				return $currencie;
			}
		}
	}
	
	public function number2string($number = null) {
		if($number > 0 && $number < 1000) {
			$number_format = floor($number);
			$suffix = '';
		} elseif($number >= 1000 && $number < 1000000) {
			$number_format = floor($number / 1000);
			$suffix = 'K';
		} elseif($number >= 1000000 && $number < 1000000000) {
			$number_format = floor($number / 1000000);
			$suffix = 'M';
		} elseif($number >= 1000000000 && $number < 1000000000000) {
			$number_format = floor($number / 1000000000);
			$suffix = 'B';
		} elseif($number >= 1000000000000) {
			$number_format = floor($number / 1000000000000);
			$suffix = 'T';
		}
		
		return !empty($number_format . $suffix) ? $number_format . $suffix : 0;
	}
	
	public function number2format($number = null) {
		return $number ? number_format($number, 0, '.', ' ') : 0;
	}
	
	public function create($table = null, $data = array(), $date = null) {
		$sql = "INSERT INTO " . $this->registry->db->escape($table) . " SET";
		
		if(!empty($data)) {
			foreach($data as $key => $value) {
				$sql .= " " . $this->registry->db->escape($key) . " = '" . $this->registry->db->escape($value) . "',";
			}
		}
		if(!empty($date)) {
			$sql .= " " . $this->registry->db->escape($date) . " = NOW()";
		}
		
		$this->registry->db->query($sql);
		
		return $this->registry->db->insert();
	}
	
	public function delete($table = null, $data = array()) {
		$sql = "DELETE FROM " . $this->registry->db->escape($table) . "";
		
		if(!empty($data)) {
			$count = count($data);
			$sql .= " WHERE";
			foreach($data as $key => $value) {
				$sql .= " " . $this->registry->db->escape($key) . " = '" . $this->registry->db->escape($value) . "'";
				
				$count--;
				if($count > 0) $sql .= " AND";
			}
		}
		
		$this->registry->db->query($sql);
	}
	
	public function update($table = null, $data = array(), $joins = array()) {
		$sql = "UPDATE " . $this->registry->db->escape($table) . "";
		
		if(!empty($data)) {
			$count = count($data);
			$sql .= " SET";
			foreach($data as $key => $value) {
				$sql .= " " . $this->registry->db->escape($key) . " = '" . $this->registry->db->escape($value) . "'";
				
				$count--;
				if($count > 0) $sql .= ",";
			}
		}
		if(!empty($joins)) {
			$count = count($joins);
			$sql .= " WHERE";
			foreach($joins as $key => $value) {
				$sql .= " " . $this->registry->db->escape($key) . " = '" . $this->registry->db->escape($value) . "'";
				
				$count--;
				if($count > 0) $sql .= " AND";
			}
		}
		
		$this->registry->db->query($sql);
	}
	
	public function get($table = null, $data = array(), $search = array(), $joins = array(), $sort = array(), $options = array()) {
		$sql = "SELECT * FROM " . $this->registry->db->escape($table) . "";
		
		foreach($joins as $key => $value) {
			$sql .= " LEFT JOIN " . $this->registry->db->escape($key) . " ON " . $this->registry->db->escape($value) . "";
		}
		if(!empty($data) || !empty($search)) {
			$countData = count($data);
			$countSearch = count($search);
			$sql .= " WHERE";
			
			foreach($data as $key => $value) {
				$sql .= " " . $this->registry->db->escape($key) . " = '" . $this->registry->db->escape($value) . "'";
				
				$countData--;
				if($countData > 0 || $countSearch > 0) $sql .= " AND";
			}
			if(!empty($search)) {
				$sql .= " (";
				foreach($search as $key => $value) {
					$sql .= " LOWER(" . $this->registry->db->escape($key) . ") LIKE '%" . $this->registry->db->escape(mb_strtolower($value)) . "%'";
					
					$countSearch--;
					if($countSearch > 0) $sql .= " OR";
				}
				$sql .= ")";
			}
		}
		if(!empty($sort)) {
			$count = count($sort);
			$sql .= " ORDER BY";
			foreach($sort as $key => $value) {
				$sql .= " " . $this->registry->db->escape($key) . " " . $this->registry->db->escape($value);
				
				$count--;
				if($count > 0) $sql .= ",";
			}
		}
		if(!empty($options)) {
			if($options['start'] < 0) {
				$options['start'] = 0;
			}
			if($options['limit'] < 1) {
				$options['limit'] = $this->config('limit');
			}
			$sql .= " LIMIT " . $this->registry->db->escape($options['start']) . "," . $this->registry->db->escape($options['limit']);
		}
		
		$query = $this->registry->db->query($sql);
		return $query->rows;
	}
	
	public function getBy($table = null, $data = array(), $search = array(), $joins = array(), $sort = array()) {
		$sql = "SELECT * FROM " . $this->registry->db->escape($table) . "";
		
		foreach($joins as $key => $value) {
			$sql .= " LEFT JOIN " . $this->registry->db->escape($key) . " ON " . $this->registry->db->escape($value) . "";
		}
		if(!empty($data) || !empty($search)) {
			$countData = count($data);
			$countSearch = count($search);
			$sql .= " WHERE";
			
			foreach($data as $key => $value) {
				$sql .= " " . $this->registry->db->escape($key) . " = '" . $this->registry->db->escape($value) . "'";
				
				$countData--;
				if($countData > 0 || $countSearch > 0) $sql .= " AND";
			}
			if(!empty($search)) {
				$sql .= " (";
				foreach($search as $key => $value) {
					$sql .= " LOWER(" . $this->registry->db->escape($key) . ") LIKE '%" . $this->registry->db->escape(mb_strtolower($value)) . "%'";
					
					$countSearch--;
					if($countSearch > 0) $sql .= " OR";
				}
				$sql .= ")";
			}
		}
		if(!empty($sort)) {
			$count = count($sort);
			$sql .= " ORDER BY";
			foreach($sort as $key => $value) {
				$sql .= " " . $this->registry->db->escape($key) . " " . $this->registry->db->escape($value);
				
				$count--;
				if($count > 0) $sql .= ",";
			}
		}
		$sql .= " LIMIT 1";
		
		$query = $this->registry->db->query($sql);
		return $query->row;
	}
	
	public function getTotal($table = null, $data = array(), $search = array(), $joins = array()) {
		$sql = "SELECT COUNT(*) AS count FROM " . $this->registry->db->escape($table) . "";
		
		foreach($joins as $key => $value) {
			$sql .= " LEFT JOIN " . $this->registry->db->escape($key) . " ON " . $this->registry->db->escape($value) . "";
		}
		if(!empty($data) || !empty($search)) {
			$countData = count($data);
			$countSearch = count($search);
			$sql .= " WHERE";
			
			foreach($data as $key => $value) {
				$sql .= " " . $this->registry->db->escape($key) . " = '" . $this->registry->db->escape($value) . "'";
				
				$countData--;
				if($countData > 0 || $countSearch > 0) $sql .= " AND";
			}
			if(!empty($search)) {
				$sql .= " (";
				foreach($search as $key => $value) {
					$sql .= " LOWER(" . $this->registry->db->escape($key) . ") LIKE '%" . $this->registry->db->escape(mb_strtolower($value)) . "%'";
					
					$countSearch--;
					if($countSearch > 0) $sql .= " OR";
				}
				$sql .= ")";
			}
		}
		
		$query = $this->registry->db->query($sql);
		return $query->row['count'];
	}
	
	public function keyLanguage() {
		preg_match_all('/([a-zA-Z]{1,8}(?:-[a-zA-Z]{1,8})?)(?:;q=([0-9.]+))?/', $this->registry->request->server['HTTP_ACCEPT_LANGUAGE'], $matches);
		
		$language = array();
		
		foreach(array_combine($matches[1], $matches[2]) as $key => $value) {
			$language[$key] = $value ? $value : 1;
		}
		
		arsort($language);
		return $language;
	}
	
	public function language() {
		if(isset($this->registry->request->get['language']) && !is_array($this->registry->request->get['language'])) {
			$query = $this->registry->db->query("SELECT * FROM system_languages WHERE language_id = '" . $this->registry->db->escape($this->languageBy($this->registry->request->get['language'], false)) . "'");
			if($query->num_rows) {
				$language = $query->row['language_id'];
			} else {
				if($this->registry->account->isLogged()) {
					$query = $this->registry->db->query("SELECT * FROM system_languages WHERE language_id = '" . $this->registry->db->escape($this->registry->account->getLanguage()) . "'");
					if($query->num_rows) {
						$language = $query->row['language_id'];
					} else {
						if(isset($this->registry->request->cookie['language']) && !is_array($this->registry->request->cookie['language'])) {
							$query = $this->registry->db->query("SELECT * FROM system_languages WHERE language_id = '" . $this->registry->db->escape($this->registry->request->cookie['language']) . "'");
							if($query->num_rows) {
								$language = $query->row['language_id'];
							} else {
								$sql = "SELECT * FROM system_languages";
								
								if(!empty($this->keyLanguage())) {
									$sql .= " WHERE";
									$count = count($this->keyLanguage());
									
									foreach($this->keyLanguage() as $key => $value) {
										$sql .= " language_key = '" . $this->registry->db->escape($key) . "'";
										
										$count--;
										if($count > 0) $sql .= " OR";
									}
								}
								
								$query = $this->registry->db->query($sql);
								if($query->num_rows) {
									$language = $query->row['language_id'];
								} else {
									$language = $this->config('language');
								}
							}
						} else {
							$sql = "SELECT * FROM system_languages";
							
							if(!empty($this->keyLanguage())) {
								$sql .= " WHERE";
								$count = count($this->keyLanguage());
								
								foreach($this->keyLanguage() as $key => $value) {
									$sql .= " language_key = '" . $this->registry->db->escape($key) . "'";
									
									$count--;
									if($count > 0) $sql .= " OR";
								}
							}
							
							$query = $this->registry->db->query($sql);
							if($query->num_rows) {
								$language = $query->row['language_id'];
							} else {
								$language = $this->config('language');
							}
						}
					}
				} elseif(isset($this->registry->request->cookie['language']) && !is_array($this->registry->request->cookie['language'])) {
					$query = $this->registry->db->query("SELECT * FROM system_languages WHERE language_id = '" . $this->registry->db->escape($this->registry->request->cookie['language']) . "'");
					if($query->num_rows) {
						$language = $query->row['language_id'];
					} else {
						$sql = "SELECT * FROM system_languages";
						
						if(!empty($this->keyLanguage())) {
							$sql .= " WHERE";
							$count = count($this->keyLanguage());
							
							foreach($this->keyLanguage() as $key => $value) {
								$sql .= " language_key = '" . $this->registry->db->escape($key) . "'";
								
								$count--;
								if($count > 0) $sql .= " OR";
							}
						}
						
						$query = $this->registry->db->query($sql);
						if($query->num_rows) {
							$language = $query->row['language_id'];
						} else {
							$language = $this->config('language');
						}
					}
				}
			}
		} elseif($this->registry->account->isLogged()) {
			$query = $this->registry->db->query("SELECT * FROM system_languages WHERE language_id = '" . $this->registry->db->escape($this->registry->account->getLanguage()) . "'");
			if($query->num_rows) {
				$language = $query->row['language_id'];
			} else {
				if(isset($this->registry->request->cookie['language']) && !is_array($this->registry->request->cookie['language'])) {
					$query = $this->registry->db->query("SELECT * FROM system_languages WHERE language_id = '" . $this->registry->db->escape($this->registry->request->cookie['language']) . "'");
					if($query->num_rows) {
						$language = $query->row['language_id'];
					} else {
						$sql = "SELECT * FROM system_languages";
						
						if(!empty($this->keyLanguage())) {
							$sql .= " WHERE";
							$count = count($this->keyLanguage());
							
							foreach($this->keyLanguage() as $key => $value) {
								$sql .= " language_key = '" . $this->registry->db->escape($key) . "'";
								
								$count--;
								if($count > 0) $sql .= " OR";
							}
						}
						
						$query = $this->registry->db->query($sql);
						if($query->num_rows) {
							$language = $query->row['language_id'];
						} else {
							$language = $this->config('language');
						}
					}
				} else {
					$sql = "SELECT * FROM system_languages";
					
					if(!empty($this->keyLanguage())) {
						$sql .= " WHERE";
						$count = count($this->keyLanguage());
						
						foreach($this->keyLanguage() as $key => $value) {
							$sql .= " language_key = '" . $this->registry->db->escape($key) . "'";
							
							$count--;
							if($count > 0) $sql .= " OR";
						}
					}
					
					$query = $this->registry->db->query($sql);
					if($query->num_rows) {
						$language = $query->row['language_id'];
					} else {
						$language = $this->config('language');
					}
				}
				
				$this->registry->db->query("UPDATE users SET user_language = '" . $this->registry->db->escape($language) . "' WHERE user_id = '" . $this->registry->db->escape($this->registry->account->getId()) . "'");
			}
		} elseif(isset($this->registry->request->cookie['language']) && !is_array($this->registry->request->cookie['language'])) {
			$query = $this->registry->db->query("SELECT * FROM system_languages WHERE language_id = '" . $this->registry->db->escape($this->registry->request->cookie['language']) . "'");
			if($query->num_rows) {
				$language = $query->row['language_id'];
			} else {
				$sql = "SELECT * FROM system_languages";
				
				if(!empty($this->keyLanguage())) {
					$sql .= " WHERE";
					$count = count($this->keyLanguage());
					
					foreach($this->keyLanguage() as $key => $value) {
						$sql .= " language_key = '" . $this->registry->db->escape($key) . "'";
						
						$count--;
						if($count > 0) $sql .= " OR";
					}
				}
				
				$query = $this->registry->db->query($sql);
				if($query->num_rows) {
					$language = $query->row['language_id'];
				} else {
					$language = $this->config('language');
				}
			}
		} else {
			$sql = "SELECT * FROM system_languages";
			
			if(!empty($this->keyLanguage())) {
				$sql .= " WHERE";
				$count = count($this->keyLanguage());
				
				foreach($this->keyLanguage() as $key => $value) {
					$sql .= " language_key = '" . $this->registry->db->escape($key) . "'";
					
					$count--;
					if($count > 0) $sql .= " OR";
				}
			}
			
			$query = $this->registry->db->query($sql);
			if($query->num_rows) {
				$language = $query->row['language_id'];
			} else {
				$language = $this->config('language');
			}
		}
		
		return $language;
	}
	
	public function timezone() {
		if($this->registry->account->isLogged()) {
			$query = $this->registry->db->query("SELECT * FROM system_timezones WHERE timezone_id = '" . $this->registry->db->escape($this->registry->account->getTimezone()) . "'");
			if($query->num_rows) {
				$timezone = $query->row['timezone_id'];
			} else {
				if(isset($this->registry->request->cookie['timezone']) && !is_array($this->registry->request->cookie['timezone'])) {
					$query = $this->registry->db->query("SELECT * FROM system_timezones WHERE timezone_id = '" . $this->registry->db->escape($this->registry->request->cookie['timezone']) . "'");
					if($query->num_rows) {
						$timezone = $query->row['timezone_id'];
					} else {
						$timezone = $this->config('timezone');
					}
				} else {
					$timezone = $this->config('timezone');
				}
				
				$this->registry->db->query("UPDATE users SET user_timezone = '" . $this->registry->db->escape($timezone) . "' WHERE user_id = '" . $this->registry->db->escape($this->registry->account->getId()) . "'");
			}
		} elseif(isset($this->registry->request->cookie['timezone']) && !is_array($this->registry->request->cookie['timezone'])) {
			$query = $this->registry->db->query("SELECT * FROM system_timezones WHERE timezone_id = '" . $this->registry->db->escape($this->registry->request->cookie['timezone']) . "'");
			if($query->num_rows) {
				$timezone = $query->row['timezone_id'];
			} else {
				$timezone = $this->config('timezone');
			}
		} else {
			$timezone = $this->config('timezone');
		}
		
		return $timezone;
	}
	
	public function currencie() {
		if($this->registry->account->isLogged()) {
			$query = $this->registry->db->query("SELECT * FROM system_currencies WHERE currencie_id = '" . $this->registry->db->escape($this->registry->account->getCurrencie()) . "'");
			if($query->num_rows) {
				$currencie = $query->row['currencie_id'];
			} else {
				if(isset($this->registry->request->cookie['currencie']) && !is_array($this->registry->request->cookie['currencie'])) {
					$query = $this->registry->db->query("SELECT * FROM system_currencies WHERE currencie_id = '" . $this->registry->db->escape($this->registry->request->cookie['currencie']) . "'");
					if($query->num_rows) {
						$currencie = $query->row['currencie_id'];
					} else {
						$currencie = $this->config('currencie');
					}
				} else {
					$currencie = $this->config('currencie');
				}
				
				$this->registry->db->query("UPDATE users SET user_currencie = '" . $this->registry->db->escape($currencie) . "' WHERE user_id = '" . $this->registry->db->escape($this->registry->account->getId()) . "'");
			}
		} elseif(isset($this->registry->request->cookie['currencie']) && !is_array($this->registry->request->cookie['currencie'])) {
			$query = $this->registry->db->query("SELECT * FROM system_currencies WHERE currencie_id = '" . $this->registry->db->escape($this->registry->request->cookie['currencie']) . "'");
			if($query->num_rows) {
				$currencie = $query->row['currencie_id'];
			} else {
				$currencie = $this->config('currencie');
			}
		} else {
			$currencie = $this->config('currencie');
		}
		
		return $currencie;
	}
	
	public function guard() {
		$ip = $this->registry->request->server['REMOTE_ADDR'];
		$agent = $this->registry->request->server['HTTP_USER_AGENT'];
		
		if($this->registry->account->isLogged()) {
			$query = $this->registry->db->query("SELECT * FROM guard_events WHERE user_id = '" . $this->registry->db->escape($this->registry->account->getId()) . "' AND event_ip = '" . $this->registry->db->escape($ip) . "' AND event_date_add > NOW() - INTERVAL '" . $this->registry->db->escape($this->config('time')) . "' SECOND");
			if($query->num_rows >= $this->config('limit')) {
				return false;
			} else {
				$this->registry->db->query("INSERT INTO guard_events SET user_id = '" . $this->registry->db->escape($this->registry->account->getId()) . "', event_ip = '" . $this->registry->db->escape($ip) . "', event_agent = '" . $this->registry->db->escape($agent) . "', event_date_add = NOW()");
				
				return true;
			}
		} else {
			$query = $this->registry->db->query("SELECT * FROM guard_logs WHERE log_ip = '" . $this->registry->db->escape($ip) . "' AND log_date_add > NOW() - INTERVAL '" . $this->registry->db->escape($this->config('time')) . "' SECOND");
			if($query->num_rows >= $this->config('limit')) {
				return false;
			} else {
				$this->registry->db->query("INSERT INTO guard_logs SET log_ip = '" . $this->registry->db->escape($ip) . "', log_agent = '" . $this->registry->db->escape($agent) . "', log_date_add = NOW()");
				
				return true;
			}
		}
	}
	
	public function getCsrf() {
		if(!isset($this->registry->session->data['csrf']) || is_array($this->registry->session->data['csrf'])) {
			$csrf = mb_strtolower(mb_substr(md5(time() . mb_substr(str_shuffle('abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 16)), rand(1, 8), 16));
			
			$this->registry->session->data['csrf'] = $csrf;
		}
		
		return $this->registry->session->data['csrf'];
	}
	
	public function string($string = null, $link = false) {
		preg_match_all('/\bhttps?:\/\/[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i', $string, $match);
		
		$position = 0;
		$start = 0;
		$last = 0;
		
		$links = array();
		$array = array();
		
		if($link) {
			foreach($match[0] as $item) {
				if(mb_stripos($string, $item, $position) !== false) {
					$links[] = array(
						'min' => mb_stripos($string, $item, $position),
						'max' => mb_stripos($string, $item, $position) + mb_strlen($item),
						'text' => mb_substr($string, mb_stripos($string, $item, $position), mb_strlen($item))
					);
					
					$position = mb_stripos($string, $item, $position) + mb_strlen($item);
				}
			}
		}
		
		if(isset($this->registry->request->get['q']) && !is_array($this->registry->request->get['q']) && mb_stripos($string, $this->registry->request->get['q'], $start) !== false) {
			while(($pos = mb_stripos($string, $this->registry->request->get['q'], $start)) !== false) {
				if($start == 0 && $pos != 0) {
					$sort = array('min' => $start, 'max' => $pos);
					
					$filter = array_values(array_filter($links, function($link) use($sort) {
						return $link['min'] >= $sort['min'] && $link['min'] < $sort['max'];
					}));
					
					if(!empty($filter)) {
						if($start < $filter[0]['min']) {
							$array[$start] = array('type' => 'search', 'text' => mb_substr($string, $start, $filter[0]['min'] - $start));
						}
						
						for($i = 0; $i < count($filter); $i++) {
							if($filter[$i]['max'] > $pos) {
								$array[$filter[$i]['min']] = array(
									'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
									'string' => array(
										array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['min'], $pos - $filter[$i]['min']))
									)
								);
								$last = $filter[$i]['min'];
							} else {
								$array[$filter[$i]['min']] = array(
									'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
									'string' => array(
										array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']))
									)
								);
								
								if($i + 1 < count($filter)) {
									$array[$filter[$i]['max']] = array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['max'], $filter[$i + 1]['min'] - $filter[$i]['max']));
								} elseif($pos > $filter[$i]['max']) {
									$array[$filter[$i]['max']] = array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['max'], $pos - $filter[$i]['max']));
								}
							}
						}
					} else {
						$array[$start] = array('type' => 'text', 'text' => mb_substr($string, $start, $pos));
					}
				}
				
				$start = $pos + mb_strlen($this->registry->request->get['q']);
				
				if(isset($array[$last]) && $pos > $last && $pos < $last + mb_strlen($array[$last]['link'])) {
					if($start > $last + mb_strlen($array[$last]['link'])) {
						$array[$last]['string'][] = array('type' => 'search', 'text' => mb_substr($string, $pos, $last + mb_strlen($array[$last]['link']) - $pos));
						
						$sort = array('min' => $last + mb_strlen($array[$last]['link']), 'max' => $start);
						
						$filter = array_values(array_filter($links, function($link) use($sort) {
							return $link['min'] >= $sort['min'] && $link['min'] < $sort['max'];
						}));
						
						if(!empty($filter)) {
							if($last + mb_strlen($array[$last]['link']) < $filter[0]['min']) {
								$array[$last + mb_strlen($array[$last]['link'])] = array('type' => 'search', 'text' => mb_substr($string, $last + mb_strlen($array[$last]['link']), $filter[0]['min'] - $last - mb_strlen($array[$last]['link'])));
							}
							
							for($i = 0; $i < count($filter); $i++) {
								if($filter[$i]['max'] > $start) {
									$array[$filter[$i]['min']] = array(
										'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
										'string' => array(
											array('type' => 'search', 'text' => mb_substr($string, $filter[$i]['min'], $start - $filter[$i]['min']))
										)
									);
									$last = $filter[$i]['min'];
								} else {
									$array[$filter[$i]['min']] = array(
										'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
										'string' => array(
											array('type' => 'search', 'text' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']))
										)
									);
									
									if($i + 1 < count($filter)) {
										$array[$filter[$i]['max']] = array('type' => 'search', 'text' => mb_substr($string, $filter[$i]['max'], $filter[$i + 1]['min'] - $filter[$i]['max'])
										);
									} elseif($start > $filter[$i]['max']) {
										$array[$filter[$i]['max']] = array('type' => 'search', 'text' => mb_substr($string, $filter[$i]['max'], $start - $filter[$i]['max']));
									}
								}
							}
						} else {
							$array[$last + mb_strlen($array[$last]['link'])] = array('type' => 'search', 'text' => mb_substr($string, $last + mb_strlen($array[$last]['link']), $start - $last - mb_strlen($array[$last]['link'])));
						}
					} else {
						$array[$last]['string'][] = array('type' => 'search', 'text' => mb_substr($string, $pos, $start - $pos));
					}
				} else {
					$sort = array('min' => $pos, 'max' => $start);
					
					$filter = array_values(array_filter($links, function($link) use($sort) {
						return $link['min'] >= $sort['min'] && $link['min'] < $sort['max'];
					}));
					
					if(!empty($filter)) {
						if($pos < $filter[0]['min']) {
							$array[$pos] = array('type' => 'search', 'text' => mb_substr($string, $pos, $filter[0]['min'] - $pos));
						}
						
						for($i = 0; $i < count($filter); $i++) {
							if($filter[$i]['max'] > $start) {
								$array[$filter[$i]['min']] = array(
									'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
									'string' => array(
										array('type' => 'search', 'text' => mb_substr($string, $filter[$i]['min'], $start - $filter[$i]['min']))
									)
								);
								$last = $filter[$i]['min'];
							} else {
								$array[$filter[$i]['min']] = array(
									'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
									'string' => array(
										array('type' => 'search', 'text' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']))
									)
								);
								
								if($i + 1 < count($filter)) {
									$array[$filter[$i]['max']] = array('type' => 'search', 'text' => mb_substr($string, $filter[$i]['max'], $filter[$i + 1]['min'] - $filter[$i]['max']));
								} elseif($start > $filter[$i]['max']) {
									$array[$filter[$i]['max']] = array('type' => 'search', 'text' => mb_substr($string, $filter[$i]['max'], $start - $filter[$i]['max']));
								}
							}
						}
					} else {
						$array[$pos] = array('type' => 'search', 'text' => mb_substr($string, $pos, $start - $pos));
					}
				}
				
				if(mb_stripos($string, $this->registry->request->get['q'], $start) !== false && $start != mb_stripos($string, $this->registry->request->get['q'], $start)) {
					if(isset($array[$last]) && $start > $last && $start < $last + mb_strlen($array[$last]['link'])) {
						if(mb_stripos($string, $this->registry->request->get['q'], $start) > $last + mb_strlen($array[$last]['link'])) {
							$array[$last]['string'][] = array('type' => 'text', 'text' => mb_substr($string, $start, $last + mb_strlen($array[$last]['link']) - $start));
							
							$sort = array('min' => $last + mb_strlen($array[$last]['link']), 'max' => mb_stripos($string, $this->registry->request->get['q'], $start));
							
							$filter = array_values(array_filter($links, function($link) use($sort) {
								return $link['min'] >= $sort['min'] && $link['min'] < $sort['max'];
							}));
							
							if(!empty($filter)) {
								if($last + mb_strlen($array[$last]['link']) < $filter[0]['min']) {
									$array[$last + mb_strlen($array[$last]['link'])] = array('type' => 'text', 'text' => mb_substr($string, $last + mb_strlen($array[$last]['link']), $filter[0]['min'] - $last - mb_strlen($array[$last]['link'])));
								}
								
								for($i = 0; $i < count($filter); $i++) {
									if($filter[$i]['max'] > mb_stripos($string, $this->registry->request->get['q'], $start)) {
										$array[$filter[$i]['min']] = array(
											'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
											'string' => array(
												array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['min'], mb_stripos($string, $this->registry->request->get['q'], $start) - $filter[$i]['min']))
											)
										);
										$last = $filter[$i]['min'];
									} else {
										$array[$filter[$i]['min']] = array(
											'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
											'string' => array(
												array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']))
											)
										);
										
										if($i + 1 < count($filter)) {
											$array[$filter[$i]['max']] = array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['max'], $filter[$i + 1]['min'] - $filter[$i]['max']));
										} elseif(mb_stripos($string, $this->registry->request->get['q'], $start) > $filter[$i]['max']) {
											$array[$filter[$i]['max']] = array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['max'], mb_stripos($string, $this->registry->request->get['q'], $start) - $filter[$i]['max']));
										}
									}
								}
							} else {
								$array[$last + mb_strlen($array[$last]['link'])] = array('type' => 'text', 'text' => mb_substr($string, $last + mb_strlen($array[$last]['link']), mb_stripos($string, $this->registry->request->get['q'], $start) - $last - mb_strlen($array[$last]['link'])));
							}
						} else {
							$array[$last]['string'][] = array('type' => 'text', 'text' => mb_substr($string, $start, mb_stripos($string, $this->registry->request->get['q'], $start) - $start));
						}
					} else {
						$sort = array('min' => $start, 'max' => mb_stripos($string, $this->registry->request->get['q'], $start));
						
						$filter = array_values(array_filter($links, function($link) use($sort) {
							return $link['min'] >= $sort['min'] && $link['min'] < $sort['max'];
						}));
						
						if(!empty($filter)) {
							if($start < $filter[0]['min']) {
								$array[$start] = array('type' => 'text', 'text' => mb_substr($string, $start, $filter[0]['min'] - $start));
							}
							
							for($i = 0; $i < count($filter); $i++) {
								if($filter[$i]['max'] > mb_stripos($string, $this->registry->request->get['q'], $start)) {
									$array[$filter[$i]['min']] = array(
										'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
										'string' => array(
											array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['min'], mb_stripos($string, $this->registry->request->get['q'], $start) - $filter[$i]['min']))
										)
									);
									$last = $filter[$i]['min'];
								} else {
									$array[$filter[$i]['min']] = array(
										'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
										'string' => array(
											array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']))
										)
									);
									
									if($i + 1 < count($filter)) {
										$array[$filter[$i]['max']] = array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['max'], $filter[$i + 1]['min'] - $filter[$i]['max']));
									} elseif(mb_stripos($string, $this->registry->request->get['q'], $start) > $filter[$i]['max']) {
										$array[$filter[$i]['max']] = array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['max'], mb_stripos($string, $this->registry->request->get['q'], $start) - $filter[$i]['max']));
									}
								}
							}
						} else {
							$array[$start] = array('type' => 'text', 'text' => mb_substr($string, $start, mb_stripos($string, $this->registry->request->get['q'], $start) - $start));
						}
					}
				} elseif(mb_stripos($string, $this->registry->request->get['q'], $start) === false && mb_strlen($string) > $start) {
					if(isset($array[$last]) && $start > $last && $start < $last + mb_strlen($array[$last]['link'])) {
						if(mb_strlen($string) > $last + mb_strlen($array[$last]['link'])) {
							$array[$last]['string'][] = array('type' => 'text', 'text' => mb_substr($string, $start, $last + mb_strlen($array[$last]['link']) - $start));
							
							$sort = array('min' => $last + mb_strlen($array[$last]['link']), 'max' => mb_strlen($string));
							
							$filter = array_values(array_filter($links, function($link) use($sort) {
								return $link['min'] >= $sort['min'] && $link['min'] < $sort['max'];
							}));
							
							if(!empty($filter)) {
								if($last + mb_strlen($array[$last]['link']) < $filter[0]['min']) {
									$array[$last + mb_strlen($array[$last]['link'])] = array('type' => 'text', 'text' => mb_substr($string, $last + mb_strlen($array[$last]['link']), $filter[0]['min'] - $last - mb_strlen($array[$last]['link'])));
								}
								
								for($i = 0; $i < count($filter); $i++) {
									if($filter[$i]['max'] > mb_strlen($string)) {
										$array[$filter[$i]['min']] = array(
											'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
											'string' => array(
												array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['min'], mb_strlen($string) - $filter[$i]['min']))
											)
										);
										$last = $filter[$i]['min'];
									} else {
										$array[$filter[$i]['min']] = array(
											'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
											'string' => array(
												array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']))
											)
										);
										
										if($i + 1 < count($filter)) {
											$array[$filter[$i]['max']] = array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['max'], $filter[$i + 1]['min'] - $filter[$i]['max']));
										} elseif(mb_strlen($string) > $filter[$i]['max']) {
											$array[$filter[$i]['max']] = array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['max'], mb_strlen($string) - $filter[$i]['max']));
										}
									}
								}
							} else {
								$array[$last + mb_strlen($array[$last]['link'])] = array('type' => 'text', 'text' => mb_substr($string, $last + mb_strlen($array[$last]['link']), mb_strlen($string) - $last - mb_strlen($array[$last]['link'])));
							}
						} else {
							$array[$last]['string'][] = array('type' => 'text', 'text' => mb_substr($string, $start, mb_strlen($string) - $start));
						}
					} else {
						$sort = array('min' => $start, 'max' => mb_strlen($string));
						
						$filter = array_values(array_filter($links, function($link) use($sort) {
							return $link['min'] >= $sort['min'] && $link['min'] < $sort['max'];
						}));
						
						if(!empty($filter)) {
							if($start < $filter[0]['min']) {
								$array[$start] = array('type' => 'text', 'text' => mb_substr($string, $start, $filter[0]['min'] - $start));
							}
							
							for($i = 0; $i < count($filter); $i++) {
								$array[$filter[$i]['min']] = array(
									'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
									'string' => array(
										array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']))
									)
								);
								
								if($i + 1 < count($filter)) {
									$array[$filter[$i]['max']] = array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['max'], $filter[$i + 1]['min'] - $filter[$i]['max']));
								} elseif(mb_strlen($string) > $filter[$i]['max']) {
									$array[$filter[$i]['max']] = array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['max'], mb_strlen($string) - $filter[$i]['max']));
								}
							}
						} else {
							$array[$start] = array('type' => 'text', 'text' => mb_substr($string, $start));
						}
					}
				}
			}
		} else {
			$sort = array('min' => $start, 'max' => mb_strlen($string));
			
			$filter = array_values(array_filter($links, function($link) use($sort) {
				return $link['min'] >= $sort['min'] && $link['min'] < $sort['max'];
			}));
			
			if(!empty($filter)) {
				if($start < $filter[0]['min']) {
					$array[$start] = array('type' => 'text', 'text' => mb_substr($string, $start, $filter[0]['min'] - $start));
				}
				
				for($i = 0; $i < count($filter); $i++) {
					$array[$filter[$i]['min']] = array(
						'link' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']),
						'string' => array(
							array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['min'], $filter[$i]['max'] - $filter[$i]['min']))
						)
					);
					
					if($i + 1 < count($filter)) {
						$array[$filter[$i]['max']] = array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['max'], $filter[$i + 1]['min'] - $filter[$i]['max']));
					} elseif(mb_strlen($string) > $filter[$i]['max']) {
						$array[$filter[$i]['max']] = array('type' => 'text', 'text' => mb_substr($string, $filter[$i]['max'], mb_strlen($string) - $filter[$i]['max']));
					}
				}
			} else {
				$array[$start] = array('type' => 'text', 'text' => mb_substr($string, $start));
			}
		}
		
		return $array;
	}
	
	public function cart($type = 0, $coupon = null) {
		$cookie = array();
		$carts = array();
		$total = array();
		$oldtotal = array();
		
		if(isset($this->registry->request->cookie['cart']) && !is_array($this->registry->request->cookie['cart']) && is_array(json_decode(htmlspecialchars_decode($this->registry->request->cookie['cart']), true))) {
			foreach(json_decode(htmlspecialchars_decode($this->registry->request->cookie['cart']), true) as $item) {
				if(is_array($item) && isset($item['id']) && !is_array($item['id']) && isset($item['count']) && !is_array($item['count']) && $this->getTotal('shop_products', array('product_id' => $item['id'])) && intval($item['count']) >= 1) {
					$product = $this->getBy('shop_products', array('product_id' => $item['id']));
					
					if(!empty(json_decode($product['product_content'], true))) {
						if(isset($item['content']) && !is_array($item['content']) && array_key_exists($item['content'], json_decode($product['product_content'], true))) {
							$c[$product['product_id'] . '+' . $item['content']] += intval($item['count']);
							
							if($product['product_discount']) {
								$price = $this->convert($product['product_price'] * (100 - $product['product_discount']) / 100 * $c[$product['product_id'] . '+' . $item['content']], $this->currencie());
							} else {
								$price = $this->convert($product['product_price'] * $c[$product['product_id'] . '+' . $item['content']], $this->currencie());
							}
							
							if($this->getTotal('shop_coupons', array('coupon_key' => $coupon))) {
								$coupon = $this->getBy('shop_coupons', array('coupon_key' => $coupon));
								
								$amount = $price * (100 - $coupon['coupon_value']) / 100;
							} else {
								$amount = $price;
							}
							
							$carts[$product['product_id'] . '+' . $item['content']] = array(
								'id' => $product['product_id'],
								'title' => isset(json_decode($product['product_title'], true)[$this->language()]) ? json_decode($product['product_title'], true)[$this->language()] : json_decode($product['product_title'], true)[$this->config('language')],
								'description' => nl2br(isset(json_decode($product['product_description'], true)[$this->language()]) ? json_decode($product['product_description'], true)[$this->language()] : json_decode($product['product_description'], true)[$this->config('language')]),
								'description_string' => $this->string(nl2br(isset(json_decode($product['product_description'], true)[$this->language()]) ? json_decode($product['product_description'], true)[$this->language()] : json_decode($product['product_description'], true)[$this->config('language')]), true),
								'text' => htmlspecialchars_decode(isset(json_decode($product['product_text'], true)[$this->language()]) ? json_decode($product['product_text'], true)[$this->language()] : json_decode($product['product_text'], true)[$this->config('language')]),
								'image' => $this->image(json_decode($product['product_image'], true)[0]),
								'content' => json_decode($product['product_content'], true)[$item['content']],
								'price' => $this->currencieInit($this->currencie(), $amount),
								'count' => $c[$product['product_id'] . '+' . $item['content']]
							);
							$cookie[$product['product_id'] . '+' . $item['content']] = array(
								'id' => $product['product_id'],
								'content' => $item['content'],
								'count' => $c[$product['product_id'] . '+' . $item['content']]
							);
							$total[$product['product_id'] . '+' . $item['content']] = $amount;
							
							if($this->getTotal('shop_coupons', array('coupon_key' => $coupon))) {
								$carts[$product['product_id'] . '+' . $item['content']]['oldprice'] = $this->currencieInit($this->currencie(), $price);
								$oldtotal[$product['product_id'] . '+' . $item['content']] = $price;
							}
						}
					} else {
						$c[$product['product_id']] += intval($item['count']);
						
						if($product['product_discount']) {
							$price = $this->convert($product['product_price'] * (100 - $product['product_discount']) / 100 * $c[$product['product_id']], $this->currencie());
						} else {
							$price = $this->convert($product['product_price'] * $c[$product['product_id']], $this->currencie());
						}
						
						if($this->getTotal('shop_coupons', array('coupon_key' => $coupon))) {
							$coupon = $this->getBy('shop_coupons', array('coupon_key' => $coupon));
							
							$amount = $price * (100 - $coupon['coupon_value']) / 100;
						} else {
							$amount = $price;
						}
						
						$carts[$product['product_id']] = array(
							'id' => $product['product_id'],
							'title' => isset(json_decode($product['product_title'], true)[$this->language()]) ? json_decode($product['product_title'], true)[$this->language()] : json_decode($product['product_title'], true)[$this->config('language')],
							'description' => nl2br(isset(json_decode($product['product_description'], true)[$this->language()]) ? json_decode($product['product_description'], true)[$this->language()] : json_decode($product['product_description'], true)[$this->config('language')]),
							'description_string' => $this->string(nl2br(isset(json_decode($product['product_description'], true)[$this->language()]) ? json_decode($product['product_description'], true)[$this->language()] : json_decode($product['product_description'], true)[$this->config('language')]), true),
							'text' => htmlspecialchars_decode(isset(json_decode($product['product_text'], true)[$this->language()]) ? json_decode($product['product_text'], true)[$this->language()] : json_decode($product['product_text'], true)[$this->config('language')]),
							'image' => $this->image(json_decode($product['product_image'], true)[0]),
							'price' => $this->currencieInit($this->currencie(), $amount),
							'count' => $c[$product['product_id']]
						);
						$cookie[$product['product_id']] = array(
							'id' => $product['product_id'],
							'count' => $c[$product['product_id']]
						);
						$total[$product['product_id']] = $amount;
						
						if($this->getTotal('shop_coupons', array('coupon_key' => $coupon))) {
							$carts[$product['product_id']]['oldprice'] = $this->currencieInit($this->currencie(), $price);
							$oldtotal[$product['product_id']] = $price;
						}
					}
				}
			}
		}
		
		if($type == 3) {
			return $oldtotal;
		} elseif($type == 2) {
			return $total;
		} elseif($type == 1) {
			return $carts;
		} else {
			return $cookie;
		}
	}
}
?>
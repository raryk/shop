<?php
class Account {
	private $registry;
	
	private $id;
	private $nickname;
	private $email;
	private $ip;
	private $deliverie;
	private $language;
	private $timezone;
	private $currencie;
	private $twa;
	private $admin;
	private $level;
	private $date_reg;
	
	public function __construct($registry) {
		$this->registry = $registry;
		
		if(isset($this->registry->request->cookie['token']) && !is_array($this->registry->request->cookie['token'])) {
			$query = $this->registry->db->query("SELECT * FROM users_tokens WHERE token_code = '" . $this->registry->db->escape($this->registry->request->cookie['token']) . "' ORDER BY token_id ASC");
			if($query->num_rows) {
				if($query->rows[$this->registry->request->cookie['account']]) {
					$query = $this->registry->db->query("SELECT * FROM users WHERE user_id = '" . $this->registry->db->escape($query->rows[$this->registry->request->cookie['account']]['user_id']) . "'");
				} else {
					$query = $this->registry->db->query("SELECT * FROM users WHERE user_id = '" . $this->registry->db->escape($query->rows[0]['user_id']) . "'");
					
					setcookie('token', $this->registry->request->cookie['token'], time() + $this->registry->functions->config('cookie'), '/', '.' . $this->registry->functions->config('domain'), ($this->registry->functions->config('connect') == 'https://' ? true : false), true);
					setcookie('account', 0, time() + $this->registry->functions->config('cookie'), '/', '.' . $this->registry->functions->config('domain'), ($this->registry->functions->config('connect') == 'https://' ? true : false), true);
				}
				
				if($query->num_rows) {
					$this->id = $query->row['user_id'];
					$this->nickname = $query->row['user_nickname'];
					$this->email = $query->row['user_email'];
					$this->ip = $query->row['user_ip'];
					$this->deliverie = $query->row['user_deliverie'];
					$this->language = $query->row['user_language'];
					$this->timezone = $query->row['user_timezone'];
					$this->currencie = $query->row['user_currencie'];
					$this->twa = $query->row['user_twa'];
					$this->admin = $query->row['user_admin'];
					$this->level = $query->row['user_level'];
					$this->date_reg = $query->row['user_date_reg'];
				} else {
					$this->logout();
				}
			} else {
				$this->logout();
			}
		}
	}
	
	public function login($userid) {
		$query = $this->registry->db->query("SELECT * FROM users WHERE user_id = '" . $this->registry->db->escape($userid) . "'");
		if($query->num_rows) {
			$ip = $this->registry->request->server['REMOTE_ADDR'];
			$agent = $this->registry->request->server['HTTP_USER_AGENT'];
			$token = mb_strtolower(mb_substr(md5(time() . mb_substr(str_shuffle('abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 16)), rand(1, 8), 16));
			
			$this->id = $query->row['user_id'];
			$this->nickname = $query->row['user_nickname'];
			$this->email = $query->row['user_email'];
			$this->ip = $query->row['user_ip'];
			$this->deliverie = $query->row['user_deliverie'];
			$this->language = $query->row['user_language'];
			$this->timezone = $query->row['user_timezone'];
			$this->currencie = $query->row['user_currencie'];
			$this->twa = $query->row['user_twa'];
			$this->admin = $query->row['user_admin'];
			$this->level = $query->row['user_level'];
			$this->date_reg = $query->row['user_date_reg'];
			
			if(isset($this->registry->request->cookie['token']) && !is_array($this->registry->request->cookie['token'])) {
				$query = $this->registry->db->query("SELECT * FROM users_tokens WHERE token_code = '" . $this->registry->db->escape($this->registry->request->cookie['token']) . "'");
				if($query->num_rows) {
					$query = $this->registry->db->query("SELECT * FROM users_tokens WHERE user_id = '" . $this->registry->db->escape($this->id) . "' AND token_code = '" . $this->registry->db->escape($query->row['token_code']) . "' ORDER BY token_id ASC");
					if($query->num_rows) {
						$query = $this->registry->db->query("SELECT * FROM users_tokens WHERE token_code = '" . $this->registry->db->escape($this->registry->request->cookie['token']) . "' ORDER BY token_id ASC");
						setcookie('account', array_search($this->id, array_column($query->rows, 'user_id')), time() + $this->registry->functions->config('cookie'), '/', '.' . $this->registry->functions->config('domain'), ($this->registry->functions->config('connect') == 'https://' ? true : false), true);
					} else {
						$this->registry->db->query("INSERT INTO users_tokens SET user_id = '" . $this->registry->db->escape($this->id) . "', token_code = '" . $this->registry->db->escape($this->registry->request->cookie['token']) . "', token_ip = '" . $this->registry->db->escape($ip) . "', token_agent = '" . $this->registry->db->escape($agent) . "', token_date_add = NOW()");
						
						$query = $this->registry->db->query("SELECT * FROM users_tokens WHERE user_id != '" . $this->registry->db->escape($this->id) . "' AND token_code = '" . $this->registry->db->escape($this->registry->request->cookie['token']) . "' ORDER BY token_id ASC");
						setcookie('account', $query->num_rows, time() + $this->registry->functions->config('cookie'), '/', '.' . $this->registry->functions->config('domain'), ($this->registry->functions->config('connect') == 'https://' ? true : false), true);
					}
				} else {
					$this->registry->db->query("INSERT INTO users_tokens SET user_id = '" . $this->registry->db->escape($this->id) . "', token_code = '" . $this->registry->db->escape($token) . "', token_ip = '" . $this->registry->db->escape($ip) . "', token_agent = '" . $this->registry->db->escape($agent) . "', token_date_add = NOW()");
					
					setcookie('token', $token, time() + $this->registry->functions->config('cookie'), '/', '.' . $this->registry->functions->config('domain'), ($this->registry->functions->config('connect') == 'https://' ? true : false), true);
					setcookie('account', 0, time() + $this->registry->functions->config('cookie'), '/', '.' . $this->registry->functions->config('domain'), ($this->registry->functions->config('connect') == 'https://' ? true : false), true);
				}
			} else {
				$this->registry->db->query("INSERT INTO users_tokens SET user_id = '" . $this->registry->db->escape($this->id) . "', token_code = '" . $this->registry->db->escape($token) . "', token_ip = '" . $this->registry->db->escape($ip) . "', token_agent = '" . $this->registry->db->escape($agent) . "', token_date_add = NOW()");
				
				setcookie('token', $token, time() + $this->registry->functions->config('cookie'), '/', '.' . $this->registry->functions->config('domain'), ($this->registry->functions->config('connect') == 'https://' ? true : false), true);
				setcookie('account', 0, time() + $this->registry->functions->config('cookie'), '/', '.' . $this->registry->functions->config('domain'), ($this->registry->functions->config('connect') == 'https://' ? true : false), true);
			}
		}
	}
	
	public function logout() {
		if(isset($this->registry->request->cookie['token']) && !is_array($this->registry->request->cookie['token'])) {
			$query = $this->registry->db->query("SELECT * FROM users_tokens WHERE user_id = '" . $this->registry->db->escape($this->id) . "' AND token_code = '" . $this->registry->db->escape($this->registry->request->cookie['token']) . "'");
			if($query->num_rows) {
				$this->registry->db->query("DELETE FROM users_tokens WHERE user_id = '" . $this->registry->db->escape($this->id) . "' AND token_code = '" . $this->registry->db->escape($this->registry->request->cookie['token']) . "'");
			} else {
				setcookie('token', null, time() - $this->registry->functions->config('cookie'), '/', '.' . $this->registry->functions->config('domain'), ($this->registry->functions->config('connect') == 'https://' ? true : false), true);
				setcookie('account', null, time() - $this->registry->functions->config('cookie'), '/', '.' . $this->registry->functions->config('domain'), ($this->registry->functions->config('connect') == 'https://' ? true : false), true);
			}
		} else {
			setcookie('token', null, time() - $this->registry->functions->config('cookie'), '/', '.' . $this->registry->functions->config('domain'), ($this->registry->functions->config('connect') == 'https://' ? true : false), true);
			setcookie('account', null, time() - $this->registry->functions->config('cookie'), '/', '.' . $this->registry->functions->config('domain'), ($this->registry->functions->config('connect') == 'https://' ? true : false), true);
		}
		
		$this->id = null;
		$this->nickname = null;
		$this->email = null;
		$this->ip = null;
		$this->deliverie = null;
		$this->language = null;
		$this->timezone = null;
		$this->currencie = null;
		$this->twa = null;
		$this->admin = null;
		$this->level = null;
		$this->date_reg = null;
	}
	
	public function accounts() {
		if(isset($this->registry->request->cookie['token']) && !is_array($this->registry->request->cookie['token'])) {
			$query = $this->registry->db->query("SELECT * FROM users_tokens LEFT JOIN users ON users_tokens.user_id=users.user_id WHERE users_tokens.user_id != '" . $this->registry->db->escape($this->id) . "' AND token_code = '" . $this->registry->db->escape($this->registry->request->cookie['token']) . "' ORDER BY token_id ASC");
			return $query->rows;
		} else {
			return array();
		}
	}
	
	public function deleteTokens() {
		if(isset($this->registry->request->cookie['token']) && !is_array($this->registry->request->cookie['token'])) {
			$query = $this->registry->db->query("SELECT * FROM users_tokens WHERE user_id = '" . $this->registry->db->escape($this->id) . "' AND token_code = '" . $this->registry->db->escape($this->registry->request->cookie['token']) . "'");
			if($query->num_rows) {
				$this->registry->db->query("DELETE FROM users_tokens WHERE user_id = '" . $this->registry->db->escape($this->id) . "' AND token_code != '" . $this->registry->db->escape($this->registry->request->cookie['token']) . "'");
			}
		}
	}
	
	public function isLogged($userid = null) {
		if(isset($this->registry->request->cookie['token']) && !is_array($this->registry->request->cookie['token'])) {
			if(is_null($userid)) {
				$query = $this->registry->db->query("SELECT * FROM users_tokens WHERE user_id = '" . $this->registry->db->escape($this->id) . "' AND token_code = '" . $this->registry->db->escape($this->registry->request->cookie['token']) . "'");
				if($query->num_rows) {
					return true;
				} else {
					return false;
				}
			} else {
				$query = $this->registry->db->query("SELECT * FROM users_tokens WHERE user_id = '" . $this->registry->db->escape($userid) . "' AND token_code = '" . $this->registry->db->escape($this->registry->request->cookie['token']) . "'");
				if($query->num_rows) {
					return true;
				} else {
					return false;
				}
			}
		} else {
			return false;
		}
	}
	
	public function isBanned($userid = null) {
		if(is_null($userid)) {
			$query = $this->registry->db->query("SELECT * FROM users_bans WHERE user_id = '" . $this->registry->db->escape($this->id) . "' AND ban_status = '1' AND (ban_time = '0' OR ban_time > '" . $this->registry->db->escape($this->registry->functions->time()) . "')");
			if($query->num_rows) {
				return true;
			} else {
				return false;
			}
		} else {
			$query = $this->registry->db->query("SELECT * FROM users_bans WHERE user_id = '" . $this->registry->db->escape($userid) . "' AND ban_status = '1' AND (ban_time = '0' OR ban_time > '" . $this->registry->db->escape($this->registry->functions->time()) . "')");
			if($query->num_rows) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public function isLevel($content = null) {
		$query = $this->registry->db->query("SELECT * FROM system_levels WHERE level_id = '" . $this->registry->db->escape($this->level) . "'");
		if($query->num_rows) {
			if(empty(json_decode($query->row['level_content'], true)) || in_array($content, json_decode($query->row['level_content'], true))) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getNickname() {
		return $this->nickname;
	}
	
	public function getEmail() {
		return $this->email;
	}
	
	public function getIp() {
		return $this->ip;
	}
	
	public function getDeliverie() {
		return $this->deliverie;
	}
	
	public function getLanguage() {
		return $this->language;
	}
	
	public function getTimezone() {
		return $this->timezone;
	}
	
	public function getCurrencie() {
		return $this->currencie;
	}
	
	public function getTwa() {
		return $this->twa;
	}
	
	public function getAdmin() {
		return $this->admin;
	}
	
	public function getLevel() {
		return $this->level;
	}
	
	public function getDateReg() {
		return $this->date_reg;
	}
}
?>
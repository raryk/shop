<?php
class Action {
	private $registry;
	private $folder;
	private $controller;
	private $method;
	private $args;
	
	public function __construct($registry) {
		$this->registry = $registry;
	}
	
	public function make($action) {
		$this->folder = null;
		$this->controller = null;
		$this->method = null;
		$this->args = null;
		
		$address = $this->removeDot($action);
		$parts = array_filter(explode('/', $address));
		
		foreach($parts as $item) {
			$fullpath = APPLICATION_DIR . 'controllers' . $this->folder . '/' . $item;
			if(is_dir($fullpath)) {
				$this->folder .= '/' . $item;
				array_shift($parts);
				continue;
			} elseif(is_file($fullpath . '.php')) {
				$this->controller = $item;
				array_shift($parts);
				break;
			} else {
				break;
			}
		}
		
		if(empty($this->folder)) {
			$this->folder = null;
		}
		
		if(empty($this->controller)) {
			$query = $this->registry->db->query("SELECT * FROM system_pages WHERE page_address = '" . $this->registry->db->escape($address) . "'");
			if($query->num_rows) {
				$this->controller = $query->row['page_address'];
			} else {
				$this->controller = 'index';
			}
		}
		
		$method = array_shift($parts);
		if($method) {
			$this->method = $method;
		} else {
			$this->method = 'index';
		}
		
		if(isset($parts[0])) {
			$this->args = $parts;
		}
	}
	
	public function go($commonEnable = false) {
		$controllerFile = APPLICATION_DIR . 'controllers' . $this->folder . '/' . $this->controller . '.php';
		$controllerClass = $this->controller . 'Controller';
		
		if((!preg_match('/common/i', $controllerFile) || $commonEnable) && is_readable($controllerFile)) {
			require_once($controllerFile);
			
			$controller = new $controllerClass($this->registry);
			
			if(!is_callable(array($controller, $this->method))) {
				$this->method = 'index';
			}
			
			if(empty($this->args)) {
				return call_user_func(array($controller, $this->method));
			} else {
				return call_user_func_array(array($controller, $this->method), $this->args);
			}
		} else {
			$query = $this->registry->db->query("SELECT * FROM system_pages WHERE page_address = '" . $this->registry->db->escape($this->controller) . "'");
			if($query->num_rows) {
				$section = array();
				foreach(json_decode($query->row['page_section'], true) as $item) {
					$section[$item['key']] = $item['value'];
				}
				
				$alternate = array();
				foreach($this->registry->functions->get('system_languages') as $item) {
					$alternate[$item['language_key']] = $this->registry->functions->config('connect') . $this->registry->functions->config('domain') . '/' . $query->row['page_address'] . '?language=' . $item['language_key'];
				}
				
				$this->registry->document->setTitle(isset(json_decode($query->row['page_title'], true)[$this->registry->functions->language()]) ? json_decode($query->row['page_title'], true)[$this->registry->functions->language()] : json_decode($query->row['page_title'], true)[$this->registry->functions->config('language')]);
				$this->registry->document->setDescription(isset(json_decode($query->row['page_description'], true)[$this->registry->functions->language()]) ? json_decode($query->row['page_description'], true)[$this->registry->functions->language()] : json_decode($query->row['page_description'], true)[$this->registry->functions->config('language')]);
				$this->registry->document->setSection($section);
				$this->registry->document->setUrl($this->registry->functions->config('connect') . $this->registry->functions->config('domain') . '/' . $query->row['page_address']);
				$this->registry->document->setCanonical($this->registry->functions->config('connect') . $this->registry->functions->config('domain') . '/' . $query->row['page_address'] . '?language=' . $this->registry->functions->languageBy($this->registry->functions->language()));
				$this->registry->document->setAlternate($alternate);
				
				$common = array();
				foreach(json_decode($query->row['page_common'], true) as $item) {
					$common['{' . $item['key'] . '}'] = $this->child($item['value']);
				}
				
				return str_replace(array_keys($common), array_values($common), htmlspecialchars_decode(isset(json_decode($query->row['page_code'], true)[$this->registry->functions->language()]) ? json_decode($query->row['page_code'], true)[$this->registry->functions->language()] : json_decode($query->row['page_code'], true)[$this->registry->functions->config('language')]));
			} else {
				return $this->registry->functions->config('404');
			}
		}
	}
	
	public function child($child) {
		$this->make($child);
		return $this->go(true);
	}
	
	private function removeDot($path) {
		if(mb_strpos($path, '.') === false) {
			return $path;
		}
		
		$inputBuffer = $path;
		$outputStack = [];
		
		while($inputBuffer != '') {
			if(mb_strpos($inputBuffer, './') === 0) {
				$inputBuffer = mb_substr($inputBuffer, 2);
				continue;
			}
			
			if(mb_strpos($inputBuffer, '../') === 0) {
				$inputBuffer = mb_substr($inputBuffer, 3);
				continue;
			}
			
			if($inputBuffer === '/.') {
				$outputStack[] = '/';
				break;
			}
			
			if(mb_substr($inputBuffer, 0, 3) === '/./') {
				$inputBuffer = mb_substr($inputBuffer, 2);
				continue;
			}
			
			if($inputBuffer === '/..') {
				array_pop($outputStack);
				$outputStack[] = '/';
				break;
			}
			
			if(mb_substr($inputBuffer, 0, 4) === '/../') {
				array_pop($outputStack);
				$inputBuffer = mb_substr($inputBuffer, 3);
				continue;
			}
			
			if($inputBuffer === '.' || $inputBuffer === '..') {
				break;
			}
			
			if(($slashPos = mb_stripos($inputBuffer, '/', 1)) === false) {
				$outputStack[] = $inputBuffer;
				break;
			} else {
				$outputStack[] = mb_substr($inputBuffer, 0, $slashPos);
				$inputBuffer = mb_substr($inputBuffer, $slashPos);
			}
		}
		
		return implode($outputStack);
	}
}
?>
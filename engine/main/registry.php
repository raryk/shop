<?php
class Registry {
	private $data = array();
	
	public function __set($key, $value) {
		$this->data[$key] = $value;
	}
	
	public function __get($key) {
		if(isset($this->data[$key])) {
			return $this->data[$key];
		}
		return false;
	}
}
?>